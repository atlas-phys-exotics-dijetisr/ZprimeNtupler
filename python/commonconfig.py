import ROOT
from ZprimeNtupler import Config

import argparse
import shlex
import fnmatch

import itertools

#
# List of GRLs
GRL={}
GRL['IGNORETOROID'] = ["GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
                       "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml",
                       "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
                       "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"]

GRL['HLT'] = ["GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
              "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS.xml",
              "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_JetHLT_Normal2017.xml",
              "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"]

GRL['ALLGOOD'] = ["GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
                  "GoodRunsLists/data16_13TeV/20170605/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
                  "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
                  "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"]

LUMICALC={}
LUMICALC['IGNORETOROID'] = ["GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                            "GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS_297730-311481_OflLumi-13TeV-008.root",
                            "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root",
                            "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]

LUMICALC['HLT'] = ["GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                   "GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_ignore_TOROID_STATUS_297730-311481_OflLumi-13TeV-008.root",
                   "GoodRunsLists/data17_13TeV/20180619/physics_25ns_JetHLT_Normal2017.lumicalc.OflLumi-13TeV-010.root",
                   "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]

LUMICALC['ALLGOOD'] = ["GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                       "GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-008.root",
                       "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root",
                       "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]

prwActualMu2017={}
prwActualMu2017['IGNORETOROID'] = 'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root'
prwActualMu2017['HLT'         ] = 'GoodRunsLists/data17_13TeV/20180619/physics_25ns_JetHLT_Normal2017.actualMu.OflLumi-13TeV-010.root'
prwActualMu2017['ALLGOOD'     ] = 'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root'

#
# b-tagging configurations
btagmodes=['FixedCutBEff']
btagWPs=[60,70,77,85]
btaggers=['DL1','DL1r']

def generate_btag_detailstr(btaggers=btaggers,btagmodes=btagmodes,btagWPs=btagWPs):
    detailStr=''
    for btagger,btagmode,btagWP in itertools.product(btaggers,btagmodes,btagWPs):
        detailStr+=' jetBTag_{btagger}_{btagmode}_{btagWP}'.format(btagger=btagger,btagWP=btagWP, btagmode=btagmode)
    return detailStr

def apply_common_config(c,isMC=False,isAFII=False,mcCampaign=None,
                        triggerSelection='',
                        triggerList='',
                        doSyst=None,
                        doJets=True, doFatJets=False, doTrackJets=False, doPhotons=True, doMuons=False, doElectrons=False, doTruthFatJets = False, doHLTObjects=False,
                        jetBtaggers   =[],jetBtagmodes   =[],jetBtagWPs   =[],
                        trkJetBtaggers=[],trkJetBtagmodes=[],trkJetBtagWPs=[],trkJetBTS='201903',
                        fatjetMuonInJet=False,
                        singleMuTriggers='',
                        msgLevel=ROOT.MSG.INFO,GRLset='IGNORETOROID',
                        args=''):

    # Extra arguments
    
    parser = argparse.ArgumentParser(prog='apply_common_config')
    parser.add_argument("--doSyst"     , action='store_true', help='Enable systematics')
    parser.add_argument("--mcCampaign" , dest='mcCampaign'  , help='Campaign to use for PRW')
    parser.add_argument("--doLooseNTUP", action='store_true', help='Set loose photon requirements, allowing purity studies')
    conargs=parser.parse_args(args=shlex.split(args))

    # Default is None for campaign and false for systs.
    
    doSyst = False
    if conargs.doSyst :
       print "Picking up from args:", conargs.doSyst
       doSyst = True
    else :
       print "Using default value for doSyst: false"
    if conargs.mcCampaign :
      mcCampaign=conargs.mcCampaign
    doLooseNTUP = conargs.doLooseNTUP

    #
    # Add sample metadata
    
    # SimulationFlavour
    # See the following for default tags
    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC16a
    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC16d
    #c.sample('*_a875_*',SimulationFlavour='AFII')
    #c.sample('*_s3126_*',SimulationFlavour='FS')

    #
    # Apply specific settings
    if not isMC: # Data Config
        jet_calibSeq   = 'JetArea_Residual_EtaJES_GSC_Insitu'
        fatjet_calibSeq= 'EtaJES_JMS_Insitu_InsituCombinedMass'
        systName       = ''
        systVal        = 0
        jetSystVector  = ''
    else: # MC Config
        jet_calibSeq   = 'JetArea_Residual_EtaJES_GSC_Smear'
        fatjet_calibSeq= 'EtaJES_JMS'
        systName       = "All" if doSyst else ''
        systVal        = 1 if doSyst else 0
        jetSystVector  = "1.0" if doSyst else ''

    # AFII
    #
    if isAFII:
        JESUncertMCType    = "AFII"
        fatJESUncertMCType = "AFII"
    else:
        JESUncertMCType    = "MC16"
        fatJESUncertMCType = "MC16"


    
    GRLlist           = GRL            .get(GRLset, GRL            ['ALLGOOD'])
    LUMICALClist      = LUMICALC       .get(GRLset, LUMICALC       ['ALLGOOD'])
    prwActualMu2017str= prwActualMu2017.get(GRLset, prwActualMu2017['ALLGOOD'])

    #
    # Public Tools

    c.tool("TrigConf::xAODConfigTool", {
        "Name": "xAODConfigTool"
    })

    c.tool("Trig::TrigDecisionTool"  , {
        "Name": "TrigDecisionTool",
        "ConfigTool" : "TrigConf::xAODConfigTool/xAODConfigTool"
    })

    #
    # Basic Event Selection
    BasicEventSelection = { "Name"                      : "BasicEventSelection",
                            "UseMetaData"               : True,
                            "ApplyGRLCut"               : True,
                            "GRLTool"                   : {
                                "Tool"            : "GoodRunsListSelectionTool",
                                "GoodRunsListVec" : GRLlist
                            },
                            "ApplyTriggerCut"           : not isMC,
                            "TriggerSelection"          : triggerSelection,
                            "StoreTrigDecisions"        : True,
                            "TrigDecTool"               : "Trig::TrigDecisionTool/TrigDecisionTool",
                            "TrigConfTool"              : "TrigConf::xAODConfigTool/xAODConfigTool",
                            "ApplyPrimaryVertexCut"     : True,
                            "ApplyJetCleaningEventFlag" : True,
                            "ApplyEventCleaningCut"     : True,
                            "ApplyCoreFlagsCut"         : True,
                            "DoPRW"                     : isMC,
                            "AutoConfigPRW"             : True,
                            "PRWSearchPaths"            : ["/dev/PileupReweighting/share","HbbISR/prw"],
                            "LumiCalcFiles"             : LUMICALClist,
                            "DataScaleFactor"           : 1/1.03,
                            "PRWActualMu2017File"       : prwActualMu2017str
    }
    if mcCampaign!=None:
      BasicEventSelection["MCCampaign"] = mcCampaign.split(',')

    c.anaalgorithm("Zprime::BasicEventSelection", BasicEventSelection )



    # Setup intermediate names, if needed
    doOR=doJets and doPhotons
    if doOR:
        nameJetContainer     ='Jets_Select'
        namePhotonContainer  ='Photons_Select'
        nameMuonContainer    ='Muons_Select'
        nameElectronContainer='Electrons_Select'
    else:
        nameJetContainer     ='SignalJets'
        namePhotonContainer  ='SignalPhotons'
        nameMuonContainer    ='SignalMuons'
        nameElectronContainer='SignalElectrons'

    #
    # Photons
    if doPhotons:
        c.algorithm("PhotonCalibrator",   { "m_name"                    : "CalibratePhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons",
                                            "m_outContainerName"        : "Photons_Calib",
                                            "m_outputAlgoSystNames"     : "Photons_Calib_Algo",
                                            "m_photonCalibMap"          : "PhotonEfficiencyCorrection/2015_2017/rel21.2/Winter2018_Prerec_v1/map0.txt", # Updated March 18 2018
                                            "m_tightIDConfigPath"       : "ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf", # Updated March 18 2018
#                                            "m_mediumIDConfigPath"      : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMMediumSelectorCutDefs.conf", # Removed March 18 2018
                                            "m_looseIDConfigPath"       : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf", # Checked March 15 2018
                                            "m_esModel"                 : "es2017_R21_v0", # Updated March 15 2018
                                            "m_decorrelationModel"      : "1NP_v1",        # Checked March 15 2018
                                            "m_useAFII"                 : isAFII,
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_sort"                    : True,
                                            "m_isMC"                    : isMC,
                                            "m_readIDFlagsFromDerivation": True, #added by EEC 050718 so that new AODs with reduced egamma vars can be read
                                            } )

        isoVar = "m_MinIsoWPCut" if not doLooseNTUP else "m_IsoWPList"
        isoVal = "FixedCutTightCaloOnly" if not doLooseNTUP else "FixedCutTightCaloOnly,FixedCutTight,FixedCutLoose"

        c.algorithm("PhotonSelector",     { "m_name"                    : "SelectPhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons_Calib",
                                            "m_inputAlgoSystNames"      : "Photons_Calib_Algo",
                                            "m_outContainerName"        : namePhotonContainer,
                                            "m_outputAlgoSystNames"     : namePhotonContainer+"_Algo",
                                            "m_createSelectedContainer" : True,
                                            "m_pT_min"                  : 10e3,
                                            "m_eta_max"                 : 2.37,
                                            "m_vetoCrack"               : True,
                                            "m_doAuthorCut"             : True,
                                            "m_doOQCut"                 : True,
                                            "m_systName"                : systName,
                                            "m_photonIdCut"             : "Tight" if not doLooseNTUP else "Loose",
                                            isoVar                      : isoVal,
                                            "m_isMC"                    : isMC,
                                            "m_readOQFromDerivation"    : True,  #added by EEC 050718 so that new AODs with reduced egamma vars can be read
                                            } )

    #
    # Muons
    if doMuons:
        c.algorithm("Zprime::MuonCalibrator", {
            "m_name" : "MuonCalibrator",
            "MuonCalibrationAndSmearingTool" : {
                "Tool" : "CP::MuonCalibrationPeriodTool"
            },
            "InContainerName"   : "Muons",
            "OutContainerName"  : "Muons_Calib",
            "OutputAlgo"        : "Muons_Calib_Algo"
            } )

        c.algorithm("MuonSelector", { "m_name"                    : "SelectMuons", 
                                      "m_msgLevel"                : msgLevel,
                                      "m_inContainerName"         : "Muons_Calib", 
                                      "m_outContainerName"        : nameMuonContainer,
                                      "m_createSelectedContainer" : True,
                                      "m_useCutFlow"              : False,
                                      "m_pT_min"                  : 10e3,
                                      "m_eta_max"                 : 2.5,
                                      "m_muonQualityStr"          : "Medium",
                                      "m_d0sig_max"               : 3,
                                      "m_z0sintheta_max"          : 1.0,
                                      "m_IsoWPList"               : "FCLoose",
                                      "m_MinIsoWPCut"             : "FCLoose",
                                      "m_singleMuTrigChains"      : singleMuTriggers
                                      } )
                                                 

    #
    # Electrons
    if doElectrons:
        c.algorithm("ElectronCalibrator", { "m_name"                : "CalibrateElectrons",
                                            "m_msgLevel"            : msgLevel,
                                            "m_inContainerName"     : "Electrons",
                                            "m_outContainerName"    : "Electrons_Calib",
                                            "m_outputAlgoSystNames" : "Electrons_Calib_Algo",
                                            "m_esModel"             : "es2017_R21_PRE",
                                            "m_decorrelationModel"  : "FULL_v1"
                                            })

        c.algorithm("ElectronSelector", { "m_name"                    : "SelectElectrons",
                                          "m_inContainerName"         : "Electrons_Calib",
                                          "m_inputAlgoSystNames"      : "Electrons_Calib_Algo",
                                          "m_outContainerName"        : nameElectronContainer,
                                          "m_outputAlgoSystNames"     : nameElectronContainer+"_Algo",
                                          "m_createSelectedContainer" : True,
                                          "m_doLHPID"                 : True,
                                          "m_doLHPIDcut"              : True,
                                          "m_LHOperatingPoint"        : "Loose",
                                          "m_MinIsoWPCut"             : "LooseTrackOnly",
                                          "m_d0sig_max"               : 5.,
                                          "m_z0sintheta_max"          : 0.5,
                                          "m_pT_min"                  : 7*1000.,
                                          "m_eta_max"                 : 2.47
                                          })

    #
    # Jets
    if doJets:
        c.algorithm("JetCalibrator",      { "m_name"                    : "CalibrateJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt4EMTopoJets",
                                            "m_outContainerName"        : "Jets_Calib",
                                            "m_outputAlgo"              : "Jets_Calib_Algo",
                                            "m_jetAlgo"                 : "AntiKt4EMTopo",
                                            "m_sort"                    : True,
                                            "m_saveAllCleanDecisions"   : True,
                                            "m_calibConfigAFII"         : "JES_MC16Recommendation_AFII_EMTopo_Apr2019_Rel21.config",
                                            "m_calibConfigFullSim"      : "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config",
                                            "m_calibConfigData"         : "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config",
                                            "m_calibSequence"           : jet_calibSeq,
                                            "m_setAFII"                 : isAFII,
                                            "m_uncertConfig"            : "rel21/Fall2018/R4_SR_Scenario1_SimpleJER.config",
                                            "m_uncertMCType"            : JESUncertMCType,
                                            "m_doCleaning"              : False,
                                            "m_redoJVT"                 : True,
                                            "m_systName"                : systName,
                                            "m_systValVectorString"     : jetSystVector,
                                            "m_systVal"                 : systVal
        } )

        c.algorithm("JetSelector",        { "m_name"                    : "SelectJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Jets_Calib",
                                            "m_inputAlgo"               : "Jets_Calib_Algo",
                                            "m_outContainerName"        : nameJetContainer,
                                            "m_outputAlgo"              : nameJetContainer+"_Algo",
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 25e3,
                                            "m_eta_max"                 : 2.8,
                                            "m_useCutFlow"              : True,
                                            "m_doBTagCut"               : False,
                                            "m_doJVF"                   : False,
                                            "m_doJVT"                   : True,
                                            "m_WorkingPointJVT"         : "Medium",
                                            "m_SFFileJVT"               : "JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root",
                                            "m_isMC"                    : isMC
                                            } )

        for btagger,btagmode,btagWP in itertools.product(jetBtaggers,jetBtagmodes,jetBtagWPs):
            btagWPstr="%s_%d"%(btagmode,btagWP)
            c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_"+nameJetContainer+"_"+btagger+"_"+btagWPstr,
                                                     "m_msgLevel"                : msgLevel,
                                                     "m_inContainerName"         : nameJetContainer,
                                                     "m_inputAlgo"               : nameJetContainer+"_Algo",
                                                     "m_systName"                : systName,
                                                     "m_systVal"                 : systVal,
                                                     "m_operatingPt"             : btagWPstr,
                                                     "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2021-04-16_v1.root",
                                                     "m_jetAuthor"               : "AntiKt4EMTopoJets_BTagging{}".format(trkJetBTS),
                                                     "m_minPt"                   : 20000,
                                                     "m_taggerName"              : btagger,
                                                     "m_EfficiencyCalibration"   : "auto",
                                                     "m_decor"                   : "BTag",
                                                     "m_isMC"                    : isMC
                                                     } )


    #
    # Fat jets
    if doFatJets:
        if not isMC or not isAFII : # Run calibration for full-sim only
            c.algorithm("JetCalibrator",      { "m_name"                    : "CalibrateFatJets",
                                                "m_msgLevel"                : msgLevel,
                                                "m_inContainerName"         : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                                                "m_outContainerName"        : "FatJets_Calib",
                                                "m_outputAlgo"              : "FatJets_Calib_Algo",
                                                "m_jetAlgo"                 : "AntiKt10LCTopoTrimmedPtFrac5SmallR20",
                                                "m_sort"                    : True,
                                                "m_saveAllCleanDecisions"   : True,
                                                "m_calibConfigFullSim"      : "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config",
                                                "m_calibConfigData"         : "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config",
                                                "m_calibSequence"           : fatjet_calibSeq,
                                                "m_forceInsitu"             : False,
                                                "m_setAFII"                 : isAFII,
                                                "m_truthBosonContainerName" : "TruthBoson",
                                                "m_truthTopContainerName"   : "TruthTop",
                                                "m_uncertConfig"            : "rel21/Summer2019/R10_CategoryReduction.config",
                                                "m_uncertMCType"            : fatJESUncertMCType,
                                                "m_doCleaning"              : False,
                                                "m_applyFatJetPreSel"       : True,    # make sure fat-jet uncertainty is applied only in valid region
                                                "m_systName"                : systName,
                                                "m_systVal"                 : systVal,
                                                "m_systValVectorString"     : jetSystVector,
            } )

            c.algorithm("Zprime::FFJetSmearingAlg", {
                "FFJetSmearingTool" : {
                    "Tool"       : "CP::FFJetSmearingTool",
                    "MassDef"    : "Comb",
                    "ConfigFile" : "rel21/Winter2021/R10_FullJMR_COMB.config"
                },
                "ContainerName"   : "FatJets_Calib",
                "InputAlgo"       : "FatJets_Calib_Algo",
                "OutputAlgo"      : "FatJets_Smeared_Algo",
                "SystName"        : "Variations"
            } )

        else:
            c.algorithm("Zprime::SortAlgo", {
                "InContainerName"  : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                "OutContainerName" : "FatJets_Calib",
                "OutputAlgo"       : "FatJets_Calib_Algo"
            } )

        c.algorithm("JetSelector",        { "m_name"                    : "SelectFatJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "FatJets_Calib",
                                            "m_inputAlgo"               : "FatJets_Smeared_Algo",
                                            "m_outContainerName"        : "SignalFatJets",
                                            "m_outputAlgo"              : "SignalFatJets_Algo",
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 200e3,
                                            "m_eta_max"                 : 2.0,
                                            "m_useCutFlow"              : False
                                            } )

        if fatjetMuonInJet:
            c.algorithm("MuonInFatJetCorrector",{ "m_name"                 : "MuonCorrectFatJets",
                                                  "m_msgLevel"             : msgLevel,
                                                  "m_fatJetContainerName"  : "SignalFatJets",
                                                  "m_trackJetContainerName": "AntiKtVR30Rmax4Rmin02TrackJets_BTagging{}".format(trkJetBTS),
                                                  "m_trackJetLinkName"     : "GhostVR30Rmax4Rmin02TrackJet_BTagging{}".format(trkJetBTS),
                                                  "m_muonContainerName"    : "Muons_Calib",
                                                  "m_inputAlgo"            : "SignalFatJets_Algo"
            } )

    #
    # Track Jets
    if doTrackJets:
        c.algorithm("JetSelector", { "m_name"                    : "SelectVRTrackJets",
                                     "m_inContainerName"         : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging{}".format(trkJetBTS),
                                     "m_outContainerName"        : "SignalVRTrackJets",
                                     "m_cleanJets"               : False,
                                     "m_pT_min"                  : 5e3,
                                     "m_eta_max"                 : 2.5,
                                     "m_useCutFlow"              : False,
                                     "m_doJVF"                   : False
                                     } )

        for btagger,btagmode,btagWP in itertools.product(trkJetBtaggers,trkJetBtagmodes,trkJetBtagWPs):
            btagWPstr="%s_%d"%(btagmode,btagWP)

            c.algorithm("BJetEfficiencyCorrector", { "m_name"                    : "BJetEffCor_SignalVRTrackJets_"+btagger+"_"+btagWPstr,
                                                     "m_msgLevel"                : msgLevel,
                                                     "m_inContainerName"         : "SignalVRTrackJets",
                                                     "m_systName"                : systName,
                                                     "m_systVal"                 : systVal,
                                                     "m_operatingPt"             : btagWPstr,
                                                     "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2021-04-16_v1.root",
                                                     "m_jetAuthor"               : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging{}".format(trkJetBTS),
                                                     "m_minPt"                   : 10000,
                                                     "m_maxEta"                  : 2.5,
                                                     "m_taggerName"              : btagger,
                                                     "m_EfficiencyCalibration"   : "auto",
                                                     "m_decor"                   : "BTag",
                                                 } )


    #
    # Photons
    if doPhotons:
        c.algorithm("PhotonCalibrator",   { "m_name"                    : "CalibratePhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons",
                                            "m_outContainerName"        : "Photons_Calib",
                                            "m_outputAlgoSystNames"     : "Photons_Calib_Algo",
                                            #"m_photonCalibMap"          : "PhotonEfficiencyCorrection/2015_2017/rel21.2/Winter2018_Prerec_v1/map0.txt", # Updated March 18 2018
                                            "m_tightIDConfigPath"       : "ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf", # Updated March 18 2018
#                                            "m_mediumIDConfigPath"      : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMMediumSelectorCutDefs.conf", # Removed March 18 2018
                                            "m_looseIDConfigPath"       : "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf", # Checked March 15 2018
                                            "m_esModel"                 : "es2018_R21_v0", # Updated September 30 2019
                                            "m_decorrelationModel"      : "1NP_v1",        # Checked March 15 2018
                                            "m_useAFII"                 : isAFII,
                                            "m_systName"                : systName,
                                            "m_systVal"                 : systVal,
                                            "m_sort"                    : True,
                                            "m_isMC"                    : isMC,
                                            "m_readIDFlagsFromDerivation": True, #added by EEC 050718 so that new AODs with reduced egamma vars can be read
                                            } )

        isoVar = "m_MinIsoWPCut" if not doLooseNTUP else "m_IsoWPList"
        isoVal = "FixedCutTightCaloOnly" if not doLooseNTUP else "FixedCutTightCaloOnly,FixedCutTight,FixedCutLoose"

        c.algorithm("PhotonSelector",     { "m_name"                    : "SelectPhotons",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "Photons_Calib",
                                            "m_inputAlgoSystNames"      : "Photons_Calib_Algo",
                                            "m_outContainerName"        : namePhotonContainer,
                                            "m_outputAlgoSystNames"     : namePhotonContainer+"_Algo",
                                            "m_createSelectedContainer" : True,
                                            "m_pT_min"                  : 10e3,
                                            "m_eta_max"                 : 2.37,
                                            "m_vetoCrack"               : True,
                                            "m_doAuthorCut"             : True,
                                            "m_doOQCut"                 : True,
                                            "m_systName"                : systName,
                                            "m_photonIdCut"             : "Tight" if not doLooseNTUP else "Loose",
                                            isoVar                      : isoVal,
                                            "m_isMC"                    : isMC,
                                            "m_readOQFromDerivation"    : True,  #added by EEC 050718 so that new AODs with reduced egamma vars can be read
                                            } )

    if doOR:
        OverlapRemover={ "m_name"                    : "RemoveOverlaps",
                         "m_outputAlgoSystNames"     : "ORAlgo_Syst",
                         "m_createSelectedContainers": True}
        if doJets:
            OverlapRemover["m_inContainerName_Jets"]      =nameJetContainer
            OverlapRemover["m_inputAlgoJets"]             =nameJetContainer+"_Algo"
            OverlapRemover["m_outContainerName_Jets"]     ="SignalJets"
        if doPhotons:
            OverlapRemover["m_inContainerName_Photons"]   =namePhotonContainer
            OverlapRemover["m_inputAlgoPhotons"]          =namePhotonContainer+"_Algo"
            OverlapRemover["m_outContainerName_Photons"]  ="SignalPhotons"
        if doMuons:
            OverlapRemover["m_inContainerName_Muons"]     =nameMuonContainer
            OverlapRemover["m_inputAlgoMuons"]            =nameMuonContainer+"_Algo"
            OverlapRemover["m_outContainerName_Muons"]    ="SignalMuons"
        if doElectrons:
            OverlapRemover["m_inContainerName_Electrons"] =nameElectronContainer
            OverlapRemover["m_inputAlgoElectrons"]        =nameElectronContainer+"_Algo"
            OverlapRemover["m_outContainerName_Electrons"]="SignalElectrons"

        c.algorithm("OverlapRemover", OverlapRemover)

    if doHLTObjects:
        c.algorithm("JetSelector",        { "m_name"                    : "SignalHLTJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "HLT_xAOD__JetContainer_a4tcemsubjesISFS",
                                            "m_outContainerName"        : "SignalHLTJets",
                                            "m_outputAlgo"              : "SignalHLTJets_Algo",
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 20e3,
                                            "m_eta_max"                 : 3.0,
                                            "m_useCutFlow"              : False,
                                            "m_doBTagCut"               : False,
                                            "m_doJVF"                   : False,
                                            "m_doJVT"                   : False,
                                            "m_isMC"                    : isMC
                                            } )


    if doTruthFatJets:
        c.algorithm("JetSelector",        { "m_name"                    : "SelectTruthFatJets",
                                            "m_msgLevel"                : msgLevel,
                                            "m_inContainerName"         : "AntiKt10TruthTrimmedPtFrac5SmallR20Jets",
                                            "m_outContainerName"        : "SignalTruthFatJets",
                                            "m_outputAlgo"              : "SignalTruthFatJets_Algo",
                                            "m_cleanJets"               : False,
                                            "m_pT_min"                  : 200e3,
                                            "m_eta_max"                 : 2.0,
                                            "m_useCutFlow"              : False,
                                            "m_doBTagCut"               : False,
                                            "m_doJVF"                   : False,
                                            "m_doJVT"                   : False,
                                            "m_isMC"                    : isMC
                                            } )

def findAlgo(c,name):
    for algo in c._algorithms:
        if algo.name()==name: return algo
    return None

def findAlgos(c,namematch):
    return filter(lambda algo: fnmatch.fnmatch(algo.name(), namematch), c._algorithms)
