#!/usr/bin/env python
# -*- coding: utf-8 -*-,
from __future__ import absolute_import
from __future__ import print_function
import logging
logger = logging.getLogger("xAH.config")

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)

import inspect
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.DualUseConfig import addPrivateTool
from AnaAlgorithm.DualUseConfig import createPublicTool

from .utils import NameGenerator

class Config(object):
  def __init__(self):
    self._algorithms = []
    self._tools      = []    
    self._samples    = {}
    self._outputs    = set([])
    self._log        = []

    # Add tools to the job
    from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
    self.seq = AnaAlgSequence( "ZprimeAnalysisSequence" )

  def algorithm(self, className, options):
    # check first argument
    if isinstance(className, unicode): className = className.encode('utf-8')
    if not isinstance(className, str):
      raise TypeError("className must be a string")

    if not isinstance(options, dict):
      raise TypeError("Must pass in a dictionary of options")

    # if m_name not set, randomly generate it
    algName = options.get("m_name", None)
    if algName is None:
      algName = str(NameGenerator())
      logger.warning("Setting missing m_name={0:s} for instance of {1:s}".format(algName, className))
      options['m_name'] = algName
    if not isinstance(algName, str) and not isinstance(algName, unicode):
      raise TypeError("'m_name' must be a string for instance of {0:s}".format(className))

    # handle msgLevels, can be string or integer
    msgLevel = options.get("m_msgLevel", "info")
    if isinstance(msgLevel, unicode): msgLevel = msgLevel.encode('utf-8')
    if not isinstance(msgLevel, str) and not isinstance(msgLevel, int) and not isinstance(msgLevel, long):
      raise TypeError("m_msgLevel must be a string or integer for instance of {0:s}".format(className))
    if isinstance(msgLevel, str):
      if not hasattr(ROOT.MSG, msgLevel.upper()):
        raise ValueError("m_msgLevel must be a valid MSG::level: {0:s}".format(msgLevel))
      msgLevel = getattr(ROOT.MSG, msgLevel.upper())
    options['m_msgLevel'] = msgLevel

    # Construct the given constructor
    #    (complicated b/c we have to deal nesting of namespaces)
    alg = reduce(lambda x,y: getattr(x, y, None), className.split('::'), ROOT)
    if alg is None:
      raise AttributeError(className)

    # get a list of parent classes
    parents = inspect.getmro(alg)
    if ROOT.EL.Algorithm in parents:
      # Construct an instance of the alg and set its attributes
      alg_obj = alg()
      alg_obj.SetName(algName)
      self._log.append((className,algName))
      alg_obj.setMsgLevel(msgLevel)
      for k,v in options.items():
        # only crash on algorithm configurations that aren't m_msgLevel and m_name (xAH specific)
        if not hasattr(alg_obj, k) and k not in ['m_msgLevel', 'm_name']:
          raise AttributeError(k)
        elif hasattr(alg_obj, k):
          #handle unicode from json
          if isinstance(v, unicode): v = v.encode('utf-8')
          self._log.append((algName, k, v))
          try:
            setattr(alg_obj, k, v)
          except:
            logger.error("There was a problem setting {0:s} to {1} for {2:s}::{3:s}".format(k, v, className, algName))
            raise
    elif ROOT.EL.AnaAlgorithm in parents:
      alg_obj = AnaAlgorithmConfig(className)
      alg_obj.setName(algName)
      self._log.append((className, algName))
      for k,v in options.items():
        if k in ['m_name', 'm_msgLevel']: continue
        # Create and configure private tool, if necessary
        if type(v)==dict and 'Tool' in v:
          addPrivateTool( alg_obj, k, v.pop('Tool') )
          tool = getattr(alg_obj, k)
          for tk,tv in v.items():
            if isinstance(tv, unicode): tv = tv.encode('utf-8')
            try:
              setattr(tool, tk, tv)
            except:
              logger.error("There was a problem setting {0:s} to {1} for {2:s}::{3:s}".format(tk, tv, className, algName, v['Tool']))
              raise
        else:
          # Set the property
          try:
            setattr(alg_obj, k, v)
          except:
            logger.error("There was a problem setting {0:s} to {1} for {2:s}::{3:s}".format(k, v, className, algName))
            raise
        
          if isinstance(v, unicode): v = v.encode('utf-8')
          self._log.append((algName, k, v))
          try:
            setattr(alg_obj, k, v)
          except:
            logger.error("There was a problem setting {0:s} to {1} for {2:s}::{3:s}".format(k, v, className, algName))
            raise
    else:
      raise TypeError("Algorithm {0:s} is not an EL::Algorithm or EL::AnaAlgorithm. I do not know how to configure it. {1}".format(className, parents))

    # Add the constructed algo to the list of algorithms to run
    self._algorithms.append(alg_obj)

  def anaalgorithm(self, className, options):
    # check first argument
    if isinstance(className, unicode): className = className.encode('utf-8')
    if not isinstance(className, str):
      raise TypeError("className must be a string")

    if not isinstance(options, dict):
      raise TypeError("Must pass in a dictionary of options")

    # if Name not set, randomly generate it
    algName = options.pop("Name", None)
    if algName is None:
      algName = str(NameGenerator())
      logger.warning("Setting missing Name={0:s} for instance of {1:s}".format(algName, className))

    if not isinstance(algName, str) and not isinstance(algName, unicode):
      raise TypeError("'Name' must be a string for instance of {0:s}".format(className))

    # Construct the given algorithms
    alg_obj = createAlgorithm( className, algName )
    if alg_obj is None:
      raise AttributeError(className)

    # Configure the algorithm properties
    for k,v in options.items():
      if isinstance(v, unicode): v = v.encode('utf-8')
      self._log.append((algName, k, v))

      # Create and configure private tool, if necessary
      if type(v)==dict and 'Tool' in v:
        addPrivateTool( alg_obj, k, v['Tool'] )
        tool = getattr(alg_obj, k)
        for tk,tv in v.items():
          if tk in ['Tool']: continue # ignore 
          if isinstance(tv, unicode): tv = tv.encode('utf-8')
          try:
            setattr(tool, tk, tv)
          except:
            logger.error("There was a problem setting {0:s} to {1} for {2:s}::{3:s}".format(tk, tv, className, algName, v['Tool']))
            raise
      else:
        # Set the property
        try:
          setattr(alg_obj, k, v)
        except:
          logger.error("There was a problem setting {0:s} to {1} for {2:s}::{3:s}".format(k, v, className, algName))
          raise

    # Add the constructed algo to the list of algorithms to run
    self.seq += alg_obj

  def tool(self, className, options):
    # check first argument
    if isinstance(className, unicode): className = className.encode('utf-8')
    if not isinstance(className, str):
      raise TypeError("className must be a string")

    if not isinstance(options, dict):
      raise TypeError("Must pass in a dictionary of options")

    # if Name not set, randomly generate it
    toolName = options.pop("Name", None)
    if toolName is None:
      toolName = str(NameGenerator())
      logger.warning("Setting missing Name={0:s} for instance of {1:s}".format(toolName, className))

    if not isinstance(toolName, str) and not isinstance(toolName, unicode):
      raise TypeError("'Name' must be a string for instance of {0:s}".format(className))

    # Construct the given tool
    tool = createPublicTool( className, toolName )
    if tool is None:
      raise AttributeError(className)

    # Configure the algorithm properties
    for k,v in options.items():
      if isinstance(v, unicode): v = v.encode('utf-8')
      self._log.append((toolName, k, v))

      # Set the property
      try:
        setattr(tool, k, v)
      except:
        logger.error("There was a problem setting {0:s} to {1} for {2:s}::{3:s}".format(k, v, className, toolName))
        raise

    # Add the constructed algo to the list of algorithms to run
    self._tools.append(tool)
    self.seq.addPublicTool( tool )

  # set based on patterns
  def sample(self, pattern, **kwargs):
    pattern = str(pattern)
    try:
      self._samples[pattern].update(kwargs)
    except KeyError:
      self._samples[pattern] = kwargs

  def output(self, name):
    self._outputs.add(str(name))
