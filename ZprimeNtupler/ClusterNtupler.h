#ifndef ZprimeNtupler_ClusterNtupler_H
#define ZprimeNtupler_ClusterNtupler_H

#include <TTree.h>

#include <vector>
#include <string>

#include <xAODCaloEvent/CaloClusterContainer.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/ParticleNtupler.h"

namespace Zprime
{
  class ClusterNtupler : public ParticleNtupler<HelperClasses::ClusterInfoSwitch>
  {
  public:
    ClusterNtupler(const std::string& name = "clus", const std::string& detailStr="", float units = 1e3, bool mc = false);
    virtual ~ClusterNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillCluster( const xAOD::CaloCluster* cluster );
    virtual StatusCode FillCluster( const xAOD::IParticle* particle );

  };
}
#endif // ZprimeNtupler_ClusterNtupler_H
