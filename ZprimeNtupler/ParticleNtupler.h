#ifndef ZprimeNtupler_ParticleNtupler_H
#define ZprimeNtupler_ParticleNtupler_H

#include <TTree.h>
#include <TLorentzVector.h>

#include <vector>
#include <string>

#include <xAODBase/IParticle.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/HelperFunctions.h"


namespace Zprime
{

  template <class T_INFOSWITCH>
  class ParticleNtupler
  {
  public:
    ParticleNtupler(const std::string& name,
		    const std::string& detailStr="",
		    float units = 1e3,
		    bool mc = false,
		    bool useMass=false,
		    bool storeSystSFs = true)
      : m_name(name),
	m_infoSwitch(detailStr),
	m_mc(mc),
	m_units(units),
	m_storeSystSFs(storeSystSFs),
	m_useMass(useMass)
    {
      m_n = 0;

      // kinematic
      m_pt  =std::make_shared<std::vector<float>>();
      m_eta =std::make_shared<std::vector<float>>();
      m_phi =std::make_shared<std::vector<float>>();
      m_E   =std::make_shared<std::vector<float>>();
      m_M   =std::make_shared<std::vector<float>>();
    }

    virtual ~ParticleNtupler() =default;

    virtual StatusCode createBranches(TTree *tree)
    {
     const std::string counterName = "n"+m_name;
     tree->Branch(counterName.c_str(), &m_n, (counterName+"/I").c_str());

     if(m_infoSwitch.m_kinematic)
       {
	 if(m_useMass)  createBranch<float>(tree,"m", m_M  );
	 else           createBranch<float>(tree,"E", m_E  );
	 createBranch<float>(tree,"pt" , m_pt );
	 createBranch<float>(tree,"phi", m_phi);
	 createBranch<float>(tree,"eta", m_eta);
       }

     return StatusCode::SUCCESS;
    }

    virtual StatusCode clear()
    {
      m_n = 0;

      if(m_infoSwitch.m_kinematic)
	{
	  if(m_useMass)  m_M->clear();
	  else           m_E->clear();
	  m_pt  ->clear();
	  m_phi ->clear();
	  m_eta ->clear();
	}

      return StatusCode::SUCCESS;      
    }

    virtual StatusCode FillParticle(const xAOD::IParticle* particle)
    {
      m_n++;

      if( m_infoSwitch.m_kinematic )
	{
	  m_pt  -> push_back( particle->pt() / m_units );
	  m_eta -> push_back( particle->eta() );
	  m_phi -> push_back( particle->phi() );
	  if(m_useMass) m_M->push_back  ( particle->m() / m_units );
	  else          m_E->push_back  ( particle->e() / m_units );
	}

      return StatusCode::SUCCESS;      
    }

  protected:
    std::string branchName(const std::string& varName)
    {
      std::string name = m_name + "_" + varName;
      return name;
    }

    template<typename T> void createBranch(TTree* tree, const std::string& varName, std::shared_ptr<std::vector<T>> localVectorPtr)
    {
      std::string name = branchName(varName);
      tree->Branch(name.c_str(), localVectorPtr.get());
    }

    template<typename T, typename U, typename V> void safeFill(const V* xAODObj, SG::AuxElement::ConstAccessor<T>& accessor, std::shared_ptr<std::vector<U>> destination, U defaultValue, int units = 1)
    {
      if ( accessor.isAvailable( *xAODObj ) )
	destination->push_back( accessor( *xAODObj ) / units );
      else
	destination->push_back( defaultValue );
    }

    template<typename T, typename U, typename V> void safeVecFill(const V* xAODObj, SG::AuxElement::ConstAccessor<std::vector<T> >& accessor, std::shared_ptr<std::vector<std::vector<U>>> destination, int units = 1)
    {
      destination->push_back( std::vector<U>() );

      if ( accessor.isAvailable( *xAODObj ) )
	{
	  for(U itemInVec : accessor(*xAODObj)) destination->back().push_back(itemInVec / units);
	}
    }

    template<typename T, typename V> void safeSFVecFill(const V* xAODObj, SG::AuxElement::ConstAccessor<std::vector<T> >& accessor, std::shared_ptr<std::vector< std::vector<T> >> destination, const std::vector<T> &defaultValue)
    {
      if ( accessor.isAvailable( *xAODObj ) )
	{
	  if ( m_storeSystSFs )
	    {
	      destination->push_back( accessor(*xAODObj) );
	    }
	  else
	    {
	      destination->push_back( std::vector< float > ({accessor(*xAODObj)[0]}) );
	    }
        }
      else
	{
          destination->push_back( defaultValue );
        }
    }

  protected:
    std::string m_name;    
    T_INFOSWITCH m_infoSwitch;
    bool  m_mc;
    float m_units;
    bool  m_storeSystSFs;
    bool  m_useMass;    

  private:
    int m_n;

    //
    // Vector branches

    // kinematic
    std::shared_ptr<std::vector<float>> m_pt;
    std::shared_ptr<std::vector<float>> m_eta;
    std::shared_ptr<std::vector<float>> m_phi;
    std::shared_ptr<std::vector<float>> m_E;
    std::shared_ptr<std::vector<float>> m_M;
  };

} //Zprime
#endif // ZprimeNtupler_ParticleNtupler_H
