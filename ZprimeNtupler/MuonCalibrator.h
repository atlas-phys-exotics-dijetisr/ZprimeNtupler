#ifndef ZprimeNtupler_MuonCalibrator_H
#define ZprimeNtupler_MuonCalibrator_H

// algorithm wrapper
#include "ZprimeNtupler/AnaAlgorithm.h"

// external tools include(s):
#include <AsgTools/ToolHandle.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>

namespace Zprime
{
  //! Muon calibration algorithm
  class MuonCalibrator : public AnaAlgorithm
  {
  private:
    //! InContainerName: Name of input container with uncalibrated muons
    std::string m_inContainerName = "";
    //! OutContainerName: Name of output container for calibrated muons
    std::string m_outContainerName = "";

    //! Sort: Sort the output container by pT
    bool m_sort = true;

    //! InputAlgo: Name of vectors with systematic variations of the input container
    std::string m_inputAlgo = "";
    //! OutputAlgo: Name of for vectors with systematic variations of this algorithm
    std::string m_outputAlgo = "MuonCalibrator_Syst";

    //! WriteSystToMetadata: Write systematics names to metadata
    bool        m_writeSystToMetadata = false;

    //! SystVal: How many sigma variations to apply
    float       m_systVal = 0.0;

    //! SystName: Name of systematics set to use (blank for none, all for all)
    std::string m_systName = "";

    //! ForceDataCalib: For applying calibration on data
    bool    m_forceDataCalib = false;

  private:
    std::vector<CP::SystematicSet> m_systList; //!

    // tools
    ToolHandle<CP::IMuonCalibrationAndSmearingTool> m_MuonCalibrationAndSmearingTool{"CP::MuonCalibrationPeriodTool", this};

  public:
    //! Standard constructor
    MuonCalibrator (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize ();
    virtual StatusCode execute ();
  };
}
#endif
