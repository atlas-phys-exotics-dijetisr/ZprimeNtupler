#ifndef ZprimeNtupler_ZprimeNtupler_H
#define ZprimeNtupler_ZprimeNtupler_H

#include <EventLoop/StatusCode.h>

//algorithm wrapper
#include "ZprimeNtupler/AnaAlgorithm.h"

// tools
#include <AsgTools/ToolHandle.h>
#include <PMGAnalysisInterfaces/IPMGCrossSectionTool.h>
#include <JetAnalysisInterfaces/IJetSelectorTool.h>

#include "ZprimeNtupler/NtuplerContainerDef.h"
#include "ZprimeNtupler/ZprimeMiniTree.h"

namespace Zprime
{
  class Ntupler : public AnaAlgorithm
  {
   
  private:
    //! EventDetailStr: event info add to tree    
    std::string m_eventDetailStr = "";

    //! TrigDetailStr: trigger info add to tree    
    std::string m_trigDetailStr  = "";

    //! Containers: container "TYPE(InputContainer->OutputPrefix);detailStr;detailStrSyst"
    std::vector<std::string> m_containersStr;

    //! TruthLevelOnly: Running over truth objects only
    bool m_truthLevelOnly        = false;

    //! InputAlgo: input algo for when running systs
    std::string m_inputAlgo      = "";

    //! SaveZprimeDecay: store the Z' particle and decay products    
    bool m_saveZprimeDecay       = false;

    //! CalcExtraVariables: running mass-decorrelated tagger (ANN) using BJT     
    bool m_calcExtraVariables    = false;

    //! DijetFatjetSlicing: Filter dijet samples such that they can be spliced with dedicated fat jet filtered dijet samples
    bool m_dijetFatjetSlicing    = false;

  private:
    std::vector<NtuplerContainerDef> m_containers;

  private:
    //! Cross-section for the sample currently being processed
    float m_weight_xs;

    //configuration variables
    std::string m_treeStream ="tree";

    ZprimeMiniTree* m_tree=nullptr; //!

    // cutflow
    int m_cf_cleanPileup        =-1;
    int m_cf_dijetFatJetSlicing =-1;

    ToolHandle<PMGTools::IPMGCrossSectionTool> m_PMGCrossSectionTool{"PMGTools::PMGCrossSectionTool", this};
    ToolHandle<IJetSelectorTool> m_JetTagger{"ANN_tagger"};

  public:
    //! Standard constructor
    Ntupler (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize();
    virtual StatusCode executeFirstEvent();
    virtual StatusCode execute();
    virtual StatusCode finalize();

    bool executeTruthAnalysis( const xAOD::EventInfo* eventInfo, 
			       const std::string& systName = "");

  private:
    /** \name Tree Management
     * @{ */

    //! \brief Create a TTree with necessary branches
    /**
     * Create a TTree with necessary branches for all requested
     * containers using ZprimeMiniTree.
     *
     * \param systNames List of systematic names to loop over
     *
     * \return status code
     */
    StatusCode createTree( const std::vector<std::string>& systNames={""} );

    StatusCode fillTree( const xAOD::EventInfo* eventInfo, 
			 const xAOD::VertexContainer* vertices,	
			 const std::vector<std::string>& systNames={""});

    /** @} */

    void calculateExtraVariables(const xAOD::JetContainer* fatjets);
  };
}

#endif
