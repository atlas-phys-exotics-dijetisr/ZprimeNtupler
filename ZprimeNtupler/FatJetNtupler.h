#ifndef ZprimeNtupler_FatJetNtupler_H
#define ZprimeNtupler_FatJetNtupler_H

#include <TTree.h>

#include <vector>
#include <string>

#include <xAODJet/JetContainer.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/HelperFunctions.h"
#include "ZprimeNtupler/ParticleNtupler.h"

namespace Zprime
{

  class FatJetNtupler : public ParticleNtupler<HelperClasses::JetInfoSwitch>
  {
  public:
    FatJetNtupler(const std::string& name = "fatjet", const std::string& detailStr="", float units = 1e3, bool mc = false);
    virtual ~FatJetNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillFatJet( const xAOD::Jet* jet           , int pvLocation );
    virtual StatusCode FillFatJet( const xAOD::IParticle* particle, int pvLocation );

    float       m_trackJetPtCut  =10e3; // slimming pT cut on associated track jets
    float       m_trackJetEtaCut =2.5;  // slimmint eta cut on associated track jets

  private:
    bool SelectTrackJet(const xAOD::Jet* TrackJet);
    float GetEMFrac(const xAOD::Jet& jet);

  private:

    //
    // Vector branches
    //

    // scales
    std::shared_ptr<std::vector<float>> m_JetConstitScaleMomentum_eta;
    std::shared_ptr<std::vector<float>> m_JetConstitScaleMomentum_phi;
    std::shared_ptr<std::vector<float>> m_JetConstitScaleMomentum_m;
    std::shared_ptr<std::vector<float>> m_JetConstitScaleMomentum_pt;

    std::shared_ptr<std::vector<float>> m_JetEMScaleMomentum_eta;
    std::shared_ptr<std::vector<float>> m_JetEMScaleMomentum_phi;
    std::shared_ptr<std::vector<float>> m_JetEMScaleMomentum_m;
    std::shared_ptr<std::vector<float>> m_JetEMScaleMomentum_pt;

    // area
    std::shared_ptr<std::vector<float>> m_GhostArea;
    std::shared_ptr<std::vector<float>> m_ActiveArea;
    std::shared_ptr<std::vector<float>> m_VoronoiArea;

    std::shared_ptr<std::vector<float>> m_ActiveArea4vec_pt;
    std::shared_ptr<std::vector<float>> m_ActiveArea4vec_eta;
    std::shared_ptr<std::vector<float>> m_ActiveArea4vec_phi;
    std::shared_ptr<std::vector<float>> m_ActiveArea4vec_m;

    // substructure
    std::shared_ptr<std::vector<float>> m_Split12;
    std::shared_ptr<std::vector<float>> m_Split23;
    std::shared_ptr<std::vector<float>> m_Split34;
    std::shared_ptr<std::vector<float>> m_tau1_wta;
    std::shared_ptr<std::vector<float>> m_tau2_wta;
    std::shared_ptr<std::vector<float>> m_tau3_wta;
    std::shared_ptr<std::vector<float>> m_tau21_wta;
    std::shared_ptr<std::vector<float>> m_tau32_wta;
    std::shared_ptr<std::vector<float>> m_ECF1;
    std::shared_ptr<std::vector<float>> m_ECF2;
    std::shared_ptr<std::vector<float>> m_ECF3;
    std::shared_ptr<std::vector<float>> m_C2;
    std::shared_ptr<std::vector<float>> m_D2;
    std::shared_ptr<std::vector<float>> m_NTrimSubjets;
    std::shared_ptr<std::vector<int>  > m_NClusters;
    std::shared_ptr<std::vector<int>  > m_nTracks;
    std::shared_ptr<std::vector<int>  > m_ungrtrk500;
    std::shared_ptr<std::vector<float>> m_EMFrac;
    std::shared_ptr<std::vector<int>  > m_nChargedParticles;

    // constituent
    std::shared_ptr<std::vector< int >> m_numConstituents;
  
    // constituentAll
    std::shared_ptr<std::vector< std::vector<float> > > m_constituentWeights;
    std::shared_ptr<std::vector< std::vector<float> > > m_constituent_pt;
    std::shared_ptr<std::vector< std::vector<float> > > m_constituent_eta;
    std::shared_ptr<std::vector< std::vector<float> > > m_constituent_phi;
    std::shared_ptr<std::vector< std::vector<float> > > m_constituent_e;

    // truth
    std::shared_ptr<std::vector<float>> m_truth_m;
    std::shared_ptr<std::vector<float>> m_truth_pt;
    std::shared_ptr<std::vector<float>> m_truth_phi;
    std::shared_ptr<std::vector<float>> m_truth_eta;

    // bosonCount
    std::shared_ptr<std::vector< int >> m_nTQuarks;
    std::shared_ptr<std::vector< int >> m_nHBosons;
    std::shared_ptr<std::vector< int >> m_nWBosons;
    std::shared_ptr<std::vector< int >> m_nZBosons;

    // Assocated Track Jets
    std::unordered_map<std::string, std::shared_ptr< std::vector<std::vector<unsigned int>> >> m_trkJetsIdx;

    // muonCorrection
    std::shared_ptr<std::vector<float>> m_muonCorrected_pt;
    std::shared_ptr<std::vector<float>> m_muonCorrected_eta;
    std::shared_ptr<std::vector<float>> m_muonCorrected_phi;
    std::shared_ptr<std::vector<float>> m_muonCorrected_m;

    // hbbtag
    std::vector<std::shared_ptr<std::vector<float>>> m_Xbb_Higgs;
    std::vector<std::shared_ptr<std::vector<float>>> m_Xbb_QCD  ;
    std::vector<std::shared_ptr<std::vector<float>>> m_Xbb_Top  ;
    
  };
}



#endif // ZprimeNtupler_FatJetNtupler_H
