#ifndef ZprimeNtupler_FFJetSmearingAlg_H
#define ZprimeNtupler_FFJetSmearingAlg_H

// algorithm wrapper
#include "ZprimeNtupler/AnaAlgorithm.h"

// external tools include(s):
#include <AsgTools/ToolHandle.h>
#include <JetCPInterfaces/ICPJetCorrectionTool.h>

namespace Zprime
{
  //! Jet uncertanties via forward folding method.
  /**
   * [more details](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FFJetSmearingTool)
   *
   * Runs over all recommended systematics and creates new output containers with the
   * name `ContainerName`+`SystName`.
   *
   * There is an assumption that the jet container is calibrated by an upstream algorithm.
   * That is, the actual input container looked for is `ContainerName`ShallowCopy.
   *
   * If `InputAlgo` is specified, then the existing contents are prepended to the 
   * `OutputAlgo` list. Only the nominal container is smeared.
   */
  class FFJetSmearingAlg : public Zprime::AnaAlgorithm
  {
  private:
    //! ContainerName: Container name of calibrated jets
    std::string m_containerName = "";

    //! Sort: Sort the output container by pT
    bool m_sort = true;
    
    //! InputAlgo: Name of vectors with systematic variations of the input container
    std::string m_inputAlgo = "";
    //! OutputAlgo: Name of for vectors with systematic variations of this algorithm
    std::string m_outputAlgo = "";

    //! WriteSystToMetadata: Write systematics names to metadata
    bool        m_writeSystToMetadata = false;

    //! SystVal: How many sigma variations to apply
    float       m_systVal = 1.0;

    //! SystName: Name of systematics set to use (blank for none, all for all)
    std::string m_systName = "";

  private:
    std::vector<CP::SystematicSet> m_systList; //!

    // tools
    ToolHandle<ICPJetCorrectionTool> m_ffJetSmearingTool{"CP::FFJetSmearingTool", this}; //!

  public:

    // this is a standard constructor
    FFJetSmearingAlg (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize ();
    virtual StatusCode execute ();
  };
}
#endif
