#ifndef ZprimeNtupler_ElectronNtupler_H
#define ZprimeNtupler_ElectronNtupler_H

#include <TTree.h>

#include <vector>
#include <string>

#include <xAODEgamma/ElectronContainer.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/ParticleNtupler.h"

namespace Zprime
{

  class ElectronNtupler : public ParticleNtupler<HelperClasses::ElectronInfoSwitch>
  {
  public:
    ElectronNtupler(const std::string& name = "el", const std::string& detailStr="", float units = 1e3, bool mc = false, bool storeSystSFs = true);
    virtual ~ElectronNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillElectron( const xAOD::Electron* elec, const xAOD::Vertex* primaryVertex );
    virtual StatusCode FillElectron( const xAOD::IParticle* particle, const xAOD::Vertex* primaryVertex );

  private:

    // kinematic
    std::shared_ptr<std::vector<float>> m_caloCluster_eta;
    std::shared_ptr<std::vector<float>> m_charge;

    // trigger
    std::shared_ptr<std::vector<int              > >m_isTrigMatched;
    std::shared_ptr<std::vector<std::vector<int> > >m_isTrigMatchedToChain;
    std::shared_ptr<std::vector<std::string      > >m_listTrigChains;

    // isolation
    std::unordered_map< std::string, std::shared_ptr<std::vector< int >> > m_isIsolated;
    std::shared_ptr<std::vector<float>> m_etcone20;
    std::shared_ptr<std::vector<float>> m_ptcone20;
    std::shared_ptr<std::vector<float>> m_ptcone30;
    std::shared_ptr<std::vector<float>> m_ptcone40;
    std::shared_ptr<std::vector<float>> m_ptvarcone20;
    std::shared_ptr<std::vector<float>> m_ptvarcone30;
    std::shared_ptr<std::vector<float>> m_ptvarcone40;
    std::shared_ptr<std::vector<float>> m_topoetcone20;
    std::shared_ptr<std::vector<float>> m_topoetcone30;
    std::shared_ptr<std::vector<float>> m_topoetcone40;

    // PID
    std::unordered_map< std::string, std::shared_ptr<std::vector< int > >> m_PID;

    // scale factors w/ sys
    // per object
    std::shared_ptr<std::vector< std::vector< float > >> m_RecoEff_SF;

    std::unordered_map< std::string, std::shared_ptr<std::vector< std::vector< float > > >> m_PIDEff_SF;
    std::unordered_map< std::string, std::shared_ptr<std::vector< std::vector< float > > >> m_IsoEff_SF;
    std::unordered_map< std::string, std::shared_ptr<std::vector< std::vector< float > > >> m_TrigEff_SF;
    std::unordered_map< std::string, std::shared_ptr<std::vector< std::vector< float > > >> m_TrigMCEff_SF;

    // reco parameters
    std::shared_ptr<std::vector<int>> m_author;
    std::shared_ptr<std::vector<int>> m_OQ;

    // track parameters
    std::shared_ptr<std::vector<float>> m_trkd0;
    std::shared_ptr<std::vector<float>> m_trkd0sig;
    std::shared_ptr<std::vector<float>> m_trkz0;
    std::shared_ptr<std::vector<float>> m_trkz0sintheta;
    std::shared_ptr<std::vector<float>> m_trkphi0;
    std::shared_ptr<std::vector<float>> m_trktheta;
    std::shared_ptr<std::vector<float>> m_trkcharge;
    std::shared_ptr<std::vector<float>> m_trkqOverP;

    // track hit content
    std::shared_ptr<std::vector<int>>   m_trknSiHits;
    std::shared_ptr<std::vector<int>>   m_trknPixHits;
    std::shared_ptr<std::vector<int>>   m_trknPixHoles;
    std::shared_ptr<std::vector<int>>   m_trknSCTHits;
    std::shared_ptr<std::vector<int>>   m_trknSCTHoles;
    std::shared_ptr<std::vector<int>>   m_trknTRTHits;
    std::shared_ptr<std::vector<int>>   m_trknTRTHoles;
    std::shared_ptr<std::vector<int>>   m_trknBLayerHits;
    std::shared_ptr<std::vector<int>>   m_trknInnermostPixLayHits; // not available in DC14
    std::shared_ptr<std::vector<float>> m_trkPixdEdX;            // not available in DC14

    // prompt lepton
    std::shared_ptr<std::vector<float>> m_PromptLeptonInput_DL1mu;
    std::shared_ptr<std::vector<float>> m_PromptLeptonInput_DRlj;
    std::shared_ptr<std::vector<float>> m_PromptLeptonInput_LepJetPtFrac;
    std::shared_ptr<std::vector<float>> m_PromptLeptonInput_PtFrac;
    std::shared_ptr<std::vector<float>> m_PromptLeptonInput_PtRel;
    std::shared_ptr<std::vector<int>>   m_PromptLeptonInput_TrackJetNTrack;
    std::shared_ptr<std::vector<float>> m_PromptLeptonInput_ip2;
    std::shared_ptr<std::vector<float>> m_PromptLeptonInput_ip3;
    std::shared_ptr<std::vector<float>> m_PromptLeptonInput_rnnip;
    std::shared_ptr<std::vector<int>>   m_PromptLeptonInput_sv1_jf_ntrkv;
    std::shared_ptr<std::vector<float>> m_PromptLeptonIso;
    std::shared_ptr<std::vector<float>> m_PromptLeptonVeto;
  };
}
#endif // ZprimeNtupler_ElectronNtupler_H
