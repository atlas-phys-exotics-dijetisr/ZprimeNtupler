/********************************************************
 * HelpTreeBase:
 *
 * This class is meant to help the user write out a tree.
 * Some branches are included by default while others
 * need to be added by the user
 *
 * John Alison (john.alison@cern.ch)
 * Gabriel Facini (gabriel.facini@cern.ch)
 * Marco Milesi (marco.milesi@cern.ch)
 * Jeff Dandoy (jeff.dandoy@cern.ch)
 *
 ********************************************************/

// Dear emacs, this is -*-c++-*-
#ifndef ZprimeNtupler_HelpTreeBase_H
#define ZprimeNtupler_HelpTreeBase_H

#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODCaloEvent/CaloClusterContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODTrigger/JetRoIContainer.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTau/TauJetContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/EventInfoNtupler.h"
#include "ZprimeNtupler/TriggerNtupler.h"
#include "ZprimeNtupler/METNtupler.h"
#include "ZprimeNtupler/JetNtupler.h"
#include "ZprimeNtupler/L1JetNtupler.h"
#include "ZprimeNtupler/VertexNtupler.h"
#include "ZprimeNtupler/ElectronNtupler.h"
#include "ZprimeNtupler/PhotonNtupler.h"
#include "ZprimeNtupler/ClusterNtupler.h"
#include "ZprimeNtupler/FatJetNtupler.h"
#include "ZprimeNtupler/TruthParticleNtupler.h"
#include "ZprimeNtupler/TrackParticleNtupler.h"
#include "ZprimeNtupler/MuonNtupler.h"
#include "ZprimeNtupler/TauJetNtupler.h"

#include <map>

// root includes
#include "TTree.h"
#include "TFile.h"

namespace Zprime
{
  /** \brief A helpful class to manage branches corresponding to containers.
   */
  class HelpTreeBase
  {  
  public:
    /**
     * \param tree TTree to use
     * \param isMC True if events are simulated
     * \param units Units for kinematics to convert from MeV (GeV = 1e3)
     */
    HelpTreeBase(TTree* tree, bool isMC, float units = 1e3);
    ~HelpTreeBase() =default;

    /** \name Event Information
     * @{ */

    StatusCode AddEvent (const std::string& detailStr = "");
    StatusCode ClearEvent();
    StatusCode FillEvent(const xAOD::EventInfo* eventInfo, const xAOD::TruthEvent* truthEvent, const xAOD::VertexContainer* vertices = nullptr);

    virtual StatusCode AddEventUser  (const std::string& detailStr = "");
    virtual StatusCode ClearEventUser();
    virtual StatusCode FillEventUser (const xAOD::EventInfo* eventInfo, const xAOD::TruthEvent* truthEvent, const xAOD::VertexContainer* vertices = nullptr);

    /** @} */

    /** \name Trigger Decisions
     * @{ */

    
    StatusCode AddTrigger  (const std::string& detailStr = "");
    StatusCode ClearTrigger();
    StatusCode FillTrigger (const xAOD::EventInfo* eventInfo);

    virtual StatusCode AddTriggerUser(const std::string& detailStr = "");
    virtual StatusCode ClearTriggerUser();
    virtual StatusCode FillTriggerUser(const xAOD::EventInfo* eventInfo);

    /** @} */

    /** \name Electrons
     * @{ */

    StatusCode AddElectrons  (const std::string& detailStr = "", const std::string& name = "el");
    StatusCode ClearElectrons(const std::string& name = "el");
    StatusCode FillElectrons (const xAOD::ElectronContainer* electrons, const xAOD::Vertex* primaryVertex, const std::string& name = "el");

    virtual StatusCode AddElectronsUser  (const std::string& detailStr = "", const std::string& name = "el");
    virtual StatusCode ClearElectronsUser(const std::string& name = "el");
    virtual StatusCode FillElectronsUser (const xAOD::ElectronContainer* electrons, const xAOD::Vertex* primaryVertex, const std::string& name = "el");
    virtual StatusCode FillElectronUser  (const xAOD::Electron* electron, const xAOD::Vertex* primaryVertex, const std::string& name = "el" );

    /** @} */

    /** \name Muons
     * @{ */

    StatusCode AddMuons  (const std::string& detailStr = "", const std::string& name = "muon");
    StatusCode ClearMuons(const std::string& name = "muon");
    StatusCode FillMuons (const xAOD::MuonContainer* muons, const xAOD::Vertex* primaryVertex, const std::string& name = "muon");
    StatusCode FillMuon  (const xAOD::Muon* muon, const xAOD::Vertex* primaryVertex, const std::string& name = "muon" );

    virtual StatusCode AddMuonsUser  (const std::string& detailStr = "", const std::string& name = "muon");
    virtual StatusCode ClearMuonsUser(const std::string& name = "muon");
    virtual StatusCode FillMuonsUser (const xAOD::MuonContainer* muons, const xAOD::Vertex* primaryVertex, const std::string& name = "muon");
    virtual StatusCode FillMuonUser  (const xAOD::Muon* muon, const xAOD::Vertex* primaryVertex, const std::string& name = "muon" );

    /** @} */

    /** \name Taus
     * @{ */

    StatusCode AddTaus  (const std::string& detailStr = "", const std::string& name = "tau");
    StatusCode ClearTaus(const std::string& name = "tau");
    StatusCode FillTaus (const xAOD::TauJetContainer* taus, const std::string& name = "tau");

    virtual StatusCode AddTausUser  (const std::string& detailStr = "", const std::string& name = "tau");
    virtual StatusCode ClearTausUser(const std::string& name = "tau");
    virtual StatusCode FillTausUser (const xAOD::TauJetContainer* taus, const std::string& name = "tau");

    /** @} */
    
    /** \name Photons
     * @{ */

    StatusCode AddPhotons  (const std::string& detailStr = "", const std::string& name = "ph");
    StatusCode ClearPhotons(const std::string& name = "ph");
    StatusCode FillPhotons (const xAOD::PhotonContainer* photons, const std::string& name = "ph");

    virtual StatusCode AddPhotonsUser  (const std::string& detailStr = "", const std::string& name = "ph");
    virtual StatusCode ClearPhotonsUser(const std::string& name = "ph");
    virtual StatusCode FillPhotonsUser (const xAOD::PhotonContainer* photons, const std::string& name = "ph");
    virtual StatusCode FillPhotonUser  (const xAOD::Photon* photon, const std::string& name = "ph");

    /** @} */

    /** \name Clusters
     * @{ */

    StatusCode AddClusters  (const std::string& detailStr = "", const std::string& name = "cl");
    StatusCode ClearClusters(const std::string& name = "cl");
    StatusCode FillClusters (const xAOD::CaloClusterContainer* clusters, const std::string& name = "cl");

    virtual StatusCode AddClustersUser  (const std::string& detailStr = "", const std::string& name = "cl");
    virtual StatusCode ClearClustersUser(const std::string& name = "cl");
    virtual StatusCode FillClustersUser (const xAOD::CaloClusterContainer* clusters, const std::string& name = "cl");

    /** @} */

    /** \name Jets
     * @{ */

    StatusCode AddJets  (const std::string& detailStr = "", const std::string& name = "jet");
    StatusCode ClearJets(const std::string& name = "jet");
    StatusCode FillJets (const xAOD::JetContainer* jets, const xAOD::Vertex* pv, int pvLocation, const std::string& name = "jet");

    virtual StatusCode AddJetsUser  (const std::string& detailStr = "", const std::string& name = "jet");
    virtual StatusCode ClearJetsUser(const std::string& name = "jet");
    virtual StatusCode FillJetsUser (const xAOD::JetContainer* jets, const xAOD::Vertex* pv, int pvLocation, const std::string& name = "jet");
    virtual StatusCode FillJetUser  (const xAOD::Jet* jets, const xAOD::Vertex* pv, int pvLocation, const std::string& name = "jet");

    /** @} */

    /** \name L1 Jets
     * @{ */

    StatusCode AddL1Jets  (const std::string& detailStr = "", const std::string& name = "l1jet");
    StatusCode ClearL1Jets(const std::string& name = "l1jet");
    StatusCode FillL1Jets (const xAOD::JetRoIContainer* jets, const std::string& name = "l1jet");

    virtual StatusCode AddL1JetsUser  (const std::string& detailStr = "", const std::string& name = "l1jet");
    virtual StatusCode ClearL1JetsUser(const std::string& name = "l1jet");
    virtual StatusCode FillL1JetsUser (const xAOD::JetRoIContainer* jets, const std::string& name = "l1jet");

    /** @} */

    /** \name Fat Jets
     * @{ */

    StatusCode AddFatJets  (const std::string& detailStr = "", const std::string& name = "fatjet");
    StatusCode ClearFatJets(const std::string& name = "fatjet");
    StatusCode FillFatJets (const xAOD::JetContainer* fatjets, int pvLocation, const std::string& name = "fatjet");

    virtual StatusCode AddFatJetsUser  (const std::string& detailStr = "", const std::string& name = "fatjet");
    virtual StatusCode ClearFatJetsUser(const std::string& name = "fatjet");
    virtual StatusCode FillFatJetsUser (const xAOD::JetContainer* fatjets, int pvLocation, const std::string& name = "fatjet");
    virtual StatusCode FillFatJetUser  (const xAOD::Jet* fatjet, int pvLocation, const std::string& name = "fatjet");

    /** @} */

    /** \name Truth Particles
     * @{ */

    StatusCode AddTruthParticles  (const std::string& detailStr = "", const std::string& name = "truth");
    StatusCode ClearTruthParticles(const std::string& name = "truth");
    StatusCode FillTruthParticles (const xAOD::TruthParticleContainer* truths, const std::string& name = "truth");
    StatusCode FillTruthParticle  (const xAOD::TruthParticle* truth, const std::string& name = "truth" );

    virtual StatusCode AddTruthParticlesUser  (const std::string& detailStr = "", const std::string& name = "truth");
    virtual StatusCode ClearTruthParticlesUser(const std::string& name = "truth");
    virtual StatusCode FillTruthParticlesUser (const xAOD::TruthParticleContainer* truths, const std::string& name = "truth");
    virtual StatusCode FillTruthParticleUser  (const xAOD::TruthParticle* truth, const std::string& name = "truth" );

    /** @} */

    /** \name Tracks
     * @{ */

    StatusCode AddTrackParticles  (const std::string& detailStr = "", const std::string& name = "trk");
    StatusCode ClearTrackParticles(const std::string& name = "trk");
    StatusCode FillTrackParticles (const xAOD::TrackParticleContainer* tracks, const std::string& name = "trk");

    virtual StatusCode AddTrackParticlesUser  (const std::string& detailStr = "", const std::string& name = "trk");
    virtual StatusCode ClearTrackParticlesUser(const std::string& name = "trk");
    virtual StatusCode FillTrackParticlesUser (const xAOD::TrackParticleContainer* tracks, const std::string& name = "trk");

    /** @} */

    /** \name Vertices
     * @{ */

    StatusCode AddVertices  (const std::string& detailStr = "", const std::string& name = "vertex");
    StatusCode ClearVertices(const std::string& name = "vertex");
    StatusCode FillVertices (const xAOD::VertexContainer* vertices, const std::string& name = "vertex");

    virtual StatusCode AddVerticesUser  (const std::string& detailStr = "", const std::string& name = "vertex");
    virtual StatusCode ClearVerticesUser(const std::string& name = "vertex");
    virtual StatusCode FillVerticesUser (const xAOD::VertexContainer* vertices, const std::string& name = "vertex");

    /** @} */

    /** \name Truth Vertices
     * @{ */

    StatusCode AddTruthVertices  (const std::string& detailStr = "", const std::string& name = "truth_vertex");
    StatusCode ClearTruthVertices(const std::string& name = "truth_vertex");
    StatusCode FillTruthVertices (const xAOD::TruthVertexContainer* vertices, const std::string& name = "truth_vertex");

    virtual StatusCode AddTruthVerticesUser  (const std::string& detailStr = "", const std::string& name = "truth_vertex");
    virtual StatusCode ClearTruthVerticesUser(const std::string& name = "truth_vertex");
    virtual StatusCode FillTruthVerticesUser (const xAOD::TruthVertexContainer* vertices, const std::string& name = "truth_vertex");

    /** @} */

    /** \name Missing Energy
     * @{ */

    StatusCode AddMET  (const std::string& detailStr = "", const std::string& name = "met");
    StatusCode ClearMET(const std::string& name = "met");
    StatusCode FillMET (const xAOD::MissingETContainer* mets, const std::string& name = "met");

    virtual StatusCode AddMETUser  (const std::string& detailStr = "", const std::string& name = "met");
    virtual StatusCode ClearMETUser(const std::string& name = "met");
    virtual StatusCode FillMETUser (const xAOD::MissingETContainer* mets, const std::string& name = "met");

    /** @} */

    // control which branches are filled

    StatusCode writeTo( TFile *file );

    /** \brief Fill TTree
     */
    void Fill();

  protected:
    //! Tree under management
    TTree* m_tree =nullptr;

    //! TTree is nominal
    bool m_nominalTree = true;

    //! Tree with simulated events
    bool m_isMC;

    //! For MeV to GeV conversion in output
    float m_units = 1e3;

    //! event
    std::shared_ptr<Zprime::EventInfoNtupler> m_eventInfo =nullptr;

    
    //! trigger
    std::shared_ptr<Zprime::TriggerNtupler> m_trigger=nullptr;

    //
    //  Jets
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::JetNtupler>> m_jets;

    //
    // L1 Jets
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::L1JetNtupler>> m_l1jets;

    //
    // Truth
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::TruthParticleNtupler>> m_truths;

    //
    // Tracks
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::TrackParticleNtupler>> m_tracks;

    //
    // fat jets
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::FatJetNtupler>> m_fatjets;

    //
    // truth fat jets
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::FatJetNtupler>> m_truth_fatjets;

    //
    // muons
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::MuonNtupler>> m_muons;
    std::unordered_map<std::string, std::vector<std::string> > m_MuonRecoEff_SF_sysNames;
    std::unordered_map<std::string, std::vector<std::string> > m_MuonIsoEff_SF_sysNames;
    std::unordered_map<std::string, std::vector<std::string> > m_MuonTrigEff_SF_sysNames;
    std::vector<std::string>  m_MuonTTVAEff_SF_sysNames;

    //
    // electrons
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::ElectronNtupler>> m_electrons;

    //
    // photons
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::PhotonNtupler>> m_photons;

    //
    // clusters
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::ClusterNtupler>> m_clusters;

    //
    // taus
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::TauJetNtupler>> m_taus;

    //
    // MET
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::METNtupler>> m_met;

    //
    // vertices
    //
    std::unordered_map<std::string, std::shared_ptr<Zprime::VertexNtupler>> m_vertices;
    std::unordered_map<std::string, std::shared_ptr<Zprime::VertexNtupler>> m_truth_vertices;
  };
  
}

#endif
