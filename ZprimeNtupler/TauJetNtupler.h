#ifndef ZprimeNtupler_TauNtupler_H
#define ZprimeNtupler_TauNtupler_H

#include <TTree.h>

#include <vector>
#include <string>

#include <xAODTau/TauJetContainer.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/ParticleNtupler.h"

namespace Zprime
{
  class TauJetNtupler : public ParticleNtupler<HelperClasses::TauInfoSwitch>
  {
  public:
    TauJetNtupler(const std::string& name = "tau", const std::string& detailStr="", float units = 1e3, bool mc = false, bool storeSystSFs = true);
    virtual ~TauJetNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillTauJet( const xAOD::TauJet* tau         );
    virtual StatusCode FillTauJet( const xAOD::IParticle* particle );

  private:

    // trigger
    std::shared_ptr<std::vector<int              > > m_isTrigMatched;
    std::shared_ptr<std::vector<std::vector<int> > > m_isTrigMatchedToChain;
    std::shared_ptr<std::vector<std::string      > > m_listTrigChains;

    std::shared_ptr<std::vector<int  > > m_ntrk;
    std::shared_ptr<std::vector<float> > m_charge;

    // scale factors w/ sys
    // per object
    std::unordered_map<std::string, std::shared_ptr<std::vector< std::vector< float > > >> m_TauEff_SF;
    std::unordered_map<std::string, std::shared_ptr<std::vector< std::vector< float > > >> m_TauTrigEff_SF;

    // might need to delete these
    std::shared_ptr<std::vector<int>   >m_isJetBDTSigVeryLoose;
    std::shared_ptr<std::vector<int>   >m_isJetBDTSigLoose;
    std::shared_ptr<std::vector<int>   >m_isJetBDTSigMedium;
    std::shared_ptr<std::vector<int>   >m_isJetBDTSigTight;

    std::shared_ptr<std::vector<float>   >m_JetBDTScore;
    std::shared_ptr<std::vector<float>   >m_JetBDTScoreSigTrans;

    std::shared_ptr<std::vector<int>   >m_isEleBDTLoose;
    std::shared_ptr<std::vector<int>   >m_isEleBDTMedium;
    std::shared_ptr<std::vector<int>   >m_isEleBDTTight;

    std::shared_ptr<std::vector<float>   >m_EleBDTScore;

    std::shared_ptr<std::vector<int>   >m_passEleOLR;

    std::shared_ptr<std::vector< float > >m_tau_matchedJetWidth;
    std::shared_ptr<std::vector< float > >m_tau_matchedJetJvt;
  
    std::shared_ptr<std::vector< std::vector<float > > > m_tau_tracks_pt;
    std::shared_ptr<std::vector< std::vector<float > > > m_tau_tracks_eta;
    std::shared_ptr<std::vector< std::vector<float > > > m_tau_tracks_phi;

    std::shared_ptr<std::vector< std::vector<int  > > > m_tau_tracks_isCore;
    std::shared_ptr<std::vector< std::vector<int  > > > m_tau_tracks_isWide;
    std::shared_ptr<std::vector< std::vector<int  > > > m_tau_tracks_failTrackFilter;
    std::shared_ptr<std::vector< std::vector<int  > > > m_tau_tracks_passTrkSel;
    std::shared_ptr<std::vector< std::vector<int  > > > m_tau_tracks_isClCharged;
    std::shared_ptr<std::vector< std::vector<int  > > > m_tau_tracks_isClIso;
    std::shared_ptr<std::vector< std::vector<int  > > > m_tau_tracks_isClConv;
    std::shared_ptr<std::vector< std::vector<int  > > > m_tau_tracks_isClFake;
  };
}
#endif // ZprimeNtupler_TauJetNtupler_H
