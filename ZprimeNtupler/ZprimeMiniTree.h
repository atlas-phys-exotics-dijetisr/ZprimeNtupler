#ifndef ZprimeNtupler_ZprimeMiniTree_H
#define ZprimeNtupler_ZprimeMiniTree_H

#include "ZprimeNtupler/HelpTreeBase.h"
#include "TTree.h"

class ZprimeMiniTree : public Zprime::HelpTreeBase
{
private:
  std::vector<int> m_photons_HLT_g60_loose;
  std::vector<int> m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI;
  std::vector<int> m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1;
  std::vector<int> m_photons_HLT_g140_loose;
  std::vector<int> m_photons_HLT_g160_loose;

  std::vector<float> m_fatjets_ANN_score;

  float m_weight;
  float m_weight_corr;
  float m_weight_xs;

  float m_Zprime_pt  ;
  float m_Zprime_eta ;
  float m_Zprime_phi ;
  float m_Zprime_m   ;
  int   m_Zprime_pdg ;

  float m_Higgs_pt   ;
  float m_Higgs_eta  ;
  float m_Higgs_phi  ;
  float m_Higgs_m    ;
  int   m_Higgs_decay;

  float m_Zboson_pt  ;
  float m_Zboson_eta ;
  float m_Zboson_phi ;
  float m_Zboson_m   ;

  float m_Wboson_pt  ;
  float m_Wboson_eta ;
  float m_Wboson_phi ;
  float m_Wboson_m   ;

  float m_reso0_pt   ;
  float m_reso0_eta  ;
  float m_reso0_phi  ;
  float m_reso0_E    ;

  float m_reso1_pt   ;
  float m_reso1_eta  ;
  float m_reso1_phi  ;
  float m_reso1_E    ;

  float m_ISR_pt     ;
  float m_ISR_eta    ;
  float m_ISR_phi    ;
  float m_ISR_E      ;
  int   m_ISR_pdgId  ;

  // HTXS
  int m_HTXS_prodMode;
  int m_HTXS_errorCode;

  float m_HTXS_Higgs_decay_eta;
  float m_HTXS_Higgs_decay_m;
  float m_HTXS_Higgs_decay_phi;
  float m_HTXS_Higgs_decay_pt;

  float m_HTXS_Higgs_eta;
  float m_HTXS_Higgs_m;
  float m_HTXS_Higgs_phi;
  float m_HTXS_Higgs_pt;

  float m_HTXS_V_decay_eta;
  float m_HTXS_V_decay_m;
  float m_HTXS_V_decay_phi;
  float m_HTXS_V_decay_pt;

  float m_HTXS_V_eta;
  float m_HTXS_V_m;
  float m_HTXS_V_phi;
  float m_HTXS_V_pt;

  int m_HTXS_Njets_pTjet25;
  int m_HTXS_Njets_pTjet30;

  int m_HTXS_Stage0_Category;
  int m_HTXS_Stage1_2_Category_pTjet25;
  int m_HTXS_Stage1_2_Category_pTjet30;
  int m_HTXS_Stage1_2_FineIndex_pTjet25;
  int m_HTXS_Stage1_2_FineIndex_pTjet30;
  int m_HTXS_Stage1_2_Fine_Category_pTjet25;
  int m_HTXS_Stage1_2_Fine_Category_pTjet30;
  int m_HTXS_Stage1_2_Fine_FineIndex_pTjet25;
  int m_HTXS_Stage1_2_Fine_FineIndex_pTjet30;
  int m_HTXS_Stage1_Category_pTjet25;
  int m_HTXS_Stage1_Category_pTjet30;
  int m_HTXS_Stage1_FineIndex_pTjet25;
  int m_HTXS_Stage1_FineIndex_pTjet30;

  int m_HTXS_isZ2vvDecay;

  // Settings
  bool m_doTruth;
  bool m_saveZprimeDecay;
  bool m_saveExtraVariables;

public:

  ZprimeMiniTree(TTree *tree, bool isMC, bool doTruthInfo = false, bool saveZprimeDecay = false, bool saveExtraVariables = false);
  ~ZprimeMiniTree() =default;

  StatusCode AddEventUser    (const std::string& detailStr = "" );
  StatusCode FillEventUser   (const xAOD::EventInfo* eventInfo, const xAOD::TruthEvent* truthEvent, const xAOD::VertexContainer* vertices = nullptr);
  StatusCode ClearEventUser  ();

  StatusCode AddPhotonsUser  (const std::string& detailStr = "", const std::string& photonName = "ph");
  StatusCode FillPhotonUser  (const xAOD::Photon* photon, const std::string& /*photonName*/);
  StatusCode ClearPhotonsUser(const std::string& /*photonName*/);

  StatusCode AddFatJetsUser  (const std::string& detailStr = "", const std::string& fatjetName = "");
  StatusCode FillFatJetUser  (const xAOD::Jet* /*jet*/, int /*pvLocation = 0*/, const std::string& /*fatjetName = "fatjet"*/);
  StatusCode ClearFatJetsUser(const std::string& /*fatjetName = "fatjet"*/);

};
#endif
