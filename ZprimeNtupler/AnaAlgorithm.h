#ifndef ZprimeNtupler_AnaAlgorithm_H
#define ZprimeNtupler_AnaAlgorithm_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include "ZprimeNtupler/HelperDefines.h"

#include <TH1D.h>

namespace Zprime
{
  //! This is the base algorithm within |Zprime|.
  /**
   * \note The expectation is that the user does not directly use this class but rather inherits from it.
   * 
   * The main goal of this algorithm class is to standardize how everyone defines an algorithm that plugs into |Zprime|
   * and provide a few useful utilities.
   */
  class AnaAlgorithm : public EL::AnaAlgorithm
  {
  public:
    /** \brief Initialization
     * \param name This is the reference name of the algorithm
     * \param pSvcLocator
     */
    AnaAlgorithm(const std::string& name, ISvcLocator* pSvcLocator);
    ~AnaAlgorithm() =default;

    /** \brief Handle common code per-sample code
     *
     * \return status code
     */
    virtual StatusCode initialize();

    /** \brief Called in execute() for the first event in a file
     *
     * Override in algorithms to run code that needs to be run once
     * per file.
     *
     * Determines the following information in base implementation:
     *  - m_mcChannelNumber
     *  - m_generator
     *  - m_shower
     *
     * \return status code
     */
    virtual StatusCode fileExecute();

    /** \brief Called in execute() for the first event in a sample.
     *
     * Override in algorithms to run code that needs to be run once
     * per sample
     *
     * \return status code.
     */
    virtual StatusCode executeFirstEvent();

    /** \brief Handle common code per-event
     *
     * \return status code
     */
    virtual StatusCode execute();

  protected:
    //! If the xAOD has a different EventInfo container name
    std::string m_eventInfoContainerName = "EventInfo";

    //! If the xAOD has a different PrimaryVertex container name
    std::string m_vertexContainerName = "PrimaryVertices";

    /** \brief Set the isMC decision
     *
     * This stores the isMC decision, and can also be used to override at the algorithm level to force analyzing MC or not.
     *
     * ===== ========================================================
     * Value Meaning
     * ===== ========================================================
     * -1    Default, use EventInfo object to determine if data or MC
     * 0     Treat the input as data
     * 1     Treat the input as MC
     *===== ========================================================
     */
    int m_isMC = -1;

    //! Set the isFastSim decision
    /**
     * This stores the isFastSim decision, and can also be used to override at the algorithm level to force analyzing FastSim or not.
     *
     * ===== ========================================================
     * Value Meaning
     * ===== ========================================================
     * -1    Default, use Metadata object to determine if FullSim or FastSim
     * 0     Treat the input as FullSim
     * 1     Treat the input as FastSim
     * ===== ========================================================
     */
    int m_isFastSim = -1;

    /** \brief MC channel number (DSID) for the file being processed
     *
     * Set during fileExecute
     */
    uint32_t m_mcChannelNumber = 0;

    /** \brief Generator for the file being processed
     *
     * Set during fileExecute
     */
    Generator m_generator = Generator::Unknown;

    /** \brief Shower for the file being processed
     *
     * Set during fileExecute
     */
    Shower m_shower = Shower::Unknown;
    
  protected:

    /** Determine if we are running over data or MC
     *
     * The :cpp:member:`xAH::Algorithm::m_isMC` can be used 
     * to fix the return value. Otherwise the `EventInfo` object is queried.
     *
     * An exception is thrown if the type cannot be determined.
     *
     * ============ =======
     * Return Value Meaning
     * ============ =======
     * 0            Data
     * 1            MC
     * ============ =======
     */
    bool isMC();

    /** Deterime if we are running over full or fast sim.
     *
     * The :cpp:member:`xAH::Algorithm::m_isFastSim` can be used
     * to fix the return value. Otherwise the metadata is queried.
     *
     * An exception is thrown if the type cannot be determined.
     *
     * ============ =======
     * Return Value Meaning
     * ============ =======
     * 0            FullSim (or Data)
     * 1            FastSim
     * ============ =======
     */
    bool isFastSim();

    /** Determine the current MC channel number
     */
    uint32_t mcChannelNumber() const;

    /** Determine the generator of the current sample
     */
    Generator generator() const;

    /** Determine the generator of the current sample
     */
    Shower shower() const;
    
    /** \brief Initialize a cutflow bin called cutName
     *
     * \param cutName name of the cut
     *
     * \return bin index used to refer to the cut
     */
    int init_cutflow(const std::string& cutName);

    /** \brief Record that event passed cut
     *
     * \param cut bin index cut
     *
     * \return status code
     */    
    StatusCode cutflow(int cut);

  private:
    //! indicates whether execute() has been called once yet (aka first event)
    bool m_firstExecute = true;

    /* \name Cutflow
     * @{ */

    //! \brief Cutflow histogram (event count)
    TH1D *m_cutflowHist  =nullptr;

    //! \brief Cutflow histogram (weighted)
    TH1D *m_cutflowHistW =nullptr;

    /** @} */
  };

}
#endif
