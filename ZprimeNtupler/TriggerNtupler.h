#ifndef ZprimeNtupler_TriggerNtupler_H
#define ZprimeNtupler_TriggerNtupler_H

#include <TTree.h>

#include <AsgTools/MessageCheck.h>

#include "ZprimeNtupler/HelperClasses.h"

namespace Zprime
{
  class TriggerNtupler
  {
  public:
    TriggerNtupler(const std::string& detailStr="");
    ~TriggerNtupler() =default;

    StatusCode createBranches(TTree *tree);
    StatusCode clear();
    StatusCode fill( const xAOD::EventInfo* eventInfo );

  private:

    HelperClasses::TriggerInfoSwitch  m_infoSwitch;

  public:
    // basic
    int m_passL1;
    int m_passHLT;

    // menuKeys
    int m_masterKey;
    int m_L1PSKey;
    int m_HLTPSKey;

    // passTriggers
    std::unordered_map<std::string,char        > m_trigger;
    std::unordered_map<std::string,char        > m_trigger_disabled;

    // prescales
    std::unordered_map<std::string,float       > m_trigger_prescale;

    // prescalesLumi
    std::unordered_map<std::string,float       > m_trigger_prescaleLumi;

    // passTrigBits
    std::unordered_map<std::string,unsigned int> m_trigger_isPassBits;

  };
}



#endif // ZprimeNtupler_TriggerNtupler_H
