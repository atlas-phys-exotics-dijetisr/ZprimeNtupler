#ifndef ZprimeNtupler_EventInfoNtupler_H
#define ZprimeNtupler_EventInfoNtupler_H

#include <TTree.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/VertexContainer.h>
#include <xAODTruth/TruthEvent.h>

#include <AsgTools/MessageCheck.h>

#include "ZprimeNtupler/HelperClasses.h"

namespace Zprime
{

  class EventInfoNtupler
  {
  public:
    EventInfoNtupler(const std::string& detailStr="", float units = 1e3, bool mc = false, bool storeSyst = true);
    ~EventInfoNtupler() =default;

    StatusCode createBranches(TTree *tree);
    StatusCode clear();
    StatusCode fill( const xAOD::EventInfo* eventInfo, const xAOD::TruthEvent* truthEvent, const xAOD::VertexContainer* vertices = nullptr);

  private:

    HelperClasses::EventInfoSwitch  m_infoSwitch;

    bool m_mc;
    bool m_storeSyst;
    float m_units;

  private:

    int      m_runNumber;
    Long64_t m_eventNumber;
    int      m_lumiBlock;
    uint32_t m_coreFlags;
    uint32_t m_timeStamp;
    uint32_t m_timeStampNSOffset;
    bool     m_TileError;
    bool     m_LArError;
    bool     m_SCTError;
    uint32_t m_TileFlags;
    uint32_t m_LArFlags;
    uint32_t m_SCTFlags;
    bool     m_eventClean_LooseBad;
    int      m_mcEventNumber;
    int      m_mcChannelNumber;
    float    m_mcEventWeight;
    std::vector<float> m_mcEventWeights;
    float    m_weight_pileup;
    float    m_weight_pileup_up;
    float    m_weight_pileup_down;
    float    m_correctedAvgMu;
    float    m_correctedAndScaledAvgMu;
    float    m_correctedMu;
    float    m_correctedAndScaledMu;
    int      m_rand_run_nr;
    int      m_rand_lumiblock_nr;
    int      m_bcid;
    int      m_DistEmptyBCID;
    int      m_DistLastUnpairedBCID;
    int      m_DistNextUnpairedBCID;

    // event pileup
    int      m_npv;
    float    m_actualMu;
    float    m_averageMu;

    // truth
    int      m_pdgId1;
    int      m_pdgId2;
    int      m_pdfId1;
    int      m_pdfId2;
    float    m_x1;
    float    m_x2;
    //float m_scale;
    float    m_q;
    //float m_pdf1;
    //float m_pdf2;
    float    m_xf1;
    float    m_xf2;

  private:
    std::vector<std::string> m_weightNames;    
  };

}


#endif // ZprimeNtupler_EventInfoNtupler_H
