#ifndef ZprimeNtupler_JetNtupler_H
#define ZprimeNtupler_JetNtupler_H

#include <TTree.h>

#include <vector>
#include <string>

#include <xAODJet/JetContainer.h>

#include <InDetTrackSelectionTool/InDetTrackSelectionTool.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/HelperFunctions.h"
#include "ZprimeNtupler/ParticleNtupler.h"

namespace Zprime
{
  class JetNtupler : public ParticleNtupler<HelperClasses::JetInfoSwitch>
  {
  public:
    JetNtupler(const std::string& name = "jet", const std::string& detailStr="", float units = 1e3, bool mc = false);
    virtual ~JetNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillJet( const xAOD::Jet* jet           , const xAOD::Vertex* pv, int pvLocation );
    virtual StatusCode FillJet( const xAOD::IParticle* particle, const xAOD::Vertex* pv, int pvLocation );

  private:

    InDet::InDetTrackSelectionTool * m_trkSelTool;

    //
    // Vector branches

    // rapidity
    std::shared_ptr<std::vector<float>> m_rapidity;

    // trigger
    std::shared_ptr<std::vector<int>              > m_isTrigMatched;
    std::shared_ptr<std::vector<std::vector<int> >> m_isTrigMatchedToChain;
    std::shared_ptr<std::vector<std::string>      > m_listTrigChains;

    // clean
    std::shared_ptr<std::vector<float>> m_Timing;
    std::shared_ptr<std::vector<float>> m_LArQuality;
    std::shared_ptr<std::vector<float>> m_HECQuality;
    std::shared_ptr<std::vector<float>> m_NegativeE;
    std::shared_ptr<std::vector<float>> m_AverageLArQF;
    std::shared_ptr<std::vector<float>> m_BchCorrCell;
    std::shared_ptr<std::vector<float>> m_N90Constituents;
    std::shared_ptr<std::vector<float>> m_LArBadHVEnergyFrac;
    std::shared_ptr<std::vector<int>  > m_LArBadHVNCell;
    std::shared_ptr<std::vector<float>> m_OotFracClusters5;
    std::shared_ptr<std::vector<float>> m_OotFracClusters10;
    std::shared_ptr<std::vector<float>> m_LeadingClusterPt;
    std::shared_ptr<std::vector<float>> m_LeadingClusterSecondLambda;
    std::shared_ptr<std::vector<float>> m_LeadingClusterCenterLambda;
    std::shared_ptr<std::vector<float>> m_LeadingClusterSecondR;
    std::shared_ptr<std::vector<int>  > m_clean_passLooseBad;
    std::shared_ptr<std::vector<int>  > m_clean_passLooseBadUgly;
    std::shared_ptr<std::vector<int>  > m_clean_passLooseBadTrigger;
    std::shared_ptr<std::vector<int>  > m_clean_passLooseBadTriggerUgly;
    std::shared_ptr<std::vector<int>  > m_clean_passTightBad;
    std::shared_ptr<std::vector<int>  > m_clean_passTightBadUgly;

    // energy
    std::shared_ptr<std::vector<float>> m_HECFrac;
    std::shared_ptr<std::vector<float>> m_EMFrac;
    std::shared_ptr<std::vector<float>> m_CentroidR;
    std::shared_ptr<std::vector<float>> m_FracSamplingMax;
    std::shared_ptr<std::vector<float>> m_FracSamplingMaxIndex;
    std::shared_ptr<std::vector<float>> m_LowEtConstituentsFrac;
    std::shared_ptr<std::vector<float>> m_GhostMuonSegmentCount;
    std::shared_ptr<std::vector<float>> m_Width;

    // scales
    std::shared_ptr<std::vector<float>> m_emScalePt;
    std::shared_ptr<std::vector<float>> m_constScalePt;
    std::shared_ptr<std::vector<float>> m_pileupScalePt;
    std::shared_ptr<std::vector<float>> m_originConstitScalePt;
    std::shared_ptr<std::vector<float>> m_etaJESScalePt;
    std::shared_ptr<std::vector<float>> m_gscScalePt;
    std::shared_ptr<std::vector<float>> m_jmsScalePt;
    std::shared_ptr<std::vector<float>> m_insituScalePt;

    std::shared_ptr<std::vector<float>> m_emScaleM;
    std::shared_ptr<std::vector<float>> m_constScaleM;
    std::shared_ptr<std::vector<float>> m_pileupScaleM;
    std::shared_ptr<std::vector<float>> m_originConstitScaleM;
    std::shared_ptr<std::vector<float>> m_etaJESScaleM;
    std::shared_ptr<std::vector<float>> m_gscScaleM;
    std::shared_ptr<std::vector<float>> m_jmsScaleM;
    std::shared_ptr<std::vector<float>> m_insituScaleM;

    // constScale Eta
    std::shared_ptr<std::vector<float>> m_constScaleEta;

    // detector Eta
    std::shared_ptr<std::vector<float>> m_detectorEta;

    // layer
    std::shared_ptr<std::vector< std::vector<float> >> m_EnergyPerSampling;

    // tracksAll
    std::shared_ptr<std::vector< std::vector<int> >  > m_NumTrkPt1000;
    std::shared_ptr<std::vector< std::vector<float> >> m_SumPtTrkPt1000;
    std::shared_ptr<std::vector< std::vector<float> >> m_TrackWidthPt1000;
    std::shared_ptr<std::vector< std::vector<int> >  > m_NumTrkPt500;
    std::shared_ptr<std::vector< std::vector<float> >> m_SumPtTrkPt500;
    std::shared_ptr<std::vector< std::vector<float> >> m_TrackWidthPt500;
    std::shared_ptr<std::vector< std::vector<float> >> m_JVF;

    // trackPV
    std::shared_ptr<std::vector<float>> m_NumTrkPt1000PV;
    std::shared_ptr<std::vector<float>> m_SumPtTrkPt1000PV;
    std::shared_ptr<std::vector<float>> m_TrackWidthPt1000PV;
    std::shared_ptr<std::vector<float>> m_NumTrkPt500PV;
    std::shared_ptr<std::vector<float>> m_SumPtTrkPt500PV;
    std::shared_ptr<std::vector<float>> m_TrackWidthPt500PV;
    std::shared_ptr<std::vector<float>> m_JVFPV;

    // trackAll or trackPV
    std::shared_ptr<std::vector<float>> m_Jvt;
    std::shared_ptr<std::vector<int  >> m_JvtPass_Loose;
    std::shared_ptr<std::vector<std::vector<float> >> m_JvtEff_SF_Loose;
    std::shared_ptr<std::vector<int  >> m_JvtPass_Medium;
    std::shared_ptr<std::vector<std::vector<float> >> m_JvtEff_SF_Medium;
    std::shared_ptr<std::vector<int  >> m_JvtPass_Tight;
    std::shared_ptr<std::vector<std::vector<float> >> m_JvtEff_SF_Tight;
    std::shared_ptr<std::vector<float>> m_JvtJvfcorr;
    std::shared_ptr<std::vector<float>> m_JvtRpt;
    std::shared_ptr<std::vector<int  >> m_fJvtPass_Medium;
    std::shared_ptr<std::vector<std::vector<float> >> m_fJvtEff_SF_Medium;
    std::shared_ptr<std::vector<int  >> m_fJvtPass_Tight;
    std::shared_ptr<std::vector<std::vector<float> >> m_fJvtEff_SF_Tight;

    // allTrack
    std::shared_ptr<std::vector<int  >               > m_GhostTrackCount;
    std::shared_ptr<std::vector<float>               > m_GhostTrackPt;
    std::shared_ptr<std::vector< std::vector<float> >> m_GhostTrack_pt;
    std::shared_ptr<std::vector< std::vector<float> >> m_GhostTrack_qOverP;
    std::shared_ptr<std::vector< std::vector<float> >> m_GhostTrack_eta;
    std::shared_ptr<std::vector< std::vector<float> >> m_GhostTrack_phi;
    std::shared_ptr<std::vector< std::vector<float> >> m_GhostTrack_e;
    std::shared_ptr<std::vector< std::vector<float> >> m_GhostTrack_d0;
    std::shared_ptr<std::vector< std::vector<float> >> m_GhostTrack_z0;

    // allTrackDetail
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nPixelHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nSCTHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nTRTHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nPixelSharedHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nPixelSplitHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nInnermostPixelLayerHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nInnermostPixelLayerSharedHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nInnermostPixelLayerSplitHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nNextToInnermostPixelLayerHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nNextToInnermostPixelLayerSharedHits;
    std::shared_ptr<std::vector< std::vector<int> >> m_GhostTrack_nNextToInnermostPixelLayerSplitHits;

    // constituent
    std::shared_ptr<std::vector<int               > > m_numConstituents;
    std::shared_ptr<std::vector<std::vector<float>> > m_constituentWeights;
    std::shared_ptr<std::vector<std::vector<float>> > m_constituent_pt;
    std::shared_ptr<std::vector<std::vector<float>> > m_constituent_eta;
    std::shared_ptr<std::vector<std::vector<float>> > m_constituent_phi;
    std::shared_ptr<std::vector<std::vector<float>> > m_constituent_e;

    // flavTag
    std::shared_ptr<std::vector<float>> m_MV2c00;
    std::shared_ptr<std::vector<float>> m_MV2c10;
    std::shared_ptr<std::vector<float>> m_MV2c10mu;
    std::shared_ptr<std::vector<float>> m_MV2c10rnn;
    std::shared_ptr<std::vector<float>> m_MV2rmu;
    std::shared_ptr<std::vector<float>> m_MV2r;
    std::shared_ptr<std::vector<float>> m_MV2c20;
    std::shared_ptr<std::vector<float>> m_MV2c100;
    std::shared_ptr<std::vector<float>> m_DL1;
    std::shared_ptr<std::vector<float>> m_DL1_pu;
    std::shared_ptr<std::vector<float>> m_DL1_pc;
    std::shared_ptr<std::vector<float>> m_DL1_pb;

    std::shared_ptr<std::vector<float>> m_DL1mu;
    std::shared_ptr<std::vector<float>> m_DL1mu_pu;
    std::shared_ptr<std::vector<float>> m_DL1mu_pc;
    std::shared_ptr<std::vector<float>> m_DL1mu_pb;

    std::shared_ptr<std::vector<float>> m_DL1rnn;
    std::shared_ptr<std::vector<float>> m_DL1rnn_pu;
    std::shared_ptr<std::vector<float>> m_DL1rnn_pc;
    std::shared_ptr<std::vector<float>> m_DL1rnn_pb;
    std::shared_ptr<std::vector<int>  > m_HadronConeExclTruthLabelID;
    std::shared_ptr<std::vector<int>  > m_HadronConeExclExtendedTruthLabelID;

    std::shared_ptr<std::vector<float>> m_DL1rmu;
    std::shared_ptr<std::vector<float>> m_DL1rmu_pu;
    std::shared_ptr<std::vector<float>> m_DL1rmu_pc;
    std::shared_ptr<std::vector<float>> m_DL1rmu_pb;

    std::shared_ptr<std::vector<float>> m_DL1r;
    std::shared_ptr<std::vector<float>> m_DL1r_pu;
    std::shared_ptr<std::vector<float>> m_DL1r_pc;
    std::shared_ptr<std::vector<float>> m_DL1r_pb;

    // Jet Fitter
    std::shared_ptr<std::vector<float> > m_JetFitter_nVTX           ;
    std::shared_ptr<std::vector<float> > m_JetFitter_nSingleTracks  ;
    std::shared_ptr<std::vector<float> > m_JetFitter_nTracksAtVtx   ;
    std::shared_ptr<std::vector<float> > m_JetFitter_mass           ;
    std::shared_ptr<std::vector<float> > m_JetFitter_energyFraction ;
    std::shared_ptr<std::vector<float> > m_JetFitter_significance3d ;
    std::shared_ptr<std::vector<float> > m_JetFitter_deltaeta       ;
    std::shared_ptr<std::vector<float> > m_JetFitter_deltaphi       ;
    std::shared_ptr<std::vector<float> > m_JetFitter_N2Tpar         ;

    // SV Details
    std::shared_ptr<std::vector<float>> m_SV0;
    std::shared_ptr<std::vector<float>> m_sv0_NGTinSvx  ;
    std::shared_ptr<std::vector<float>> m_sv0_N2Tpair   ;
    std::shared_ptr<std::vector<float>> m_sv0_massvx    ;
    std::shared_ptr<std::vector<float>> m_sv0_efracsvx  ;
    std::shared_ptr<std::vector<float>> m_sv0_normdist  ;

    std::shared_ptr<std::vector<float>> m_SV1;
    std::shared_ptr<std::vector<float>> m_SV1IP3D;
    std::shared_ptr<std::vector<float>> m_COMBx;
    std::shared_ptr<std::vector<float>> m_sv1_pu        ;
    std::shared_ptr<std::vector<float>> m_sv1_pb        ;
    std::shared_ptr<std::vector<float>> m_sv1_pc        ;
    std::shared_ptr<std::vector<float>> m_sv1_c         ;
    std::shared_ptr<std::vector<float>> m_sv1_cu        ;
    std::shared_ptr<std::vector<float>> m_sv1_NGTinSvx  ;
    std::shared_ptr<std::vector<float>> m_sv1_N2Tpair   ;
    std::shared_ptr<std::vector<float>> m_sv1_massvx    ;
    std::shared_ptr<std::vector<float>> m_sv1_efracsvx  ;
    std::shared_ptr<std::vector<float>> m_sv1_normdist  ;
    std::shared_ptr<std::vector<float>> m_sv1_Lxy       ;
    std::shared_ptr<std::vector<float>> m_sv1_sig3d       ;
    std::shared_ptr<std::vector<float>> m_sv1_L3d       ;
    std::shared_ptr<std::vector<float>> m_sv1_distmatlay;
    std::shared_ptr<std::vector<float>> m_sv1_dR        ;

    // IP3D
    std::shared_ptr<std::vector<float>> m_IP2D_pu                   ;
    std::shared_ptr<std::vector<float>> m_IP2D_pb                   ;
    std::shared_ptr<std::vector<float>> m_IP2D_pc                   ;
    std::shared_ptr<std::vector<float>> m_IP2D                      ;
    std::shared_ptr<std::vector<float>> m_IP2D_c                    ;
    std::shared_ptr<std::vector<float>> m_IP2D_cu                   ;
    std::shared_ptr<std::vector<float>> m_nIP2DTracks               ;
    
    std::shared_ptr<std::vector<std::vector<float> >> m_IP2D_gradeOfTracks        ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP2D_flagFromV0ofTracks   ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP2D_valD0wrtPVofTracks   ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP2D_sigD0wrtPVofTracks   ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP2D_weightBofTracks      ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP2D_weightCofTracks      ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP2D_weightUofTracks      ;

    std::shared_ptr<std::vector<float>> m_IP3D_pu                   ;
    std::shared_ptr<std::vector<float>> m_IP3D_pb                   ;
    std::shared_ptr<std::vector<float>> m_IP3D_pc                   ;
    std::shared_ptr<std::vector<float>> m_IP3D                      ;
    std::shared_ptr<std::vector<float>> m_IP3D_c                    ;
    std::shared_ptr<std::vector<float>> m_IP3D_cu                   ;
    std::shared_ptr<std::vector<float>> m_nIP3DTracks               ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_gradeOfTracks        ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_flagFromV0ofTracks   ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_valD0wrtPVofTracks   ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_sigD0wrtPVofTracks   ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_valZ0wrtPVofTracks   ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_sigZ0wrtPVofTracks   ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_weightBofTracks      ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_weightCofTracks      ;
    std::shared_ptr<std::vector<std::vector<float> >> m_IP3D_weightUofTracks      ;

    std::shared_ptr<std::vector<float>> m_vtxOnlineValid;
    std::shared_ptr<std::vector<float>> m_vtxHadDummy;

    std::shared_ptr<std::vector<float>> m_bs_online_vx;
    std::shared_ptr<std::vector<float>> m_bs_online_vy;
    std::shared_ptr<std::vector<float>> m_bs_online_vz;

    std::shared_ptr<std::vector<float>> m_vtx_offline_x0;
    std::shared_ptr<std::vector<float>> m_vtx_offline_y0;
    std::shared_ptr<std::vector<float>> m_vtx_offline_z0;

    std::shared_ptr<std::vector<float>> m_vtx_online_x0;
    std::shared_ptr<std::vector<float>> m_vtx_online_y0;
    std::shared_ptr<std::vector<float>> m_vtx_online_z0;

    std::shared_ptr<std::vector<float>> m_vtx_online_bkg_x0;
    std::shared_ptr<std::vector<float>> m_vtx_online_bkg_y0;
    std::shared_ptr<std::vector<float>> m_vtx_online_bkg_z0;

    struct btagOpPoint
    {
      bool m_mc;
      std::string m_accessorName;
      bool m_isContinuous;

      // branches
      std::shared_ptr<std::vector<int                 > > m_isTag;
      std::shared_ptr<std::vector< std::vector<float> > > m_sf;
      std::shared_ptr<std::vector< std::vector<float> > > m_ineffSf; // for continuous
      std::shared_ptr<std::vector< std::vector<float> > > m_eff;
      std::shared_ptr<std::vector<float               > > m_mceff;      

      btagOpPoint(bool mc, const std::string& tagger, const std::string& wp)
	: m_mc(mc), m_accessorName(tagger+"_"+wp)
      {
	m_isTag = std::make_shared<std::vector<int                >>();
	m_sf    = std::make_shared<std::vector<std::vector<float> >>();

	m_isContinuous = (wp == "Continuous");
	if(m_isContinuous)
	  m_ineffSf = std::make_shared<std::vector< std::vector<float> >>();

	m_eff   = std::make_shared<std::vector<std::vector<float> >>();
	m_mceff = std::make_shared<std::vector<float              >>();
      }

      StatusCode createBranch(TTree* tree, const std::string& jetName)
      {
	std::string id = m_isContinuous ? "_Quantile_" : "_is_";
	tree->Branch((jetName+id+m_accessorName         ).c_str()              , m_isTag.get());
	if ( m_mc )
	  {
	    tree->Branch((jetName+"_SF_"+m_accessorName ).c_str()              , m_sf.get());
	    if(m_isContinuous)
	      tree->Branch((jetName+"_InefficiencySF_"+m_accessorName).c_str() , m_ineffSf.get());
	    tree->Branch((jetName+"_eff_"  +m_accessorName).c_str()            , m_eff  .get());
	    tree->Branch((jetName+"_mceff_"+m_accessorName).c_str()            , m_mceff.get());
	  }
	return StatusCode::SUCCESS;
      }

      StatusCode clear()
      {
	m_isTag->clear();
	m_sf->clear();
	if(m_isContinuous)
	  m_ineffSf->clear();
	m_eff  ->clear();
	m_mceff->clear();
	return StatusCode::SUCCESS;	
      }

      StatusCode Fill( const xAOD::Jet* jet )
      {
	static const std::vector<float> junk(1,-999);

	if( m_isContinuous )
	  {
	    SG::AuxElement::ConstAccessor< int > quantile("BTag_Quantile_"+m_accessorName);
	    m_isTag->push_back( quantile.isAvailable(*jet) ? quantile(*jet) : -1 );

	    if(m_mc)
	      {
		SG::AuxElement::ConstAccessor< std::vector<float> > sf     ("BTag_SF_"            +m_accessorName);
		SG::AuxElement::ConstAccessor< std::vector<float> > ineffSf("BTag_InefficiencySF_"+m_accessorName);

		m_sf     ->push_back(      sf.isAvailable( *jet ) ?      sf( *jet ) : junk);
		m_ineffSf->push_back( ineffSf.isAvailable( *jet ) ? ineffSf( *jet ) : junk);
	      }
	  }
	else
	  {
	    SG::AuxElement::ConstAccessor< char > isTag("BTag_"+m_accessorName);
	    m_isTag->push_back( isTag.isAvailable( *jet ) ? isTag( *jet ) : -1 );

	    if(m_mc)
	      {
		SG::AuxElement::ConstAccessor< std::vector<float> > sf("BTag_SF_"+m_accessorName);
		m_sf->push_back( sf.isAvailable( *jet ) ? sf   ( *jet ) : junk);
	      }
	  }

	if(m_mc)
	  {
	    SG::AuxElement::ConstAccessor< std::vector<float> > eff  ("BTag_Eff_"  +m_accessorName);
	    SG::AuxElement::ConstAccessor< float              > mceff("BTag_MCEff_"+m_accessorName);

	    m_eff  ->push_back(eff  .isAvailable( *jet ) ?   eff( *jet ) : junk );
	    m_mceff->push_back(mceff.isAvailable( *jet ) ? mceff( *jet ) : -1   );
	  }

	return StatusCode::SUCCESS;
      } // Fill
    };

    std::vector<btagOpPoint> m_btags;

    // JVC
    std::shared_ptr<std::vector<double>> m_JetVertexCharge_discriminant;

    // area
    std::shared_ptr<std::vector<float>> m_GhostArea;
    std::shared_ptr<std::vector<float>> m_ActiveArea;
    std::shared_ptr<std::vector<float>> m_VoronoiArea;
    std::shared_ptr<std::vector<float>> m_ActiveArea4vec_pt;
    std::shared_ptr<std::vector<float>> m_ActiveArea4vec_eta;
    std::shared_ptr<std::vector<float>> m_ActiveArea4vec_phi;
    std::shared_ptr<std::vector<float>> m_ActiveArea4vec_m;

    // truth
    std::shared_ptr<std::vector<int>  > m_ConeTruthLabelID;
    std::shared_ptr<std::vector<int>  > m_TruthCount;
    std::shared_ptr<std::vector<float>> m_TruthLabelDeltaR_B;
    std::shared_ptr<std::vector<float>> m_TruthLabelDeltaR_C;
    std::shared_ptr<std::vector<float>> m_TruthLabelDeltaR_T;
    std::shared_ptr<std::vector<int>  > m_PartonTruthLabelID;
    std::shared_ptr<std::vector<float>> m_GhostTruthAssociationFraction;
    std::shared_ptr<std::vector<float>> m_truth_E;
    std::shared_ptr<std::vector<float>> m_truth_pt;
    std::shared_ptr<std::vector<float>> m_truth_phi;
    std::shared_ptr<std::vector<float>> m_truth_eta;

    // truth detail
    std::shared_ptr<std::vector<int>  > m_GhostBHadronsFinalCount;
    std::shared_ptr<std::vector<int>  > m_GhostBHadronsInitialCount;
    std::shared_ptr<std::vector<int>  > m_GhostBQuarksFinalCount;
    std::shared_ptr<std::vector<float>> m_GhostBHadronsFinalPt;
    std::shared_ptr<std::vector<float>> m_GhostBHadronsInitialPt;
    std::shared_ptr<std::vector<float>> m_GhostBQuarksFinalPt;

    std::shared_ptr<std::vector<int>  > m_GhostCHadronsFinalCount;
    std::shared_ptr<std::vector<int>  > m_GhostCHadronsInitialCount;
    std::shared_ptr<std::vector<int>  > m_GhostCQuarksFinalCount;
    std::shared_ptr<std::vector<float>> m_GhostCHadronsFinalPt;
    std::shared_ptr<std::vector<float>> m_GhostCHadronsInitialPt;
    std::shared_ptr<std::vector<float>> m_GhostCQuarksFinalPt;

    std::shared_ptr<std::vector<int>  > m_GhostTausFinalCount;
    std::shared_ptr<std::vector<float>> m_GhostTausFinalPt;

    // charge
    std::shared_ptr<std::vector<double>> m_charge;

    // passSel
    std::shared_ptr<std::vector<char>> m_passSel;
  };
}



#endif // ZprimeNtupler_JetNtupler_H
