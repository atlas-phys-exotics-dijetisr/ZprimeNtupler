/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack
/// @author Karol Krizka



#ifndef ZPRIMENTUPLER_PYFT_LOCAL_DRIVER_HH
#define ZPRIMENTUPLER_PYFT_LOCAL_DRIVER_HH

#include <EventLoop/Global.h>

#include <SampleHandler/Global.h>

#include <ZprimeNtupler/PyTFDriver.h>

namespace Zprime
{
  /// \brief a \ref Driver for running batch jobs locally using 
  /// PyTaskFarmer.
  ///
  class PyTFLocalDriver final : public Zprime::PyTFDriver
  {
    //
    // public interface
    //

    /// effects: test the invariant of this object
    /// guarantee: no-fail
  public:
    void testInvariant () const;


    /// effects: standard default constructor
    /// guarantee: strong
    /// failures: low level errors I
  public:
    PyTFLocalDriver ();



    //
    // interface inherited from BatchDriver
    //

  protected:
    virtual ::StatusCode
    doManagerStep (EL::Detail::ManagerData& data) const override;



    //
    // private interface
    //

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Winconsistent-missing-override"
    ClassDef(PyTFLocalDriver, 1);
#pragma GCC diagnostic pop
  };
}

#endif
