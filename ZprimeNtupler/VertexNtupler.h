#ifndef ZprimeNtupler_VertexNtupler_H
#define ZprimeNtupler_VertexNtupler_H

#include <TTree.h>

#include <xAODTracking/VertexContainer.h>
#include <xAODTruth/TruthVertexContainer.h>

#include <AsgTools/MessageCheck.h>

#include "ZprimeNtupler/HelperClasses.h"

namespace Zprime
{

  class VertexNtupler
  {
  public:
    VertexNtupler(const std::string& name = "vertex", const std::string& detailStr="");
    ~VertexNtupler() =default;

    StatusCode createBranches(TTree *tree);
    StatusCode clear();
    StatusCode FillVertices( const xAOD::VertexContainer* vertices);
    StatusCode FillTruthVertices( const xAOD::TruthVertexContainer* truthVertices);

  private:
    std::string branchName(const std::string& varName);

    template<typename T> void createBranch(TTree* tree, std::string varName, std::shared_ptr<std::vector<T>> localVectorPtr)
    {
      std::string name = branchName(varName);
      tree->Branch(name.c_str(), localVectorPtr.get());
    }

  private:
    std::string m_name;
    std::string m_detailStr;    

    // Vector branches
    std::shared_ptr<std::vector<float>> m_vertex_x;
    std::shared_ptr<std::vector<float>> m_vertex_y;
    std::shared_ptr<std::vector<float>> m_vertex_z;
  };

}

#endif // ZprimeNtupler_VertexNtupler_H
