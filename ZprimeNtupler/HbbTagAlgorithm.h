#ifndef ZprimeNtupler_HbbTagAlgorithm_H
#define ZprimeNtupler_HbbTagAlgorithm_H

#include <EventLoop/StatusCode.h>

//algorithm wrapper
#include "ZprimeNtupler/AnaAlgorithm.h"

// tools
#include <AsgTools/ToolHandle.h>
#include <JetInterface/IJetDecorator.h>

namespace Zprime
{
  class HbbTagAlgorithm : public AnaAlgorithm
  {
   
  private:
    //! InputAlgo: name of systematics container
    std::string m_inputAlgo = "";
    
    //! InContainerName: name of input container with large R jets
    std::string m_inContainerName = "";

  private:
    ToolHandle<IJetDecorator> m_HbbTagTool{"HbbTagTool", this};

  public:
    //! Standard constructor
    HbbTagAlgorithm (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode execute();

  private:
  };
}

#endif
