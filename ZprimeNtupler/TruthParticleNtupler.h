#ifndef ZprimeNtupler_TruthParticleNtupler_H
#define ZprimeNtupler_TruthParticleNtupler_H

#include <TTree.h>

#include <vector>
#include <string>

#include <xAODTruth/TruthParticle.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/HelperFunctions.h"
#include "ZprimeNtupler/ParticleNtupler.h"

namespace Zprime
{
  class TruthParticleNtupler : public ParticleNtupler<HelperClasses::TruthInfoSwitch>
  {
  public:
    TruthParticleNtupler(const std::string& name = "truth", const std::string& detailStr="", float units = 1e3);
    virtual ~TruthParticleNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillTruthParticle( const xAOD::TruthParticle* truth );
    virtual StatusCode FillTruthParticle( const xAOD::IParticle* particle );

  private:

    //
    // Vector branches
    //
  
    // All
    std::shared_ptr<std::vector<int>> m_pdgId;
    std::shared_ptr<std::vector<int>> m_status;
    std::shared_ptr<std::vector<int>> m_barcode;

    // type
    std::shared_ptr<std::vector<int>> m_is_higgs;
    std::shared_ptr<std::vector<int>> m_is_bhad;

    // bVtx
    std::shared_ptr<std::vector<float>> m_Bdecay_x;
    std::shared_ptr<std::vector<float>> m_Bdecay_y;
    std::shared_ptr<std::vector<float>> m_Bdecay_z;

    // parents
    std::shared_ptr<std::vector<int>> m_nParents;
    std::shared_ptr<std::vector< std::vector<int> >> m_parent_pdgId;
    std::shared_ptr<std::vector< std::vector<int> >> m_parent_barcode;
    std::shared_ptr<std::vector< std::vector<int> >> m_parent_status;

    // children
    std::shared_ptr<std::vector<int>> m_nChildren;
    std::shared_ptr<std::vector< std::vector<int> >> m_child_pdgId;
    std::shared_ptr<std::vector< std::vector<int> >> m_child_barcode;
    std::shared_ptr<std::vector< std::vector<int> >> m_child_status;

    // dressed
    std::shared_ptr<std::vector<float>> m_pt_dressed;
    std::shared_ptr<std::vector<float>> m_eta_dressed;
    std::shared_ptr<std::vector<float>> m_phi_dressed;
    std::shared_ptr<std::vector<float>> m_e_dressed;
    
    // origin
    std::shared_ptr<std::vector<unsigned int>> m_origin;

  };
}
#endif // ZprimeNtupler_TruthParticleNtupler_H
