#ifndef ZprimeNtupler_METNtupler_H
#define ZprimeNtupler_METNtupler_H

#include <TTree.h>
#include <string>

#include <xAODMissingET/MissingETContainer.h>

#include <AsgTools/MessageCheck.h>

#include "ZprimeNtupler/HelperClasses.h"

namespace Zprime
{

  class METNtupler
  {
  public:
    METNtupler(const std::string& name = "met", const std::string& detailStr="", float units = 1e3);
    ~METNtupler() =default;

    StatusCode createBranches(TTree *tree);
    StatusCode clear();
    StatusCode FillMET( const xAOD::MissingETContainer* met);

  private:
    template <typename T_BR>
    void createBranch(TTree *tree, const std::string& name, T_BR *variable, const std::string& type);

  private:

    std::string m_name;
    HelperClasses::METInfoSwitch  m_infoSwitch;
    float m_units;

  private:

    // met
    float m_metFinalClus;
    float m_metFinalClusPx;
    float m_metFinalClusPy;
    float m_metFinalClusPhi;
    float m_metFinalClusSumEt;
    
    float m_metFinalClusOverSqrtSumEt;
    float m_metFinalClusOverSqrtHt;
    float m_metFinalClusSignificance;
    float m_metFinalClusSigDirectional;
    float m_metFinalClusRho;
    float m_metFinalClusVarL;
    float m_metFinalClusVarT;

    float m_metFinalTrk;
    float m_metFinalTrkPx;
    float m_metFinalTrkPy;
    float m_metFinalTrkPhi;
    float m_metFinalTrkSumEt;
    
    float m_metFinalTrkOverSqrtSumEt;
    float m_metFinalTrkOverSqrtHt;
    float m_metFinalTrkSignificance;
    float m_metFinalTrkSigDirectional;
    float m_metFinalTrkRho;
    float m_metFinalTrkVarL;
    float m_metFinalTrkVarT;

    float m_metEle;       float m_metEleSumEt;      float m_metElePhi;
    float m_metGamma;     float m_metGammaSumEt;    float m_metGammaPhi;
    float m_metTau;       float m_metTauSumEt;      float m_metTauPhi;
    float m_metMuons;     float m_metMuonsSumEt;    float m_metMuonsPhi;
    float m_metJet;       float m_metJetSumEt;      float m_metJetPhi;
    float m_metJetTrk;    float m_metJetTrkSumEt;   float m_metJetTrkPhi;
    float m_metSoftClus;  float m_metSoftClusSumEt; float m_metSoftClusPhi;
    float m_metSoftTrk;   float m_metSoftTrkSumEt;  float m_metSoftTrkPhi;



  };

  template <typename T_BR> void METNtupler::createBranch(TTree *tree, const std::string& name, T_BR *variable, const std::string& type)
    {
      if (!type.empty())
        tree->Branch((m_name + name).c_str(), variable, (m_name + name + "/" + type).c_str());
      else
        tree->Branch((m_name + name).c_str(), variable);
    }

}



#endif // ZprimeNtupler_METNtupler_H
