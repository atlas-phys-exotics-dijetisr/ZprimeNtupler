#ifndef ZprimeNtupler_SortAlgo_H
#define ZprimeNtupler_SortAlgo_H

// algorithm wrapper
#include "ZprimeNtupler/AnaAlgorithm.h"

#include "ZprimeNtupler/HelperFunctions.h"
#include "ZprimeNtupler/HelperDefines.h"

namespace Zprime
{
  class SortAlgo : public AnaAlgorithm
  {
  private:
    //! InContainerName: Input container
    std::string m_inContainerName;
    //! OutContainerName: Output container
    std::string m_outContainerName;
    //! InputAlgo: Vector with input systematics
    std::string m_inputAlgo;
    //! OutputAlgo: Vector where output systematics will be stored
    std::string m_outputAlgo;

    // useulf functions
  private:
    // Container type for sorting, determined from first event
    ObjectType m_containerType;

    /** \brief Sort input container and store the output
     *
     * Check is performed to see if the input container name exist. If it does not,
     * failure is reported only if processing the nominal systematic (blank systName).
     *
     * \param systName Name of systematic to process (look for input InContainerName+systName)
     *
     * \return Status code
     */
    template<typename T> StatusCode sort(const std::string& systName)
    {
      if(!evtStore()->contains<DataVector<T>>(m_inContainerName+systName))
	return (systName.empty())?StatusCode::FAILURE : StatusCode::SUCCESS;

      const DataVector<T> *inParticles;
      ANA_CHECK(evtStore()->retrieve(inParticles, m_inContainerName+systName));

      std::unique_ptr<ConstDataVector<DataVector<T>>> outParticles=std::make_unique<ConstDataVector<DataVector<T>>>(SG::VIEW_ELEMENTS);
      for(const T* particle : *inParticles) outParticles->push_back(particle);

      std::sort(outParticles->begin(), outParticles->end(), HelperFunctions::pt_sort());

      ANA_CHECK(evtStore()->record(std::move(outParticles), m_outContainerName+systName ));

      return StatusCode::SUCCESS;
    }

  public:
    SortAlgo (const std::string& className, ISvcLocator* pSvcLocator);

    virtual StatusCode executeFirstEvent ();
    virtual StatusCode execute ();
  };
}
#endif
