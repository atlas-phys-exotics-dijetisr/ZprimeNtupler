#ifndef ZprimeNtupler_NtuplerContainerDef_H
#define ZprimeNtupler_NtuplerContainerDef_H

#include <string>

#include "ZprimeNtupler/HelperDefines.h"

namespace Zprime
{
  class NtuplerContainerDef
  {
  public:
    NtuplerContainerDef();
    NtuplerContainerDef(ObjectType type, const std::string& containerName, const std::string& branchName, const std::string& detailStr, const std::string& detailStrSyst="");
    NtuplerContainerDef(ObjectType type, const std::string& branchName, const std::string& detailStr);

    ObjectType m_type;
    // TTree configuration
    std::string m_containerName;
    std::string m_branchName;
    std::string m_detailStr;
    std::string m_detailStrSyst;

    std::string m_subjetDetailStr; // Only for FatJetContainer
    std::string m_subjetDetailStrSyst; // Only for FatJetContainer
  private:
  };
}
#endif // ZprimeNtupler_NtuplerContainerDef_H
