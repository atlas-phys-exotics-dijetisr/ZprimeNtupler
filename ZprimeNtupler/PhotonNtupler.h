#ifndef ZprimeNtupler_PhotonNtupler_H
#define ZprimeNtupler_PhotonNtupler_H

#include <TTree.h>

#include <vector>
#include <string>

#include <xAODEgamma/PhotonContainer.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/ParticleNtupler.h"

namespace Zprime
{
  class PhotonNtupler : public ParticleNtupler<HelperClasses::PhotonInfoSwitch>
  {
  public:
    PhotonNtupler(const std::string& name = "ph", const std::string& detailStr="", float units = 1e3, bool mc = false);
    virtual ~PhotonNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillPhoton( const xAOD::Photon* photon );
    virtual StatusCode FillPhoton( const xAOD::IParticle* particle );

  private:

    // isolation
    std::shared_ptr<std::vector<int  >> m_isIsolated_Cone40CaloOnly;
    std::shared_ptr<std::vector<int  >> m_isIsolated_Cone40;
    std::shared_ptr<std::vector<int  >> m_isIsolated_Cone20;
    //std::shared_ptr<std::vector<float>> m_etcone20;
    std::shared_ptr<std::vector<float>> m_ptcone20;
    std::shared_ptr<std::vector<float>> m_ptcone30;
    std::shared_ptr<std::vector<float>> m_ptcone40;
    std::shared_ptr<std::vector<float>> m_ptvarcone20;
    std::shared_ptr<std::vector<float>> m_ptvarcone30;
    std::shared_ptr<std::vector<float>> m_ptvarcone40;
    std::shared_ptr<std::vector<float>> m_topoetcone20;
    std::shared_ptr<std::vector<float>> m_topoetcone30;
    std::shared_ptr<std::vector<float>> m_topoetcone40;

    // PID
    int m_n_IsLoose;
    std::shared_ptr<std::vector<int  >> m_IsLoose;
    int m_n_IsMedium;
    std::shared_ptr<std::vector<int  >> m_IsMedium;
    int m_n_IsTight;
    std::shared_ptr<std::vector<int  >> m_IsTight;

    //Purity
    std::shared_ptr<std::vector<float>> m_radhad1;
    std::shared_ptr<std::vector<float>> m_radhad;
    std::shared_ptr<std::vector<float>> m_e277;
    std::shared_ptr<std::vector<float>> m_reta;
    std::shared_ptr<std::vector<float>> m_rphi;
    std::shared_ptr<std::vector<float>> m_weta2;
    std::shared_ptr<std::vector<float>> m_f1;
    std::shared_ptr<std::vector<float>> m_wtot;
    //std::shared_ptr<std::vector<float>> m_w1;
    std::shared_ptr<std::vector<float>> m_deltae;
    std::shared_ptr<std::vector<float>> m_eratio;

    // effSF
    std::shared_ptr<std::vector<float>> m_LooseEffSF;
    std::shared_ptr<std::vector<float>> m_MediumEffSF;
    std::shared_ptr<std::vector<float>> m_TightEffSF;

    std::shared_ptr<std::vector<float>> m_LooseEffSF_Error;
    std::shared_ptr<std::vector<float>> m_MediumEffSF_Error;
    std::shared_ptr<std::vector<float>> m_TightEffSF_Error;

    // trigger
    std::shared_ptr<std::vector<std::vector<std::string> >> m_trigMatched;
  };
}
#endif // ZprimeNtupler_PhotonNtupler_H
