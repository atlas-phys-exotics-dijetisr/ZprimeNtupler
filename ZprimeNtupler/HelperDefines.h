#ifndef ZPRIME_HELPERDEFINES_H
#define ZPRIME_HELPERDEFINES_H

namespace Zprime
{
  /** Different object types
   */
  enum ObjectType
  {
    Jet,
    FatJet,
    Photon,
    Muon,
    Electron,
    TruthParticle
  };

  /** Different generator chains
   */
  enum class Generator
    {
      Sherpa, /**< Sherpa (any version) */
      PowhegPythia, /**< Powheg ME + Pythia shower */
      Pythia8EvtGen, /**< Pythia8 ME and shower + EvtGen b-decay */
      PowhegPythia8EvtGen,  /**< Powheg ME + Pythia8 shower + EvtGen b-decay */
      PowhegHerwigEvtGen,  /**< Powheg ME + Herwig shower + EvtGen b-decay */
      PowhegHerwig7EvtGen,  /**< Powheg ME + Herwig7 shower + EvtGen b-decay */
      aMcAtNloPythia8EvtGen, /**< aMcAtNlo + Pythia8 shower + EvtGen b-decay */
      Herwigpp, /**< Herwig++ for everything */
      Unknown /**< Not determined */
    };

  /** Different showers
   */
  enum class Shower
    {
      Sherpa, /**< Sherpa (any version) */
      Pythia, /**< Pythia */
      Pythia8EvtGen,  /**< Pythia8 shower + EvtGen b-decay */
      HerwigEvtGen,   /**< Herwig6 shower + EvtGen b-decay */
      Herwigpp,       /**< Herwig++ shower */
      Herwig7EvtGen,  /**< Herwig7 shower + EvtGen b-decay */
      Unknown /**< Not determined */
    };
  
}

#endif // ZPRIME_HELPERDEFINES_H
