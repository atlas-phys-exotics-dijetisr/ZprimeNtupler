#ifndef ZprimeNtupler_L1JetNtupler_H
#define ZprimeNtupler_L1JetNtupler_H

#include <TTree.h>

#include <vector>
#include <string>

#include <xAODTrigger/JetRoIContainer.h>

#include <AsgTools/MessageCheck.h>

#include "ZprimeNtupler/HelperClasses.h"

namespace Zprime
{

  class L1JetNtupler
  {
  public:
    L1JetNtupler(const std::string& name = "l1jet", const std::string& detailStr="", float units = 1e3, bool mc = false);
    virtual ~L1JetNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillL1Jet( const xAOD::JetRoI* jet );

  private:
    std::string m_name;
    float m_units;
    bool m_mc;

    //
    // Branches
    int m_n;
    
    // Vector branches
    std::shared_ptr<std::vector<float>> m_l1Jet_et8x8;
    std::shared_ptr<std::vector<float>> m_l1Jet_eta;
    std::shared_ptr<std::vector<float>> m_l1Jet_phi;
  };
}

#endif // ZprimeNtupler_L1JetNtupler_H
