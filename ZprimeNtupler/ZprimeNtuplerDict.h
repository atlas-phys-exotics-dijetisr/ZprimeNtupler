#ifndef ZPRIMENTUPLER_ZPRIMENTUPLERDICT_H
#define ZPRIMENTUPLER_ZPRIMENTUPLERDICT_H

// This file includes all the header files that you need to create
// dictionaries for.

#include <ZprimeNtupler/BasicEventSelection.h>

#include <ZprimeNtupler/MuonCalibrator.h>
#include <ZprimeNtupler/FFJetSmearingAlg.h>

#include <ZprimeNtupler/HbbTagAlgorithm.h>

#include <ZprimeNtupler/SortAlgo.h>
#include <ZprimeNtupler/ZprimeNtupler.h>

#endif
