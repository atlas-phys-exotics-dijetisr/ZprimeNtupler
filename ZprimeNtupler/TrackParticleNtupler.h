#ifndef ZprimeNtupler_TrackParticleNtupler_H
#define ZprimeNtupler_TrackParticleNtupler_H

#include <TTree.h>
#include <string>

#include <xAODTracking/TrackParticle.h>

#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/ParticleNtupler.h"

namespace Zprime
{

  class TrackParticleNtupler : public ParticleNtupler<HelperClasses::TrackInfoSwitch>
  {
  public:
    TrackParticleNtupler(const std::string& name = "track", const std::string& detailStr="", float units = 1e3);
    virtual ~TrackParticleNtupler() =default;

    virtual StatusCode createBranches(TTree *tree);
    virtual StatusCode clear();
    virtual StatusCode FillTrackParticle( const xAOD::TrackParticle* track );
    virtual StatusCode FillTrackParticle( const xAOD::IParticle* particle );

  private:
    // 
    // Vector branches
    //

    // fitpars
    std::shared_ptr<std::vector<float             >> m_chiSquared;
    std::shared_ptr<std::vector<float             >> m_d0;
    std::shared_ptr<std::vector<std::vector<float>>> m_definingParametersCovMatrix;
    std::shared_ptr<std::vector<unsigned char     >> m_expectInnermostPixelLayerHit;
    std::shared_ptr<std::vector<unsigned char     >> m_expectNextToInnermostPixelLayerHit;
    std::shared_ptr<std::vector<float             >> m_numberDoF;

    // numbers
    std::shared_ptr<std::vector<unsigned char>> m_numberOfInnermostPixelLayerHits;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfNextToInnermostPixelLayerHits;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfPhiHoleLayers;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfPhiLayers;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfPixelDeadSensors;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfPixelHits;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfPixelHoles;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfPixelSharedHits;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfPrecisionHoleLayers;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfPrecisionLayers;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfSCTDeadSensors;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfSCTHits;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfSCTHoles;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfSCTSharedHits;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfTRTHits;
    std::shared_ptr<std::vector<unsigned char>> m_numberOfTRTOutliers;
    std::shared_ptr<std::vector<float>> m_phi;
    std::shared_ptr<std::vector<float>> m_qOverP;
    std::shared_ptr<std::vector<float>> m_theta;
    std::shared_ptr<std::vector<Int_t>> m_vertexLink;
    std::shared_ptr<std::vector<UInt_t>> m_vertexLink_persIndex;
    std::shared_ptr<std::vector<UInt_t>> m_vertexLink_persKey;
    std::shared_ptr<std::vector<float>> m_vz;
    std::shared_ptr<std::vector<float>> m_z0;
  };
}

#endif // ZprimeNtupler_TrackParticleNtupler_H
