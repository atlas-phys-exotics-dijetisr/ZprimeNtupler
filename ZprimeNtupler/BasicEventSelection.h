/**
 * @file   BasicEventSelection.h
 * @author Gabriel Facini <gabriel.facini@cern.ch>
 * @author Marco Milesi <marco.milesi@cern.ch>
 * @author Jeff Dandoy <jeff.dandoy@cern.ch>
 * @author John Alison <john.alison@cern.ch>
 * @brief  Algorithm performing general basic cuts for an analysis (GRL, Event Cleaning, Min nr. Tracks for PV candidate).
 *
 */

#ifndef ZprimeNtupler_BasicEventSelection_H
#define ZprimeNtupler_BasicEventSelection_H

// ROOT include(s):
#include "TH1D.h"

// algorithm wrapper
#include "ZprimeNtupler/AnaAlgorithm.h"

// external tools include(s):
#include <AsgTools/AnaToolHandle.h>
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgAnalysisInterfaces/IPileupReweightingTool.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <PATInterfaces/IWeightTool.h>

namespace Zprime
{
  /**
   * This algorithm performs the very basic event selection. This should be the first algo in the algo chain. It can create weighted and unweighted cutflow objects to be picked up downstream by other xAH algos, and your own. The selection applied in data only is:
   *
   *  - GRL (can be turned off)
   *  - LAr Error
   *  - Tile Error
   *  - Core Flag
   *
   * .. note:: For MC only, the pileup reweight can also be applied.
   *
   * In both data and simulation (MC), the following cuts are applied
   *
   * - the highest sum :math:`p_{T}^2` primary vertex has 2 or more tracks (see :cpp:member:`~BasicEventSelection::m_applyPrimaryVertexCut`)
   * - trigger requirements (see :cpp:member:`~BasicEventSelection::m_applyTriggerCut`)
   *
   * For derivations, the metadata can be accessed and added to the cutflow for normalization. The parameters to control the trigger are described in this header file. If one wants to write out some of the trigger information into a tree using :cpp:class:`~HelpTreeBase`, flags must be set here.
   *
   */
  class BasicEventSelection : public AnaAlgorithm
  {
  private:
    //! Protection when running on truth xAOD
    bool m_truthLevelOnly = false;

    //! brief Apply GRL selection
    bool m_applyGRLCut = false;

    //
    //PU Reweighting
    bool m_doPRW = false;
    bool m_doPRWSys = false;

    //! Automatically configure PRW using config files from SUSYTools
    bool m_autoconfigPRW = false;

    //! PRW config files directories (for autoconfig)
    std::vector<std::string> m_prwSearchPaths = {"/dev/PileupReweighting/share"};
    //! PRW config files
    std::vector<std::string> m_prwConfigFiles = {};
    //! Lumi-calc files
    std::vector<std::string> m_lumiCalcFiles = {};
    //! actualMu configuration file for the MC16a campaign (2015/2016)
    std::string m_prwActualMu2016File = "";
    //! actualMu configuration file for the MC16d campaign (2017)
    std::string m_prwActualMu2017File = "";
    //! actualMu configuration file for the MC16e campaign (2018)
    std::string m_prwActualMu2018File = "";
    //! mc16(acd) to bypass the automatic campaign determination from run number (for autoconfig)
    std::vector<std::string> m_mcCampaign = {};
    //! DataScaleFactor for PRW
    double m_DataScaleFactor = 1/1.03;

    /// @brief The minimum threshold for <tt>EventInfo::actualInteractionsPerCrossing()</tt>
    int m_actualMuMin = -1; // Default to off
    /// @brief The maximum threshold for <tt>EventInfo::actualInteractionsPerCrossing()</tt>
    int m_actualMuMax = -1; // Default to off

    /// @brief Calculate distance to nearest empty and unpaired BCIDs
    bool m_calcBCIDInfo = false;

    //
    // Primary Vertex

    //! Enable to apply a primary vertex cut
    bool m_applyPrimaryVertexCut = false;

    //! @brief Minimum number of tracks from **the** primary vertex (Harmonized Cut)
    int m_PVNTrack = 2;

    // Event Cleaning
    bool m_applyEventCleaningCut = false;
    bool m_applyCoreFlagsCut = false;

    // Jet Cleaning (see also https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HowToCleanJets2017)
    bool m_applyJetCleaningEventFlag = false;
    /// should only ever be used in 2015 and 2016 data, for analyses which may be of interest for analyses where fake MET can be an issue
    bool m_applyIsBadBatmanFlag = false;

    // Print branch List
    bool m_printBranchList = false;

    //
    // Trigger

    //! RegEx expression to choose triggers to consider to be cut on with :cpp:member:`~BasicEventSelection::m_applyTriggerCut`
    std::string m_triggerSelection = "";

    //! Decisions of triggers which are saved but not cut on
    std::string m_extraTriggerSelection = "";

    //! Skip events in which the trigger string :cpp:member:`~BasicEventSelection::m_triggerSelection` does not fire
    bool m_applyTriggerCut = false;

    //! Save trigger fired matching :cpp:member:`~BasicEventSelection::m_triggerSelection`
    bool m_storeTrigDecisions = false;

    //! Save if any L1 trigger fired, e.g. ``"L1_.*"``
    bool m_storePassL1 = false;

    //! Save if any HLT trigger fired, e.g. ``"HLT_.*"``
    bool m_storePassHLT = false;

    //! Save master, L1, and HLT key
    bool m_storeTrigKeys = false;

    //
    // Metadata

    //! The name of the derivation (use this as an override)
    std::string m_derivationName = "";

    //! Retrieve and save information on DAOD selection
    bool m_useMetaData = true;

    //! Store TTree with event/run numbers for duplicate checkings
    bool m_checkDuplicates = false;

  private:

    // trigger unprescale chains
    std::vector<std::string> m_triggerUnprescaleList; //!
    // decisions of triggers which are saved but not cut on, converted into a list
    std::vector<std::string> m_triggerSelectionList;
    std::vector<std::string> m_extraTriggerSelectionList;

    // tools
    ToolHandle<IGoodRunsListSelectionTool> m_grlTool     {"GoodRunsListSelectionTool"              , this};
    ToolHandle<TrigConf::ITrigConfigTool > m_trigConfTool;
    ToolHandle<Trig::TrigDecisionTool    > m_trigDecTool ;

    asg::AnaToolHandle<CP::IPileupReweightingTool> m_prwTool{"CP::PileupReweightingTool/Pileup"    , this};

    uint32_t m_eventCounter =0;

    StatusCode autoconfigurePileupRWTool();
    
    //
    // Meta histograms
    TH1* m_histSumW = nullptr;     //!
    TH1* m_histEventCount = nullptr;          //!

    std::string m_mcCampaignMD; //!

    // cutflow
    TH1* m_cutflowHist = nullptr;      //!
    TH1* m_cutflowHistW = nullptr;     //!
    int m_cutflow_all;        //!
    int m_cutflow_init;       //!
    int m_cutflow_grl;        //!
    int m_cutflow_lar;        //!
    int m_cutflow_tile;       //!
    int m_cutflow_SCT;        //!
    int m_cutflow_core;       //!
    int m_cutflow_jetcleaning; //!
    int m_cutflow_isbadbatman; //!
    int m_cutflow_npv;        //!
    int m_cutflow_trigger;    //!

    /** TTree for duplicates bookeeping */
    TTree*   m_duplicatesTree = nullptr;  //!
    int      m_duplRunNumber;
    long int m_duplEventNumber;

  public:
    // this is a standard constructor
    BasicEventSelection (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize ();
    virtual StatusCode fileExecute ();
    virtual StatusCode executeFirstEvent ();
    virtual StatusCode execute ();
    virtual StatusCode finalize ();
  };
}

#endif
