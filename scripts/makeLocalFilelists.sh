#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} datasetpath"
    exit 1
fi

datasetpath=$(realpath ${1})

for dataset in $(find ${datasetpath} -maxdepth 1 -type d)
do
    ds=$(basename ${dataset})
    echo ${ds}
    find ${dataset} -type f -name '*.root*' > filelists/${ds}.txt
done

