#!/usr/bin/env python
# Grab future to use print as a function
from __future__ import print_function

# Multiprocessing, to deal with a pool of work
from multiprocessing import Pool,current_process
# Argparse (duh), time for logging, subprocess for execution, os for environment,errno for exceptions
import argparse,time,subprocess,os,errno,sys
# fcntl to deal with file locks
from fcntl import flock,LOCK_EX,LOCK_NB,LOCK_UN
# hashlib to generate lock file paths
from hashlib import md5
# For timeline output
import json
# Needed for cleanup
import signal

# This assumes $SCRATCH points to a disk area that supports file locking
# Here is a little helper for many other file systems
if   'SCRATCH' in os.environ: lock_area = os.environ['SCRATCH']+'/' # Cori!
elif 'TMP'     in os.environ: lock_area = os.environ['TMP']+'/' 
elif 'TMPDIR'  in os.environ: lock_area = os.environ['TMPDIR']+'/'
else: lock_area = None # Really didn't define anything...

#
# Keeps track of available, completed and unprocessed tasks.
#
# A list of tasks is defined using a file containing a task per line,
# with supporting status files defined using a suffix. The task ID is
# defined as the line number (starting at 0) inside the main task file.
#
# Assuming that the defining tasklist is called `tasklist`, then
# the supporting status files are:
# - `tasklist_toprocess`: List of tasks that still need to be processed. The
#  format is "taskID task".
# - `tasklist_finished`: List of tasks that succesfully finished (return code 0). The
#  format is "taskID task".
# - `tasklist_failed`: List of tasks that finished unsuccesfully (return code not 0). The
#  format is "taskID task".
# - `tasklist_lock`: Used to get RW lock on all tasklist files. If the filesystem does not
#  support file locking, an alternate path can be used via a global `lock_area` variable.
# In this case, the `tasklist_lock` path becomes `lock_area/md5(absolute path of tasklist)_lock`.
#
# The list and corresponding operations are defined in a process-safe manner using
# the supporting files to synchronize the state. This means that multiple TaskLists
# can be created for a single tasklist (even on multiple machines with a shared filesystem).
class TaskList:
    def __init__(self,path):
        # Lock file handle on the task list
        self.lockfh=None

        # Paths to all necessary files
        self.path=path
        self.path_toprocess=path+"_toprocess"
        self.path_finished =path+"_finished"
        self.path_failed   =path+"_failed"

        if lock_area==None:
            self.path_lock =path+"_lock"
        else:
            abspath=os.path.abspath(path)
            pathhash=md5(abspath.encode()).hexdigest()        
            self.path_lock =lock_area+'/'+pathhash+'_lock'

        # Load complete list of tasks
        in_tasks = open(path,'r')
        self.tasks = [line.strip() for line in in_tasks.readlines() if not line.startswith('#') and line.strip()!='']
        in_tasks.close()

    # Get lock on the tasklist and supporting files
    def lock(self):
        if self.lockfh!=None and not self.lockfh.closed:
            return # I already have lock
        self.lockfh = open( self.path_lock,'w+' )

        for i in range(10000):
            try:
                flock( self.lockfh , LOCK_EX | LOCK_NB )
                return
            except IOError: # lock not acquired
                time.sleep(0.001)

        self.lockfh.close()
        self.lockfh=None
        print(time.asctime(),'Lock error...')
        raise RuntimeError("Unable to lock after 10 seconds. Something is wrong!")

    # Get rid of lock on the tasklist and supporting files
    def unlock(self):
        if self.lockfh==None or self.lockfh.closed:
            return # No more lock
        self.lockfh.close()
        self.lockfh=None

    # Pop a task from the toprocess list 
    # \return task id for the next task
    def pop_task(self):
        nexttask=None

        # Get list of unfinished tasks
        self.lock()        
        tasks_toprocess=self.read_toprocess()
        if len(tasks_toprocess)>0:
            nexttask=tasks_toprocess.pop(0)
            self.write_toprocess(tasks_toprocess)
        self.unlock()

        return nexttask

    # Record a task that is not processed
    # \param taskid Task ID of toprocess task
    def record_toprocess_task(self, taskid):
        self.lock()

        # Unfinished task list
        tasks=self.read_toprocess()
        tasks.append(taskid)
        # Save
        self.write_toprocess(tasks)
        
        self.unlock()
    
    # Record a completed task
    # \param taskid Task ID of completed task
    # \param returncode Return code from the task
    def record_done_task(self, taskid, returncode):
        self.lock()

        # Deterine output path
        path = self.path_finished if returncode==0 else self.path_failed
        # Open the task list
        finished = open( path,'a+' )
        # Write the command on its own line
        finished.write('{} {}\n'.format(taskid, self.tasks[taskid]))
        # Close finished task list
        finished.close()

        self.unlock()

    # Get list of toprocess tasks
    # Not thread safe!
    # \return list of toprocess task id's
    def read_toprocess(self):
        if os.path.exists(self.path_toprocess):
            toprocess=open(self.path_toprocess)
            tasks=[int(line.split(' ')[0]) for line in toprocess.readlines()]
            return tasks
        else: # All tasks still unprocessed
            return list(range(len(self.tasks)))

    # Write list of toprocess tasks
    # Not thread safe!    
    # \param tasks List of toprocess task IDs
    def write_toprocess(self,tasks):
        tasks=sorted(set(tasks)) #sort before saving, make unique
        toprocess=open(self.path_toprocess, 'w')
        for task in tasks:
            toprocess.write('{} {}\n'.format(task,self.tasks[task]))
        toprocess.close()        

# Clean-up handler that terminates the worker pool when certain signals are received
def handler(signum, frame):
    print(time.asctime(),'User interrupt during excecution')
    # Terminate the worker pool
    global pool
    pool.terminate()
    pool.join()
    sys.exit(1)
        
# The executor itself. This is the main function that's doing work.
# Takes the list of tasks, pops the next one off the top
# \return tuple with task ID, start time, end time
def executor(my_input):
    # Signal handlers
    # The big ones are handled on top
    signal.signal(signal.SIGUSR1, signal.SIG_IGN)
    signal.signal(signal.SIGINT , signal.SIG_IGN)
    signal.signal(signal.SIGALRM, signal.SIG_IGN)
    # cleanup handler on termination
    def term_handler(signum, frame):
        print(time.asctime(),'Kill executor processing task',taskid)
        # Kill the process
        execute.kill()
        # Update toprocess list
        tasks.record_toprocess_task(taskid)

        # Done
        sys.exit(1)
    signal.signal(signal.SIGTERM, term_handler)

    # Decode the input
    tasklist = my_input[0]
    logdir   = my_input[1]
    # Prepare the task list
    tasks = TaskList(tasklist)
    # Get the command we want to run
    taskid  = tasks.pop_task()
    if taskid==None: # Run out of tasks
        return

    command = tasks.tasks[taskid]
    # Re-format as appropriate
    print(time.asctime(),'Executing task',taskid,':',command)
    # Set up the logging per process
    logfile = open(logdir+'/log_'+str(taskid),'w')
    # Ensure that we have a handle on which process is being run in the queue
    my_env = os.environ
    my_env['PROC_NUMBER'] = str(taskid)
    # Start the process and wait for it to finish
    start_time=time.time()
    execute = subprocess.Popen(command.split(),stdin=None,stdout=logfile,stderr=logfile,env=my_env)
    execute.communicate()    
    end_time  =time.time()
    print(time.asctime(),'Completed task',taskid,':',command)
    # Close up the log file
    logfile.close()
    # Register the task as complete - we do this last so that if there's a timeout
    #  the worst case is that work is finished but we didn't report back in time
    tasks.record_done_task(taskid, execute.returncode)
    return (current_process().pid,taskid,start_time,end_time)

# This is the actual main -- the meat of the farmer itself.
if __name__ == '__main__':
    # Get command line arguments
    parser = argparse.ArgumentParser(description='Simple task queue')
    parser.add_argument('tasklist', metavar='List', type=str, help='The task list')
    parser.add_argument('--proc', dest='proc', metavar='Processes', type=int, nargs='?', default=8, help='Number of processes')
    parser.add_argument('--timeout', dest='timeout', type=int, required=False, default=None, help='End execution in timeout seconds')
    parser.add_argument('--logDir', dest='logdir', type=str, required=False, default='logs/', help='Directory in which to store logs')
    parser.add_argument('--verbose', type=bool, dest='verb', required=False, default=False)
    args = parser.parse_args()

    # Tell the user what we got
    print(time.asctime(),'Working on task list',args.tasklist)
    # Grab the work
    tasks=TaskList(args.tasklist)    
    n_tasks = len(tasks.tasks)
    print(time.asctime(),'Found',n_tasks,'total tasks')

    # Prepare log files
    print(time.asctime(),'Will store logs in',args.logdir)
    if not os.access(args.logdir, os.R_OK):
        # Create it
        try:
            os.mkdir( args.logdir )
        except OSError as e:
            # If the error wasn't EEXIST we should raise it.
            if e.errno != errno.EEXIST: raise
        # Multiple nodes may be racing to make this directory
        # Don't test anything further in case that's problematic

    signal.signal(signal.SIGUSR1, handler) # cori signal telling us job is about to complete
        
    # Set up the pool
    pool = Pool(processes=args.proc)

    # Register cleanup signals
    signal.signal(signal.SIGUSR1, handler) # cori signal telling us job is about to complete
    signal.signal(signal.SIGINT , handler) # Keyboard interrupt
    if args.timeout!=None:
        signal.signal(signal.SIGALRM, handler) # alarm from timeout
        signal.alarm(args.timeout)
    
    # This is a bit wasteful if we run on many nodes. This sets up a single
    #  task for all the lines in the task file. If the file is being processed
    #  by many nodes, then it could be that we reach the end of the file much
    #  earlier. In such cases, this simply does a bunch of null-op work that should
    #  be fairly quick (just set up / tear down / locking overheads) and then
    #  returns, so we hope this won't waste too much time
    # Set up the tasks.
    for taskResult in pool.imap_unordered(executor, zip(n_tasks*[args.tasklist],n_tasks*[args.logdir])):
        if taskResult==None:
            continue # Nothing was done

        threadid,taskid,start_time,end_time=taskResult
        tasks.lock()            
        # Load existing timeline
        timeline=[]
        if os.path.exists(tasks.path+'_timeline.json'):
            with open(tasks.path+'_timeline.json') as fh_timeline:
                timeline=json.load(fh_timeline)
        # Append task information
        jobtime={
            "cat": "task",
            "name": tasks.tasks[taskid],
            "pid": os.getpid(),
            "tid": threadid,
            "ts": start_time*1e6,
            "dur": (end_time-start_time)*1e6,
            "ph": "X",
            "args": {}
        }
        timeline.append(jobtime)

        # Store the timeline
        with open(tasks.path+'_timeline.json','w') as fh_timeline:
            json.dump(timeline, fh_timeline)
        tasks.unlock()

    print(time.asctime(),'Finished working')

    # cleanup of workers    
    pool.close()
    pool.join()
