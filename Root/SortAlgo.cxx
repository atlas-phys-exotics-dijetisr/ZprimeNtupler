#include "ZprimeNtupler/SortAlgo.h"

#include <xAODJet/JetContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTruth/TruthParticleContainer.h>

using namespace Zprime;

SortAlgo :: SortAlgo (const std::string& name, ISvcLocator* pSvcLocator)
  : AnaAlgorithm(name, pSvcLocator)
{
  declareProperty("InContainerName" , m_inContainerName );
  declareProperty("OutContainerName", m_outContainerName);
  declareProperty("InputAlgo"       , m_inputAlgo       );
  declareProperty("OutputAlgo"      , m_outputAlgo      );
}

StatusCode SortAlgo::executeFirstEvent()
{
  ANA_CHECK(AnaAlgorithm::executeFirstEvent());

  if     (evtStore()->contains<xAOD::JetContainer>(m_inContainerName))
    {
      m_containerType=ObjectType::Jet; // Jet and FatJet are the same in this case
    }
  else if(evtStore()->contains<xAOD::PhotonContainer>(m_inContainerName))
    {
      m_containerType=ObjectType::Photon;
    }    
  else if(evtStore()->contains<xAOD::MuonContainer>(m_inContainerName))
    {
      m_containerType=ObjectType::Muon;
    }
    
  else if(evtStore()->contains<xAOD::ElectronContainer>(m_inContainerName))
    {
      m_containerType=ObjectType::Electron;
    }
    
  else if(evtStore()->contains<xAOD::TruthParticleContainer>(m_inContainerName))
    {
      m_containerType=ObjectType::TruthParticle;
    }
  else
    {
      ANA_MSG_FATAL("SortAlgo called with unsupported container " << m_inContainerName << ".");
      return StatusCode::FAILURE;
    }


  return StatusCode::SUCCESS;
}

StatusCode SortAlgo::execute ()
{
  ANA_CHECK(AnaAlgorithm::execute());
  
  // get vector of strings giving the names of variations
  std::vector<std::string>* systNames = nullptr;  
  if(!m_inputAlgo.empty())
    {
      ANA_CHECK(evtStore()->retrieve(systNames, m_inputAlgo));
    }
  else
    {
      systNames=new std::vector<std::string>({""});
    }

  // sort all of the necessary containers
  std::unique_ptr< std::vector<std::string> > outSystNames = std::make_unique< std::vector< std::string > >();
  for(const std::string& systName : *systNames)
    {
      outSystNames->push_back(systName);

      switch(m_containerType)
	{
	case ObjectType::Jet:
	case ObjectType::FatJet:
	  ANA_CHECK(sort<xAOD::Jet          >(systName));
	  break;
	case ObjectType::Photon:
	  ANA_CHECK(sort<xAOD::Photon       >(systName));
	  break;
	case ObjectType::Electron:
	  ANA_CHECK(sort<xAOD::Electron     >(systName));
	  break;
	case ObjectType::Muon:
	  ANA_CHECK(sort<xAOD::Muon         >(systName));
	  break;
	case ObjectType::TruthParticle:
	  ANA_CHECK(sort<xAOD::TruthParticle>(systName));
	  break;
	}
    }

  if(!m_outputAlgo.empty())
    { ANA_CHECK( evtStore()->record( std::move(outSystNames), m_outputAlgo)); }

  if(m_inputAlgo.empty())
    delete systNames;

  return StatusCode::SUCCESS;
}
