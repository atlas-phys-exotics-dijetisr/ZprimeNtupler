#include "ZprimeNtupler/AnaAlgorithm.h"

// RCU include for throwing an exception+message
#include <RootCoreUtils/ThrowMsg.h>

#include <EventLoop/Worker.h>

#include <xAODEventInfo/EventInfo.h>

#include <xAODMetaData/FileMetaData.h>
#include <xAODTruth/TruthMetaDataContainer.h>

using namespace Zprime;

AnaAlgorithm::AnaAlgorithm(const std::string& name, ISvcLocator* pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator)
{
  declareProperty("EventInfoContainerName", m_eventInfoContainerName = "EventInfo"      );
  declareProperty("VertexContainerName"   , m_vertexContainerName    = "PrimaryVertices");
  declareProperty("isMC"                  , m_isMC                   = -1               );
  declareProperty("isFastSim"             , m_isFastSim              = -1               );
}

bool AnaAlgorithm::isMC()
{    
  // If decision is established, return the decision
  if(m_isMC == 0 || m_isMC == 1) return m_isMC;
  
  const xAOD::EventInfo* eventInfo(nullptr);
  // couldn't retrieve it
  if(!evtStore()->retrieve(eventInfo, m_eventInfoContainerName))
    RCU_THROW_MSG( "Could not retrieve eventInfo container (" + m_eventInfoContainerName+") for isMC() check.");

  static SG::AuxElement::ConstAccessor<uint32_t> eventType("eventTypeBitmask");
  if(!eventType.isAvailable(*eventInfo))
    RCU_THROW_MSG( "eventType is not available for isMC() check.");

  // reached here, return True or False since we have all we need
  m_isMC = (static_cast<uint32_t>(eventType(*eventInfo)) & xAOD::EventInfo::IS_SIMULATION);
  return m_isMC;
}

bool AnaAlgorithm::isFastSim()
{

  // If decision is established, return the decision
  if(m_isFastSim == 0 || m_isFastSim == 1) return m_isFastSim;
    
  std::string SimulationFlavour; 
  const xAOD::FileMetaData* fmd = nullptr;
  ANA_CHECK( wk()->xaodEvent()->retrieveMetaInput(fmd, "FileMetaData") );
  fmd->value(xAOD::FileMetaData::simFlavour, SimulationFlavour);
  
  if( SimulationFlavour == "AtlfastII" )
    m_isFastSim = 1;
  else
    m_isFastSim = 0;
  
  return m_isFastSim;
}

uint32_t AnaAlgorithm::mcChannelNumber() const
{ return m_mcChannelNumber; }

Generator AnaAlgorithm::generator() const
{ return m_generator; }

Shower AnaAlgorithm::shower() const
{ return m_shower; }

int AnaAlgorithm::init_cutflow(const std::string& cutName)
{ return 0; }

StatusCode AnaAlgorithm::cutflow(int cut)
{ return StatusCode::SUCCESS; }

StatusCode AnaAlgorithm::initialize()
{
  ANA_CHECK (requestFileExecute ());
  return StatusCode::SUCCESS;
}

StatusCode AnaAlgorithm::fileExecute()
{
  //
  // Get information about the sample being processed
  if (inputMetaStore()->contains<xAOD::TruthMetaDataContainer>("TruthMetaData"))
      {
	const xAOD::TruthMetaDataContainer *tmd{};
        ATH_CHECK (inputMetaStore()->retrieve(tmd, "TruthMetaData"));

        // If we only have one metadata item take MC channel from there if needed
	if(tmd->size()!=1)
	  {
	    ANA_MSG_FATAL("There is " << tmd->size() << "x TruthMetaData. Not sure what to do...");
	    return StatusCode::FAILURE;
	  }

	// DSID
	m_mcChannelNumber = tmd->at(0)->mcChannelNumber();

	// Generator and shower
	const std::string& generator=tmd->at(0)->generators();
	if(generator=="Sherpa")
	  {
	    m_generator=Generator::Sherpa;
	    m_shower   =Shower::Sherpa;
	  }
	else if(generator=="Herwigpp")
	  {
	    m_generator=Generator::Herwigpp;
	    m_shower   =Shower::Herwigpp;
	  }
	else if(generator=="Powheg+Py")
	  {
	    m_generator=Generator::PowhegPythia;
	    m_shower   =Shower::Pythia;
	  }
	else if(generator=="Powheg+Pythia8+EvtGen")
	  {
	    m_generator=Generator::PowhegPythia8EvtGen;
	    m_shower   =Shower::Pythia8EvtGen;
	  }
	else if(generator=="Powheg+Herwig+EvtGen")
	  {
	    m_generator=Generator::PowhegHerwigEvtGen;
	    m_shower   =Shower::HerwigEvtGen;
	  }
	else if(generator=="Powheg+Herwig7+EvtGen")
	  {
	    m_generator=Generator::PowhegHerwig7EvtGen;
	    m_shower   =Shower::Herwig7EvtGen;
	  }
	else if(generator=="aMcAtNlo+Pythia8+EvtGen")
	  {
	    m_generator=Generator::aMcAtNloPythia8EvtGen;
	    m_shower   =Shower::Pythia8EvtGen;
	  }
	else if(generator=="MadGraph+Pythia8+EvtGen")
	  {
	    m_generator=Generator::aMcAtNloPythia8EvtGen;
	    m_shower   =Shower::Pythia8EvtGen;
	  }
	else if(generator=="Pythia8+EvtGen")
	  {
	    m_generator=Generator::Pythia8EvtGen;
	    m_shower   =Shower::Pythia8EvtGen;
	  }
	else
	  {
	    ANA_MSG_FATAL("Unknown generator: " << generator);
	    return StatusCode::FAILURE;
	  }
      }

  return StatusCode::SUCCESS;
}

StatusCode AnaAlgorithm::executeFirstEvent()
{
  m_firstExecute = false;
  return StatusCode::SUCCESS;
}

StatusCode AnaAlgorithm::execute()
{
  if(m_firstExecute)
    ANA_CHECK(executeFirstEvent());

  return StatusCode::SUCCESS;
}
