#include "ZprimeNtupler/HbbTagAlgorithm.h"
#include "ZprimeNtupler/HelperFunctions.h"

#include <regex>

#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <AthContainers/ConstDataVector.h>

#include <SampleHandler/MetaFields.h>

#include <xAODTracking/VertexContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODTrigger/JetRoIContainer.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODEventInfo/EventInfo.h>
#include <PathResolver/PathResolver.h>

#include <BoostedJetTaggers/JSSWTopTaggerANN.h>
#include <PMGTools/PMGCrossSectionTool.h>

#include "ZprimeNtupler/HelperFunctions.h"

using namespace Zprime;

HbbTagAlgorithm :: HbbTagAlgorithm (const std::string& name, ISvcLocator* pSvcLocator)
  : AnaAlgorithm(name, pSvcLocator)
{
  // Configurable properties
  declareProperty("InputAlgo"          , m_inputAlgo         );
  declareProperty("InContainerName"    , m_inContainerName   );

  //
  // Tools
  declareProperty ("HbbTagTool", m_HbbTagTool, "HbbTag tool handle");
}

StatusCode HbbTagAlgorithm::execute()
{
  ANA_CHECK(AnaAlgorithm::execute());

  // get vector of strings giving the names
  std::vector<std::string>* systNames;
  if(!m_inputAlgo.empty())
    { ANA_CHECK(evtStore()->retrieve(systNames, m_inputAlgo)); }
  else
    { systNames=new std::vector<std::string>({""}); }

  for(const std::string& systName : *systNames)
    {
      std::string containerName = m_inContainerName+systName;
      const xAOD::JetContainer* fatjets = nullptr;
      if(evtStore()->contains<xAOD::JetContainer>(containerName))
	{
	  ANA_CHECK(evtStore()->retrieve(fatjets, containerName));
	  m_HbbTagTool->decorate(*fatjets);
	}
    }

  if(m_inputAlgo.empty())
    { delete systNames; }

  return StatusCode::SUCCESS;
}
