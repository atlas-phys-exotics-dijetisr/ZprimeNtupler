#include "ZprimeNtupler/VertexNtupler.h"

#include "ZprimeNtupler/HelperFunctions.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

VertexNtupler::VertexNtupler(const std::string& name, const std::string& detailStr)
  : m_name(name), m_detailStr(detailStr)
{
  if(m_detailStr!="")
    {
      m_vertex_x  = std::make_shared<std::vector<float>>();
      m_vertex_y  = std::make_shared<std::vector<float>>();
      m_vertex_z  = std::make_shared<std::vector<float>>();
    }
}
std::string VertexNtupler::branchName(const std::string& varName)
{
  return m_name + "_" + varName;
}

StatusCode VertexNtupler::createBranches(TTree *tree)
{
  if(!m_detailStr.empty())
    {
      createBranch<float>(tree,"x",m_vertex_x);
      createBranch<float>(tree,"y",m_vertex_y);
      createBranch<float>(tree,"z",m_vertex_z);
    }
  return StatusCode::SUCCESS;
}

StatusCode VertexNtupler::clear()
{
  if(!m_detailStr.empty())
    {
      m_vertex_x->clear();
      m_vertex_y->clear();
      m_vertex_z->clear();
    }

  return StatusCode::SUCCESS;
}

StatusCode VertexNtupler::FillVertices( const xAOD::VertexContainer* vertices)
{
  if(m_detailStr == "primary")
    { // hard-scatter vertex only
      uint32_t pvLocation = HelperFunctions::getPrimaryVertexLocation( vertices );
      m_vertex_x->push_back( vertices->at(pvLocation)->x() );
      m_vertex_y->push_back( vertices->at(pvLocation)->y() );
      m_vertex_z->push_back( vertices->at(pvLocation)->z() );
    }
  else if (m_detailStr == "all")
    {
      for( const xAOD::Vertex* vertex : *vertices)
	{
	  m_vertex_x->push_back( vertex->x() );
	  m_vertex_y->push_back( vertex->y() );
	  m_vertex_z->push_back( vertex->z() );
	}
    }

  return StatusCode::SUCCESS;
}

StatusCode VertexNtupler::FillTruthVertices( const xAOD::TruthVertexContainer* truthVertices)
{
  if(m_detailStr == "primary")
    { // hard-scatter vertex only
      int hsBarcode = -999;
      const xAOD::TruthVertex* hsTruthVertex(nullptr);
      for ( const xAOD::TruthVertex *truthVertex : *truthVertices )
	{
	  if ( truthVertex->barcode()<0 && truthVertex->barcode()>hsBarcode ) {
	    hsBarcode = truthVertex->barcode();
	    hsTruthVertex = truthVertex;
	  }
	}

      m_vertex_x->push_back( hsTruthVertex->x() );
      m_vertex_y->push_back( hsTruthVertex->y() );
      m_vertex_z->push_back( hsTruthVertex->z() );
    }
  else if (m_detailStr == "all")
    {
      for( const xAOD::TruthVertex* truthVertex : *truthVertices)
	{
	  m_vertex_x->push_back( truthVertex->x() );
	  m_vertex_y->push_back( truthVertex->y() );
	  m_vertex_z->push_back( truthVertex->z() );
	}
    }

  return StatusCode::SUCCESS;
}
