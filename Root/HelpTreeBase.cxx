#include "ZprimeNtupler/HelpTreeBase.h"
// c++ include(s):
#include <iostream>

// EDM include(s):
//#include "xAODTracking/TrackParticle.h"
//#include "xAODTracking/TrackSummaryAccessors_v1.h"
//#include "xAODJet/JetConstituentVector.h"
//#include "xAODPrimitives/IsolationType.h"

// package include(s):
#include "ZprimeNtupler/HelperFunctions.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

HelpTreeBase::HelpTreeBase(TTree* tree, bool isMC, float units) :
  m_tree(tree),
  m_isMC(isMC), m_units(units)
{
  m_nominalTree = strcmp(m_tree->GetName(), "outTree") == 0;
}

void HelpTreeBase::Fill()
{
  m_tree->Fill();
}

/*********************
 *
 *   EVENT
 *
 ********************/
StatusCode HelpTreeBase::AddEvent( const std::string& detailStr )
{
  ANA_MSG_DEBUG("AddEvent: Adding event variables: " << detailStr);

  m_eventInfo = std::make_shared<Zprime::EventInfoNtupler>(detailStr, m_units, m_isMC, m_nominalTree);
  m_eventInfo -> createBranches(m_tree);
  ANA_CHECK(AddEventUser(detailStr));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddEventUser(const std::string& detailStr )
{
  ANA_MSG_DEBUG("AddEventUser: Empty function called from HelpTreeBase with " << detailStr);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearEvent()
{
  ANA_CHECK(m_eventInfo->clear());
  ANA_CHECK(ClearEventUser());
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearEventUser()
{ return StatusCode::SUCCESS; }

StatusCode HelpTreeBase::FillEvent( const xAOD::EventInfo* eventInfo, const xAOD::TruthEvent* truthEvent, const xAOD::VertexContainer* vertices )
{
  ANA_CHECK(ClearEvent());

  ANA_CHECK(m_eventInfo->fill(eventInfo, truthEvent, vertices));

  ANA_CHECK(FillEventUser(eventInfo, truthEvent, vertices));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillEventUser( const xAOD::EventInfo* /*eventInfo*/, const xAOD::TruthEvent* /*truthEvent*/, const xAOD::VertexContainer* /*vertices*/ )
{ return StatusCode::SUCCESS; }

/*********************
 *
 *   TRIGGER
 *
 ********************/
StatusCode HelpTreeBase::AddTrigger( const std::string& detailStr )
{
  ANA_MSG_DEBUG("AddTrigger: Adding trigger variables: " << detailStr);

  m_trigger = std::make_shared<Zprime::TriggerNtupler>(detailStr);
  m_trigger -> createBranches(m_tree);
  ANA_CHECK(AddTriggerUser(detailStr));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddTriggerUser(const std::string& detailStr)
{
  ANA_MSG_DEBUG("AddTriggerUser: Empty function called from HelpTreeBase with " << detailStr);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTrigger()
{
  ANA_CHECK(m_trigger->clear());
  ANA_CHECK(ClearTriggerUser());
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTriggerUser()
{ return StatusCode::SUCCESS; }

StatusCode HelpTreeBase::FillTrigger( const xAOD::EventInfo* eventInfo )
{
  ANA_CHECK(ClearTrigger());

  ANA_CHECK(m_trigger->fill(eventInfo));

  ANA_CHECK(FillTriggerUser(eventInfo));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTriggerUser(const xAOD::EventInfo* /*eventInfo*/)
{ return StatusCode::SUCCESS; }

/*********************
 *
 *   ELECTRONS
 *
 ********************/

StatusCode HelpTreeBase::AddElectrons(const std::string& detailStr, const std::string& name)
{
  m_electrons[name] = std::make_shared<Zprime::ElectronNtupler>(name, detailStr, m_units, m_isMC, m_nominalTree);
  std::shared_ptr<Zprime::ElectronNtupler> thisElectron = m_electrons[name];

  ANA_CHECK(thisElectron->createBranches(m_tree));
  ANA_CHECK(AddElectronsUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearElectrons(const std::string& name)
{
  std::shared_ptr<Zprime::ElectronNtupler> thisElectron = m_electrons[name];
  ANA_CHECK(thisElectron->clear());
  ANA_CHECK(ClearElectronsUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillElectrons( const xAOD::ElectronContainer* electrons, const xAOD::Vertex* primaryVertex, const std::string& name )
{
  ANA_CHECK(ClearElectrons(name));

  std::shared_ptr<Zprime::ElectronNtupler> thisElectron = m_electrons[name];    
  for( const xAOD::Electron* electron : *electrons )
    {
      ANA_CHECK(thisElectron->FillElectron(electron, primaryVertex));
      ANA_CHECK(FillElectronUser(electron, primaryVertex, name));
    }

  ANA_CHECK(FillElectronsUser(electrons, primaryVertex, name));  

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddElectronsUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddElectronsUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearElectronsUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearElectronsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillElectronsUser (const xAOD::ElectronContainer* /*electrons*/, const xAOD::Vertex* /*primaryVertex*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillElectronsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  

StatusCode HelpTreeBase::FillElectronUser  (const xAOD::Electron* /*electron*/, const xAOD::Vertex* /*primaryVertex*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillElectronUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  

/*********************
 *
 *   MUONS
 *
 ********************/

StatusCode HelpTreeBase::AddMuons(const std::string& detailStr, const std::string& name)
{
  m_muons[name] = std::make_shared<Zprime::MuonNtupler>(name, detailStr, m_units, m_isMC, m_nominalTree);
  std::shared_ptr<Zprime::MuonNtupler> thisMuon = m_muons[name];

  ANA_CHECK(thisMuon->createBranches(m_tree));
  ANA_CHECK(AddMuonsUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearMuons(const std::string& name)
{
  std::shared_ptr<Zprime::MuonNtupler> thisMuon = m_muons[name];
  ANA_CHECK(thisMuon->clear());
  ANA_CHECK(ClearMuonsUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillMuons( const xAOD::MuonContainer* muons, const xAOD::Vertex* primaryVertex, const std::string& name )
{
  ANA_CHECK(ClearMuons(name));

  std::shared_ptr<Zprime::MuonNtupler> thisMuon = m_muons[name];  
  for( const xAOD::Muon* muon : *muons )
    {
      ANA_CHECK(thisMuon->FillMuon(muon, primaryVertex));
      ANA_CHECK(FillMuonUser(muon, primaryVertex, name));
    }

  ANA_CHECK(FillMuonsUser(muons, primaryVertex, name));  

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddMuonsUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddMuonsUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearMuonsUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearMuonsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillMuonsUser (const xAOD::MuonContainer* /*muons*/, const xAOD::Vertex* /*primaryVertex*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillMuonsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  

StatusCode HelpTreeBase::FillMuonUser (const xAOD::Muon* /*muon*/, const xAOD::Vertex* /*primaryVertex*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillMuonUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  

/*********************
 *
 *   TAUS
 *
 ********************/

StatusCode HelpTreeBase::AddTaus(const std::string& detailStr, const std::string& name)
{
  m_taus[name] = std::make_shared<Zprime::TauJetNtupler>(name, detailStr, m_units, m_isMC, m_nominalTree);
  std::shared_ptr<Zprime::TauJetNtupler> thisTau = m_taus[name];

  ANA_CHECK(thisTau->createBranches(m_tree));
  ANA_CHECK(AddTausUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTaus(const std::string& name)
{
  std::shared_ptr<Zprime::TauJetNtupler> thisTau = m_taus[name];
  ANA_CHECK(thisTau->clear());
  ANA_CHECK(ClearTausUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTaus( const xAOD::TauJetContainer* taus, const std::string& name )
{
  ANA_CHECK(ClearTaus(name));

  std::shared_ptr<Zprime::TauJetNtupler> thisTau = m_taus[name];
  for( const xAOD::TauJet* tau : *taus )
    { ANA_CHECK(thisTau->FillTauJet(tau)); }

  ANA_CHECK(FillTausUser(taus, name));  

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddTausUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddTausUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTausUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearTausUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTausUser (const xAOD::TauJetContainer* /*taus*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillTausUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  


/*********************
 *
 *   PHOTONS
 *
 ********************/

StatusCode HelpTreeBase::AddPhotons(const std::string& detailStr, const std::string& name)
{
  m_photons[name] = std::make_shared<Zprime::PhotonNtupler>(name, detailStr, m_units, m_isMC);
  std::shared_ptr<Zprime::PhotonNtupler> thisPhoton = m_photons[name];

  ANA_CHECK(thisPhoton->createBranches(m_tree));
  ANA_CHECK(AddPhotonsUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearPhotons(const std::string& name)
{
  std::shared_ptr<Zprime::PhotonNtupler> thisPhoton = m_photons[name];
  ANA_CHECK(thisPhoton->clear());
  ANA_CHECK(ClearPhotonsUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillPhotons( const xAOD::PhotonContainer* photons, const std::string& name )
{
  ANA_CHECK(ClearPhotons(name));

  std::shared_ptr<Zprime::PhotonNtupler> thisPhoton = m_photons[name];  
  for( const xAOD::Photon* photon : *photons )
    {
      ANA_CHECK(thisPhoton->FillPhoton(photon));
      ANA_CHECK(FillPhotonUser(photon, name));
    }

  ANA_CHECK(FillPhotonsUser(photons, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddPhotonsUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddPhotonsUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearPhotonsUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearPhotonsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillPhotonsUser (const xAOD::PhotonContainer* /*photons*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillPhotonsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillPhotonUser (const xAOD::Photon* /*photons*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillPhotonUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

/*********************
 *
 * CLUSTERS
 *
 *********************/

StatusCode HelpTreeBase::AddClusters(const std::string& detailStr, const std::string& name)
{
  m_clusters[name] = std::make_shared<Zprime::ClusterNtupler>(name, detailStr, m_units);
  std::shared_ptr<Zprime::ClusterNtupler> thisCluster = m_clusters[name];

  ANA_CHECK(thisCluster->createBranches(m_tree));
  ANA_CHECK(AddClustersUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearClusters(const std::string& name)
{
  std::shared_ptr<Zprime::ClusterNtupler> thisCluster = m_clusters[name];
  ANA_CHECK(thisCluster->clear());
  ANA_CHECK(ClearClustersUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillClusters( const xAOD::CaloClusterContainer* clusters, const std::string& name )
{
  ANA_CHECK(ClearClusters(name));

  std::shared_ptr<Zprime::ClusterNtupler> thisCluster = m_clusters[name];  
  for( const xAOD::CaloCluster* cluster : *clusters )
    { ANA_CHECK(thisCluster->FillCluster(cluster)); }

  ANA_CHECK(FillClustersUser(clusters, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddClustersUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddClustersUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearClustersUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearClustersUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillClustersUser (const xAOD::CaloClusterContainer* /*clusters*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillClustersUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

/*********************
 *
 *   JETS
 *
 ********************/

StatusCode HelpTreeBase::AddJets(const std::string& detailStr, const std::string& name)
{
  m_jets[name] = std::make_shared<Zprime::JetNtupler>(name, detailStr, m_units, m_isMC);
  std::shared_ptr<Zprime::JetNtupler> thisJet = m_jets[name];

  ANA_CHECK(thisJet->createBranches(m_tree));
  ANA_CHECK(AddJetsUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearJets(const std::string& name)
{
  std::shared_ptr<Zprime::JetNtupler> thisJet = m_jets[name];
  ANA_CHECK(thisJet->clear());
  ANA_CHECK(ClearJetsUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillJets( const xAOD::JetContainer* jets, const xAOD::Vertex* pv, int pvLocation, const std::string& name )
{
  ANA_CHECK(ClearJets(name));

  std::shared_ptr<Zprime::JetNtupler> thisJet = m_jets[name];  
  for( const xAOD::Jet* jet : *jets )
    {
      ANA_CHECK(thisJet->FillJet(jet, pv, pvLocation));
      ANA_CHECK(FillJetUser(jet, pv, pvLocation, name));
    }

  ANA_CHECK(FillJetsUser(jets, pv, pvLocation, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddJetsUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddJetsUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearJetsUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearJetsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillJetsUser (const xAOD::JetContainer* /*jets*/, const xAOD::Vertex* /*pv*/, int /*pvLocation*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillJetsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillJetUser  (const xAOD::Jet* /*jet*/, const xAOD::Vertex* /*pv*/, int /*pvLocation*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillJetUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

/*********************
 *
 *   L1 JETS
 *
 ********************/

StatusCode HelpTreeBase::AddL1Jets(const std::string& detailStr, const std::string& name)
{
  m_l1jets[name] = std::make_shared<Zprime::L1JetNtupler>(name, detailStr, m_units);
  std::shared_ptr<Zprime::L1JetNtupler> thisJet = m_l1jets[name];

  ANA_CHECK(thisJet->createBranches(m_tree));
  ANA_CHECK(AddL1JetsUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearL1Jets(const std::string& name)
{
  std::shared_ptr<Zprime::L1JetNtupler> thisJet = m_l1jets[name];
  ANA_CHECK(thisJet->clear());
  ANA_CHECK(ClearL1JetsUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillL1Jets( const xAOD::JetRoIContainer* l1jets, const std::string& name )
{
  ANA_CHECK(ClearL1Jets(name));

  std::shared_ptr<Zprime::L1JetNtupler> thisJet = m_l1jets[name];
  for( const xAOD::JetRoI* l1jet : *l1jets )
    { ANA_CHECK(thisJet->FillL1Jet(l1jet)); }

  ANA_CHECK(FillL1JetsUser(l1jets, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddL1JetsUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddL1JetsUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearL1JetsUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearL1JetsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillL1JetsUser (const xAOD::JetRoIContainer* /*l1jets*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillL1JetsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

/*********************
 *
 *   FAT JETS
 *
 ********************/

StatusCode HelpTreeBase::AddFatJets(const std::string& detailStr, const std::string& name)
{
  m_fatjets[name] = std::make_shared<Zprime::FatJetNtupler>(name, detailStr, m_units, m_isMC);
  std::shared_ptr<Zprime::FatJetNtupler> thisFatJet = m_fatjets[name];

  ANA_CHECK(thisFatJet->createBranches(m_tree));
  ANA_CHECK(AddFatJetsUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearFatJets(const std::string& name)
{
  std::shared_ptr<Zprime::FatJetNtupler> thisFatJet = m_fatjets[name];
  ANA_CHECK(thisFatJet->clear());
  ANA_CHECK(ClearFatJetsUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillFatJets( const xAOD::JetContainer* fatjets, int pvLocation, const std::string& name )
{
  ANA_CHECK(ClearFatJets(name));

  std::shared_ptr<Zprime::FatJetNtupler> thisFatJet = m_fatjets[name];
  for( const xAOD::Jet* fatjet : *fatjets )
    {
      ANA_CHECK(thisFatJet->FillFatJet(fatjet, pvLocation));
      ANA_CHECK(FillFatJetUser(fatjet, pvLocation, name));
    }

  ANA_CHECK(FillFatJetsUser(fatjets, pvLocation, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddFatJetsUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddFatJetsUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearFatJetsUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearFatJetsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillFatJetsUser (const xAOD::JetContainer* /*fatjets*/, int /*pvLocation*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillFatJetsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillFatJetUser  (const xAOD::Jet* /*fatjet*/, int /*pvLocation*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillFatJetsUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

/*********************
 *
 *   TRUTH
 *
 ********************/

StatusCode HelpTreeBase::AddTruthParticles(const std::string& detailStr, const std::string& name)
{
  m_truths[name] = std::make_shared<Zprime::TruthParticleNtupler>(name, detailStr, m_units);
  std::shared_ptr<Zprime::TruthParticleNtupler> thisTruthParticle = m_truths[name];

  ANA_CHECK(thisTruthParticle->createBranches(m_tree));
  ANA_CHECK(AddTruthParticlesUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTruthParticles(const std::string& name)
{
  std::shared_ptr<Zprime::TruthParticleNtupler> thisTruthParticle = m_truths[name];
  ANA_CHECK(thisTruthParticle->clear());
  ANA_CHECK(ClearTruthParticlesUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTruthParticles( const xAOD::TruthParticleContainer* truths, const std::string& name )
{
  ANA_CHECK(ClearTruthParticles(name));

  std::shared_ptr<Zprime::TruthParticleNtupler> thisTruthParticle = m_truths[name];  
  for( const xAOD::TruthParticle* truth : *truths )
    {
      ANA_CHECK(thisTruthParticle->FillTruthParticle(truth));
      ANA_CHECK(FillTruthParticleUser(truth, name));
    }

  ANA_CHECK(FillTruthParticlesUser(truths, name));  

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddTruthParticlesUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddTruthParticlesUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTruthParticlesUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearTruthParticlesUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTruthParticlesUser (const xAOD::TruthParticleContainer* /*truths*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillTruthParticlesUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  

StatusCode HelpTreeBase::FillTruthParticleUser  (const xAOD::TruthParticle* /*truth*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillTruthParticleUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  

/*********************
 *
 *   TRACKS
 *
 ********************/

StatusCode HelpTreeBase::AddTrackParticles(const std::string& detailStr, const std::string& name)
{
  m_tracks[name] = std::make_shared<Zprime::TrackParticleNtupler>(name, detailStr, m_units);
  std::shared_ptr<Zprime::TrackParticleNtupler> thisTrackParticle = m_tracks[name];

  ANA_CHECK(thisTrackParticle->createBranches(m_tree));
  ANA_CHECK(AddTrackParticlesUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTrackParticles(const std::string& name)
{
  std::shared_ptr<Zprime::TrackParticleNtupler> thisTrackParticle = m_tracks[name];
  ANA_CHECK(thisTrackParticle->clear());
  ANA_CHECK(ClearTrackParticlesUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTrackParticles( const xAOD::TrackParticleContainer* tracks, const std::string& name )
{
  ANA_CHECK(ClearTrackParticles(name));

  std::shared_ptr<Zprime::TrackParticleNtupler> thisTrackParticle = m_tracks[name];  
  for( const xAOD::TrackParticle* track : *tracks )
    { ANA_CHECK(thisTrackParticle->FillTrackParticle(track)); }

  ANA_CHECK(FillTrackParticlesUser(tracks, name));  

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddTrackParticlesUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddTrackParticlesUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTrackParticlesUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearTrackParticlesUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTrackParticlesUser (const xAOD::TrackParticleContainer* /*tracks*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillTrackParticlesUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  

/*********************
 *
 *   VERTICES
 *
 ********************/

StatusCode HelpTreeBase::AddVertices(const std::string& detailStr, const std::string& name)
{
  m_vertices[name] = std::make_shared<Zprime::VertexNtupler>(name, detailStr);
  std::shared_ptr<Zprime::VertexNtupler> thisVertex = m_vertices[name];

  ANA_CHECK(thisVertex->createBranches(m_tree));
  ANA_CHECK(AddVerticesUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearVertices(const std::string& name)
{
  std::shared_ptr<Zprime::VertexNtupler> thisVertex = m_vertices[name];
  ANA_CHECK(thisVertex->clear());
  ANA_CHECK(ClearVerticesUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillVertices( const xAOD::VertexContainer* vertices, const std::string& name )
{  
  ANA_CHECK(ClearVertices(name));

  std::shared_ptr<Zprime::VertexNtupler> thisVertex = m_vertices[name];
  ANA_CHECK(thisVertex->FillVertices(vertices));

  ANA_CHECK(FillVerticesUser(vertices, name));  

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddVerticesUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddVerticesUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearVerticesUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearVerticesUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillVerticesUser (const xAOD::VertexContainer* /*vertices*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillVerticesUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}  

/*********************
 *
 *   TRUTH VERTICES
 *
 ********************/

StatusCode HelpTreeBase::AddTruthVertices(const std::string& detailStr, const std::string& name)
{
  m_truth_vertices[name] = std::make_shared<Zprime::VertexNtupler>(name, detailStr);
  std::shared_ptr<Zprime::VertexNtupler> thisVertex = m_truth_vertices[name];

  ANA_CHECK(thisVertex->createBranches(m_tree));
  ANA_CHECK(AddTruthVerticesUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTruthVertices(const std::string& name)
{
  std::shared_ptr<Zprime::VertexNtupler> thisVertex = m_truth_vertices[name];
  ANA_CHECK(thisVertex->clear());
  ANA_CHECK(ClearTruthVerticesUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTruthVertices( const xAOD::TruthVertexContainer* vertices, const std::string& name )
{  
  ANA_CHECK(ClearTruthVertices(name));

  std::shared_ptr<Zprime::VertexNtupler> thisVertex = m_truth_vertices[name];
  ANA_CHECK(thisVertex->FillTruthVertices(vertices));

  ANA_CHECK(FillTruthVerticesUser(vertices, name));  

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddTruthVerticesUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddTruthVerticesUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearTruthVerticesUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearTruthVerticesUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillTruthVerticesUser (const xAOD::TruthVertexContainer* /*vertices*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillTruthVerticesUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

/*********************
 *
 *   MET
 *
 ********************/

StatusCode HelpTreeBase::AddMET(const std::string& detailStr, const std::string& name)
{
  m_met[name] = std::make_shared<Zprime::METNtupler>(name, detailStr);
  std::shared_ptr<Zprime::METNtupler> thisMET = m_met[name];

  ANA_CHECK(thisMET->createBranches(m_tree));
  ANA_CHECK(AddMETUser(detailStr, name));

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearMET(const std::string& name)
{
  std::shared_ptr<Zprime::METNtupler> thisMET = m_met[name];
  ANA_CHECK(thisMET->clear());
  ANA_CHECK(ClearMETUser(name));
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillMET( const xAOD::MissingETContainer* met, const std::string& name )
{  
  ANA_CHECK(ClearMET(name));

  std::shared_ptr<Zprime::METNtupler> thisMET = m_met[name];
  ANA_CHECK(thisMET->FillMET(met));

  ANA_CHECK(FillMETUser(met, name));  

  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::AddMETUser  (const std::string& detailStr , const std::string& name )
{
  ANA_MSG_DEBUG("AddMETUser: Empty function called from HelpTreeBase with " << detailStr << " for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::ClearMETUser(const std::string& name )
{
  ANA_MSG_DEBUG("ClearMETUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::FillMETUser (const xAOD::MissingETContainer* /*met*/, const std::string& name )
{
  ANA_MSG_DEBUG("FillMETUser: Empty function called from HelpTreeBase for " << name);
  return StatusCode::SUCCESS;
}

StatusCode HelpTreeBase::writeTo( TFile* file )
{
  file->cd(); // necessary?
  int status( m_tree->Write() );
  if ( status == 0 ) { return StatusCode::FAILURE; }
  return StatusCode::SUCCESS;
}
