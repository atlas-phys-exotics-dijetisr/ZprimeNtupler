#include "ZprimeNtupler/FatJetNtupler.h"

#include "ZprimeNtupler/HelperFunctions.h"

#include <xAODCaloEvent/CaloCluster.h>

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

FatJetNtupler::FatJetNtupler(const std::string& name, const std::string& detailStr, float units, bool mc)
  : ParticleNtupler(name,detailStr,units,mc, true, true)
{
  if (m_infoSwitch.m_scales)
    {
      m_JetConstitScaleMomentum_eta = std::make_shared<std::vector<float>>();
      m_JetConstitScaleMomentum_phi = std::make_shared<std::vector<float>>();
      m_JetConstitScaleMomentum_m   = std::make_shared<std::vector<float>>();
      m_JetConstitScaleMomentum_pt  = std::make_shared<std::vector<float>>();

      m_JetEMScaleMomentum_eta      = std::make_shared<std::vector<float>>();
      m_JetEMScaleMomentum_phi      = std::make_shared<std::vector<float>>();
      m_JetEMScaleMomentum_m        = std::make_shared<std::vector<float>>();
      m_JetEMScaleMomentum_pt       = std::make_shared<std::vector<float>>();
    }

  if (m_infoSwitch.m_area)
    {
      m_GhostArea   = std::make_shared<std::vector<float>>();
      m_ActiveArea  = std::make_shared<std::vector<float>>();
      m_VoronoiArea = std::make_shared<std::vector<float>>();

      m_ActiveArea4vec_pt  = std::make_shared<std::vector<float>>();
      m_ActiveArea4vec_eta = std::make_shared<std::vector<float>>();
      m_ActiveArea4vec_phi = std::make_shared<std::vector<float>>();
      m_ActiveArea4vec_m   = std::make_shared<std::vector<float>>();
    }

  if ( m_infoSwitch.m_substructure )
    {
      m_Split12           = std::make_shared<std::vector<float>>();
      m_Split23           = std::make_shared<std::vector<float>>();
      m_Split34           = std::make_shared<std::vector<float>>();
      m_tau1_wta          = std::make_shared<std::vector<float>>();
      m_tau2_wta          = std::make_shared<std::vector<float>>();
      m_tau3_wta          = std::make_shared<std::vector<float>>();
      m_tau21_wta         = std::make_shared<std::vector<float>>();
      m_tau32_wta         = std::make_shared<std::vector<float>>();
      m_ECF1              = std::make_shared<std::vector<float>>();
      m_ECF2              = std::make_shared<std::vector<float>>();
      m_ECF3              = std::make_shared<std::vector<float>>();
      m_C2                = std::make_shared<std::vector<float>>();
      m_D2                = std::make_shared<std::vector<float>>();
      m_NTrimSubjets      = std::make_shared<std::vector<float>>();
      m_NClusters         = std::make_shared<std::vector<int  >>();
      m_nTracks           = std::make_shared<std::vector<int  >>();
      m_ungrtrk500	  = std::make_shared<std::vector<int  >>();
      m_EMFrac		  = std::make_shared<std::vector<float>>();
      m_nChargedParticles = std::make_shared<std::vector<int  >>();
  }

  if ( m_infoSwitch.m_constituent)
    {
      m_numConstituents    = std::make_shared<std::vector<int >>();
    }

  if ( m_infoSwitch.m_constituentAll)
    {
      m_constituentWeights  = std::make_shared<std::vector< std::vector<float> >>();
      m_constituent_pt      = std::make_shared<std::vector< std::vector<float> >>();
      m_constituent_eta     = std::make_shared<std::vector< std::vector<float> >>();
      m_constituent_phi     = std::make_shared<std::vector< std::vector<float> >>();
      m_constituent_e       = std::make_shared<std::vector< std::vector<float> >>();
    }

  if ( m_infoSwitch.m_truth && m_mc )
    {
      m_truth_m  =std::make_shared<std::vector<float>>();
      m_truth_pt =std::make_shared<std::vector<float>>();
      m_truth_phi=std::make_shared<std::vector<float>>();
      m_truth_eta=std::make_shared<std::vector<float>>();
    }

  if ( m_infoSwitch.m_bosonCount && m_mc)
    {
      m_nTQuarks  = std::make_shared<std::vector<int>>();
      m_nHBosons  = std::make_shared<std::vector<int>>();
      m_nWBosons  = std::make_shared<std::vector<int>>();
      m_nZBosons  = std::make_shared<std::vector<int>>();
    }

  if (m_infoSwitch.m_muonCorrection)
    {
      m_muonCorrected_pt  = std::make_shared<std::vector<float>>();
      m_muonCorrected_eta = std::make_shared<std::vector<float>>();
      m_muonCorrected_phi = std::make_shared<std::vector<float>>();
      m_muonCorrected_m   = std::make_shared<std::vector<float>>();
    }

  for(uint32_t i=0; i<m_infoSwitch.m_hbbtag.size(); ++i)
    {
      m_Xbb_Higgs.push_back(std::make_shared<std::vector<float>>());
      m_Xbb_QCD  .push_back(std::make_shared<std::vector<float>>());
      m_Xbb_Top  .push_back(std::make_shared<std::vector<float>>());
    }

  for(const std::string& trackJetName : m_infoSwitch.m_trackJetNames)
    {
      std::string trkJetName = name;
      trkJetName += "_"+trackJetName;

      m_trkJetsIdx[trackJetName] = std::make_shared< std::vector<std::vector<unsigned int> > >();
    }

  m_trackJetPtCut  = m_infoSwitch.m_trackJetPtCut *1e3;
  m_trackJetEtaCut = m_infoSwitch.m_trackJetEtaCut;
}

StatusCode FatJetNtupler::createBranches(TTree *tree)
{
  ANA_CHECK(ParticleNtupler::createBranches(tree));

  if ( m_infoSwitch.m_scales ) {
      createBranch<float>(tree, "JetConstitScaleMomentum_eta", m_JetConstitScaleMomentum_eta);
      createBranch<float>(tree, "JetConstitScaleMomentum_phi", m_JetConstitScaleMomentum_phi);
      createBranch<float>(tree, "JetConstitScaleMomentum_m", m_JetConstitScaleMomentum_m);
      createBranch<float>(tree, "JetConstitScaleMomentum_pt", m_JetConstitScaleMomentum_pt);

      createBranch<float>(tree, "JetEMScaleMomentum_eta", m_JetEMScaleMomentum_eta);
      createBranch<float>(tree, "JetEMScaleMomentum_phi", m_JetEMScaleMomentum_phi);
      createBranch<float>(tree, "JetEMScaleMomentum_m", m_JetEMScaleMomentum_m);
      createBranch<float>(tree, "JetEMScaleMomentum_pt", m_JetEMScaleMomentum_pt);
  }

  if ( m_infoSwitch.m_area ) {
    createBranch<float>(tree, "GhostArea", m_GhostArea);
    createBranch<float>(tree, "ActiveArea", m_ActiveArea);
    createBranch<float>(tree, "VoronoiArea", m_VoronoiArea);
    createBranch<float>(tree, "ActiveArea4vec_pt", m_ActiveArea4vec_pt);
    createBranch<float>(tree, "ActiveArea4vec_eta", m_ActiveArea4vec_eta);
    createBranch<float>(tree, "ActiveArea4vec_phi", m_ActiveArea4vec_phi);
    createBranch<float>(tree, "ActiveArea4vec_m", m_ActiveArea4vec_m);

  }

  if ( m_infoSwitch.m_substructure ) {
    createBranch<float>(tree, "Split12",      m_Split12);
    createBranch<float>(tree, "Split23",      m_Split23);
    createBranch<float>(tree, "Split34",      m_Split34);
    createBranch<float>(tree, "tau1_wta",     m_tau1_wta);
    createBranch<float>(tree, "tau2_wta",     m_tau2_wta);
    createBranch<float>(tree, "tau3_wta",     m_tau3_wta);
    createBranch<float>(tree, "tau21_wta",    m_tau21_wta);
    createBranch<float>(tree, "tau32_wta",    m_tau32_wta);
    createBranch<float>(tree, "ECF1",         m_ECF1);
    createBranch<float>(tree, "ECF2",         m_ECF2);
    createBranch<float>(tree, "ECF3",         m_ECF3);
    createBranch<float>(tree, "C2",           m_C2);
    createBranch<float>(tree, "D2",           m_D2);
    createBranch<float>(tree, "NTrimSubjets", m_NTrimSubjets);
    createBranch<int>  (tree, "Nclusters",    m_NClusters);
    createBranch<int>  (tree, "nTracks",      m_nTracks);
    createBranch<int>  (tree, "ungrtrk500",   	m_ungrtrk500);
    createBranch<float>(tree, "EMFrac",	   	m_EMFrac);
    createBranch<int>  (tree, "nChargedParticles",	m_nChargedParticles);
  }

  if ( m_infoSwitch.m_constituent) {
    createBranch<int>  (tree, "numConstituents",    m_numConstituents);
  }

  if ( m_infoSwitch.m_constituentAll) {
    createBranch< std::vector<float> >(tree, "constituentWeights",  m_constituentWeights);
    createBranch< std::vector<float> >(tree, "constituent_pt",      m_constituent_pt);
    createBranch< std::vector<float> >(tree, "constituent_eta",     m_constituent_eta);
    createBranch< std::vector<float> >(tree, "constituent_phi",     m_constituent_phi);
    createBranch< std::vector<float> >(tree, "constituent_e",       m_constituent_e);
  }

  if ( m_infoSwitch.m_truth && m_mc ) {
    createBranch<float>(tree, "truth_m"  , m_truth_m  );
    createBranch<float>(tree, "truth_pt" , m_truth_pt );
    createBranch<float>(tree, "truth_phi", m_truth_phi);
    createBranch<float>(tree, "truth_eta", m_truth_eta);
  }
    
  if ( m_infoSwitch.m_bosonCount && m_mc ) {
    createBranch< int >(tree, "nTQuarks",       m_nTQuarks);
    createBranch< int >(tree, "nHBosons",       m_nHBosons);
    createBranch< int >(tree, "nWBosons",       m_nWBosons);
    createBranch< int >(tree, "nZBosons",       m_nZBosons);
  }

  if (m_infoSwitch.m_muonCorrection) {
    createBranch<float> (tree, "muonCorrected_pt" , m_muonCorrected_pt );
    createBranch<float> (tree, "muonCorrected_eta", m_muonCorrected_eta);
    createBranch<float> (tree, "muonCorrected_phi", m_muonCorrected_phi);
    createBranch<float> (tree, "muonCorrected_m"  , m_muonCorrected_m  );
  }

  for(uint32_t i=0; i<m_infoSwitch.m_hbbtag.size(); ++i) {
    const std::string& hbbtag = m_infoSwitch.m_hbbtag[i];
    createBranch<float> (tree, "Xbb"+hbbtag+"_Higgs" , m_Xbb_Higgs[i] );
    createBranch<float> (tree, "Xbb"+hbbtag+"_QCD"   , m_Xbb_QCD  [i] );
    createBranch<float> (tree, "Xbb"+hbbtag+"_Top"   , m_Xbb_Top  [i] );
  }

  for(const std::pair< std::string, std::shared_ptr< std::vector<std::vector<unsigned int>> >>& kv : m_trkJetsIdx)  
    {
      createBranch< std::vector<unsigned int> >(tree, "trkJetsIdx_"+kv.first, m_trkJetsIdx[kv.first]);
    }

  return StatusCode::SUCCESS;
}


StatusCode FatJetNtupler::clear()
{
  ANA_CHECK(ParticleNtupler::clear());

  if ( m_infoSwitch.m_scales )
    {
      m_JetConstitScaleMomentum_eta->clear();
      m_JetConstitScaleMomentum_phi->clear();
      m_JetConstitScaleMomentum_m  ->clear();
      m_JetConstitScaleMomentum_pt ->clear();

      m_JetEMScaleMomentum_eta     ->clear();
      m_JetEMScaleMomentum_phi     ->clear();
      m_JetEMScaleMomentum_m       ->clear();
      m_JetEMScaleMomentum_pt      ->clear();
    }

  if ( m_infoSwitch.m_area )
    {
      m_GhostArea         ->clear();
      m_ActiveArea        ->clear();
      m_VoronoiArea       ->clear();

      m_ActiveArea4vec_pt ->clear();
      m_ActiveArea4vec_eta->clear();
      m_ActiveArea4vec_phi->clear();
      m_ActiveArea4vec_m  ->clear();
    }

  if ( m_infoSwitch.m_substructure ) {
    m_Split12     ->clear();
    m_Split23     ->clear();
    m_Split34     ->clear();
    m_tau1_wta    ->clear();
    m_tau2_wta    ->clear();
    m_tau3_wta    ->clear();
    m_tau21_wta   ->clear();
    m_tau32_wta   ->clear();
    m_ECF1        ->clear();
    m_ECF2        ->clear();
    m_ECF3        ->clear();
    m_C2          ->clear();
    m_D2          ->clear();
    m_NTrimSubjets->clear();
    m_NClusters   ->clear();
    m_nTracks     ->clear();
    m_ungrtrk500  	->clear();
    m_EMFrac	  	->clear();
    m_nChargedParticles	->clear();
  }

  if ( m_infoSwitch.m_constituent) {
    m_numConstituents->clear();
  }

  if ( m_infoSwitch.m_constituentAll) {
    m_constituentWeights->clear();
    m_constituent_pt    ->clear();
    m_constituent_eta   ->clear();
    m_constituent_phi   ->clear();
    m_constituent_e     ->clear();
  }

  if ( m_infoSwitch.m_truth && m_mc ) {
    m_truth_m  ->clear();
    m_truth_pt ->clear();
    m_truth_phi->clear();
    m_truth_eta->clear();
  }
  
  if ( m_infoSwitch.m_bosonCount && m_mc) {
    m_nTQuarks->clear();
    m_nHBosons->clear();
    m_nWBosons->clear();
    m_nZBosons->clear();
  }

  if ( m_infoSwitch.m_muonCorrection) {
    m_muonCorrected_pt ->clear();
    m_muonCorrected_eta->clear();
    m_muonCorrected_phi->clear();
    m_muonCorrected_m  ->clear();
  }

  for(uint32_t i=0; i<m_infoSwitch.m_hbbtag.size(); ++i) {
    m_Xbb_Higgs[i]->clear();
    m_Xbb_QCD  [i]->clear();
    m_Xbb_Top  [i]->clear();
  }

  for(const std::pair< std::string, std::shared_ptr< std::vector<std::vector<unsigned int>> > >& kv : m_trkJetsIdx)
    {
      m_trkJetsIdx[kv.first]->clear();
    }

  return StatusCode::SUCCESS;
}

StatusCode FatJetNtupler::FillFatJet( const xAOD::Jet* jet, int pvLocation )
{
  return FillFatJet(static_cast<const xAOD::IParticle*>(jet), pvLocation);
}

StatusCode FatJetNtupler::FillFatJet( const xAOD::IParticle* particle, int pvLocation )
{
  ANA_CHECK(ParticleNtupler::FillParticle(particle));

  const xAOD::Jet* fatjet=dynamic_cast<const xAOD::Jet*>(particle);

  static SG::AuxElement::ConstAccessor<ElementLink<xAOD::JetContainer>> acc_parent("Parent");
  const xAOD::Jet* fatjetParent =fatjet; // Use groomed fat jet as backup
  if (acc_parent.isAvailable(*fatjet))
    {
      ElementLink<xAOD::JetContainer> fatjetParentLink = acc_parent(*fatjet);
      if (fatjetParentLink.isValid())
	fatjetParent = *fatjetParentLink;
    }

  if( m_infoSwitch.m_scales )
    {
      static SG::AuxElement::ConstAccessor<float> acc_JetConstitScaleMomentum_eta("JetConstitScaleMomentum_eta");
      safeFill<float, float, xAOD::Jet>(fatjet, acc_JetConstitScaleMomentum_eta, m_JetConstitScaleMomentum_eta, -999);
      static SG::AuxElement::ConstAccessor<float> acc_JetConstitScaleMomentum_phi("JetConstitScaleMomentum_phi");
      safeFill<float, float, xAOD::Jet>(fatjet, acc_JetConstitScaleMomentum_phi, m_JetConstitScaleMomentum_phi, -999);
      static SG::AuxElement::ConstAccessor<float> acc_JetConstitScaleMomentum_m("JetConstitScaleMomentum_m");
      safeFill<float, float, xAOD::Jet>(fatjet, acc_JetConstitScaleMomentum_m, m_JetConstitScaleMomentum_m, -999, m_units);
      static SG::AuxElement::ConstAccessor<float> acc_JetConstitScaleMomentum_pt("JetConstitScaleMomentum_pt");
      safeFill<float, float, xAOD::Jet>(fatjet, acc_JetConstitScaleMomentum_pt, m_JetConstitScaleMomentum_pt, -999, m_units);

      static SG::AuxElement::ConstAccessor<float> acc_JetEMScaleMomentum_eta("JetEMScaleMomentum_eta");
      safeFill<float, float, xAOD::Jet>(fatjet, acc_JetEMScaleMomentum_eta, m_JetEMScaleMomentum_eta, -999);
      static SG::AuxElement::ConstAccessor<float> acc_JetEMScaleMomentum_phi("JetEMScaleMomentum_phi");
      safeFill<float, float, xAOD::Jet>(fatjet, acc_JetEMScaleMomentum_phi, m_JetEMScaleMomentum_phi, -999);
      static SG::AuxElement::ConstAccessor<float> acc_JetEMScaleMomentum_m("JetEMScaleMomentum_m");
      safeFill<float, float, xAOD::Jet>(fatjet, acc_JetEMScaleMomentum_m, m_JetEMScaleMomentum_m, -999, m_units);
      static SG::AuxElement::ConstAccessor<float> acc_JetEMScaleMomentum_pt("JetEMScaleMomentum_pt");
      safeFill<float, float, xAOD::Jet>(fatjet, acc_JetEMScaleMomentum_pt, m_JetEMScaleMomentum_pt, -999, m_units);
    }

  if ( m_infoSwitch.m_area ) {
    static SG::AuxElement::ConstAccessor<float> acc_GhostArea("GhostArea");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_GhostArea, m_GhostArea, -999);
    static SG::AuxElement::ConstAccessor<float> acc_ActiveArea("ActiveArea");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_ActiveArea, m_ActiveArea, -999);
    static SG::AuxElement::ConstAccessor<float> acc_VoronoiArea("VoronoiArea");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_VoronoiArea, m_VoronoiArea, -999);
    static SG::AuxElement::ConstAccessor<float> acc_ActiveArea4vec_pt("ActiveArea4vec_pt");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_ActiveArea4vec_pt, m_ActiveArea4vec_pt, -999, m_units);
    static SG::AuxElement::ConstAccessor<float> acc_ActiveArea4vec_eta("ActiveArea4vec_eta");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_ActiveArea4vec_eta, m_ActiveArea4vec_eta, -999);
    static SG::AuxElement::ConstAccessor<float> acc_ActiveArea4vec_phi("ActiveArea4vec_phi");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_ActiveArea4vec_phi, m_ActiveArea4vec_phi, -999);
    static SG::AuxElement::ConstAccessor<float> acc_ActiveArea4vec_m("ActiveArea4vec_m");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_ActiveArea4vec_m, m_ActiveArea4vec_m, -999, m_units);
  }

  if( m_infoSwitch.m_substructure ){
    static SG::AuxElement::ConstAccessor<float> acc_Split12("Split12");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_Split12, m_Split12, -999, m_units);

    static SG::AuxElement::ConstAccessor<float> acc_Split23("Split23");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_Split23, m_Split23, -999, m_units);

    static SG::AuxElement::ConstAccessor<float> acc_Split34("Split34");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_Split34, m_Split34, -999, m_units);

    static SG::AuxElement::ConstAccessor<float> acc_tau1_wta ("Tau1_wta");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_tau1_wta, m_tau1_wta, -999);

    static SG::AuxElement::ConstAccessor<float> acc_tau2_wta ("Tau2_wta");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_tau2_wta, m_tau2_wta, -999);

    static SG::AuxElement::ConstAccessor<float> acc_tau3_wta ("Tau3_wta");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_tau3_wta, m_tau3_wta, -999);

    static SG::AuxElement::ConstAccessor<float> acc_tau21_wta ("Tau21_wta");
    if(acc_tau21_wta.isAvailable( *fatjet )){
      m_tau21_wta->push_back( acc_tau21_wta( *fatjet ) );
    } else if ( acc_tau1_wta.isAvailable( *fatjet ) && acc_tau2_wta.isAvailable( *fatjet ) ) {
      m_tau21_wta->push_back( acc_tau2_wta( *fatjet ) / acc_tau1_wta( *fatjet ) );
    } else { m_tau21_wta->push_back( -999 ); }

    static SG::AuxElement::ConstAccessor<float> acc_tau32_wta ("Tau32_wta");
    if(acc_tau32_wta.isAvailable( *fatjet )){
      m_tau32_wta->push_back( acc_tau32_wta( *fatjet ) );
    } else if ( acc_tau2_wta.isAvailable( *fatjet ) && acc_tau3_wta.isAvailable( *fatjet ) ) {
      m_tau32_wta->push_back( acc_tau3_wta( *fatjet ) / acc_tau2_wta( *fatjet ) );
    } else { m_tau32_wta->push_back( -999 ); }

    static SG::AuxElement::ConstAccessor<int> acc_NClusters ("MyNClusters");
    safeFill<int, int, xAOD::Jet>(fatjet, acc_NClusters, m_NClusters, -999);

    static SG::AuxElement::ConstAccessor<float> acc_ECF1 ("ECF1");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_ECF1, m_ECF1, -999, m_units);

    static SG::AuxElement::ConstAccessor<float> acc_ECF2("ECF2");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_ECF2, m_ECF2, -999, m_units);

    static SG::AuxElement::ConstAccessor<float> acc_ECF3 ("ECF3");
    safeFill<float, float, xAOD::Jet>(fatjet, acc_ECF3, m_ECF3, -999, m_units);

    static SG::AuxElement::ConstAccessor<int> NTrimSubjets("NTrimSubjets");
    safeFill<int, float, xAOD::Jet>(fatjet, NTrimSubjets, m_NTrimSubjets, -999);

    static SG::AuxElement::ConstAccessor<float> acc_D2 ("D2");
    if( acc_D2.isAvailable( *fatjet ) ) {
      m_D2->push_back( acc_D2( *fatjet ));
    } else if (acc_ECF1.isAvailable( *fatjet ) && acc_ECF2.isAvailable( *fatjet ) && acc_ECF3.isAvailable( *fatjet )){
      float e2=(acc_ECF2( *fatjet )/(acc_ECF1( *fatjet )*acc_ECF1( *fatjet )));
      float e3=(acc_ECF3( *fatjet )/(acc_ECF1( *fatjet )*acc_ECF1( *fatjet )*acc_ECF1( *fatjet )));
      m_D2->push_back( e3/(e2*e2*e2) );
    } else{ m_D2->push_back(-999); }

    static SG::AuxElement::ConstAccessor<float> acc_C2("C2");
    if(acc_C2.isAvailable(*fatjet)){
      m_C2->push_back(acc_C2(*fatjet));
    } else if( acc_ECF1.isAvailable(*fatjet) && acc_ECF2.isAvailable(*fatjet) && acc_ECF3.isAvailable(*fatjet)){
      m_C2->push_back( acc_ECF3(*fatjet)*acc_ECF1(*fatjet)/pow(acc_ECF2(*fatjet),2.0));
    } else{ m_C2->push_back(-999); }

    static SG::AuxElement::ConstAccessor<int> acc_GhostTrackCount("GhostTrackCount");
    if( acc_GhostTrackCount.isAvailable( *fatjet ) ) {
      m_nTracks->push_back( acc_GhostTrackCount( *fatjet ));
    } else { m_nTracks->push_back(-999); }

    static SG::AuxElement::ConstAccessor< std::vector<int> > acc_NumTrkPt500("NumTrkPt500");    
    if(acc_NumTrkPt500.isAvailable( *fatjetParent ) )
      m_ungrtrk500->push_back( acc_NumTrkPt500( *fatjetParent )[pvLocation] );
    else
      m_ungrtrk500->push_back( -999 );

    m_EMFrac->push_back(GetEMFrac (*fatjet));
	
    static SG::AuxElement::ConstAccessor<int> acc_nChargedParticles("nChargedParticles");
    if( acc_nChargedParticles.isAvailable( *fatjet ) ) {
      m_nChargedParticles->push_back( acc_nChargedParticles( *fatjet ));
    } else { m_nChargedParticles->push_back(-999); }

  }

  if( m_infoSwitch.m_constituent ){
    m_numConstituents->push_back( fatjet->numConstituents() );
  }


  if( m_infoSwitch.m_constituentAll ){
    m_constituentWeights->push_back( fatjet->getAttribute< std::vector<float> >( "constituentWeights" ) );
    std::vector<float> pt;
    std::vector<float> eta;
    std::vector<float> phi;
    std::vector<float> e;
    xAOD::JetConstituentVector consVec = fatjet->getConstituents();

    if( consVec.isValid() ) {

      // use the example provided in
      // http://acode-browser.usatlas.bnl.gov/lxr/source/atlas/Event/xAOD/xAODJet/xAODJet/JetConstituentVector.h
      xAOD::JetConstituentVector::iterator constit = consVec.begin();
      xAOD::JetConstituentVector::iterator constitE = consVec.end();
      for( ; constit != constitE; constit++){
	pt. push_back( constit->pt() / m_units );
	eta.push_back( constit->eta() );
	phi.push_back( constit->phi() );
	e.  push_back( constit->e() / m_units  );
      }
    }
    m_constituent_pt ->push_back( pt  );
    m_constituent_eta->push_back( eta );
    m_constituent_phi->push_back( phi );
    m_constituent_e  ->push_back( e   );
  }

  if ( m_infoSwitch.m_truth && m_mc ) {
    const xAOD::Jet* truthJet = HelperFunctions::getLink<xAOD::Jet>( fatjet, "GhostTruthAssociationLink" );
    if(truthJet) {
      m_truth_pt->push_back ( truthJet->pt() / m_units );
      m_truth_eta->push_back( truthJet->eta() );
      m_truth_phi->push_back( truthJet->phi() );
      m_truth_m->push_back  ( truthJet->m() / m_units );
    } else {
      m_truth_pt->push_back ( -999 );
      m_truth_eta->push_back( -999 );
      m_truth_phi->push_back( -999 );
      m_truth_m->push_back  ( -999 );
    }

  }

  if(m_infoSwitch.m_bosonCount && m_mc)
    {
      static SG::AuxElement::ConstAccessor< int > truthfatjet_TQuarks("GhostTQuarksFinalCount");
      safeFill<int, int, xAOD::Jet>(fatjetParent, truthfatjet_TQuarks, m_nTQuarks, -999);

      static SG::AuxElement::ConstAccessor< int > truthfatjet_WBosons("GhostWBosonsCount");
      safeFill<int, int, xAOD::Jet>(fatjetParent, truthfatjet_WBosons, m_nWBosons, -999);

      static SG::AuxElement::ConstAccessor< int > truthfatjet_ZBosons("GhostZBosonsCount");
      safeFill<int, int, xAOD::Jet>(fatjetParent, truthfatjet_ZBosons, m_nZBosons, -999);

      static SG::AuxElement::ConstAccessor< int > truthfatjet_HBosons("GhostHBosonsCount");
      safeFill<int, int, xAOD::Jet>(fatjetParent, truthfatjet_HBosons, m_nHBosons, -999);
    }

  if (m_infoSwitch.m_muonCorrection)
    {
      static const SG::AuxElement::ConstAccessor<TLorentzVector> acc_correctedFatJets_tlv("correctedFatJets_tlv");
      m_muonCorrected_pt ->push_back(acc_correctedFatJets_tlv(*fatjet).Pt() / m_units);
      m_muonCorrected_eta->push_back(acc_correctedFatJets_tlv(*fatjet).Eta());
      m_muonCorrected_phi->push_back(acc_correctedFatJets_tlv(*fatjet).Phi());
      m_muonCorrected_m  ->push_back(acc_correctedFatJets_tlv(*fatjet).M()  / m_units);
  }

  for(uint32_t i=0; i<m_infoSwitch.m_hbbtag.size(); ++i) {
    const std::string& hbbtag = m_infoSwitch.m_hbbtag[i];

    SG::AuxElement::ConstAccessor< float > Xbb_Higgs("Xbb"+hbbtag+"_Higgs");
    m_Xbb_Higgs[i]->push_back(Xbb_Higgs(*fatjet));

    SG::AuxElement::ConstAccessor< float > Xbb_QCD  ("Xbb"+hbbtag+"_QCD"  );
    m_Xbb_QCD  [i]->push_back(Xbb_QCD  (*fatjet));

    SG::AuxElement::ConstAccessor< float > Xbb_Top  ("Xbb"+hbbtag+"_Top"  );
    m_Xbb_Top  [i]->push_back(Xbb_Top  (*fatjet));
  }

  //
  // Associated track jets
  //
  if( !m_infoSwitch.m_trackJetNames.empty() )
    {
      // Associate the different track jet collections
      std::vector<const xAOD::Jet*> assotrkjets;
      for(const std::string& trackJetName : m_infoSwitch.m_trackJetNames)
	{
	  try
	    {
	      assotrkjets = fatjetParent->getAssociatedObjects<xAOD::Jet>(trackJetName);
	      std::sort( assotrkjets.begin(), assotrkjets.end(), HelperFunctions::sort_pt );
	    }
	  catch (...)
	    {
	      ANA_MSG_ERROR("Unable to fetch \"" +trackJetName+ "\" link from fat jet");
	      return StatusCode::FAILURE;
	    }

	  std::vector<unsigned int> trkJetsIdx;
	  for(const xAOD::Jet* TrackJet : assotrkjets)
	    {
	      if(!SelectTrackJet(TrackJet)) continue;
	      trkJetsIdx.push_back(TrackJet->index());
	    }
	  m_trkJetsIdx[trackJetName]->push_back(trkJetsIdx);
	}
    }

  return StatusCode::SUCCESS;
}

bool FatJetNtupler::SelectTrackJet(const xAOD::Jet* TrackJet)
{
  //std::cout << " Try pt,eta,phi,N = " << TrackJet->pt() << ", " << TrackJet->eta() << ", " << TrackJet->phi() << ", " << TrackJet->numConstituents() << std::endl;
  if( TrackJet->pt() < m_trackJetPtCut )            return false;
  if( fabs(TrackJet->eta()) > m_trackJetEtaCut )    return false;
  if( TrackJet->numConstituents() < 2 )             return false;
  //std::cout << "  KEEP!" << std::endl;
  return true;
}

float FatJetNtupler::GetEMFrac(const xAOD::Jet& jet)
{
  float eInSample = 0.; 
  float eInSampleFull = 0.; 
  float emfrac = 0.; 
  const xAOD::JetConstituentVector constituents = jet.getConstituents();
  if (!constituents.isValid())
    {
      //ATH_MSG_WARNING("Unable to retrieve valid constituents from parent of large R jet");
      return -1.;
    }
  for (const auto& constituent : constituents)
    {
      if(!constituent) continue;
      if(constituent->rawConstituent()->type()!=xAOD::Type::CaloCluster)
	{
	  //ATH_MSG_WARNING("Tried to call fillEperSamplingCluster with a jet constituent that is not a cluster!");
	  continue;
	}
      const xAOD::CaloCluster* constit = static_cast<const xAOD::CaloCluster*>(constituent->rawConstituent());  
      if(!constit) continue;
      for (int s=0;s<CaloSampling::Unknown; s++)
	eInSampleFull += constit->eSample(CaloSampling::CaloSample(s));

      eInSample += constit->eSample(CaloSampling::EMB1);
      eInSample += constit->eSample(CaloSampling::EMB2);
      eInSample += constit->eSample(CaloSampling::EMB3);
      eInSample += constit->eSample(CaloSampling::EME1);
      eInSample += constit->eSample(CaloSampling::EME2);
      eInSample += constit->eSample(CaloSampling::EME3);
      eInSample += constit->eSample(CaloSampling::FCAL1);
    }
  emfrac  = eInSample/eInSampleFull;
  if ( emfrac > 1.0 ) emfrac = 1.;
  else if ( emfrac < 0.0 ) emfrac = 0.;

  return emfrac;
}

