#include "ZprimeNtupler/L1JetNtupler.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

L1JetNtupler::L1JetNtupler(const std::string& name, const std::string& /*detailStr*/, float units, bool mc)
  : m_name(name), m_units(units), m_mc(mc)
{
  m_l1Jet_et8x8=std::shared_ptr<std::vector<float>>();
  m_l1Jet_eta  =std::shared_ptr<std::vector<float>>();  
  m_l1Jet_phi  =std::shared_ptr<std::vector<float>>();
}

StatusCode L1JetNtupler::createBranches(TTree *tree)
{
  tree->Branch(("n"+m_name).c_str(), &m_n, ("n"+m_name+"/I").c_str());

  tree->Branch((m_name+"_et8x8").c_str(),m_l1Jet_et8x8.get());
  tree->Branch((m_name+"_eta"  ).c_str(),m_l1Jet_eta  .get());
  tree->Branch((m_name+"_phi"  ).c_str(),m_l1Jet_phi  .get());  

  return StatusCode::SUCCESS;
}

StatusCode L1JetNtupler::clear()
{
  m_n = 0;

  m_l1Jet_et8x8->clear();
  m_l1Jet_eta  ->clear();
  m_l1Jet_phi  ->clear();

  return StatusCode::SUCCESS;
}

StatusCode L1JetNtupler::FillL1Jet( const xAOD::JetRoI* jet )
{
  m_n++;

  m_l1Jet_et8x8->push_back( jet->et8x8() / m_units );
  m_l1Jet_eta  ->push_back( jet->eta  () );
  m_l1Jet_phi  ->push_back( jet->phi  () );

  return StatusCode::SUCCESS;
}
