#include "ZprimeNtupler/TriggerNtupler.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

TriggerNtupler::TriggerNtupler(const std::string& detailStr)
  : m_infoSwitch(detailStr)
{ }

StatusCode TriggerNtupler::createBranches(TTree *tree)
{
  // Add these basic branches
  if ( m_infoSwitch.m_basic )
    {
      tree->Branch("passL1",          &m_passL1,       "passL1/I"      );
      tree->Branch("passHLT",         &m_passHLT,      "passHLT/I"     );
    }

  // Detailed trigger infoformation related to the menu (might be useful)
  // This is useful for using this database: https://atlas-trigconf.cern.ch/
  if ( m_infoSwitch.m_menuKeys )
    {
      tree->Branch("masterKey",       &m_masterKey,    "masterKey/I"         );
      tree->Branch("lvl1PrescaleKey", &m_L1PSKey,      "lvl1PrescaleKey/I"   );
      tree->Branch("hltPrescaleKey",  &m_HLTPSKey,     "hltPrescaleKey/I"    );
    }

  // Pre trigger information
  for( const std::string& trigger : m_infoSwitch.m_triggerList )
    {
      if(m_infoSwitch.m_passTriggers)
	{
	  m_trigger             [trigger]=-1;
	  tree->Branch(("trigger_"+trigger                ).c_str(), &m_trigger             [trigger], ("trigger_"+trigger+"/B"             ).c_str());

	  m_trigger_disabled    [trigger]=-1;
	  tree->Branch(("trigger_"+trigger+"_disabled"    ).c_str(), &m_trigger_disabled    [trigger], ("trigger_"+trigger+"_disabled/B"    ).c_str());
	}

      if(m_infoSwitch.m_prescales)
	{
	  m_trigger_prescale    [trigger]=-1;
	  tree->Branch(("trigger_"+trigger+"_prescale"    ).c_str(), &m_trigger_prescale    [trigger], ("trigger_"+trigger+"_prescale/F"    ).c_str());
}

      if(m_infoSwitch.m_prescalesLumi)
	{
	  m_trigger_prescaleLumi[trigger]=0;
	  tree->Branch(("trigger_"+trigger+"_prescaleLumi").c_str(), &m_trigger_prescaleLumi[trigger], ("trigger_"+trigger+"_prescaleLumi/F").c_str());
	}

      if(m_infoSwitch.m_passTrigBits)
	{
	  m_trigger_isPassBits  [trigger]=0;	  
	  tree->Branch(("trigger_"+trigger+"_isPassBits"  ).c_str(), &m_trigger_isPassBits  [trigger], ("trigger_"+trigger+"_isPassBits/i"  ).c_str());	  
	}
    }

  return StatusCode::SUCCESS;
}

StatusCode TriggerNtupler::clear()
{
  // basic
  m_passHLT = 0;
  m_passL1  = 0;

  // menuKeys
  m_masterKey =0;
  m_L1PSKey   =0;
  m_HLTPSKey  =0;

  // passTriggers
  for( std::pair<std::string, char> kv : m_trigger)
    m_trigger             [kv.first]=false;

  for( std::pair<std::string, char> kv : m_trigger_disabled)
    m_trigger_disabled    [kv.first]=false;

  for( std::pair<std::string, float> kv : m_trigger_prescale)
    m_trigger_prescale    [kv.first]=0;

  for( std::pair<std::string, float> kv : m_trigger_prescaleLumi)
    m_trigger_prescaleLumi[kv.first]=0;

  for( std::pair<std::string, uint32_t> kv : m_trigger_isPassBits)
    m_trigger_isPassBits  [kv.first]=0;

  return StatusCode::SUCCESS;
}

StatusCode TriggerNtupler::fill( const xAOD::EventInfo* eventInfo)
{
  // Grab the global pass information from the TrigDecisionTool
  if ( m_infoSwitch.m_basic )
    {
      static SG::AuxElement::ConstAccessor< int > passL1 ("passL1" );
      m_passL1 =(passL1 .isAvailable( *eventInfo )) ? passL1 ( *eventInfo ) : -1;

      static SG::AuxElement::ConstAccessor< int > passHLT ("passHLT");
      m_passHLT=(passHLT.isAvailable( *eventInfo )) ? passHLT( *eventInfo ) : -1;
    }

  // If detailed menu information about the configuration keys, turn this on.
  // This is useful for using this database: https://atlas-trigconf.cern.ch/
  if ( m_infoSwitch.m_menuKeys )
    {
      static SG::AuxElement::ConstAccessor< int > masterKey ("masterKey");
      m_masterKey=(masterKey.isAvailable( *eventInfo )) ? masterKey( *eventInfo ) : -1;

      static SG::AuxElement::ConstAccessor< int > L1PSKey ("L1PSKey");
      m_L1PSKey  =(L1PSKey  .isAvailable( *eventInfo )) ? L1PSKey  ( *eventInfo ) : -1;

      static SG::AuxElement::ConstAccessor< int > HLTPSKey ("HLTPSKey");
      m_HLTPSKey =(HLTPSKey .isAvailable( *eventInfo )) ? HLTPSKey ( *eventInfo ) : -1;
  }

  // Fill trigger decisions, trigger-per-branch style
  for( const std::string& trigger : m_infoSwitch.m_triggerList )
    {

      if(m_infoSwitch.m_passTriggers)
	{
	  SG::AuxElement::ConstAccessor< char     > isPassed    (trigger                );
	  m_trigger             [trigger]=isPassed    .isAvailable( *eventInfo ) ? isPassed    ( *eventInfo ) : false;	  

	  SG::AuxElement::ConstAccessor< char     > disabled    (trigger+"_disabled"    );
	  m_trigger_disabled    [trigger]=disabled    .isAvailable( *eventInfo ) ? disabled    ( *eventInfo ) : false;
	}

      if(m_infoSwitch.m_prescales)
	{
	  SG::AuxElement::ConstAccessor< float    > prescale    (trigger+"_prescale"    );
	  m_trigger_prescale    [trigger]=prescale    .isAvailable( *eventInfo ) ? prescale    ( *eventInfo ) : false;
	}

      if(m_infoSwitch.m_prescalesLumi)
	{
	  SG::AuxElement::ConstAccessor< float    > prescaleLumi(trigger+"_prescaleLumi");
	  m_trigger_prescaleLumi[trigger]=prescaleLumi.isAvailable( *eventInfo ) ? prescaleLumi( *eventInfo ) : false;
	}

      if(m_infoSwitch.m_passTrigBits)
	{
	  SG::AuxElement::ConstAccessor< uint32_t > isPassBits  (trigger+"_isPassBits"  );
	  m_trigger_isPassBits  [trigger]=isPassBits  .isAvailable( *eventInfo ) ? isPassBits  ( *eventInfo ) : false;
	}
    }

  return StatusCode::SUCCESS;
}
