#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "ZprimeNtupler/ZprimeMiniTree.h"

ZprimeMiniTree :: ZprimeMiniTree(TTree *tree, bool isMC, bool doTruthInfo, bool saveZprimeDecay, bool saveExtraVariables)
  : HelpTreeBase(tree, isMC, 1e3),
    m_doTruth(doTruthInfo), m_saveZprimeDecay(saveZprimeDecay), m_saveExtraVariables(saveExtraVariables)
{ }

//////////////////// Connect Defined variables to branches here /////////////////////////////
StatusCode ZprimeMiniTree::AddEventUser(const std::string& /*detailStr*/)
{
  m_tree->Branch("weight",    &m_weight   , "weight/F");
  m_tree->Branch("weight_xs", &m_weight_xs, "weight_xs/F");

  if (m_doTruth)
    {
      m_tree->Branch("Higgs_pt"   , &m_Higgs_pt   ,"Higgs_pt/F"   );
      m_tree->Branch("Higgs_eta"  , &m_Higgs_eta  ,"Higgs_eta/F"  );
      m_tree->Branch("Higgs_phi"  , &m_Higgs_phi  ,"Higgs_phi/F"  );
      m_tree->Branch("Higgs_m"    , &m_Higgs_m    ,"Higgs_m/F"    );
      m_tree->Branch("Higgs_decay", &m_Higgs_decay,"Higgs_decay/i");

      m_tree->Branch("Zboson_pt"  , &m_Zboson_pt  ,"Zboson_pt/F"  );
      m_tree->Branch("Zboson_eta" , &m_Zboson_eta ,"Zboson_eta/F" );
      m_tree->Branch("Zboson_phi" , &m_Zboson_phi ,"Zboson_phi/F" );
      m_tree->Branch("Zboson_m"   , &m_Zboson_m   ,"Zboson_m/F"   );

      m_tree->Branch("Wboson_pt"  , &m_Wboson_pt  ,"Wboson_pt/F"  );
      m_tree->Branch("Wboson_eta" , &m_Wboson_eta ,"Wboson_eta/F" );
      m_tree->Branch("Wboson_phi" , &m_Wboson_phi ,"Wboson_phi/F" );
      m_tree->Branch("Wboson_m"   , &m_Wboson_m   ,"Wboson_m/F"   );

      m_tree->Branch("Zprime_pt"  , &m_Zprime_pt  ,"Zprime_pt/F"  );
      m_tree->Branch("Zprime_eta" , &m_Zprime_eta ,"Zprime_eta/F" );
      m_tree->Branch("Zprime_phi" , &m_Zprime_phi ,"Zprime_phi/F" );
      m_tree->Branch("Zprime_m"   , &m_Zprime_m   ,"Zprime_m/F"   );
      m_tree->Branch("Zprime_pdg" , &m_Zprime_pdg ,"Zprime_pdg/i" );

      if(m_saveZprimeDecay)
	{
	  m_tree->Branch("reso0_pt" , &m_reso0_pt ,"reso0_pt/F");
	  m_tree->Branch("reso0_eta", &m_reso0_eta,"reso0_eta/F");
	  m_tree->Branch("reso0_phi", &m_reso0_phi,"reso0_phi/F");
	  m_tree->Branch("reso0_E"  , &m_reso0_E  ,"reso0_E/F");

	  m_tree->Branch("reso1_pt" , &m_reso1_pt ,"reso1_pt/F");
	  m_tree->Branch("reso1_eta", &m_reso1_eta,"reso1_eta/F");
	  m_tree->Branch("reso1_phi", &m_reso1_phi,"reso1_phi/F");
	  m_tree->Branch("reso1_E"  , &m_reso1_E  ,"reso1_E/F");

	  m_tree->Branch("ISR_pt",    &m_ISR_pt ,  "ISR_pt/F");
	  m_tree->Branch("ISR_eta",   &m_ISR_eta,  "ISR_eta/F");
	  m_tree->Branch("ISR_phi",   &m_ISR_phi,  "ISR_phi/F");
	  m_tree->Branch("ISR_E",     &m_ISR_E  ,  "ISR_E/F");
	  m_tree->Branch("ISR_pdgId", &m_ISR_pdgId,"ISR_pdgId/I");
	}

      m_tree->Branch("HTXS_prodMode" , &m_HTXS_prodMode , "HTXS_prodMode/I" );
      m_tree->Branch("HTXS_errorCode", &m_HTXS_errorCode, "HTXS_errorCode/I");

      m_tree->Branch("HTXS_Higgs_decay_eta", &m_HTXS_Higgs_decay_eta, "HTXS_Higgs_decay_eta/F");
      m_tree->Branch("HTXS_Higgs_decay_m"  , &m_HTXS_Higgs_decay_m  , "HTXS_Higgs_decay_m/F"  );
      m_tree->Branch("HTXS_Higgs_decay_phi", &m_HTXS_Higgs_decay_phi, "HTXS_Higgs_decay_phi/F");
      m_tree->Branch("HTXS_Higgs_decay_pt" , &m_HTXS_Higgs_decay_pt , "HTXS_Higgs_decay_pt/F" );

      m_tree->Branch("HTXS_Higgs_eta", &m_HTXS_Higgs_eta, "HTXS_Higgs_eta/F");
      m_tree->Branch("HTXS_Higgs_m"  , &m_HTXS_Higgs_m  , "HTXS_Higgs_m/F"  );
      m_tree->Branch("HTXS_Higgs_phi", &m_HTXS_Higgs_phi, "HTXS_Higgs_phi/F");
      m_tree->Branch("HTXS_Higgs_pt" , &m_HTXS_Higgs_pt , "HTXS_Higgs_pt/F" );

      m_tree->Branch("HTXS_V_decay_eta", &m_HTXS_V_decay_eta, "HTXS_V_decay_eta/F");
      m_tree->Branch("HTXS_V_decay_m"  , &m_HTXS_V_decay_m  , "HTXS_V_decay_m/F"  );
      m_tree->Branch("HTXS_V_decay_phi", &m_HTXS_V_decay_phi, "HTXS_V_decay_phi/F");
      m_tree->Branch("HTXS_V_decay_pt" , &m_HTXS_V_decay_pt , "HTXS_V_decay_pt/F" );

      m_tree->Branch("HTXS_V_eta", &m_HTXS_V_eta, "HTXS_V_eta/F");
      m_tree->Branch("HTXS_V_m"  , &m_HTXS_V_m  , "HTXS_V_m/F"  );
      m_tree->Branch("HTXS_V_phi", &m_HTXS_V_phi, "HTXS_V_phi/F");
      m_tree->Branch("HTXS_V_pt" , &m_HTXS_V_pt , "HTXS_V_pt/F" );

      m_tree->Branch("HTXS_Njets_pTjet25", &m_HTXS_Njets_pTjet25, "HTXS_Njets_pTjet25/I");
      m_tree->Branch("HTXS_Njets_pTjet30", &m_HTXS_Njets_pTjet30, "HTXS_Njets_pTjet30/I");

      m_tree->Branch("HTXS_Stage0_Category"                , &m_HTXS_Stage0_Category                , "HTXS_Stage0_Category/I"                );
      m_tree->Branch("HTXS_Stage1_2_Category_pTjet25"      , &m_HTXS_Stage1_2_Category_pTjet25      , "HTXS_Stage1_2_Category_pTjet25/I"      );
      m_tree->Branch("HTXS_Stage1_2_Category_pTjet30"      , &m_HTXS_Stage1_2_Category_pTjet30      , "HTXS_Stage1_2_Category_pTjet30/I"      );
      m_tree->Branch("HTXS_Stage1_2_FineIndex_pTjet25"     , &m_HTXS_Stage1_2_FineIndex_pTjet25     , "HTXS_Stage1_2_FineIndex_pTjet25/I"     );
      m_tree->Branch("HTXS_Stage1_2_FineIndex_pTjet30"     , &m_HTXS_Stage1_2_FineIndex_pTjet30     , "HTXS_Stage1_2_FineIndex_pTjet30/I"     );
      m_tree->Branch("HTXS_Stage1_2_Fine_Category_pTjet25" , &m_HTXS_Stage1_2_Fine_Category_pTjet25 , "HTXS_Stage1_2_Fine_Category_pTjet25/I" );
      m_tree->Branch("HTXS_Stage1_2_Fine_Category_pTjet30" , &m_HTXS_Stage1_2_Fine_Category_pTjet30 , "HTXS_Stage1_2_Fine_Category_pTjet30/I" );
      m_tree->Branch("HTXS_Stage1_2_Fine_FineIndex_pTjet25", &m_HTXS_Stage1_2_Fine_FineIndex_pTjet25, "HTXS_Stage1_2_Fine_FineIndex_pTjet25/I");
      m_tree->Branch("HTXS_Stage1_2_Fine_FineIndex_pTjet30", &m_HTXS_Stage1_2_Fine_FineIndex_pTjet30, "HTXS_Stage1_2_Fine_FineIndex_pTjet30/I");
      m_tree->Branch("HTXS_Stage1_Category_pTjet25"        , &m_HTXS_Stage1_Category_pTjet25        , "HTXS_Stage1_Category_pTjet25/I"        );
      m_tree->Branch("HTXS_Stage1_Category_pTjet30"        , &m_HTXS_Stage1_Category_pTjet30        , "HTXS_Stage1_Category_pTjet30/I"        );
      m_tree->Branch("HTXS_Stage1_FineIndex_pTjet25"       , &m_HTXS_Stage1_FineIndex_pTjet25       , "HTXS_Stage1_FineIndex_pTjet25/I"       );
      m_tree->Branch("HTXS_Stage1_FineIndex_pTjet30"       , &m_HTXS_Stage1_FineIndex_pTjet30       , "HTXS_Stage1_FineIndex_pTjet30/I"       );

      m_tree->Branch("HTXS_isZ2vvDecay", &m_HTXS_isZ2vvDecay, "HTXS_isZ2vvDecay/I");
    }

  return StatusCode::SUCCESS;
}

//////////////////// Connect Defined variables to branches here ///////////////////////////// 
StatusCode ZprimeMiniTree::AddPhotonsUser(const std::string& /*detailStr*/, const std::string& /*photonName*/)
{

  m_tree->Branch("ph_trig_match_HLT_g75_tight_3j25noL1_L1EM22VHI", &m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI);
  m_tree->Branch("ph_trig_match_HLT_g85_tight_L1EM22VHI_3j50noL1", &m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1);
  m_tree->Branch("ph_trig_match_HLT_g60_loose", &m_photons_HLT_g60_loose);
  m_tree->Branch("ph_trig_match_HLT_g140_loose", &m_photons_HLT_g140_loose);
  m_tree->Branch("ph_trig_match_HLT_g160_loose", &m_photons_HLT_g160_loose);

  return StatusCode::SUCCESS;
}

StatusCode ZprimeMiniTree::AddFatJetsUser(const std::string& /*detailStr*/, const std::string& /*fatjetName*/)
{
  if(m_saveExtraVariables)
    {
      m_tree->Branch("fatjet_ANN_score", &m_fatjets_ANN_score);
    }

  return StatusCode::SUCCESS;
}

//////////////////// Clear any defined vectors here ////////////////////////////
StatusCode ZprimeMiniTree::ClearEventUser() {
  m_weight_corr = -999;
  m_weight    = -999;
  m_weight_xs = -999;

  if (m_doTruth) {
    m_Higgs_pt   = -999;
    m_Higgs_eta  = -999;
    m_Higgs_phi  = -999;
    m_Higgs_m    = -999;
    m_Higgs_decay= -999;

    m_Zboson_pt  = -999;
    m_Zboson_eta = -999;
    m_Zboson_phi = -999;
    m_Zboson_m   = -999;

    m_Wboson_pt  = -999;
    m_Wboson_eta = -999;
    m_Wboson_phi = -999;
    m_Wboson_m   = -999;
    
    m_Zprime_pt  = -999;
    m_Zprime_eta = -999;
    m_Zprime_phi = -999;
    m_Zprime_m   = -999;
    m_Zprime_pdg = -999;

    if(m_saveZprimeDecay)
      {
	m_reso0_pt  = -999;
	m_reso0_eta = -999;
	m_reso0_phi = -999;
	m_reso0_E   = -999;

	m_reso1_pt  = -999;
	m_reso1_eta = -999;
	m_reso1_phi = -999;
	m_reso1_E   = -999;

	m_ISR_pt  = -999;
	m_ISR_eta = -999;
	m_ISR_phi = -999;
	m_ISR_E   = -999;
	m_ISR_pdgId   = -999;
      }

    m_HTXS_prodMode = -999;
    m_HTXS_errorCode= -999;

    m_HTXS_Higgs_decay_eta= -999;
    m_HTXS_Higgs_decay_m  = -999;
    m_HTXS_Higgs_decay_phi= -999;
    m_HTXS_Higgs_decay_pt = -999;

    m_HTXS_Higgs_eta= -999;
    m_HTXS_Higgs_m  = -999;
    m_HTXS_Higgs_phi= -999;
    m_HTXS_Higgs_pt = -999;

    m_HTXS_V_decay_eta= -999;
    m_HTXS_V_decay_m  = -999;
    m_HTXS_V_decay_phi= -999;
    m_HTXS_V_decay_pt = -999;

    m_HTXS_V_eta= -999;
    m_HTXS_V_m  = -999;
    m_HTXS_V_phi= -999;
    m_HTXS_V_pt = -999;

    m_HTXS_Njets_pTjet25= -999;
    m_HTXS_Njets_pTjet30= -999;

    m_HTXS_Stage0_Category                = -999;
    m_HTXS_Stage1_2_Category_pTjet25      = -999;
    m_HTXS_Stage1_2_Category_pTjet30      = -999;
    m_HTXS_Stage1_2_FineIndex_pTjet25     = -999;
    m_HTXS_Stage1_2_FineIndex_pTjet30     = -999;
    m_HTXS_Stage1_2_Fine_Category_pTjet25 = -999;
    m_HTXS_Stage1_2_Fine_Category_pTjet30 = -999;
    m_HTXS_Stage1_2_Fine_FineIndex_pTjet25= -999;
    m_HTXS_Stage1_2_Fine_FineIndex_pTjet30= -999;
    m_HTXS_Stage1_Category_pTjet25        = -999;
    m_HTXS_Stage1_Category_pTjet30        = -999;
    m_HTXS_Stage1_FineIndex_pTjet25       = -999;
    m_HTXS_Stage1_FineIndex_pTjet30       = -999;

    m_HTXS_isZ2vvDecay = -999;
  }

  return StatusCode::SUCCESS;
}

//////////////////// Clear any defined vectors here //////////////////////////// 
StatusCode ZprimeMiniTree::ClearPhotonsUser(const std::string& /*photonName*/)
{
  m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI.clear();
  m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1.clear();
  m_photons_HLT_g60_loose.clear();
  m_photons_HLT_g140_loose.clear();
  m_photons_HLT_g160_loose.clear();

  return StatusCode::SUCCESS;
}

StatusCode ZprimeMiniTree::ClearFatJetsUser(const std::string& /*fatjetName*/)
{
  m_fatjets_ANN_score.clear();
  return StatusCode::SUCCESS;
}

/////////////////// Assign values to defined event variables here ////////////////////////
StatusCode ZprimeMiniTree::FillEventUser(const xAOD::EventInfo* eventInfo, const xAOD::TruthEvent* /*truthEvent*/, const xAOD::VertexContainer* /*vertices*/)
{
  if( eventInfo->isAvailable< float >( "weight" ) )
    m_weight = eventInfo->auxdecor< float >( "weight" );
  if( eventInfo->isAvailable< float >( "weight_xs" ) )
    m_weight_xs = eventInfo->auxdecor< float >( "weight_xs" );

  // truth Z' kinematic info
  if (m_doTruth) {
      if( eventInfo->isAvailable< float >( "Higgs_pt"   ) )
	m_Higgs_pt   = eventInfo->auxdecor< float >( "Higgs_pt"   );
      if( eventInfo->isAvailable< float >( "Higgs_eta"  ) )
	m_Higgs_eta  = eventInfo->auxdecor< float >( "Higgs_eta"  );
      if( eventInfo->isAvailable< float >( "Higgs_phi"  ) )
	m_Higgs_phi  = eventInfo->auxdecor< float >( "Higgs_phi"  );
      if( eventInfo->isAvailable< float >( "Higgs_m"    ) )
	m_Higgs_m    = eventInfo->auxdecor< float >( "Higgs_m"    );
      if( eventInfo->isAvailable< int   >( "Higgs_decay") )
	m_Higgs_decay= eventInfo->auxdecor< int   >( "Higgs_decay");

      if( eventInfo->isAvailable< float >( "Zboson_pt"  ) )
	m_Zboson_pt  = eventInfo->auxdecor< float >( "Zboson_pt"  );
      if( eventInfo->isAvailable< float >( "Zboson_eta" ) )
	m_Zboson_eta = eventInfo->auxdecor< float >( "Zboson_eta" );
      if( eventInfo->isAvailable< float >( "Zboson_phi" ) )
	m_Zboson_phi = eventInfo->auxdecor< float >( "Zboson_phi" );
      if( eventInfo->isAvailable< float >( "Zboson_m"   ) )
	m_Zboson_m   = eventInfo->auxdecor< float >( "Zboson_m"   );

      if( eventInfo->isAvailable< float >( "Wboson_pt"  ) )
	m_Wboson_pt  = eventInfo->auxdecor< float >( "Wboson_pt"  );
      if( eventInfo->isAvailable< float >( "Wboson_eta" ) )
	m_Wboson_eta = eventInfo->auxdecor< float >( "Wboson_eta" );
      if( eventInfo->isAvailable< float >( "Wboson_phi" ) )
	m_Wboson_phi = eventInfo->auxdecor< float >( "Wboson_phi" );
      if( eventInfo->isAvailable< float >( "Wboson_m"   ) )
	m_Wboson_m   = eventInfo->auxdecor< float >( "Wboson_m"   );
      
      if( eventInfo->isAvailable< float >( "Zprime_pt"  ) )
	m_Zprime_pt  = eventInfo->auxdecor< float >( "Zprime_pt"  );
      if( eventInfo->isAvailable< float >( "Zprime_eta" ) )
	m_Zprime_eta = eventInfo->auxdecor< float >( "Zprime_eta" );
      if( eventInfo->isAvailable< float >( "Zprime_phi" ) )
	m_Zprime_phi = eventInfo->auxdecor< float >( "Zprime_phi" );
      if( eventInfo->isAvailable< float >( "Zprime_m"   ) )
	m_Zprime_m   = eventInfo->auxdecor< float >( "Zprime_m"   );
       if( eventInfo->isAvailable< int   >( "Zprime_pdg" ) )
	m_Zprime_pdg = eventInfo->auxdecor< int   >( "Zprime_pdg" );

    if(m_saveZprimeDecay){
      if( eventInfo->isAvailable< float >( "reso0_pt" ) )
	m_reso0_pt = eventInfo->auxdecor< float >( "reso0_pt" );
      if( eventInfo->isAvailable< float >( "reso0_eta" ) )
	m_reso0_eta = eventInfo->auxdecor< float >( "reso0_eta" );
      if( eventInfo->isAvailable< float >( "reso0_phi" ) )
	m_reso0_phi = eventInfo->auxdecor< float >( "reso0_phi" );
      if( eventInfo->isAvailable< float >( "reso0_E" ) )
	m_reso0_E = eventInfo->auxdecor< float >( "reso0_E" );
      
      if( eventInfo->isAvailable< float >( "reso1_pt" ) )
	m_reso1_pt = eventInfo->auxdecor< float >( "reso1_pt" );
      if( eventInfo->isAvailable< float >( "reso1_eta" ) )
	m_reso1_eta = eventInfo->auxdecor< float >( "reso1_eta" );
      if( eventInfo->isAvailable< float >( "reso1_phi" ) )
	m_reso1_phi = eventInfo->auxdecor< float >( "reso1_phi" );
      if( eventInfo->isAvailable< float >( "reso1_E" ) )
	m_reso1_E = eventInfo->auxdecor< float >( "reso1_E" );
      
      if( eventInfo->isAvailable< float >( "ISR_pt" ) )
	m_ISR_pt = eventInfo->auxdecor< float >( "ISR_pt" );
      if( eventInfo->isAvailable< float >( "ISR_eta" ) )
	m_ISR_eta = eventInfo->auxdecor< float >( "ISR_eta" );
      if( eventInfo->isAvailable< float >( "ISR_phi" ) )
	m_ISR_phi = eventInfo->auxdecor< float >( "ISR_phi" );
      if( eventInfo->isAvailable< float >( "ISR_E" ) )
	m_ISR_E = eventInfo->auxdecor< float >( "ISR_E" );
      if( eventInfo->isAvailable< int >( "ISR_pdgId" ) )
	m_ISR_pdgId = eventInfo->auxdecor< int >( "ISR_pdgId" );
    }

    static SG::AuxElement::ConstAccessor<int  > HTXS_prodMode  ("HTXS_prodMode" );
    if( HTXS_prodMode.isAvailable( *eventInfo ) )
      m_HTXS_prodMode = HTXS_prodMode( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_errorCode ("HTXS_errorCode");
    if( HTXS_errorCode.isAvailable( *eventInfo ) )
      m_HTXS_errorCode = HTXS_errorCode( *eventInfo );

    static SG::AuxElement::ConstAccessor<float> HTXS_Higgs_decay_eta ("HTXS_Higgs_decay_eta");
    if( HTXS_Higgs_decay_eta.isAvailable( *eventInfo ) )
      m_HTXS_Higgs_decay_eta = HTXS_Higgs_decay_eta( *eventInfo );
    static SG::AuxElement::ConstAccessor<float> HTXS_Higgs_decay_m ("HTXS_Higgs_decay_m");
    if( HTXS_Higgs_decay_m.isAvailable( *eventInfo ) )
      m_HTXS_Higgs_decay_m   = HTXS_Higgs_decay_m  ( *eventInfo )/1000;
    static SG::AuxElement::ConstAccessor<float> HTXS_Higgs_decay_phi ("HTXS_Higgs_decay_phi");
    if( HTXS_Higgs_decay_phi.isAvailable( *eventInfo ) )
      m_HTXS_Higgs_decay_phi = HTXS_Higgs_decay_phi( *eventInfo );
    static SG::AuxElement::ConstAccessor<float> HTXS_Higgs_decay_pt ("HTXS_Higgs_decay_pt");
    if( HTXS_Higgs_decay_pt.isAvailable( *eventInfo ) )
      m_HTXS_Higgs_decay_pt  = HTXS_Higgs_decay_pt ( *eventInfo )/1000;

    static SG::AuxElement::ConstAccessor<float> HTXS_Higgs_eta ("HTXS_Higgs_eta");
    if( HTXS_Higgs_eta.isAvailable( *eventInfo ) )
      m_HTXS_Higgs_eta = HTXS_Higgs_eta( *eventInfo );
    static SG::AuxElement::ConstAccessor<float> HTXS_Higgs_m ("HTXS_Higgs_m");
    if( HTXS_Higgs_m.isAvailable( *eventInfo ) )
      m_HTXS_Higgs_m   = HTXS_Higgs_m  ( *eventInfo )/1000;
    static SG::AuxElement::ConstAccessor<float> HTXS_Higgs_phi ("HTXS_Higgs_phi");
    if( HTXS_Higgs_phi.isAvailable( *eventInfo ) )
      m_HTXS_Higgs_phi = HTXS_Higgs_phi( *eventInfo );
    static SG::AuxElement::ConstAccessor<float> HTXS_Higgs_pt ("HTXS_Higgs_pt");
    if( HTXS_Higgs_pt.isAvailable( *eventInfo ) )
      m_HTXS_Higgs_pt  = HTXS_Higgs_pt ( *eventInfo )/1000;

    static SG::AuxElement::ConstAccessor<float> HTXS_V_decay_eta ("HTXS_V_decay_eta");
    if( HTXS_V_decay_eta.isAvailable( *eventInfo ) )
      m_HTXS_V_decay_eta = HTXS_V_decay_eta( *eventInfo );
    static SG::AuxElement::ConstAccessor<float> HTXS_V_decay_m   ("HTXS_V_decay_m");
    if( HTXS_V_decay_m  .isAvailable( *eventInfo ) )
      m_HTXS_V_decay_m   = HTXS_V_decay_m  ( *eventInfo )/1000;
    static SG::AuxElement::ConstAccessor<float> HTXS_V_decay_phi ("HTXS_V_decay_phi");
    if( HTXS_V_decay_phi.isAvailable( *eventInfo ) )
      m_HTXS_V_decay_phi = HTXS_V_decay_phi( *eventInfo );
    static SG::AuxElement::ConstAccessor<float> HTXS_V_decay_pt  ("HTXS_V_decay_pt");
    if( HTXS_V_decay_pt .isAvailable( *eventInfo ) )
      m_HTXS_V_decay_pt  = HTXS_V_decay_pt ( *eventInfo )/1000;

    static SG::AuxElement::ConstAccessor<float> HTXS_V_eta ("HTXS_V_eta");
    if( HTXS_V_eta.isAvailable( *eventInfo ) )
      m_HTXS_V_eta = HTXS_V_eta( *eventInfo );
    static SG::AuxElement::ConstAccessor<float> HTXS_V_m   ("HTXS_V_m");
    if( HTXS_V_m  .isAvailable( *eventInfo ) )
      m_HTXS_V_m   = HTXS_V_m  ( *eventInfo )/1000;
    static SG::AuxElement::ConstAccessor<float> HTXS_V_phi ("HTXS_V_phi");
    if( HTXS_V_phi.isAvailable( *eventInfo ) )
      m_HTXS_V_phi = HTXS_V_phi( *eventInfo );
    static SG::AuxElement::ConstAccessor<float> HTXS_V_pt  ("HTXS_V_pt");
    if( HTXS_V_pt .isAvailable( *eventInfo ) )
      m_HTXS_V_pt  = HTXS_V_pt ( *eventInfo )/1000;

    static SG::AuxElement::ConstAccessor<int  > HTXS_Njets_pTjet25 ("HTXS_Njets_pTjet25");
    if( HTXS_Njets_pTjet25.isAvailable( *eventInfo ) )
      m_HTXS_Njets_pTjet25 = HTXS_Njets_pTjet25( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Njets_pTjet30 ("HTXS_Njets_pTjet30");
    if( HTXS_Njets_pTjet30.isAvailable( *eventInfo ) )
      m_HTXS_Njets_pTjet30 = HTXS_Njets_pTjet30( *eventInfo );

    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage0_Category                 ("HTXS_Stage0_Category"                );
    if( HTXS_Stage0_Category.isAvailable( *eventInfo ) )
      m_HTXS_Stage0_Category = HTXS_Stage0_Category( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_2_Category_pTjet25       ("HTXS_Stage1_2_Category_pTjet25"      );
    if( HTXS_Stage1_2_Category_pTjet25.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_2_Category_pTjet25 = HTXS_Stage1_2_Category_pTjet25( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_2_Category_pTjet30       ("HTXS_Stage1_2_Category_pTjet30"      );
    if( HTXS_Stage1_2_Category_pTjet30.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_2_Category_pTjet30 = HTXS_Stage1_2_Category_pTjet30( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_2_FineIndex_pTjet25      ("HTXS_Stage1_2_FineIndex_pTjet25"     );
    if( HTXS_Stage1_2_FineIndex_pTjet25.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_2_FineIndex_pTjet25 = HTXS_Stage1_2_FineIndex_pTjet25( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_2_FineIndex_pTjet30      ("HTXS_Stage1_2_FineIndex_pTjet30"     );
    if( HTXS_Stage1_2_FineIndex_pTjet30.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_2_FineIndex_pTjet30 = HTXS_Stage1_2_FineIndex_pTjet30( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_2_Fine_Category_pTjet25  ("HTXS_Stage1_2_Fine_Category_pTjet25" );
    if( HTXS_Stage1_2_Fine_Category_pTjet25.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_2_Fine_Category_pTjet25 = HTXS_Stage1_2_Fine_Category_pTjet25( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_2_Fine_Category_pTjet30  ("HTXS_Stage1_2_Fine_Category_pTjet30" );
    if( HTXS_Stage1_2_Fine_Category_pTjet30.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_2_Fine_Category_pTjet30 = HTXS_Stage1_2_Fine_Category_pTjet30( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_2_Fine_FineIndex_pTjet25 ("HTXS_Stage1_2_Fine_FineIndex_pTjet25");
    if( HTXS_Stage1_2_Fine_FineIndex_pTjet25.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_2_Fine_FineIndex_pTjet25 = HTXS_Stage1_2_Fine_FineIndex_pTjet25( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_2_Fine_FineIndex_pTjet30 ("HTXS_Stage1_2_Fine_FineIndex_pTjet30");
    if( HTXS_Stage1_2_Fine_FineIndex_pTjet30.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_2_Fine_FineIndex_pTjet30 = HTXS_Stage1_2_Fine_FineIndex_pTjet30( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_Category_pTjet25         ("HTXS_Stage1_Category_pTjet25"        );
    if( HTXS_Stage1_Category_pTjet25.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_Category_pTjet25 = HTXS_Stage1_Category_pTjet25( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_Category_pTjet30         ("HTXS_Stage1_Category_pTjet30"        );
    if( HTXS_Stage1_Category_pTjet30.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_Category_pTjet30 = HTXS_Stage1_Category_pTjet30( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_FineIndex_pTjet25        ("HTXS_Stage1_FineIndex_pTjet25"       );
    if( HTXS_Stage1_FineIndex_pTjet25.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_FineIndex_pTjet25 = HTXS_Stage1_FineIndex_pTjet25( *eventInfo );
    static SG::AuxElement::ConstAccessor<int  > HTXS_Stage1_FineIndex_pTjet30        ("HTXS_Stage1_FineIndex_pTjet30"       );
    if( HTXS_Stage1_FineIndex_pTjet30.isAvailable( *eventInfo ) )
      m_HTXS_Stage1_FineIndex_pTjet30 = HTXS_Stage1_FineIndex_pTjet30( *eventInfo );

    static SG::AuxElement::ConstAccessor<int  > HTXS_isZ2vvDecay ("HTXS_isZ2vvDecay");
    if( HTXS_isZ2vvDecay.isAvailable( *eventInfo ) )
      m_HTXS_isZ2vvDecay = HTXS_isZ2vvDecay( *eventInfo );
  }

  return StatusCode::SUCCESS;
}

StatusCode ZprimeMiniTree::FillPhotonUser( const xAOD::Photon* ph_itr, const std::string& /*photonName*/)
{
  static SG::AuxElement::ConstAccessor< char > HLT_g60 ("DFCommonTrigMatch_HLT_g60_loose" );
  static SG::AuxElement::ConstAccessor< char > HLT_g140("DFCommonTrigMatch_HLT_g140_loose");
  static SG::AuxElement::ConstAccessor< char > HLT_g160("DFCommonTrigMatch_HLT_g160_loose");

  static SG::AuxElement::ConstAccessor< char > HLT_g75 ("DFCommonTrigMatch_HLT_g75_tight_3j25noL1_L1EM22VHI");
  static SG::AuxElement::ConstAccessor< char > HLT_g85 ("DFCommonTrigMatch_HLT_g85_tight_L1EM22VHI_3j50noL1");

  if (HLT_g60.isAvailable( *ph_itr ))
    m_photons_HLT_g60_loose.push_back(HLT_g60( *ph_itr ));
  else
    m_photons_HLT_g60_loose.push_back(-1);

  if (HLT_g140.isAvailable( *ph_itr ))
    m_photons_HLT_g140_loose.push_back(HLT_g140( *ph_itr ));
  else
    m_photons_HLT_g140_loose.push_back(-1);

  if (HLT_g160.isAvailable( *ph_itr ))
    m_photons_HLT_g160_loose.push_back(HLT_g160( *ph_itr ));
  else
    m_photons_HLT_g160_loose.push_back(-1);

  if (HLT_g75.isAvailable( *ph_itr ))
    m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI.push_back(HLT_g75( *ph_itr ));
  else
    m_photons_HLT_g75_tight_3j25noL1_L1EM22VHI.push_back(-1);

  if (HLT_g85.isAvailable( *ph_itr ))
    m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1.push_back(HLT_g85( *ph_itr ));
  else
    m_photons_HLT_g85_tight_L1EM22VHI_3j50noL1.push_back(-1);

  return StatusCode::SUCCESS;
}

StatusCode ZprimeMiniTree::FillFatJetUser(const xAOD::Jet* fatjet, int /*pvLocation = 0*/, const std::string& /*fatjetName = "fatjet"*/)
{
  static SG::AuxElement::ConstAccessor< float > ANNTagger_Score("ANNTagger_Score");

  if(m_saveExtraVariables)
    {
      if(ANNTagger_Score.isAvailable(*fatjet))
	m_fatjets_ANN_score.push_back(ANNTagger_Score(*fatjet));
      else
	m_fatjets_ANN_score.push_back(-999.);      
    }

  return StatusCode::SUCCESS;
}
