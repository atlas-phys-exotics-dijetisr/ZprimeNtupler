#include "ZprimeNtupler/TauJetNtupler.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

TauJetNtupler::TauJetNtupler(const std::string& name, const std::string& detailStr, float units, bool mc, bool storeSystSFs)
  : ParticleNtupler(name, detailStr, units, mc, true, storeSystSFs)
{

  // trigger
  if ( m_infoSwitch.m_trigger ) {
    m_isTrigMatched          =std::make_shared<std::vector<int>               >();
    m_isTrigMatchedToChain   =std::make_shared<std::vector<std::vector<int> > >();
    m_listTrigChains         =std::make_shared<std::vector<std::string>       >();
  }
  
  if( m_infoSwitch.m_kinematic) {
    m_ntrk    = std::make_shared<std::vector<int>   >();
    m_charge  = std::make_shared<std::vector<float> >();
  }
  
  // might need to delete these  
  if( m_infoSwitch.m_JetID) {
    m_isJetBDTSigVeryLoose = std::make_shared<std::vector<int>   >();
    m_isJetBDTSigLoose     = std::make_shared<std::vector<int>   >();
    m_isJetBDTSigMedium    = std::make_shared<std::vector<int>   >();
    m_isJetBDTSigTight     = std::make_shared<std::vector<int>   >();
    
    m_JetBDTScore          = std::make_shared<std::vector<float>   >();
    m_JetBDTScoreSigTrans  = std::make_shared<std::vector<float>   >();
  }

  if( m_infoSwitch.m_EleVeto) {
    m_isEleBDTLoose  = std::make_shared<std::vector<int>   >();
    m_isEleBDTMedium = std::make_shared<std::vector<int>   >();
    m_isEleBDTTight  = std::make_shared<std::vector<int>   >();
    
    m_EleBDTScore    = std::make_shared<std::vector<float> >();
    m_passEleOLR     = std::make_shared<std::vector<int>   >();
  }

  if( m_infoSwitch.m_xahTauJetMatching) {
    m_tau_matchedJetWidth = std::make_shared<std::vector<float>   >();
    m_tau_matchedJetJvt   = std::make_shared<std::vector<float>   >();
  }

  if( m_infoSwitch.m_trackAll) {
    m_tau_tracks_pt       = std::make_shared<std::vector< std::vector<float> > >();
    m_tau_tracks_eta      = std::make_shared<std::vector< std::vector<float> > >();
    m_tau_tracks_phi      = std::make_shared<std::vector< std::vector<float> > >();

    m_tau_tracks_isCore          = std::make_shared<std::vector< std::vector<int> > >();
    m_tau_tracks_isWide          = std::make_shared<std::vector< std::vector<int> > >();
    m_tau_tracks_failTrackFilter = std::make_shared<std::vector< std::vector<int> > >();
    m_tau_tracks_passTrkSel      = std::make_shared<std::vector< std::vector<int> > >();
    m_tau_tracks_isClCharged     = std::make_shared<std::vector< std::vector<int> > >();
    m_tau_tracks_isClIso         = std::make_shared<std::vector< std::vector<int> > >();
    m_tau_tracks_isClConv        = std::make_shared<std::vector< std::vector<int> > >();
    m_tau_tracks_isClFake        = std::make_shared<std::vector< std::vector<int> > >();
  }

  // scale factors w/ sys
  // per object
  if ( m_infoSwitch.m_effSF && m_mc )
    {
      for (const std::string& tauEffWP : m_infoSwitch.m_tauEffWPs)
	{
	  m_TauEff_SF    [tauEffWP] =std::make_shared< std::vector<std::vector<float>> >();
	  m_TauTrigEff_SF[tauEffWP] =std::make_shared< std::vector<std::vector<float>> >();
	}
    }
}

StatusCode TauJetNtupler::createBranches(TTree *tree)
{
  ANA_CHECK(ParticleNtupler::createBranches(tree));

  if ( m_infoSwitch.m_trigger ){
    // this is true if there's a match for at least one trigger chain
    createBranch<int>(tree,"isTrigMatched", m_isTrigMatched);
    // a vector of trigger match decision for each tau trigger chain
    createBranch<std::vector<int> >(tree,"isTrigMatchedToChain", m_isTrigMatchedToChain );
    // a vector of strings for each tau trigger chain - 1:1 correspondence w/ vector above
    createBranch<std::string>(tree, "listTrigChains", m_listTrigChains );
  }

  if ( m_infoSwitch.m_kinematic )
    {
      createBranch<int>  (tree,"ntrk"  , m_ntrk  );
      createBranch<float>(tree,"charge", m_charge);
  }

  if ( m_infoSwitch.m_effSF && m_mc ) {
    
    for (const std::string& tauEffWP : m_infoSwitch.m_tauEffWPs)
      {
	createBranch< std::vector<float> >(tree, "TauEff_SF_"     + tauEffWP, m_TauEff_SF    [tauEffWP]);
	createBranch< std::vector<float> >(tree, "TauTrigEff_SF_" + tauEffWP, m_TauTrigEff_SF[tauEffWP]);
      }
  }

  // might need to delete these
  if ( m_infoSwitch.m_JetID ){
    createBranch<int>   (tree,"isJetBDTSigVeryLoose", m_isJetBDTSigVeryLoose);
    createBranch<int>   (tree,"isJetBDTSigLoose", m_isJetBDTSigLoose);
    createBranch<int>   (tree,"isJetBDTSigMedium", m_isJetBDTSigMedium);
    createBranch<int>   (tree,"isJetBDTSigTight", m_isJetBDTSigTight);
    
    createBranch<float> (tree,"JetBDTScore", m_JetBDTScore);
    createBranch<float> (tree,"JetBDTScoreSigTrans", m_JetBDTScoreSigTrans);

  }
  
  if ( m_infoSwitch.m_EleVeto ){
    createBranch<int>   (tree,"isEleBDTLoose", m_isEleBDTLoose);
    createBranch<int>   (tree,"isEleBDTMedium", m_isEleBDTMedium);
    createBranch<int>   (tree,"isEleBDTTight", m_isEleBDTTight);
    
    createBranch<float> (tree,"EleBDTScore", m_EleBDTScore);

    createBranch<int>   (tree,"passEleOLR", m_passEleOLR);
  }
 
  if( m_infoSwitch.m_xahTauJetMatching) {
    createBranch<float>  (tree, "matchedJetWidth",    m_tau_matchedJetWidth);
    createBranch<float>  (tree, "matchedJetJvt",    m_tau_matchedJetJvt);
  } 
  
  if( m_infoSwitch.m_trackAll) {
    tree->Branch( (m_name + "_tracks_pt").c_str() , &m_tau_tracks_pt );
    tree->Branch( (m_name + "_tracks_eta").c_str() , &m_tau_tracks_eta );
    tree->Branch( (m_name + "_tracks_phi").c_str() , &m_tau_tracks_phi );
    
    tree->Branch( (m_name + "_tracks_isCore").c_str() , &m_tau_tracks_isCore );
    tree->Branch( (m_name + "_tracks_isWide").c_str() , &m_tau_tracks_isWide );
    tree->Branch( (m_name + "_tracks_failTrackFilter").c_str() , &m_tau_tracks_failTrackFilter );
    tree->Branch( (m_name + "_tracks_passTrkSel").c_str() , &m_tau_tracks_passTrkSel );
    tree->Branch( (m_name + "_tracks_isClCharged").c_str() , &m_tau_tracks_isClCharged );
    tree->Branch( (m_name + "_tracks_isClIso").c_str() , &m_tau_tracks_isClIso );
    tree->Branch( (m_name + "_tracks_isClConv").c_str() , &m_tau_tracks_isClConv );
    tree->Branch( (m_name + "_tracks_isClFake").c_str() , &m_tau_tracks_isClFake );
  }

  return StatusCode::SUCCESS;
}

StatusCode TauJetNtupler::clear()
{  
  ANA_CHECK(ParticleNtupler::clear());

  if ( m_infoSwitch.m_trigger ) {
    m_isTrigMatched->clear();
    m_isTrigMatchedToChain->clear();
    m_listTrigChains->clear();
  }

  if ( m_infoSwitch.m_kinematic ) {
    m_ntrk->clear();
    m_charge->clear();
  }
 
  if ( m_infoSwitch.m_effSF && m_mc )
    {
      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>>> > kv : m_TauEff_SF)
	kv.second->clear();

      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>>> > kv : m_TauTrigEff_SF)
	kv.second->clear();
  }

  // might need to delete these
  if ( m_infoSwitch.m_JetID ) {
    m_isJetBDTSigVeryLoose->clear();
    m_isJetBDTSigLoose->clear();
    m_isJetBDTSigMedium->clear();
    m_isJetBDTSigTight->clear();
    
    m_JetBDTScore->clear();
    m_JetBDTScoreSigTrans->clear();
  }

  if ( m_infoSwitch.m_EleVeto ) {
    m_isEleBDTLoose->clear();
    m_isEleBDTMedium->clear();
    m_isEleBDTTight->clear();
    
    m_EleBDTScore->clear();
    m_passEleOLR->clear();
  }

  if( m_infoSwitch.m_xahTauJetMatching) {
    m_tau_matchedJetWidth->clear();
    m_tau_matchedJetJvt->clear();
  }

  if( m_infoSwitch.m_trackAll) {
    m_tau_tracks_pt->clear();
    m_tau_tracks_eta->clear();
    m_tau_tracks_phi->clear();

    m_tau_tracks_isCore->clear();
    m_tau_tracks_isWide->clear();
    m_tau_tracks_failTrackFilter->clear();
    m_tau_tracks_passTrkSel->clear();
    m_tau_tracks_isClCharged->clear();
    m_tau_tracks_isClIso->clear();
    m_tau_tracks_isClConv->clear();
    m_tau_tracks_isClFake->clear();
  }

  return StatusCode::SUCCESS;
}


StatusCode TauJetNtupler::FillTauJet( const xAOD::TauJet* tau )
{
  return FillTauJet(static_cast<const xAOD::IParticle*>(tau));
}

StatusCode TauJetNtupler::FillTauJet( const xAOD::IParticle* particle )
{
  ANA_CHECK(ParticleNtupler::FillParticle(particle));

  const xAOD::TauJet* tau=dynamic_cast<const xAOD::TauJet*>(particle);

  if ( m_infoSwitch.m_trigger )
    {

      // retrieve map<string,char> w/ <chain,isMatched>
      //
      static SG::AuxElement::ConstAccessor< std::map<std::string,char> > isTrigMatchedMapTau("isTrigMatchedMapTau");

      std::vector<int> matches;

      if ( isTrigMatchedMapTau.isAvailable( *tau ) )
	{
	  // loop over map and fill branches
	  //
	  for ( auto const &it : (isTrigMatchedMapTau( *tau )) )
	    {
	      matches.push_back( static_cast<int>(it.second) );
	      m_listTrigChains->push_back( it.first );
	    }
	}
      else
	{
	  matches.push_back( -1 );
	  m_listTrigChains->push_back("NONE");
	}

      m_isTrigMatchedToChain->push_back(matches);

      // if at least one match among the chains is found, say this tau is trigger matched
      if ( std::find(matches.begin(), matches.end(), 1) != matches.end() ) { m_isTrigMatched->push_back(1); }
      else { m_isTrigMatched->push_back(0); }

    }

  if ( m_infoSwitch.m_kinematic )
    {
      m_charge->push_back( tau->charge()  );
      m_ntrk  ->push_back( tau->nTracks() );
    }

  if ( m_infoSwitch.m_effSF && m_mc )
    {
      static const std::vector<float> junkSF(1,-1.0);
    
      for (const std::string& tauEffWP : m_infoSwitch.m_tauEffWPs)
	{
	  SG::AuxElement::ConstAccessor< std::vector< float > > TauEff_SF_syst("TauEff_SF_syst_" + tauEffWP);
	  safeSFVecFill<float, xAOD::TauJet>( tau, TauEff_SF_syst, m_TauEff_SF[ tauEffWP ], junkSF );
	}

      for (const std::string& trigWP : m_infoSwitch.m_trigWPs)
	{
	  SG::AuxElement::ConstAccessor< std::vector< float > > TauEff_SF_syst("TauEff_SF_syst_" + trigWP);
	  safeSFVecFill<float, xAOD::TauJet>( tau, TauEff_SF_syst, m_TauTrigEff_SF[ trigWP ], junkSF );
	}

    }

  // might need to delete these
  if ( m_infoSwitch.m_JetID ) {
    
    static SG::AuxElement::Accessor<int> isJetBDTSigVeryLooseAcc ("isJetBDTSigVeryLoose");
    safeFill<int, int, xAOD::TauJet>(tau, isJetBDTSigVeryLooseAcc, m_isJetBDTSigVeryLoose, -1);

    static SG::AuxElement::Accessor<int> isJetBDTSigLooseAcc ("isJetBDTSigLoose");
    safeFill<int, int, xAOD::TauJet>(tau, isJetBDTSigLooseAcc, m_isJetBDTSigLoose, -1);

    static SG::AuxElement::Accessor<int> isJetBDTSigMediumAcc ("isJetBDTSigMedium");
    safeFill<int, int, xAOD::TauJet>(tau, isJetBDTSigMediumAcc, m_isJetBDTSigMedium, -1);

    static SG::AuxElement::Accessor<int> isJetBDTSigTightAcc ("isJetBDTSigTight");
    safeFill<int, int, xAOD::TauJet>(tau, isJetBDTSigTightAcc, m_isJetBDTSigTight, -1);
    
    static SG::AuxElement::Accessor<float> JetBDTScoreAcc ("JetBDTScore");
    safeFill<float, float, xAOD::TauJet>(tau, JetBDTScoreAcc, m_JetBDTScore, -999.);

    static SG::AuxElement::Accessor<float> JetBDTScoreSigTransAcc ("JetBDTScoreSigTrans");
    safeFill<float, float, xAOD::TauJet>(tau, JetBDTScoreSigTransAcc, m_JetBDTScoreSigTrans, -999.);
  }

  if ( m_infoSwitch.m_EleVeto ) {
    
    static SG::AuxElement::Accessor<int> isEleBDTLooseAcc ("isEleBDTLoose");
    safeFill<int, int, xAOD::TauJet>(tau, isEleBDTLooseAcc, m_isEleBDTLoose, -1);

    static SG::AuxElement::Accessor<int> isEleBDTMediumAcc ("isEleBDTMedium");
    safeFill<int, int, xAOD::TauJet>(tau, isEleBDTMediumAcc, m_isEleBDTMedium, -1);

    static SG::AuxElement::Accessor<int> isEleBDTTightAcc ("isEleBDTTight");
    safeFill<int, int, xAOD::TauJet>(tau, isEleBDTTightAcc, m_isEleBDTTight, -1);

    static SG::AuxElement::Accessor<float> EleBDTScoreAcc ("EleBDTScore");
    safeFill<float, float, xAOD::TauJet>(tau, EleBDTScoreAcc, m_EleBDTScore, -999.);

    static SG::AuxElement::Accessor<int> passEleOLRAcc ("passEleOLR");
    safeFill<int, int, xAOD::TauJet>(tau, passEleOLRAcc, m_passEleOLR, -1);
  }

  if( m_infoSwitch.m_xahTauJetMatching) {
    static SG::AuxElement::Accessor< float > jetWidthAcc("JetWidth");
    safeFill<float, float, xAOD::TauJet>(tau, jetWidthAcc, m_tau_matchedJetWidth, -1.);
    static SG::AuxElement::Accessor< float > jetJvtAcc("JetJvt");
    safeFill<float, float, xAOD::TauJet>(tau, jetJvtAcc, m_tau_matchedJetJvt, -1.);
  }

  if( m_infoSwitch.m_trackAll) {
    static SG::AuxElement::ConstAccessor< std::vector<float>   >   tauTrackPtAcc("trackPt");
    safeVecFill<float, float, xAOD::TauJet>(tau, tauTrackPtAcc, m_tau_tracks_pt);
    
    static SG::AuxElement::ConstAccessor< std::vector<float>   >   tauTrackEtaAcc("trackEta");
    safeVecFill<float, float, xAOD::TauJet>(tau, tauTrackEtaAcc, m_tau_tracks_eta);
    
    static SG::AuxElement::ConstAccessor< std::vector<float>   >   tauTrackPhiAcc("trackPhi");
    safeVecFill<float, float, xAOD::TauJet>(tau, tauTrackPhiAcc, m_tau_tracks_phi);
    
    // track classification
    static SG::AuxElement::ConstAccessor< std::vector<int>   >   tauTrackIsCoreAcc("trackIsCore");
    safeVecFill<int, int, xAOD::TauJet>(tau, tauTrackIsCoreAcc, m_tau_tracks_isCore);
    
    static SG::AuxElement::ConstAccessor< std::vector<int>   >   tauTrackIsWideAcc("trackIsWide");
    safeVecFill<int, int, xAOD::TauJet>(tau, tauTrackIsWideAcc, m_tau_tracks_isWide);
    
    static SG::AuxElement::ConstAccessor< std::vector<int>   >   tauTrackFailTrackFilterAcc("trackFailTrackFilter");
    safeVecFill<int, int, xAOD::TauJet>(tau, tauTrackFailTrackFilterAcc, m_tau_tracks_failTrackFilter);
    
    static SG::AuxElement::ConstAccessor< std::vector<int>   >   tauTrackPassTrkSelAcc("trackPassTrkSel");
    safeVecFill<int, int, xAOD::TauJet>(tau, tauTrackPassTrkSelAcc, m_tau_tracks_passTrkSel);
    
    static SG::AuxElement::ConstAccessor< std::vector<int>   >   tauTrackIsClChargedAcc("trackIsClCharged");
    safeVecFill<int, int, xAOD::TauJet>(tau, tauTrackIsClChargedAcc, m_tau_tracks_isClCharged);
    
    static SG::AuxElement::ConstAccessor< std::vector<int>   >   tauTrackIsClIsoAcc("trackIsClIso");
    safeVecFill<int, int, xAOD::TauJet>(tau, tauTrackIsClIsoAcc, m_tau_tracks_isClIso);
    
    static SG::AuxElement::ConstAccessor< std::vector<int>   >   tauTrackIsClConvAcc("trackIsClConv");
    safeVecFill<int, int, xAOD::TauJet>(tau, tauTrackIsClConvAcc, m_tau_tracks_isClConv);
    
    static SG::AuxElement::ConstAccessor< std::vector<int>   >   tauTrackIsClFakeAcc("trackIsClFake");
    safeVecFill<int, int, xAOD::TauJet>(tau, tauTrackIsClFakeAcc, m_tau_tracks_isClFake);
  }

  return StatusCode::SUCCESS;
}
