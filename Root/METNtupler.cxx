#include "ZprimeNtupler/METNtupler.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

METNtupler::METNtupler(const std::string& name, const std::string& detailStr, float units)
  : m_name(name), m_infoSwitch(detailStr), m_units(units)
{ }

StatusCode METNtupler::createBranches(TTree *tree)
{
  if ( m_infoSwitch.m_metClus || !m_infoSwitch.m_metTrk )
    {
      createBranch(tree, "FinalClus",         &m_metFinalClus,      "F");
      createBranch(tree, "FinalClusPx",       &m_metFinalClusPx,    "F");
      createBranch(tree, "FinalClusPy",       &m_metFinalClusPy,    "F");
      createBranch(tree, "FinalClusSumEt",    &m_metFinalClusSumEt, "F");
      createBranch(tree, "FinalClusPhi",      &m_metFinalClusPhi,   "F");
    }

  if ( m_infoSwitch.m_sigClus ) {
    createBranch(tree, "FinalClusOverSqrtSumEt",  &m_metFinalClusOverSqrtSumEt,  "/F");
    createBranch(tree, "FinalClusOverSqrtHt",     &m_metFinalClusOverSqrtHt,     "/F");
    createBranch(tree, "FinalClusSignificance",   &m_metFinalClusSignificance,   "/F");
    createBranch(tree, "FinalClusSigDirectional", &m_metFinalClusSigDirectional, "/F");
  }

  if ( m_infoSwitch.m_sigResolutionClus ) {
    createBranch(tree, "FinalClusRho",  &m_metFinalClusRho,  "F");
    createBranch(tree, "FinalClusVarL", &m_metFinalClusVarL, "F");
    createBranch(tree, "FinalClusVarT", &m_metFinalClusVarT, "F");
  }

  if ( m_infoSwitch.m_metTrk || !m_infoSwitch.m_metClus ) {
    createBranch(tree, "FinalTrk",          &m_metFinalTrk,       "F");
    createBranch(tree, "FinalTrkPx",        &m_metFinalTrkPx,     "F");
    createBranch(tree, "FinalTrkPy",        &m_metFinalTrkPy,     "F");
    createBranch(tree, "FinalTrkSumEt",     &m_metFinalTrkSumEt,  "F");
    createBranch(tree, "FinalTrkPhi",       &m_metFinalTrkPhi,    "F");
  }

  if ( m_infoSwitch.m_sigTrk ) {
    createBranch(tree, "FinalTrkOverSqrtSumEt",  &m_metFinalTrkOverSqrtSumEt,  "F");
    createBranch(tree, "FinalTrkOverSqrtHt",     &m_metFinalTrkOverSqrtHt,     "F");
    createBranch(tree, "FinalTrkSignificance",   &m_metFinalTrkSignificance,   "F");
    createBranch(tree, "FinalTrkSigDirectional", &m_metFinalTrkSigDirectional, "F");
  }

  if ( m_infoSwitch.m_sigResolutionTrk ) {
    createBranch(tree, "FinalTrkRho",  &m_metFinalTrkRho,  "F");
    createBranch(tree, "FinalTrkVarL", &m_metFinalTrkVarL, "F");
    createBranch(tree, "FinalTrkVarT", &m_metFinalTrkVarT, "F");
  }

  if ( m_infoSwitch.m_refEle ) {
    createBranch(tree, "Ele",             &m_metEle,            "F");
    createBranch(tree, "EleSumEt",        &m_metEleSumEt,       "F");
    createBranch(tree, "ElePhi",          &m_metElePhi,         "F");
  }

  if ( m_infoSwitch.m_refGamma ) {
    createBranch(tree, "Gamma",           &m_metGamma,          "F");
    createBranch(tree, "GammaSumEt",      &m_metGammaSumEt,     "F");
    createBranch(tree, "GammaPhi",        &m_metGammaPhi,       "F");
  }

  if ( m_infoSwitch.m_refTau ) {
    createBranch(tree, "Tau",             &m_metTau,            "F");
    createBranch(tree, "TauSumEt",        &m_metTauSumEt,       "F");
    createBranch(tree, "TauPhi",          &m_metTauPhi,         "F");
  }

  if ( m_infoSwitch.m_refMuons ) {
    createBranch(tree, "Muons",           &m_metMuons,          "F");
    createBranch(tree, "MuonsSumEt",      &m_metMuonsSumEt,     "F");
    createBranch(tree, "MuonsPhi",        &m_metMuonsPhi,       "F");
  }

  if ( m_infoSwitch.m_refJet ) {
    createBranch(tree, "Jet",             &m_metJet,            "F");
    createBranch(tree, "JetSumEt",        &m_metJetSumEt,       "F");
    createBranch(tree, "JetPhi",          &m_metJetPhi,         "F");
  }

  if ( m_infoSwitch.m_refJetTrk ) {
    createBranch(tree, "JetTrk",          &m_metJetTrk,         "F");
    createBranch(tree, "JetTrkSumEt",     &m_metJetTrkSumEt,    "F");
    createBranch(tree, "JetTrkPhi",       &m_metJetTrkPhi,      "F");
  }

  if ( m_infoSwitch.m_softClus) {
    createBranch(tree, "SoftClus",        &m_metSoftClus,       "F");
    createBranch(tree, "SoftClusSumEt",   &m_metSoftClusSumEt,  "F");
    createBranch(tree, "SoftClusPhi",     &m_metSoftClusPhi,    "F");
  }

  if ( m_infoSwitch.m_softTrk) {
    createBranch(tree, "SoftTrk",         &m_metSoftTrk,        "F");
    createBranch(tree, "SoftTrkSumEt",    &m_metSoftTrkSumEt,   "F");
    createBranch(tree, "SoftTrkPhi",      &m_metSoftTrkPhi,     "F");
  }

  return StatusCode::SUCCESS;
}


StatusCode METNtupler::clear()
{
  if ( m_infoSwitch.m_metClus || !m_infoSwitch.m_metTrk ) {
    m_metFinalClus      = -999;
    m_metFinalClusPx    = -999;
    m_metFinalClusPy    = -999;
    m_metFinalClusSumEt = -999;
    m_metFinalClusPhi   = -999;
  }

  if ( m_infoSwitch.m_metTrk || !m_infoSwitch.m_metClus ) {
    m_metFinalTrk	      = -999;
    m_metFinalTrkPx     = -999;
    m_metFinalTrkPy     = -999;
    m_metFinalTrkSumEt  = -999;
    m_metFinalTrkPhi    = -999;
  }

  if ( m_infoSwitch.m_sigClus ) {
    m_metFinalClusOverSqrtSumEt  = -999;
    m_metFinalClusOverSqrtHt     = -999;
    m_metFinalClusSignificance   = -999;
    m_metFinalClusSigDirectional = -999;
  }
  if ( m_infoSwitch.m_sigTrk ) {
    m_metFinalTrkOverSqrtSumEt  = -999;
    m_metFinalTrkOverSqrtHt     = -999;
    m_metFinalTrkSignificance   = -999;
    m_metFinalTrkSigDirectional = -999;
  }
  if ( m_infoSwitch.m_sigResolutionClus ) {
    m_metFinalClusRho  = -999;
    m_metFinalClusVarL = -999;
    m_metFinalClusVarT = -999;
  }
  if ( m_infoSwitch.m_sigResolutionTrk ) {
    m_metFinalTrkRho  = -999;
    m_metFinalTrkVarL = -999;
    m_metFinalTrkVarT = -999;
  }

  if ( m_infoSwitch.m_refEle ) {
    m_metEle = m_metEleSumEt = m_metElePhi = -999;
  }
  if ( m_infoSwitch.m_refGamma ) {
    m_metGamma = m_metGammaSumEt = m_metGammaPhi = -999;
  }
  if ( m_infoSwitch.m_refTau ) {
    m_metTau = m_metTauSumEt = m_metTauPhi = -999;
  }
  if ( m_infoSwitch.m_refMuons ) {
    m_metMuons = m_metMuonsSumEt = m_metMuonsPhi = -999;
  }
  if ( m_infoSwitch.m_refJet ) {
    m_metJet = m_metJetSumEt = m_metJetPhi = -999;
  }
  if ( m_infoSwitch.m_refJetTrk ) {
    m_metJetTrk = m_metJetTrkSumEt = m_metJetTrkPhi = -999;
  }
  if ( m_infoSwitch.m_softClus) {
    m_metSoftClus = m_metSoftClusSumEt = m_metSoftClusPhi = -999;
  }

  return StatusCode::SUCCESS;
}

StatusCode METNtupler::FillMET( const xAOD::MissingETContainer* met)
{
  if ( m_infoSwitch.m_metClus || !m_infoSwitch.m_metTrk )
    {
      const xAOD::MissingET* final_clus = *met->find("FinalClus"); // ("FinalClus" uses the calocluster-based soft terms, "FinalTrk" uses the track-based onleares)
      m_metFinalClus      = final_clus->met() / m_units;
      m_metFinalClusPx    = final_clus->mpx() / m_units;
      m_metFinalClusPy    = final_clus->mpy() / m_units;
      m_metFinalClusSumEt = final_clus->sumet() / m_units;
      m_metFinalClusPhi   = final_clus->phi();

      if ( m_infoSwitch.m_sigClus )
	{
	  m_metFinalClusOverSqrtSumEt  = final_clus->auxdecor<double>("METOverSqrtSumET");
	  m_metFinalClusOverSqrtHt     = final_clus->auxdecor<double>("METOverSqrtHT");
	  m_metFinalClusSignificance   = final_clus->auxdecor<double>("Significance");
	  m_metFinalClusSigDirectional = final_clus->auxdecor<double>("SigDirectional");
	}
    
      if ( m_infoSwitch.m_sigResolutionClus )
	{
	  m_metFinalClusRho  = final_clus->auxdecor<double>("Rho");
	  m_metFinalClusVarL = final_clus->auxdecor<double>("VarL");
	  m_metFinalClusVarT = final_clus->auxdecor<double>("VarT");
	}
    }

  if ( m_infoSwitch.m_metTrk || !m_infoSwitch.m_metClus ) {
    const xAOD::MissingET* final_trk = *met->find("FinalTrk"); // ("FinalClus" uses the calocluster-based soft terms, "FinalTrk" uses the track-based ones)
    m_metFinalTrk       = final_trk->met() / m_units;
    m_metFinalTrkPx     = final_trk->mpx() / m_units;
    m_metFinalTrkPy     = final_trk->mpy() / m_units;
    m_metFinalTrkSumEt  = final_trk->sumet() / m_units;
    m_metFinalTrkPhi    = final_trk->phi();

    if ( m_infoSwitch.m_sigTrk ) {
      m_metFinalTrkOverSqrtSumEt  = final_trk->auxdecor<double>("METOverSqrtSumET");
      m_metFinalTrkOverSqrtHt     = final_trk->auxdecor<double>("METOverSqrtHT");
      m_metFinalTrkSignificance   = final_trk->auxdecor<double>("Significance");
      m_metFinalTrkSigDirectional = final_trk->auxdecor<double>("SigDirectional");
    }

    if ( m_infoSwitch.m_sigResolutionTrk ) {
      m_metFinalTrkRho  = final_trk->auxdecor<double>("Rho");
      m_metFinalTrkVarL = final_trk->auxdecor<double>("VarL");
      m_metFinalTrkVarT = final_trk->auxdecor<double>("VarT");
    }
  }

  if ( m_infoSwitch.m_refEle ) {
    const xAOD::MissingET* ref_ele       = *met->find("RefEle");
    m_metEle         		         = ref_ele->met() / m_units;
    m_metEleSumEt    		         = ref_ele->sumet() / m_units;
    m_metElePhi      		         = ref_ele->phi();
  }

  if ( m_infoSwitch.m_refGamma ) {
    const xAOD::MissingET* ref_gamma     = *met->find("RefGamma");
    m_metGamma             	         = ref_gamma->met() / m_units;
    m_metGammaSumEt        	         = ref_gamma->sumet() / m_units;
    m_metGammaPhi          	         = ref_gamma->phi();
  }

  if ( m_infoSwitch.m_refTau ) {
    const xAOD::MissingET* ref_tau  	 = *met->find("RefTau");
    m_metTau             	    	 = ref_tau->met() / m_units;
    m_metTauSumEt        	    	 = ref_tau->sumet() / m_units;
    m_metTauPhi          	    	 = ref_tau->phi();
  }

  if ( m_infoSwitch.m_refMuons ) {
    const xAOD::MissingET* ref_muon  	 = *met->find("Muons");
    m_metMuons             	     	 = ref_muon->met() / m_units;
    m_metMuonsSumEt        	     	 = ref_muon->sumet() / m_units;
    m_metMuonsPhi          	     	 = ref_muon->phi();
  }

  if ( m_infoSwitch.m_refJet ) {
    const xAOD::MissingET* ref_jet  	 = *met->find("RefJet");
    m_metJet             	     	 = ref_jet->met() / m_units;
    m_metJetSumEt        	     	 = ref_jet->sumet() / m_units;
    m_metJetPhi          	     	 = ref_jet->phi();
  }

  if ( m_infoSwitch.m_refJetTrk ) {
    const xAOD::MissingET* ref_jet_trk   = *met->find("RefJetTrk");
    m_metJetTrk             	     	 = ref_jet_trk->met() / m_units;
    m_metJetTrkSumEt        	     	 = ref_jet_trk->sumet() / m_units;
    m_metJetTrkPhi          	     	 = ref_jet_trk->phi();
  }

  if ( m_infoSwitch.m_softClus) {
    const xAOD::MissingET* ref_soft_clus = *met->find("SoftClus");
    m_metSoftClus            		 = ref_soft_clus->met() / m_units;
    m_metSoftClusSumEt       		 = ref_soft_clus->sumet() / m_units;
    m_metSoftClusPhi         		 = ref_soft_clus->phi();
  }

  if ( m_infoSwitch.m_softTrk) {
    const xAOD::MissingET* ref_soft_trk  = *met->find("PVSoftTrk");
    m_metSoftTrk             		 = ref_soft_trk->met() / m_units;
    m_metSoftTrkSumEt        		 = ref_soft_trk->sumet() / m_units;
    m_metSoftTrkPhi          		 = ref_soft_trk->phi();
  }


  return StatusCode::SUCCESS;
}
