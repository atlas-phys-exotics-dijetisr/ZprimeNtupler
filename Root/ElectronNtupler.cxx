#include "ZprimeNtupler/ElectronNtupler.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

ElectronNtupler::ElectronNtupler(const std::string& name, const std::string& detailStr, float units, bool mc, bool storeSystSFs)
  : ParticleNtupler(name, detailStr, units, mc, true, storeSystSFs)
{

  if ( m_infoSwitch.m_kinematic )
    {
      m_caloCluster_eta = std::make_shared<std::vector<float>>();
      m_charge          = std::make_shared<std::vector<float>>();
    }

  if ( m_infoSwitch.m_trigger )
    {
      m_isTrigMatched        = std::make_shared<std::vector<int>              >();
      m_isTrigMatchedToChain = std::make_shared<std::vector<std::vector<int> >>();
      m_listTrigChains       = std::make_shared<std::vector<std::string>      >();
    }

  if ( m_infoSwitch.m_isolation )
    {
      for (const std::string& isolWP : m_infoSwitch.m_isolWPs)
	m_isIsolated[isolWP] = std::make_shared<std::vector<int>>();
    }

  if ( m_infoSwitch.m_isolationKinematics )
    {
      m_etcone20     = std::make_shared<std::vector<float>>();
      m_ptcone20     = std::make_shared<std::vector<float>>();
      m_ptcone30     = std::make_shared<std::vector<float>>();
      m_ptcone40     = std::make_shared<std::vector<float>>();
      m_ptvarcone20  = std::make_shared<std::vector<float>>();
      m_ptvarcone30  = std::make_shared<std::vector<float>>();
      m_ptvarcone40  = std::make_shared<std::vector<float>>();
      m_topoetcone20 = std::make_shared<std::vector<float>>();
      m_topoetcone30 = std::make_shared<std::vector<float>>();
      m_topoetcone40 = std::make_shared<std::vector<float>>();
    }

  if ( m_infoSwitch.m_PID )
    {
      for (const std::string& PIDWP : m_infoSwitch.m_PIDWPs)
	m_PID[PIDWP] = std::make_shared<std::vector<int>>();
    }  

  if ( m_infoSwitch.m_effSF && m_mc )
    {
      for (const std::string& PIDWP : m_infoSwitch.m_PIDSFWPs)
	{
	  m_PIDEff_SF[PIDWP] = std::make_shared< std::vector<std::vector<float>> >();

	  for (const std::string& isolWP : m_infoSwitch.m_isolWPs)
	    {
	      m_IsoEff_SF[PIDWP+isolWP] = std::make_shared< std::vector<std::vector<float>> >();

	      for (const std::string& trigWP : m_infoSwitch.m_trigWPs)
		{
		  m_TrigEff_SF  [ trigWP+PIDWP+isolWP ] = std::make_shared< std::vector<std::vector<float>> >();
		  m_TrigMCEff_SF[ trigWP+PIDWP+isolWP ] = std::make_shared< std::vector<std::vector<float>> >();
		}
	    }
	}

      m_RecoEff_SF = std::make_shared<std::vector< std::vector< float > >>();
    }

  if ( m_infoSwitch.m_recoparams )
    {
      m_author = std::make_shared<std::vector<int>>();
      m_OQ     = std::make_shared<std::vector<int>>();
    }

  if ( m_infoSwitch.m_trackparams )
    {
      m_trkd0           = std::make_shared<std::vector<float>>();
      m_trkd0sig        = std::make_shared<std::vector<float>>();
      m_trkz0           = std::make_shared<std::vector<float>>();
      m_trkz0sintheta   = std::make_shared<std::vector<float>>();
      m_trkphi0         = std::make_shared<std::vector<float>>();
      m_trktheta        = std::make_shared<std::vector<float>>();
      m_trkcharge       = std::make_shared<std::vector<float>>();
      m_trkqOverP       = std::make_shared<std::vector<float>>();
    }

  if ( m_infoSwitch.m_trackhitcont )
    {
      m_trknSiHits                 = std::make_shared<std::vector<int>  >();
      m_trknPixHits                = std::make_shared<std::vector<int>  >();
      m_trknPixHoles               = std::make_shared<std::vector<int>  >();
      m_trknSCTHits                = std::make_shared<std::vector<int>  >();
      m_trknSCTHoles               = std::make_shared<std::vector<int>  >();
      m_trknTRTHits                = std::make_shared<std::vector<int>  >();
      m_trknTRTHoles               = std::make_shared<std::vector<int>  >();
      m_trknBLayerHits             = std::make_shared<std::vector<int>  >();
      m_trknInnermostPixLayHits    = std::make_shared<std::vector<int>  >();
      m_trkPixdEdX                 = std::make_shared<std::vector<float>>();
    }
  
  if ( m_infoSwitch.m_promptlepton )
    {
      m_PromptLeptonInput_DL1mu           = std::make_shared<std::vector<float>>();
      m_PromptLeptonInput_DRlj            = std::make_shared<std::vector<float>>();
      m_PromptLeptonInput_LepJetPtFrac    = std::make_shared<std::vector<float>>();
      m_PromptLeptonInput_PtFrac          = std::make_shared<std::vector<float>>();
      m_PromptLeptonInput_PtRel           = std::make_shared<std::vector<float>>();
      m_PromptLeptonInput_TrackJetNTrack  = std::make_shared<std::vector<int>  >();
      m_PromptLeptonInput_ip2             = std::make_shared<std::vector<float>>();
      m_PromptLeptonInput_ip3             = std::make_shared<std::vector<float>>();
      m_PromptLeptonInput_rnnip           = std::make_shared<std::vector<float>>();
      m_PromptLeptonInput_sv1_jf_ntrkv    = std::make_shared<std::vector<int>  >();
      m_PromptLeptonIso                   = std::make_shared<std::vector<float>>();
      m_PromptLeptonVeto                  = std::make_shared<std::vector<float>>();
    }
}

StatusCode ElectronNtupler::createBranches(TTree *tree)
{
  ANA_CHECK(ParticleNtupler::createBranches(tree));

  if ( m_infoSwitch.m_kinematic )
    {
      createBranch<float>(tree,"caloCluster_eta", m_caloCluster_eta);
      createBranch<float>(tree,"charge",          m_charge);
    }

  if ( m_infoSwitch.m_trigger )
    {
      createBranch<int              >(tree,"isTrigMatched",        m_isTrigMatched       );
      createBranch<std::vector<int> >(tree,"isTrigMatchedToChain", m_isTrigMatchedToChain);
      createBranch<std::string      >(tree,"listTrigChains",       m_listTrigChains      );
    }

  if ( m_infoSwitch.m_isolation )
    {
      for (const std::string& isolWP : m_infoSwitch.m_isolWPs)
	createBranch<int>(tree, "isIsolated_" + isolWP, m_isIsolated[isolWP]);
    }

  if ( m_infoSwitch.m_isolationKinematics )
    {
      createBranch<float>(tree, "etcone20",         m_etcone20);
      createBranch<float>(tree, "ptcone20",         m_ptcone20);
      createBranch<float>(tree, "ptcone30",         m_ptcone30);
      createBranch<float>(tree, "ptcone40",         m_ptcone40);
      createBranch<float>(tree, "ptvarcone20",      m_ptvarcone20);
      createBranch<float>(tree, "ptvarcone30",      m_ptvarcone30);
      createBranch<float>(tree, "ptvarcone40",      m_ptvarcone40);
      createBranch<float>(tree, "topoetcone20",     m_topoetcone20);
      createBranch<float>(tree, "topoetcone30",     m_topoetcone30);
      createBranch<float>(tree, "topoetcone40",     m_topoetcone40);
    }

  if ( m_infoSwitch.m_PID )
    {
      for (const std::string& PIDWP : m_infoSwitch.m_PIDWPs)      
        createBranch<int>(tree, PIDWP, m_PID[PIDWP]);
    }

  if ( m_infoSwitch.m_effSF && m_mc )
    {
      for (const std::string& PIDWP : m_infoSwitch.m_PIDSFWPs)
	{
	  createBranch<std::vector<float>>(tree, "PIDEFF_SF_"+PIDWP, m_PIDEff_SF[PIDWP]);

	  for (const std::string& isolWP : m_infoSwitch.m_isolWPs)
	    {
	      createBranch<std::vector<float>>(tree, "IsoEff_SF_"+PIDWP+"_isol"+isolWP, m_IsoEff_SF[PIDWP+isolWP]);

	      for (const std::string& trigWP : m_infoSwitch.m_trigWPs)
		{
		  createBranch<std::vector<float>>(tree, "TrigEff_SF_"   + trigWP + "_" + PIDWP + (!isolWP.empty() ? "_isol" + isolWP : ""), m_TrigEff_SF  [ trigWP+PIDWP+isolWP ] );
		  createBranch<std::vector<float>>(tree, "TrigMCEff_SF_" + trigWP + "_" + PIDWP + (!isolWP.empty() ? "_isol" + isolWP : ""), m_TrigMCEff_SF[ trigWP+PIDWP+isolWP ] );
		}
	    }
	}

      createBranch<std::vector<float> >(tree, "RecoEff_SF", m_RecoEff_SF);
    }

  if ( m_infoSwitch.m_recoparams )
    {
      createBranch<int>(tree, "author",   m_author);
      createBranch<int>(tree, "OQ",       m_OQ);
    }

  if ( m_infoSwitch.m_trackparams )
    {
      createBranch<float>(tree, "trkd0",          m_trkd0);
      createBranch<float>(tree, "trkd0sig",       m_trkd0sig);
      createBranch<float>(tree, "trkz0",          m_trkz0);
      createBranch<float>(tree, "trkz0sintheta",  m_trkz0sintheta);
      createBranch<float>(tree, "trkphi0",        m_trkphi0);
      createBranch<float>(tree, "trktheta",       m_trktheta);
      createBranch<float>(tree, "trkcharge",      m_trkcharge);
      createBranch<float>(tree, "trkqOverP",      m_trkqOverP);
    }

  if ( m_infoSwitch.m_trackhitcont )
    {
      createBranch<int>(tree, "trknSiHits",    m_trknSiHits);
      createBranch<int>(tree, "trknPixHits",   m_trknPixHits);
      createBranch<int>(tree, "trknPixHoles",  m_trknPixHoles);
      createBranch<int>(tree, "trknSCTHits",   m_trknSCTHits);
      createBranch<int>(tree, "trknSCTHoles",  m_trknSCTHoles);
      createBranch<int>(tree, "trknTRTHits",   m_trknTRTHits);
      createBranch<int>(tree, "trknTRTHoles",  m_trknTRTHoles);
      createBranch<int>(tree, "trknBLayerHits",m_trknBLayerHits);
      createBranch<int>(tree, "trknInnermostPixLayHits",  m_trknInnermostPixLayHits);
      createBranch<float>(tree, "trkPixdEdX",    m_trkPixdEdX);
    }

  if ( m_infoSwitch.m_promptlepton )
    {
      createBranch<float>(tree, "PromptLeptonInput_DL1mu",           m_PromptLeptonInput_DL1mu);
      createBranch<float>(tree, "PromptLeptonInput_DRlj",            m_PromptLeptonInput_DRlj);
      createBranch<float>(tree, "PromptLeptonInput_LepJetPtFrac",    m_PromptLeptonInput_LepJetPtFrac);
      createBranch<float>(tree, "PromptLeptonInput_PtFrac",          m_PromptLeptonInput_PtFrac);
      createBranch<float>(tree, "PromptLeptonInput_PtRel",           m_PromptLeptonInput_PtRel);
      createBranch<int>  (tree, "PromptLeptonInput_TrackJetNTrack",  m_PromptLeptonInput_TrackJetNTrack);
      createBranch<float>(tree, "PromptLeptonInput_ip2",             m_PromptLeptonInput_ip2);
      createBranch<float>(tree, "PromptLeptonInput_ip3",             m_PromptLeptonInput_ip3);
      createBranch<float>(tree, "PromptLeptonInput_rnnip",           m_PromptLeptonInput_rnnip);
      createBranch<int>  (tree, "PromptLeptonInput_sv1_jf_ntrkv",    m_PromptLeptonInput_sv1_jf_ntrkv);
      createBranch<float>(tree, "PromptLeptonIso",                   m_PromptLeptonIso);
      createBranch<float>(tree, "PromptLeptonVeto",                  m_PromptLeptonVeto);
    }

  return StatusCode::SUCCESS;
}



StatusCode ElectronNtupler::clear()
{
  ANA_CHECK(ParticleNtupler::clear());

  if ( m_infoSwitch.m_kinematic )
    {
      m_caloCluster_eta ->clear();
      m_charge          ->clear();
    }

  if ( m_infoSwitch.m_trigger )
    {
      m_isTrigMatched        ->clear();
      m_isTrigMatchedToChain ->clear();
      m_listTrigChains       ->clear();
    }

  if ( m_infoSwitch.m_isolation )
    {
      for(std::pair<std::string, std::shared_ptr< std::vector<int> >> kv : m_isIsolated)
	kv.second->clear();
    }

  if ( m_infoSwitch.m_isolationKinematics )
    {
      m_etcone20                               ->clear();
      m_ptcone20                               ->clear();
      m_ptcone30                               ->clear();
      m_ptcone40                               ->clear();
      m_ptvarcone20                            ->clear();
      m_ptvarcone30                            ->clear();
      m_ptvarcone40                            ->clear();
      m_topoetcone20                           ->clear();
      m_topoetcone30                           ->clear();
      m_topoetcone40                           ->clear();
    }

  if ( m_infoSwitch.m_PID )
    {
      for(std::pair<std::string, std::shared_ptr< std::vector<int> >> kv : m_PID)
	kv.second->clear();
    }

  if ( m_infoSwitch.m_effSF && m_mc )
    {
      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>> > > kv : m_PIDEff_SF   )
	kv.second->clear();

      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>> > > kv : m_IsoEff_SF   )
	kv.second->clear();

      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>> > > kv : m_TrigEff_SF  )
	kv.second->clear();

      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>> > > kv : m_TrigMCEff_SF)
	kv.second->clear();

      m_RecoEff_SF->clear();
    }

  if ( m_infoSwitch.m_recoparams )
    {
      m_author    -> clear();
      m_OQ        -> clear();
    }

  if ( m_infoSwitch.m_trackparams )
    {
      m_trkd0           -> clear();
      m_trkd0sig        -> clear();
      m_trkz0           -> clear();
      m_trkz0sintheta   -> clear();
      m_trkphi0         -> clear();
      m_trktheta        -> clear();
      m_trkcharge       -> clear();
      m_trkqOverP       -> clear();
    }

  if ( m_infoSwitch.m_trackhitcont )
    {
      m_trknSiHits                 -> clear();
      m_trknPixHits                -> clear();
      m_trknPixHoles               -> clear();
      m_trknSCTHits                -> clear();
      m_trknSCTHoles               -> clear();
      m_trknTRTHits                -> clear();
      m_trknTRTHoles               -> clear();
      m_trknBLayerHits             -> clear();
      m_trknInnermostPixLayHits    -> clear();
      m_trkPixdEdX                 -> clear();
    }

  if ( m_infoSwitch.m_promptlepton )
    {
      m_PromptLeptonInput_DL1mu            -> clear();
      m_PromptLeptonInput_DRlj             -> clear();
      m_PromptLeptonInput_LepJetPtFrac     -> clear();
      m_PromptLeptonInput_PtFrac           -> clear();
      m_PromptLeptonInput_PtRel            -> clear();
      m_PromptLeptonInput_TrackJetNTrack   -> clear();
      m_PromptLeptonInput_ip2              -> clear();
      m_PromptLeptonInput_ip3              -> clear();
      m_PromptLeptonInput_rnnip            -> clear();
      m_PromptLeptonInput_sv1_jf_ntrkv     -> clear();
      m_PromptLeptonIso                    -> clear();
      m_PromptLeptonVeto                   -> clear();
    }

  return StatusCode::SUCCESS;
}


StatusCode ElectronNtupler::FillElectron( const xAOD::Electron* elec, const xAOD::Vertex* primaryVertex )
{
  ANA_CHECK(FillElectron(static_cast<const xAOD::IParticle*>(elec), primaryVertex));
  return StatusCode::SUCCESS;
}

StatusCode ElectronNtupler::FillElectron( const xAOD::IParticle* particle, const xAOD::Vertex* primaryVertex )
{
  ParticleNtupler::FillParticle(particle);

  const xAOD::Electron* elec=dynamic_cast<const xAOD::Electron*>(particle);
  const xAOD::TrackParticle* trk = elec->trackParticle();

  if ( m_infoSwitch.m_kinematic ) {
    float calo_eta   = ( elec->caloCluster() ) ? elec->caloCluster()->etaBE(2) : -999.0;
    m_caloCluster_eta->push_back( calo_eta );

    m_charge->push_back( elec->charge() );
  }

  if ( m_infoSwitch.m_trigger ) {

    // retrieve map<string,char> w/ <chain,isMatched>
    //
    static SG::AuxElement::Accessor< std::map<std::string,char> > isTrigMatchedMapElAcc("isTrigMatchedMapEl");

    std::vector<int> matches;

    if ( isTrigMatchedMapElAcc.isAvailable( *elec ) ) {
      // loop over map and fill branches
      //
      for ( auto const &it : (isTrigMatchedMapElAcc( *elec )) ) {
	matches.push_back( static_cast<int>(it.second) );
	m_listTrigChains->push_back( it.first );
      }
    } else {
      matches.push_back( -1 );
      m_listTrigChains->push_back("NONE");
    }

    m_isTrigMatchedToChain->push_back(matches);

    // if at least one match among the chains is found, say this electron is trigger matched
    if ( std::find(matches.begin(), matches.end(), 1) != matches.end() ) { m_isTrigMatched->push_back(1); }
    else { m_isTrigMatched->push_back(0); }

  }

  if ( m_infoSwitch.m_isolation )
    {
      for (const std::string& isolWP : m_infoSwitch.m_isolWPs)
	{
	  SG::AuxElement::ConstAccessor<char> acc_isIsolated("isIsolated_" + isolWP);
	  safeFill<char, int, xAOD::Electron>( elec, acc_isIsolated, m_isIsolated[isolWP], -1);
	}
    }

  if ( m_infoSwitch.m_isolationKinematics )
    {
      m_etcone20    ->push_back( elec->isolation( xAOD::Iso::etcone20 )    /m_units );
      m_ptcone20    ->push_back( elec->isolation( xAOD::Iso::ptcone20 )    /m_units );
      m_ptcone30    ->push_back( elec->isolation( xAOD::Iso::ptcone30 )    /m_units );
      m_ptcone40    ->push_back( elec->isolation( xAOD::Iso::ptcone40 )    /m_units );
      m_ptvarcone20 ->push_back( elec->isolation( xAOD::Iso::ptvarcone20 ) /m_units );
      m_ptvarcone30 ->push_back( elec->isolation( xAOD::Iso::ptvarcone30 ) /m_units );
      m_ptvarcone40 ->push_back( elec->isolation( xAOD::Iso::ptvarcone40 ) /m_units );
      m_topoetcone20->push_back( elec->isolation( xAOD::Iso::topoetcone20 )/m_units );
      m_topoetcone30->push_back( elec->isolation( xAOD::Iso::topoetcone30 )/m_units );
      m_topoetcone40->push_back( elec->isolation( xAOD::Iso::topoetcone40 )/m_units );
    }

  if ( m_infoSwitch.m_PID )
    {
      static SG::AuxElement::ConstAccessor<bool> acc_bLayerPass( "bLayerPass" );

      for(const std::string& PIDWP : m_infoSwitch.m_PIDWPs)
	{
	  if (PIDWP == "LHLooseBL")
	    {
	      static SG::AuxElement::ConstAccessor<char> acc_LHLoose( "LHLoose" );
	      if ( acc_LHLoose.isAvailable( *elec ) && acc_bLayerPass.isAvailable( *elec ) )
		m_PID[PIDWP]->push_back( acc_bLayerPass( *elec ) == 1 && acc_LHLoose( *elec ) == 1 );
	      else
		m_PID[PIDWP]->push_back( -1 );
	    }
	  else
	    {
	      SG::AuxElement::ConstAccessor<char> acc_PIDWP( PIDWP );
	      safeFill<char, int, xAOD::Electron>( elec, acc_PIDWP, m_PID[PIDWP], -1 );
	    }
	}
    }

  if ( m_infoSwitch.m_recoparams )
    {
      m_author -> push_back(elec->author());
      m_OQ     -> push_back(elec->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON));
    }

  if ( m_infoSwitch.m_trackparams ) {
    if ( trk ) {

      //
      // NB.:
      // All track parameters are calculated at the perigee, i.e., the point of closest approach to the origin of some r.f. (which in RunII is NOT the ATLAS detector r.f!).
      // The reference  frame is chosen to be a system centered in the beamspot position, with z axis parallel to the beam line.
      // Remember that the beamspot size ( of O(10 micrometers) in the transverse plane) is << average vertex transverse position resolution ( O(60-80 micrometers) )
      // The coordinates of this r.f. wrt. the ATLAS system origin are returned by means of vx(), vy(), vz()
      //
      m_trkd0->push_back( trk->d0() );

      static SG::AuxElement::Accessor<float> d0SigAcc ("d0sig");
      float d0_significance =  ( d0SigAcc.isAvailable( *elec ) ) ? d0SigAcc( *elec ) : -1.0;
      m_trkd0sig->push_back( d0_significance );
      m_trkz0->push_back( trk->z0()  - ( primaryVertex->z() - trk->vz() ) );

      static SG::AuxElement::Accessor<float> z0sinthetaAcc("z0sintheta");
      float z0sintheta =  ( z0sinthetaAcc.isAvailable( *elec ) ) ? z0sinthetaAcc( *elec ) : -999.0;

      m_trkz0sintheta->push_back( z0sintheta );
      m_trkphi0->push_back( trk->phi0() );
      m_trktheta->push_back( trk->theta() );
      m_trkcharge->push_back( trk->charge() );
      m_trkqOverP->push_back( trk->qOverP() );

    } else {

      m_trkd0->push_back( -999.0 );
      m_trkd0sig->push_back( -999.0 );
      m_trkz0->push_back( -999.0 );
      m_trkz0sintheta->push_back( -999.0 );
      m_trkphi0->push_back( -999.0 );
      m_trktheta->push_back( -999.0 );
      m_trkcharge->push_back( -999.0 );
      m_trkqOverP->push_back( -999.0 );
    }
  }

  if ( m_infoSwitch.m_trackhitcont ) {
    uint8_t nPixHits(-1), nPixHoles(-1), nSCTHits(-1), nSCTHoles(-1), nTRTHits(-1), nTRTHoles(-1), nBLayerHits(-1), nInnermostPixLayHits(-1);
    float pixdEdX(-1.0);
    if ( trk ) {
      trk->summaryValue( nPixHits,  xAOD::numberOfPixelHits );
      trk->summaryValue( nPixHoles, xAOD::numberOfPixelHoles );
      trk->summaryValue( nSCTHits,  xAOD::numberOfSCTHits );
      trk->summaryValue( nSCTHoles, xAOD::numberOfSCTHoles );
      trk->summaryValue( nTRTHits,  xAOD::numberOfTRTHits );
      trk->summaryValue( nTRTHoles, xAOD::numberOfTRTHoles );
      trk->summaryValue( nBLayerHits,  xAOD::numberOfBLayerHits );
      trk->summaryValue( nInnermostPixLayHits, xAOD::numberOfInnermostPixelLayerHits );
      trk->summaryValue( pixdEdX,   xAOD::pixeldEdx);
    }
    m_trknSiHits->push_back( nPixHits + nSCTHits );
    m_trknPixHits->push_back( nPixHits );
    m_trknPixHoles->push_back( nPixHoles );
    m_trknSCTHits->push_back( nSCTHits );
    m_trknSCTHoles->push_back( nSCTHoles );
    m_trknTRTHits->push_back( nTRTHits );
    m_trknTRTHoles->push_back( nTRTHoles );
    m_trknBLayerHits->push_back( nBLayerHits );
    m_trknInnermostPixLayHits->push_back( nInnermostPixLayHits );
    m_trkPixdEdX->push_back( pixdEdX );
  }

  if ( m_infoSwitch.m_promptlepton ) {
    SG::AuxElement::ConstAccessor<float> acc_DL1mu          ("PromptLeptonInput_DL1mu");
    SG::AuxElement::ConstAccessor<float> acc_DRlj           ("PromptLeptonInput_DRlj");
    SG::AuxElement::ConstAccessor<float> acc_LepJetPtFrac   ("PromptLeptonInput_LepJetPtFrac");
    SG::AuxElement::ConstAccessor<float> acc_PtFrac         ("PromptLeptonInput_PtFrac");
    SG::AuxElement::ConstAccessor<float> acc_PtRel          ("PromptLeptonInput_PtRel");
    SG::AuxElement::ConstAccessor<short> acc_TrackJetNTrack ("PromptLeptonInput_TrackJetNTrack");
    SG::AuxElement::ConstAccessor<float> acc_ip2            ("PromptLeptonInput_ip2");
    SG::AuxElement::ConstAccessor<float> acc_ip3            ("PromptLeptonInput_ip3");
    SG::AuxElement::ConstAccessor<float> acc_rnnip          ("PromptLeptonInput_rnnip");
    SG::AuxElement::ConstAccessor<short> acc_sv1_jf_ntrkv   ("PromptLeptonInput_sv1_jf_ntrkv");
    SG::AuxElement::ConstAccessor<float> acc_Iso            ("PromptLeptonIso");
    SG::AuxElement::ConstAccessor<float> acc_Veto           ("PromptLeptonVeto");

    m_PromptLeptonInput_DL1mu          ->push_back( acc_DL1mu          .isAvailable(*elec) ? acc_DL1mu(*elec)          : -100);
    m_PromptLeptonInput_DRlj           ->push_back( acc_DRlj           .isAvailable(*elec) ? acc_DRlj(*elec)           : -100);
    m_PromptLeptonInput_LepJetPtFrac   ->push_back( acc_LepJetPtFrac   .isAvailable(*elec) ? acc_LepJetPtFrac(*elec)   : -100);
    m_PromptLeptonInput_PtFrac         ->push_back( acc_PtFrac         .isAvailable(*elec) ? acc_PtFrac(*elec)         : -100);
    m_PromptLeptonInput_PtRel          ->push_back( acc_PtRel          .isAvailable(*elec) ? acc_PtRel(*elec)          : -100);
    m_PromptLeptonInput_TrackJetNTrack ->push_back( acc_TrackJetNTrack .isAvailable(*elec) ? acc_TrackJetNTrack(*elec) : -100);
    m_PromptLeptonInput_ip2            ->push_back( acc_ip2            .isAvailable(*elec) ? acc_ip2(*elec)            : -100);
    m_PromptLeptonInput_ip3            ->push_back( acc_ip3            .isAvailable(*elec) ? acc_ip3(*elec)            : -100);
    m_PromptLeptonInput_rnnip          ->push_back( acc_rnnip          .isAvailable(*elec) ? acc_rnnip(*elec)          : -100);
    m_PromptLeptonInput_sv1_jf_ntrkv   ->push_back( acc_sv1_jf_ntrkv   .isAvailable(*elec) ? acc_sv1_jf_ntrkv(*elec)   : -100);
    m_PromptLeptonIso                  ->push_back( acc_Iso            .isAvailable(*elec) ? acc_Iso(*elec)            : -100);
    m_PromptLeptonVeto                 ->push_back( acc_Veto           .isAvailable(*elec) ? acc_Veto(*elec)           : -100);
  }

  if ( m_infoSwitch.m_effSF && m_mc )
    {
      static const std::vector<float> junkSF (1,-1.0);
      static const std::vector<float> junkEff(1,-1.0);

      for (const std::string& PIDWP : m_infoSwitch.m_PIDSFWPs)
	{
	  SG::AuxElement::ConstAccessor< std::vector< float > > acc_PIDSF("ElPIDEff_SF_syst_" + PIDWP);
	  safeSFVecFill<float, xAOD::Electron>(elec, acc_PIDSF, m_PIDEff_SF[PIDWP], junkSF );

	  for (const std::string& isolWP : m_infoSwitch.m_isolWPs)
	    {
	      SG::AuxElement::ConstAccessor< std::vector< float > > acc_IsoSF("ElIsoEff_SF_syst_" + PIDWP + "_isol" + isolWP);	      
	      safeSFVecFill<float, xAOD::Electron>(elec, acc_IsoSF, m_IsoEff_SF[PIDWP+isolWP], junkSF );

	      for (const std::string& trigWP : m_infoSwitch.m_trigWPs)
		{

		  SG::AuxElement::ConstAccessor< std::vector< float > > acc_TrigSF  ("ElTrigEff_SF_syst_"   + trigWP + "_" + PIDWP + (!isolWP.empty() ? "_isol" + isolWP : ""));
		  safeSFVecFill<float, xAOD::Electron>( elec, acc_TrigSF  , m_TrigEff_SF  [trigWP+PIDWP+isolWP], junkSF);

		  SG::AuxElement::ConstAccessor< std::vector< float > > acc_TrigMCSF("ElTrigMCEff_SF_syst_" + trigWP + "_" + PIDWP + (!isolWP.empty() ? "_isol" + isolWP : ""));
		  safeSFVecFill<float, xAOD::Electron>( elec, acc_TrigMCSF, m_TrigMCEff_SF[trigWP+PIDWP+isolWP], junkSF);
		}
	    }
	}

      static SG::AuxElement::Accessor< std::vector< float > > accRecoSF("ElRecoEff_SF_syst_Reconstruction");
      safeSFVecFill<float, xAOD::Electron>( elec, accRecoSF, m_RecoEff_SF, junkSF );
    }

  return StatusCode::SUCCESS;
}
