// c++ include(s):
#include <iostream>

// EL include(s):
#include <EventLoop/Worker.h>

// EDM include(s):
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>
#include <AthContainers/ConstDataVector.h>
#include <AthContainers/DataVector.h>
#include <xAODCore/ShallowCopy.h>

// package include(s):
#include "ZprimeNtupler/FFJetSmearingAlg.h"
#include "ZprimeNtupler/HelperFunctions.h"

using namespace Zprime;

FFJetSmearingAlg :: FFJetSmearingAlg (const std::string& name, ISvcLocator* pSvcLocator)
  : AnaAlgorithm(name, pSvcLocator)
{
  //
  // Configurable properties
  declareProperty("ContainerName"      , m_containerName        );
  declareProperty("Sort"               , m_sort                   );
  declareProperty("InputAlgo"          , m_inputAlgo              );
  declareProperty("OutputAlgo"         , m_outputAlgo             );
  declareProperty("WriteSystToMetadata", m_writeSystToMetadata    );

  declareProperty("SystVal"            , m_systVal                );
  declareProperty("SystName"           , m_systName               );

  //
  // Tools
  declareProperty ("FFJetSmearingTool", m_ffJetSmearingTool, "Forward folded uncerertainties tool");
}

StatusCode FFJetSmearingAlg :: initialize ()
{
  ANA_CHECK(AnaAlgorithm::initialize());

  if ( m_containerName.empty() ) {
    ANA_MSG_ERROR( "ContainerName is empty!");
    return StatusCode::FAILURE;
  }

  // ***********************************************************

  // Get a list of recommended systematics for this tool
  //
  //const CP::SystematicSet recSyst = CP::SystematicSet();
  const CP::SystematicSet& recSyst = m_ffJetSmearingTool->recommendedSystematics();

  ANA_MSG_INFO(" Initializing Forward Folded Jet Systematics:");
  //
  // Make a list of systematics to be used, based on configuration input
  // Use HelperFunctions::getListofSystematics() for this!
  //
  m_systList = HelperFunctions::getListofSystematics( recSyst, m_systName, m_systVal, msg() );

  ANA_MSG_INFO("Will be using FFJetSmearingTool systematic:");
  for ( const CP::SystematicSet& syst_it : m_systList ) {
    if ( m_systName.empty() ) {
      ANA_MSG_INFO("\t Running w/ nominal configuration only!");
      break;
    }
    ANA_MSG_INFO("\t " << syst_it.name());
  }

  // Write output sys names
  if ( m_writeSystToMetadata ) {
    TFile *fileMD = wk()->getOutputFile ("metadata");
    HelperFunctions::writeSystematicsListHist(m_systList, name(), fileMD);
  }

  ANA_MSG_INFO( "Succesfully initialized!" );

  return StatusCode::SUCCESS;
}


StatusCode FFJetSmearingAlg :: execute ()
{
  ANA_CHECK(AnaAlgorithm::execute());  

  //
  // Useful containers
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, m_eventInfoContainerName));  

  const xAOD::JetContainer* inJets = nullptr;
  ANA_CHECK(evtStore()->retrieve(inJets, m_containerName + "ShallowCopy"));

  // loop over available systematics - remember syst == EMPTY_STRING --> baseline
  // prepare a vector of the names of CDV containers
  // must be a pointer to be recorded in TStore
  //
  std::unique_ptr< std::vector< std::string > > vecOutContainerNames
    = std::make_unique< std::vector< std::string > >();

  if ( !m_inputAlgo.empty() ) // Existing systematics
    {
      const std::vector< std::string >* vecInContainerNames;
      ANA_CHECK( evtStore()->retrieve(vecInContainerNames, m_inputAlgo));
      std::copy(vecInContainerNames->begin(), vecInContainerNames->end(), std::back_inserter(*vecOutContainerNames));
    }

  for ( const CP::SystematicSet& syst_it : m_systList ) {
    // always append the name of the variation, including nominal which is an empty string
    std::string outSCContainerName    = m_containerName + syst_it.name() + "ShallowCopy";
    std::string outSCAuxContainerName = outSCContainerName + "Aux.";
    std::string outContainerName      = m_containerName + syst_it.name();
    vecOutContainerNames->push_back( syst_it.name() );

    // apply syst
    //
    if ( m_ffJetSmearingTool->applySystematicVariation(syst_it) != CP::SystematicCode::Ok ) {
      ANA_MSG_ERROR( "Failed to configure FFJetSmearingTool for systematic " << syst_it.name());
      return StatusCode::FAILURE;
    }

    // create shallow copy for calibration - one per syst
    //
    std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > calibJetsSC = xAOD::shallowCopyContainer( *inJets );
    // create ConstDataVector to be eventually stored in TStore
    //
    std::unique_ptr<ConstDataVector<xAOD::JetContainer>> calibJetsCDV
      = std::make_unique<ConstDataVector<xAOD::JetContainer>>(SG::VIEW_ELEMENTS);
    calibJetsCDV->reserve( calibJetsSC.first->size() );

    // now calibrate!
    //
    unsigned int idx(0);
    if ( isMC() ) {

      for ( xAOD::Jet* jet : *(calibJetsSC.first) ) {

	ANA_MSG_VERBOSE( "  uncailbrated jet " << idx << ", pt = " << jet->pt()*1e-3 << " GeV");
	if ( m_ffJetSmearingTool->applyCorrection(*jet) == CP::CorrectionCode::Error ) {
	  ANA_MSG_WARNING( "FFJetSmearingTool returned Error CorrectionCode");
	}

	ANA_MSG_VERBOSE( "  corrected jet pt = " << jet->pt()*1e-3 << " GeV");
	++idx;
      }

    } // close calibration loop

    // save pointers in ConstDataVector with same order
    //
    for(const xAOD::Jet* jet : *calibJetsSC.first)
      { calibJetsCDV->push_back(jet); }

    // sort after coping to CDV
    if ( m_sort ) {
      ANA_MSG_DEBUG( "sorting");
      std::sort( calibJetsCDV->begin(), calibJetsCDV->end(), HelperFunctions::sort_pt );
    }

    // add SC container to TStore
    //
    ANA_MSG_DEBUG( "record shallow copy" );
    ANA_CHECK( evtStore()->record( calibJetsSC.first,  outSCContainerName  ));
    ANA_CHECK( evtStore()->record( calibJetsSC.second, outSCAuxContainerName ));

    //
    // add ConstDataVector to TStore
    //
    ANA_MSG_DEBUG( "record data view");
    ANA_CHECK( evtStore()->record( std::move(calibJetsCDV), outContainerName));

  } // close loop on systematics

  // add vector<string container_names_syst> to TStore
  //
  ANA_MSG_DEBUG( "record systematic names");
  ANA_CHECK( evtStore()->record( std::move(vecOutContainerNames), m_outputAlgo));

  return StatusCode::SUCCESS;
}
