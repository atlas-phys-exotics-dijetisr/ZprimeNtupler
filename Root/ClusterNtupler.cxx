#include "ZprimeNtupler/ClusterNtupler.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

ClusterNtupler::ClusterNtupler(const std::string& name, const std::string& detailStr, float units, bool mc)
  : ParticleNtupler(name, detailStr, units, mc, true)
{ }

StatusCode ClusterNtupler::createBranches(TTree *tree)
{
  ANA_CHECK(ParticleNtupler::createBranches(tree));
  return StatusCode::SUCCESS;
}

StatusCode ClusterNtupler::clear()
{
  ANA_CHECK(ParticleNtupler::clear());
  return StatusCode::SUCCESS;
}

StatusCode ClusterNtupler::FillCluster( const xAOD::CaloCluster* cluster )
{
  ANA_CHECK(FillCluster(static_cast<const xAOD::IParticle*>(cluster)));
  return StatusCode::SUCCESS;
}

StatusCode ClusterNtupler::FillCluster( const xAOD::IParticle* particle )
{
  ANA_CHECK(ParticleNtupler::FillParticle(particle));
  return StatusCode::SUCCESS;
}
