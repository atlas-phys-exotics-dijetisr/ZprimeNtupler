#include <ZprimeNtupler/NtuplerContainerDef.h>

using namespace Zprime;

NtuplerContainerDef::NtuplerContainerDef()
{ }

NtuplerContainerDef::NtuplerContainerDef(ObjectType type, const std::string& containerName, const std::string& branchName, const std::string& detailStr, const std::string& detailStrSyst)
  : m_type(type), m_containerName(containerName), m_branchName(branchName), m_detailStr(detailStr), m_detailStrSyst(detailStrSyst)
{ }

NtuplerContainerDef::NtuplerContainerDef(ObjectType type, const std::string& branchName, const std::string& detailStr)
  : m_type(type), m_branchName(branchName), m_detailStr(detailStr)
{ }
