// c++ include(s):
#include <iostream>

// EL include(s):
#include <EventLoop/Worker.h>

// EDM include(s):
#include <xAODEventInfo/EventInfo.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/Muon.h>
#include <AthContainers/ConstDataVector.h>
#include <AthContainers/DataVector.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODBase/IParticleHelpers.h>

// package include(s):
#include "ZprimeNtupler/HelperFunctions.h"
#include "ZprimeNtupler/HelperClasses.h"
#include "ZprimeNtupler/MuonCalibrator.h"
#include "PATInterfaces/CorrectionCode.h" // to check the return correction code status of tools

using HelperClasses::ToolName;
using namespace Zprime;

MuonCalibrator :: MuonCalibrator (const std::string& name, ISvcLocator* pSvcLocator)
  : AnaAlgorithm(name, pSvcLocator)
{
  //
  // Configurable properties
  declareProperty("InContainerName"    , m_inContainerName        );
  declareProperty("OutContainerName"   , m_outContainerName       );
  declareProperty("Sort"               , m_sort                   );
  declareProperty("InputAlgo"          , m_inputAlgo              );
  declareProperty("OutputAlgo"         , m_outputAlgo             );
  declareProperty("WriteSystToMetadata", m_writeSystToMetadata    );

  declareProperty("SystVal"            , m_systVal                );
  declareProperty("SystName"           , m_systName               );

  declareProperty("ForceDataCalib"     , m_forceDataCalib         );

  //
  // Tools
  declareProperty ("MuonCalibrationAndSmearingTool", m_MuonCalibrationAndSmearingTool, "Muon calibration and smearing tool");
}

StatusCode MuonCalibrator :: initialize ()
{
  ANA_CHECK(AnaAlgorithm::initialize());

  if ( m_inContainerName.empty() ) {
    ANA_MSG_ERROR( "InContainerName is empty!");
    return StatusCode::FAILURE;
  }

  // ***********************************************************

  // Get a list of recommended systematics for this tool
  //
  //const CP::SystematicSet recSyst = CP::SystematicSet();
  const CP::SystematicSet& recSyst = m_MuonCalibrationAndSmearingTool->recommendedSystematics();

  ANA_MSG_INFO(" Initializing Muon Calibrator Systematics :");
  //
  // Make a list of systematics to be used, based on configuration input
  // Use HelperFunctions::getListofSystematics() for this!
  //
  m_systList = HelperFunctions::getListofSystematics( recSyst, m_systName, m_systVal, msg() );

  ANA_MSG_INFO("Will be using MuonCalibrationAndSmearingTool systematic:");
  for ( const CP::SystematicSet& syst_it : m_systList ) {
    if ( m_systName.empty() ) {
      ANA_MSG_INFO("\t Running w/ nominal configuration only!");
      break;
    }
    ANA_MSG_INFO("\t " << syst_it.name());
  }

  // Write output sys names
  if ( m_writeSystToMetadata ) {
    TFile *fileMD = wk()->getOutputFile ("metadata");
    HelperFunctions::writeSystematicsListHist(m_systList, name(), fileMD);
  }

  ANA_MSG_INFO( "Succesfully initialized!" );

  return StatusCode::SUCCESS;
}

StatusCode MuonCalibrator :: execute ()
{
  ANA_CHECK(AnaAlgorithm::execute());  

  if ( !isMC() && !m_forceDataCalib ) {
    ANA_MSG_ERROR( "Sample is Data! Do not apply any Muon Calibration... ");
  }

  //
  // Useful containers
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, m_eventInfoContainerName));  

  const xAOD::MuonContainer* inMuons = nullptr;
  ANA_CHECK(evtStore()->retrieve(inMuons, m_inContainerName));

  // loop over available systematics - remember syst == EMPTY_STRING --> baseline
  // prepare a vector of the names of CDV containers
  // must be a pointer to be recorded in TStore
  //
  std::unique_ptr< std::vector< std::string > > vecOutContainerNames = std::make_unique< std::vector< std::string > >();

  for ( const CP::SystematicSet& syst_it : m_systList ) {
    // always append the name of the variation, including nominal which is an empty string
    std::string outSCContainerName    = m_outContainerName + syst_it.name() + "ShallowCopy";
    std::string outSCAuxContainerName = outSCContainerName + "Aux.";
    std::string outContainerName      = m_outContainerName + syst_it.name();
    vecOutContainerNames->push_back( syst_it.name() );

    // apply syst
    //
    if ( m_MuonCalibrationAndSmearingTool->applySystematicVariation(syst_it) != CP::SystematicCode::Ok ) {
      ANA_MSG_ERROR( "Failed to configure MuonCalibrationAndSmearingTool for systematic " << syst_it.name());
      return StatusCode::FAILURE;
    }

    // create shallow copy for calibration - one per syst
    //
    std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > calibMuonsSC = xAOD::shallowCopyContainer( *inMuons );
    // create ConstDataVector to be eventually stored in TStore
    //
    std::unique_ptr<ConstDataVector<xAOD::MuonContainer>> calibMuonsCDV
      = std::make_unique<ConstDataVector<xAOD::MuonContainer>>(SG::VIEW_ELEMENTS);
    calibMuonsCDV->reserve( calibMuonsSC.first->size() );

    // now calibrate!
    //
    unsigned int idx(0);
    if ( isMC() || m_forceDataCalib ) {

      for ( xAOD::Muon* muon : *(calibMuonsSC.first) ) {

	ANA_MSG_VERBOSE( "  uncailbrated muon " << idx << ", pt = " << muon->pt()*1e-3 << " GeV");
	if(muon-> primaryTrackParticle()) {
	  if ( m_MuonCalibrationAndSmearingTool->applyCorrection(*muon) == CP::CorrectionCode::Error ) {  // Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
	    ANA_MSG_WARNING( "MuonCalibrationAndSmearingTool returned Error CorrectionCode");		  // If OutOfValidityRange is returned no modification is made and the original muon values are taken.
	  }
	}

	ANA_MSG_VERBOSE( "  corrected muon pt = " << muon->pt()*1e-3 << " GeV");
	++idx;

      } // close calibration loop
    }

    ANA_MSG_DEBUG( "setOriginalObjectLink");
    if ( !xAOD::setOriginalObjectLink(*inMuons, *(calibMuonsSC.first)) ) {
      ANA_MSG_ERROR( "Failed to set original object links -- MET rebuilding cannot proceed.");
    }

    // save pointers in ConstDataVector with same order
    //
    for(const xAOD::Muon* muon : *calibMuonsSC.first)
      { calibMuonsCDV->push_back(muon); }

    // sort after coping to CDV
    if ( m_sort ) {
      ANA_MSG_DEBUG( "sorting");
      std::sort( calibMuonsCDV->begin(), calibMuonsCDV->end(), HelperFunctions::sort_pt );
    }

    // add SC container to TStore
    //
    ANA_MSG_DEBUG( "recording shallow copy" );
    ANA_CHECK( evtStore()->record( calibMuonsSC.first,  outSCContainerName  ));
    ANA_CHECK( evtStore()->record( calibMuonsSC.second, outSCAuxContainerName ));

    //
    // add ConstDataVector to TStore
    //
    ANA_MSG_DEBUG( "record data view");
    ANA_CHECK( evtStore()->record( std::move(calibMuonsCDV), outContainerName));

  } // close loop on systematics

  // add vector<string container_names_syst> to TStore
  //
  ANA_MSG_DEBUG( "record systematic names");
  ANA_CHECK( evtStore()->record( std::move(vecOutContainerNames), m_outputAlgo));

  return StatusCode::SUCCESS;
}
