#include "ZprimeNtupler/MuonNtupler.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

MuonNtupler::MuonNtupler(const std::string& name, const std::string& detailStr, float units, bool mc, bool storeSystSFs)
  : ParticleNtupler(name, detailStr, units, mc, true, storeSystSFs)
{

  if ( m_infoSwitch.m_kinematic )
    {
      m_charge = std::make_shared< std::vector<float> > ();
    }

  // trigger
  if ( m_infoSwitch.m_trigger )
    {
      m_isTrigMatched          = std::make_shared< std::vector<int              > >();
      m_isTrigMatchedToChain   = std::make_shared< std::vector<std::vector<int> > >();
      m_listTrigChains         = std::make_shared< std::vector<std::string      > >();
    }

  // isolation
  if ( m_infoSwitch.m_isolation )
    {
      for (const std::string& isol : m_infoSwitch.m_isolWPs)
	{
	  if (!isol.empty())
	    {
	      m_isIsolated[isol] = std::make_shared<std::vector<int>>();
	    }
	}
    }

  if ( m_infoSwitch.m_isolationKinematics ) {
    m_ptcone20                                   = std::make_shared< std::vector<float> >();
    m_ptcone30                                   = std::make_shared< std::vector<float> >();
    m_ptcone40                                   = std::make_shared< std::vector<float> >();
    m_ptvarcone20                                = std::make_shared< std::vector<float> >();
    m_ptvarcone30                                = std::make_shared< std::vector<float> >();
    m_ptvarcone40                                = std::make_shared< std::vector<float> >();
    m_topoetcone20                               = std::make_shared< std::vector<float> >();
    m_topoetcone30                               = std::make_shared< std::vector<float> >();
    m_topoetcone40                               = std::make_shared< std::vector<float> >();
  }

  // quality
  if ( m_infoSwitch.m_quality )
    {
      for (const std::string& quality : m_infoSwitch.m_recoWPs)
	{
	  if (!quality.empty())
	    {
	      m_quality[quality] = std::make_shared< std::vector<int> >();
	    }
	}
    }

  // scale factors w/ sys
  // per object
  if ( m_infoSwitch.m_effSF && m_mc )
    {

      for (const std::string& reco : m_infoSwitch.m_recoWPs)
	{
	  m_RecoEff_SF[reco] = std::make_shared< std::vector< std::vector< float > > >();

	  for (const std::string& trig : m_infoSwitch.m_trigWPs)
	    {
	      m_TrigEff_SF[trig+reco] = std::make_shared< std::vector< std::vector< float > > >();
	      m_TrigMCEff [trig+reco] = std::make_shared< std::vector< std::vector< float > > >();
	    }
	}
      
      for (const std::string& isol : m_infoSwitch.m_isolWPs)
	{
	  m_RecoEff_SF[isol] = std::make_shared< std::vector< std::vector< float > > >();
	}

      m_TTVAEff_SF = std::make_shared<std::vector< std::vector< float > > >();
  }
      // track parameters
  if ( m_infoSwitch.m_trackparams ) {
    m_trkd0            = std::make_shared<std::vector<float> >();
    m_trkd0sig         = std::make_shared<std::vector<float> >();
    m_trkz0            = std::make_shared<std::vector<float> >();
    m_trkz0sintheta    = std::make_shared<std::vector<float> >();
    m_trkphi0          = std::make_shared<std::vector<float> >();
    m_trktheta         = std::make_shared<std::vector<float> >();
    m_trkcharge        = std::make_shared<std::vector<float> >();
    m_trkqOverP        = std::make_shared<std::vector<float> >();
  }

      // track hit content
  if ( m_infoSwitch.m_trackhitcont ) {
    m_trknSiHits                = std::make_shared<std::vector<int>   >();
    m_trknPixHits               = std::make_shared<std::vector<int>   >();
    m_trknPixHoles              = std::make_shared<std::vector<int>   >();
    m_trknSCTHits               = std::make_shared<std::vector<int>   >();
    m_trknSCTHoles              = std::make_shared<std::vector<int>   >();
    m_trknTRTHits               = std::make_shared<std::vector<int>   >();
    m_trknTRTHoles              = std::make_shared<std::vector<int>   >();
    m_trknBLayerHits            = std::make_shared<std::vector<int>   >();
    m_trknInnermostPixLayHits   = std::make_shared<std::vector<int>   >();         // not available in DC14
    m_trkPixdEdX                = std::make_shared<std::vector<float> >();         // not available in DC14
  }

  if ( m_infoSwitch.m_energyLoss ) {
    m_EnergyLoss                   = std::make_shared<std::vector<float>         >();
    m_EnergyLossSigma              = std::make_shared<std::vector<float>         >();
    m_energyLossType               = std::make_shared<std::vector<unsigned char> >();
    m_MeasEnergyLoss               = std::make_shared<std::vector<float>         >();
    m_MeasEnergyLossSigma          = std::make_shared<std::vector<float>         >();
    m_ParamEnergyLoss              = std::make_shared<std::vector<float>         >();
    m_ParamEnergyLossSigmaMinus    = std::make_shared<std::vector<float>         >();
    m_ParamEnergyLossSigmaPlus     = std::make_shared<std::vector<float>         >();
  }

  if ( m_infoSwitch.m_promptlepton )
    {
      m_PromptLeptonInput_DL1mu           = std::make_shared<std::vector<float> >();
      m_PromptLeptonInput_DRlj            = std::make_shared<std::vector<float> >();
      m_PromptLeptonInput_LepJetPtFrac    = std::make_shared<std::vector<float> >();
      m_PromptLeptonInput_PtFrac          = std::make_shared<std::vector<float> >();
      m_PromptLeptonInput_PtRel           = std::make_shared<std::vector<float> >();
      m_PromptLeptonInput_TrackJetNTrack  = std::make_shared<std::vector<int>   >();
      m_PromptLeptonInput_ip2             = std::make_shared<std::vector<float> >();
      m_PromptLeptonInput_ip3             = std::make_shared<std::vector<float> >();
      m_PromptLeptonInput_rnnip           = std::make_shared<std::vector<float> >();
      m_PromptLeptonInput_sv1_jf_ntrkv    = std::make_shared<std::vector<int>   >();
      m_PromptLeptonIso                   = std::make_shared<std::vector<float> >();
      m_PromptLeptonVeto                  = std::make_shared<std::vector<float> >();
    }

}

StatusCode MuonNtupler::createBranches(TTree *tree)
{
  ANA_CHECK(ParticleNtupler::createBranches(tree));

  if ( m_infoSwitch.m_kinematic )
    {
      createBranch<float>(tree, "charge", m_charge);
    }

  if ( m_infoSwitch.m_trigger )
    {
      // this is true if there's a match for at least one trigger chain
      createBranch<int>(tree,"isTrigMatched", m_isTrigMatched);
      // a vector of trigger match decision for each muon trigger chain
      createBranch<std::vector<int> >(tree,"isTrigMatchedToChain", m_isTrigMatchedToChain );
      // a vector of strings for each muon trigger chain - 1:1 correspondence w/ vector above
      createBranch<std::string>(tree, "listTrigChains", m_listTrigChains );
    }

  if ( m_infoSwitch.m_isolation )
    {
      for (const std::string& isol : m_infoSwitch.m_isolWPs)
	{
	  if (!isol.empty())
	    {
	      createBranch<int>(tree, "isIsolated_" + isol, m_isIsolated[isol]);
	    }
	}
    }

  if ( m_infoSwitch.m_isolationKinematics )
    {
      createBranch<float>(tree,"ptcone20"    , m_ptcone20);
      createBranch<float>(tree,"ptcone30"    , m_ptcone30);
      createBranch<float>(tree,"ptcone40"    , m_ptcone40);
      createBranch<float>(tree,"ptvarcone20" , m_ptvarcone20);
      createBranch<float>(tree,"ptvarcone30" , m_ptvarcone30);
      createBranch<float>(tree,"ptvarcone40" , m_ptvarcone40);
      createBranch<float>(tree,"topoetcone20", m_topoetcone20);
      createBranch<float>(tree,"topoetcone30", m_topoetcone30);
      createBranch<float>(tree,"topoetcone40", m_topoetcone40);
    }

  if ( m_infoSwitch.m_effSF && m_mc )
    {

      for (const std::string& reco : m_infoSwitch.m_recoWPs)
	{
	  createBranch<std::vector<float>>(tree, "RecoEff_SF_Reco" + reco, m_RecoEff_SF[ reco ]);

	  for (const std::string& trig : m_infoSwitch.m_trigWPs)
	    {
	      createBranch<std::vector<float>>( tree, "TrigEff_SF_" + trig + "_Reco" + reco, m_TrigEff_SF[ trig+reco ] );
	      createBranch<std::vector<float>>( tree, "TrigMCEff_"  + trig + "_Reco" + reco, m_TrigMCEff [ trig+reco ] );
	    }
	}

      for (const std::string& isol : m_infoSwitch.m_isolWPs)
	{
	  createBranch<std::vector<float>>(tree, "IsoEff_SF_Iso" + isol, m_IsoEff_SF[ isol ] );
	}

      createBranch<std::vector<float> >(tree,"TTVAEff_SF",  m_TTVAEff_SF);
    }

  if ( m_infoSwitch.m_quality )
    {
      for (const std::string& quality : m_infoSwitch.m_recoWPs)
	{
	  if (!quality.empty())
	    {
	      createBranch<int>(tree, "is" + quality, m_quality[quality]);
	    }
	}
    }

  if ( m_infoSwitch.m_trackparams ) {
    createBranch<float>(tree,"trkd0",          m_trkd0);
    createBranch<float>(tree,"trkd0sig",       m_trkd0sig);
    createBranch<float>(tree,"trkz0",          m_trkz0);
    createBranch<float>(tree,"trkz0sintheta",  m_trkz0sintheta);
    createBranch<float>(tree,"trkphi0",        m_trkphi0);
    createBranch<float>(tree,"trktheta",       m_trktheta);
    createBranch<float>(tree,"trkcharge",      m_trkcharge);
    createBranch<float>(tree,"trkqOverP",      m_trkqOverP);
  }

  if ( m_infoSwitch.m_trackhitcont ) {
    createBranch<int>(tree,"trknSiHits",    m_trknSiHits);
    createBranch<int>(tree,"trknPixHits",   m_trknPixHits);
    createBranch<int>(tree,"trknPixHoles",  m_trknPixHoles);
    createBranch<int>(tree,"trknSCTHits",   m_trknSCTHits);
    createBranch<int>(tree,"trknSCTHoles",  m_trknSCTHoles);
    createBranch<int>(tree,"trknTRTHits",   m_trknTRTHits);
    createBranch<int>(tree,"trknTRTHoles",  m_trknTRTHoles);
    createBranch<int>(tree,"trknBLayerHits",m_trknBLayerHits);
    createBranch<int>(tree,"trknInnermostPixLayHits",  m_trknInnermostPixLayHits);
    createBranch<float>(tree,"trkPixdEdX",    m_trkPixdEdX);
  }

  if( m_infoSwitch.m_energyLoss ) {
    createBranch<float>(tree,"EnergyLoss"                ,  m_EnergyLoss               );
    createBranch<float>(tree,"EnergyLossSigma"           ,  m_EnergyLossSigma          );
    createBranch<unsigned char>(tree,"energyLossType"    ,  m_energyLossType           );
    createBranch<float>(tree,"MeasEnergyLoss"            ,  m_MeasEnergyLoss           );
    createBranch<float>(tree,"MeasEnergyLossSigma"       ,  m_MeasEnergyLossSigma      );
    createBranch<float>(tree,"ParamEnergyLoss"           ,  m_ParamEnergyLoss          );
    createBranch<float>(tree,"ParamEnergyLossSigmaMinus" ,  m_ParamEnergyLossSigmaMinus);
    createBranch<float>(tree,"ParamEnergyLossSigmaPlus"  ,  m_ParamEnergyLossSigmaPlus );
  }
  
  if ( m_infoSwitch.m_promptlepton ) {
    createBranch<float>(tree, "PromptLeptonInput_DL1mu",           m_PromptLeptonInput_DL1mu);
    createBranch<float>(tree, "PromptLeptonInput_DRlj",            m_PromptLeptonInput_DRlj);
    createBranch<float>(tree, "PromptLeptonInput_LepJetPtFrac",    m_PromptLeptonInput_LepJetPtFrac);
    createBranch<float>(tree, "PromptLeptonInput_PtFrac",          m_PromptLeptonInput_PtFrac);
    createBranch<float>(tree, "PromptLeptonInput_PtRel",           m_PromptLeptonInput_PtRel);
    createBranch<int>  (tree, "PromptLeptonInput_TrackJetNTrack",  m_PromptLeptonInput_TrackJetNTrack);
    createBranch<float>(tree, "PromptLeptonInput_ip2",             m_PromptLeptonInput_ip2);
    createBranch<float>(tree, "PromptLeptonInput_ip3",             m_PromptLeptonInput_ip3);
    createBranch<float>(tree, "PromptLeptonInput_rnnip",           m_PromptLeptonInput_rnnip);
    createBranch<int>  (tree, "PromptLeptonInput_sv1_jf_ntrkv",    m_PromptLeptonInput_sv1_jf_ntrkv);
    createBranch<float>(tree, "PromptLeptonIso",                   m_PromptLeptonIso);
    createBranch<float>(tree, "PromptLeptonVeto",                  m_PromptLeptonVeto);
  }

  return StatusCode::SUCCESS;
}



StatusCode MuonNtupler::clear()
{
  
  ANA_CHECK(ParticleNtupler::clear());

  if ( m_infoSwitch.m_kinematic )
    {
      m_charge->clear();
    }

  if ( m_infoSwitch.m_trigger )
    {
      m_isTrigMatched       ->clear();
      m_isTrigMatchedToChain->clear();
      m_listTrigChains      ->clear();
    }

  if ( m_infoSwitch.m_isolation )
    {
      for(std::pair<std::string, std::shared_ptr< std::vector<int> >> kv : m_isIsolated)
	kv.second->clear();
    }

  if ( m_infoSwitch.m_isolationKinematics ) {
    m_ptcone20->clear();
    m_ptcone30->clear();
    m_ptcone40->clear();
    m_ptvarcone20->clear();
    m_ptvarcone30->clear();
    m_ptvarcone40->clear();
    m_topoetcone20->clear();
    m_topoetcone30->clear();
    m_topoetcone40->clear();
  }

  if ( m_infoSwitch.m_quality )
    {
      for(std::pair<std::string, std::shared_ptr< std::vector<int> >> kv : m_quality)
	kv.second->clear();
    }

  if ( m_infoSwitch.m_trackparams ) {
    m_trkd0->clear();
    m_trkd0sig->clear();
    m_trkz0->clear();
    m_trkz0sintheta->clear();
    m_trkphi0->clear();
    m_trktheta->clear();
    m_trkcharge->clear();
    m_trkqOverP->clear();
  }

  if ( m_infoSwitch.m_trackhitcont ) {
    m_trknSiHits->clear();
    m_trknPixHits->clear();
    m_trknPixHoles->clear();
    m_trknSCTHits->clear();
    m_trknSCTHoles->clear();
    m_trknTRTHits->clear();
    m_trknTRTHoles->clear();
    m_trknBLayerHits->clear();
    m_trknInnermostPixLayHits->clear();
    m_trkPixdEdX->clear();
  }

  if ( m_infoSwitch.m_promptlepton )
    {
      m_PromptLeptonInput_DL1mu         -> clear();
      m_PromptLeptonInput_DRlj          -> clear();
      m_PromptLeptonInput_LepJetPtFrac  -> clear();
      m_PromptLeptonInput_PtFrac        -> clear();
      m_PromptLeptonInput_PtRel         -> clear();
      m_PromptLeptonInput_TrackJetNTrack-> clear();
      m_PromptLeptonInput_ip2           -> clear();
      m_PromptLeptonInput_ip3           -> clear();
      m_PromptLeptonInput_rnnip         -> clear();
      m_PromptLeptonInput_sv1_jf_ntrkv  -> clear();
      m_PromptLeptonIso                 -> clear();
      m_PromptLeptonVeto                -> clear();
    }

  if ( m_infoSwitch.m_effSF && m_mc )
    {
      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>> > > kv : m_RecoEff_SF )
	kv.second->clear();

      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>> > > kv : m_IsoEff_SF )
	kv.second->clear();

      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>> > > kv : m_TrigEff_SF )
	kv.second->clear();

      for(std::pair<std::string, std::shared_ptr< std::vector<std::vector<float>> > > kv : m_TrigMCEff )
	kv.second->clear();

      m_TTVAEff_SF->clear();
    }

  if ( m_infoSwitch.m_energyLoss )
    {
      m_EnergyLoss               ->clear();
      m_EnergyLossSigma          ->clear();
      m_energyLossType           ->clear();
      m_MeasEnergyLoss           ->clear();
      m_MeasEnergyLossSigma      ->clear();
      m_ParamEnergyLoss          ->clear();
      m_ParamEnergyLossSigmaMinus->clear();
      m_ParamEnergyLossSigmaPlus ->clear();

  }

  return StatusCode::SUCCESS;
}


StatusCode MuonNtupler::FillMuon( const xAOD::Muon* muon, const xAOD::Vertex* primaryVertex )
{
  return FillMuon(static_cast<const xAOD::IParticle*>(muon), primaryVertex);
}

StatusCode MuonNtupler::FillMuon( const xAOD::IParticle* particle, const xAOD::Vertex* primaryVertex )
{
  ANA_CHECK(ParticleNtupler::FillParticle(particle));

  const xAOD::Muon* muon=dynamic_cast<const xAOD::Muon*>(particle);

  if ( m_infoSwitch.m_kinematic )
    {
      m_charge->push_back( muon->charge() );
    }

  if ( m_infoSwitch.m_trigger )
    {

    // retrieve map<string,char> w/ <chain,isMatched>
    //
    static SG::AuxElement::Accessor< std::map<std::string,char> > isTrigMatchedMapMuAcc("isTrigMatchedMapMu");

    std::vector<int> matches;

    if ( isTrigMatchedMapMuAcc.isAvailable( *muon ) ) {
      // loop over map and fill branches
      //
      for ( auto const &it : (isTrigMatchedMapMuAcc( *muon )) ) {
	matches.push_back( static_cast<int>(it.second) );
	m_listTrigChains->push_back( it.first );
      }
    } else {
      matches.push_back( -1 );
      m_listTrigChains->push_back("NONE");
    }

    m_isTrigMatchedToChain->push_back(matches);
    
    // if at least one match among the chains is found, say this muon is trigger matched
    if ( std::find(matches.begin(), matches.end(), 1) != matches.end() ) { m_isTrigMatched->push_back(1); }
    else { m_isTrigMatched->push_back(0); }
    
  }
  
  
  if ( m_infoSwitch.m_isolation )
    {
      for (const std::string& isol : m_infoSwitch.m_isolWPs)
	{
	  if (!isol.empty())
	    {
	      SG::AuxElement::ConstAccessor<char> isIsolated("isIsolated_" + isol);
	      safeFill<char, int, xAOD::Muon>( muon, isIsolated, m_isIsolated[isol], -1 );
	    }
	}
    }

  if ( m_infoSwitch.m_isolationKinematics )
    {
      m_ptcone20    ->push_back( muon->isolation( xAOD::Iso::ptcone20 )    /m_units );
      m_ptcone30    ->push_back( muon->isolation( xAOD::Iso::ptcone30 )    /m_units );
      m_ptcone40    ->push_back( muon->isolation( xAOD::Iso::ptcone40 )    /m_units );
      m_ptvarcone20 ->push_back( muon->isolation( xAOD::Iso::ptvarcone20 ) /m_units );
      m_ptvarcone30 ->push_back( muon->isolation( xAOD::Iso::ptvarcone30 ) /m_units );
      m_ptvarcone40 ->push_back( muon->isolation( xAOD::Iso::ptvarcone40 ) /m_units );
      m_topoetcone20->push_back( muon->isolation( xAOD::Iso::topoetcone20 )/m_units );
      m_topoetcone30->push_back( muon->isolation( xAOD::Iso::topoetcone30 )/m_units );
      m_topoetcone40->push_back( muon->isolation( xAOD::Iso::topoetcone40 )/m_units );
  }

  if ( m_infoSwitch.m_quality )
    {
      for (const std::string& quality : m_infoSwitch.m_recoWPs)
	{
	  if (!quality.empty())
	    {
	      SG::AuxElement::ConstAccessor<char> isQ( "is" + quality + "Q" );
	      safeFill<char, int, xAOD::Muon>( muon, isQ, m_quality[quality], -1 );
	    }
	}
    }

  const xAOD::TrackParticle* trk = muon->primaryTrackParticle();

  if ( m_infoSwitch.m_trackparams ) {
    if ( trk ) {
      //
      // NB.:
      // All track parameters are calculated at the perigee, i.e., the point of closest approach to the origin of some r.f. (which in RunII is NOT the ATLAS detector r.f!).
      // The reference  frame is chosen to be a system centered in the beamspot position, with z axis parallel to the beam line.
      // Remember that the beamspot size ( of O(10 micrometers) in the transverse plane) is << average vertex transverse position resolution ( O(60-80 micrometers) )
      // The coordinates of this r.f. wrt. the ATLAS system origin are returned by means of vx(), vy(), vz()
      //
      m_trkd0->push_back( trk->d0() );

      static SG::AuxElement::Accessor<float> d0SigAcc ("d0sig");
      float d0_significance =  ( d0SigAcc.isAvailable( *muon ) ) ? d0SigAcc( *muon ) : -1.0;
      m_trkd0sig->push_back( d0_significance );

      if (primaryVertex)
        m_trkz0->push_back( trk->z0()  - ( primaryVertex->z() - trk->vz() ) );
      else
        m_trkz0->push_back( -999.0 );
	    
      static SG::AuxElement::Accessor<float> z0sinthetaAcc("z0sintheta");
      float z0sintheta =  ( z0sinthetaAcc.isAvailable( *muon ) ) ? z0sinthetaAcc( *muon ) : -999.0;
      m_trkz0sintheta->push_back( z0sintheta );
      m_trkphi0  ->push_back( trk->phi0() );
      m_trktheta ->push_back( trk->theta() );
      m_trkcharge->push_back( trk->charge() );
      m_trkqOverP->push_back( trk->qOverP() );
    
    } else {
      m_trkd0          -> push_back( -999.0 );
      m_trkd0sig       -> push_back( -999.0 );
      m_trkz0          -> push_back( -999.0 );
      m_trkz0sintheta  -> push_back( -999.0 );
      m_trkphi0        -> push_back( -999.0 );
      m_trktheta       -> push_back( -999.0 );
      m_trkcharge      -> push_back( -999.0 );
      m_trkqOverP      -> push_back( -999.0 );
    }

  }

  if ( m_infoSwitch.m_trackhitcont ) {
    uint8_t nPixHits(-1), nPixHoles(-1), nSCTHits(-1), nSCTHoles(-1), nTRTHits(-1), nTRTHoles(-1), nBLayerHits(-1), nInnermostPixLayHits(-1);
    float pixdEdX(-1.0);
    
    if ( trk ) {
      trk->summaryValue( nPixHits,     xAOD::numberOfPixelHits );
      trk->summaryValue( nPixHoles,    xAOD::numberOfPixelHoles );
      trk->summaryValue( nSCTHits,     xAOD::numberOfSCTHits );
      trk->summaryValue( nSCTHoles,    xAOD::numberOfSCTHoles );
      trk->summaryValue( nTRTHits,     xAOD::numberOfTRTHits );
      trk->summaryValue( nTRTHoles,    xAOD::numberOfTRTHoles );
      trk->summaryValue( nBLayerHits,  xAOD::numberOfBLayerHits );
      trk->summaryValue( nInnermostPixLayHits, xAOD::numberOfInnermostPixelLayerHits );
      trk->summaryValue( pixdEdX,   xAOD::pixeldEdx);
    }
    
    m_trknSiHits               ->  push_back( nPixHits + nSCTHits );
    m_trknPixHits              ->  push_back( nPixHits );
    m_trknPixHoles             ->  push_back( nPixHoles );
    m_trknSCTHits              ->  push_back( nSCTHits );
    m_trknSCTHoles             ->  push_back( nSCTHoles );
    m_trknTRTHits              ->  push_back( nTRTHits );
    m_trknTRTHoles             ->  push_back( nTRTHoles );
    m_trknBLayerHits           ->  push_back( nBLayerHits );
    m_trknInnermostPixLayHits  ->  push_back( nInnermostPixLayHits );
    m_trkPixdEdX               ->  push_back( pixdEdX );
  }

  if ( m_infoSwitch.m_promptlepton ) {
    SG::AuxElement::ConstAccessor<float> acc_DL1mu          ("PromptLeptonInput_DL1mu");
    SG::AuxElement::ConstAccessor<float> acc_DRlj           ("PromptLeptonInput_DRlj");
    SG::AuxElement::ConstAccessor<float> acc_LepJetPtFrac   ("PromptLeptonInput_LepJetPtFrac");
    SG::AuxElement::ConstAccessor<float> acc_PtFrac         ("PromptLeptonInput_PtFrac");
    SG::AuxElement::ConstAccessor<float> acc_PtRel          ("PromptLeptonInput_PtRel");
    SG::AuxElement::ConstAccessor<short> acc_TrackJetNTrack ("PromptLeptonInput_TrackJetNTrack");
    SG::AuxElement::ConstAccessor<float> acc_ip2            ("PromptLeptonInput_ip2");
    SG::AuxElement::ConstAccessor<float> acc_ip3            ("PromptLeptonInput_ip3");
    SG::AuxElement::ConstAccessor<float> acc_rnnip          ("PromptLeptonInput_rnnip");
    SG::AuxElement::ConstAccessor<short> acc_sv1_jf_ntrkv   ("PromptLeptonInput_sv1_jf_ntrkv");
    SG::AuxElement::ConstAccessor<float> acc_Iso            ("PromptLeptonIso");
    SG::AuxElement::ConstAccessor<float> acc_Veto           ("PromptLeptonVeto");

    m_PromptLeptonInput_DL1mu          ->push_back( acc_DL1mu          .isAvailable(*muon) ? acc_DL1mu(*muon)          : -100);
    m_PromptLeptonInput_DRlj           ->push_back( acc_DRlj           .isAvailable(*muon) ? acc_DRlj(*muon)           : -100);
    m_PromptLeptonInput_LepJetPtFrac   ->push_back( acc_LepJetPtFrac   .isAvailable(*muon) ? acc_LepJetPtFrac(*muon)   : -100);
    m_PromptLeptonInput_PtFrac         ->push_back( acc_PtFrac         .isAvailable(*muon) ? acc_PtFrac(*muon)         : -100);
    m_PromptLeptonInput_PtRel          ->push_back( acc_PtRel          .isAvailable(*muon) ? acc_PtRel(*muon)          : -100);
    m_PromptLeptonInput_TrackJetNTrack ->push_back( acc_TrackJetNTrack .isAvailable(*muon) ? acc_TrackJetNTrack(*muon) : -100);
    m_PromptLeptonInput_ip2            ->push_back( acc_ip2            .isAvailable(*muon) ? acc_ip2(*muon)            : -100);
    m_PromptLeptonInput_ip3            ->push_back( acc_ip3            .isAvailable(*muon) ? acc_ip3(*muon)            : -100);
    m_PromptLeptonInput_rnnip          ->push_back( acc_rnnip          .isAvailable(*muon) ? acc_rnnip(*muon)          : -100);
    m_PromptLeptonInput_sv1_jf_ntrkv   ->push_back( acc_sv1_jf_ntrkv   .isAvailable(*muon) ? acc_sv1_jf_ntrkv(*muon)   : -100);
    m_PromptLeptonIso                  ->push_back( acc_Iso            .isAvailable(*muon) ? acc_Iso(*muon)            : -100);
    m_PromptLeptonVeto                 ->push_back( acc_Veto           .isAvailable(*muon) ? acc_Veto(*muon)           : -100);
  }

  if ( m_infoSwitch.m_effSF && m_mc )
    {

      static const std::vector<float> junkSF(1,-1.0);
      static const std::vector<float> junkEff(1,-1.0);

      for (const std::string& reco : m_infoSwitch.m_recoWPs)
	{
	  SG::AuxElement::ConstAccessor< std::vector< float > > MuRecoEff_SF_syst_Reco( "MuRecoEff_SF_syst_Reco" + reco );
	  safeSFVecFill<float, xAOD::Muon>( muon, MuRecoEff_SF_syst_Reco, m_RecoEff_SF[ reco ], junkSF );

	  for (const std::string& trig : m_infoSwitch.m_trigWPs)
	    {
	      SG::AuxElement::ConstAccessor< std::vector< float > > MuTrigEff_SF_syst_Trig_Reco( "MuTrigEff_SF_syst_" + trig + "_Reco" + reco );
	      safeSFVecFill<float, xAOD::Muon>( muon, MuTrigEff_SF_syst_Trig_Reco, m_TrigEff_SF[ trig+reco ], junkSF );

	      SG::AuxElement::ConstAccessor< std::vector< float > > MuTrigMCEff_syst_Trig_Reco( "MuTrigMCEff_syst_" + trig + "_Reco" + reco );
	      safeSFVecFill<float, xAOD::Muon>( muon, MuTrigMCEff_syst_Trig_Reco, m_TrigMCEff[ trig+reco ], junkEff );
	    }
	}

      for (const std::string& isol : m_infoSwitch.m_isolWPs)
	{
	  SG::AuxElement::Accessor< std::vector< float > > MuIsoEff_SF_syst_Iso( "MuIsoEff_SF_syst_Iso" + isol );
	  safeSFVecFill<float, xAOD::Muon>( muon, MuIsoEff_SF_syst_Iso, m_IsoEff_SF[ isol ], junkSF );
	}

      static SG::AuxElement::Accessor< std::vector< float > > accTTVASF("MuTTVAEff_SF_syst_TTVA");
      safeSFVecFill<float, xAOD::Muon>( muon, accTTVASF, m_TTVAEff_SF, junkSF );
    }

  if(m_infoSwitch.m_energyLoss )
    {
      static SG::AuxElement::Accessor< float >         accMuon_EnergyLoss                ("EnergyLoss");
      safeFill<float, float, xAOD::Muon>(muon, accMuon_EnergyLoss, m_EnergyLoss, -1);

      static SG::AuxElement::Accessor< float >         accMuon_EnergyLossSigma           ("EnergyLossSigma");
      safeFill<float, float, xAOD::Muon>(muon, accMuon_EnergyLossSigma, m_EnergyLossSigma, -1);

      static SG::AuxElement::Accessor< unsigned char > accMuon_energyLossType            ("energyLossType");
      safeFill<unsigned char, unsigned char, xAOD::Muon>(muon, accMuon_energyLossType, m_energyLossType, -1);

      static SG::AuxElement::Accessor< float >         accMuon_MeasEnergyLoss            ("MeasEnergyLoss");
      safeFill<float, float, xAOD::Muon>(muon, accMuon_MeasEnergyLoss, m_MeasEnergyLoss, -1);

      static SG::AuxElement::Accessor< float >         accMuon_MeasEnergyLossSigma       ("MeasEnergyLossSigma");
      safeFill<float, float, xAOD::Muon>(muon, accMuon_MeasEnergyLossSigma, m_MeasEnergyLossSigma, -1);

      static SG::AuxElement::Accessor< float >         accMuon_ParamEnergyLoss           ("ParamEnergyLoss");
      safeFill<float, float, xAOD::Muon>(muon, accMuon_ParamEnergyLoss, m_ParamEnergyLoss, -1);

      static SG::AuxElement::Accessor< float >         accMuon_ParamEnergyLossSigmaMinus ("ParamEnergyLossSigmaMinus");
      safeFill<float, float, xAOD::Muon>(muon, accMuon_ParamEnergyLossSigmaMinus, m_ParamEnergyLossSigmaMinus, -1);

      static SG::AuxElement::Accessor< float >         accMuon_ParamEnergyLossSigmaPlus  ("ParamEnergyLossSigmaPlus");
      safeFill<float, float, xAOD::Muon>(muon, accMuon_ParamEnergyLossSigmaPlus, m_ParamEnergyLossSigmaPlus, -1);

    }
  
  return StatusCode::SUCCESS;
}
