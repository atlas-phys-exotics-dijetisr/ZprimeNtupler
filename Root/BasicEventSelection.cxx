#include "ZprimeNtupler/BasicEventSelection.h"

// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

// package include(s):
#include <ZprimeNtupler/HelperFunctions.h>

#include "PATInterfaces/CorrectionCode.h"
#include <PathResolver/PathResolver.h>

// tools
#include "PileupReweighting/PileupReweightingTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"

// ROOT include(s):
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TSystem.h"
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"

using namespace Zprime;

BasicEventSelection :: BasicEventSelection(const std::string& name, ISvcLocator* pSvcLocator)
  : AnaAlgorithm(name, pSvcLocator)
{
  declareProperty("UseMetaData"    , m_useMetaData    );
  declareProperty("DerivationName" , m_derivationName );
  declareProperty("TruthLevelOnly" , m_truthLevelOnly );
  declareProperty("CheckDuplicates", m_checkDuplicates);
  declareProperty("DoPRW"          , m_doPRW          );

  // Posible selections
  declareProperty("ApplyGRLCut"              , m_applyGRLCut              );
  declareProperty("ApplyTriggerCut"          , m_applyTriggerCut          );
  declareProperty("ApplyPrimaryVertexCut"    , m_applyPrimaryVertexCut    );
  declareProperty("ApplyJetCleaningEventFlag", m_applyJetCleaningEventFlag);		  
  declareProperty("ApplyEventCleaningCut"    , m_applyEventCleaningCut    );
  declareProperty("ApplyCoreFlagsCut"        , m_applyCoreFlagsCut        );

  // Trigger
  declareProperty("TriggerSelection"     , m_triggerSelection     );
  declareProperty("ExtraTriggerSelection", m_extraTriggerSelection);
  declareProperty("StoreTrigDecisions"   , m_storeTrigDecisions   );
  declareProperty("StorePassL1"          , m_storePassL1          );
  declareProperty("StorePassHLT"         , m_storePassHLT         );
  declareProperty("StoreTrigKeys"        , m_storeTrigKeys        );

  // Pileup Reweighting
  declareProperty("AutoConfigPRW"      , m_autoconfigPRW      );
  declareProperty("PRWSearchPaths"     , m_prwSearchPaths     );
  declareProperty("PRWConfigFiles"     , m_prwConfigFiles     );
  declareProperty("LumiCalcFiles"      , m_lumiCalcFiles      );
  declareProperty("PRWActualMu2016File", m_prwActualMu2016File);
  declareProperty("PRWActualMu2017File", m_prwActualMu2017File);
  declareProperty("PRWActualMu2018File", m_prwActualMu2018File);
  declareProperty("MCCampaign"         , m_mcCampaign         );
  declareProperty("DataScaleFactor"    , m_DataScaleFactor    );

  // Tools
  declareProperty("GRLTool"     , m_grlTool     );
  declareProperty("TrigDecTool" , m_trigDecTool );
  declareProperty("TrigConfTool", m_trigConfTool);
}

StatusCode BasicEventSelection :: initialize ()
{
  ANA_CHECK( AnaAlgorithm::initialize() );

  //
  // Number of alternate weights
  uint32_t numWeights=1;
  if (  m_useMetaData )
    {
      // Now, let's find the actual information
      //
      const xAOD::CutBookkeeperContainer* completeCBC(nullptr);
      ANA_CHECK(inputMetaStore()->retrieve(completeCBC, "CutBookkeepers"));

      for ( const xAOD::CutBookkeeper* cbk: *completeCBC )
	{
	  if(cbk->name().substr(0,37) == "AllExecutedEvents_NonNominalMCWeight_" && cbk->name().length()!=17 && cbk->inputStream() == "StreamAOD")
	    numWeights++;
	}
    }

  //
  // event counts from meta data
  if ( !m_histEventCount )
    {
      ANA_CHECK(book(TH1D("MetaData_EventCount", "MetaData_EventCount", 6, 0.5, 6.5)));

      m_histEventCount = hist("MetaData_EventCount");
      m_histEventCount -> GetXaxis() -> SetBinLabel(1, "nEvents initial");
      m_histEventCount -> GetXaxis() -> SetBinLabel(2, "nEvents selected");
      m_histEventCount -> GetXaxis() -> SetBinLabel(3, "sumOfWeights initial");
      m_histEventCount -> GetXaxis() -> SetBinLabel(4, "sumOfWeights selected");
      m_histEventCount -> GetXaxis() -> SetBinLabel(5, "sumOfWeightsSquared initial");
      m_histEventCount -> GetXaxis() -> SetBinLabel(6, "sumOfWeightsSquared selected");
    }

  //
  // sumW's from meta data
  if ( !m_histSumW )
    {
      ANA_CHECK(book(TH1D("MetaData_SumW", "MetaData_SumW", numWeights, -0.5, numWeights-0.5)));

      m_histSumW = hist("MetaData_SumW");
    }

  //
  // Cutflow histograms

  // Note: the following code is needed for anyone developing/running in ROOT 6.04.10+
  // Bin extension is not done anymore via TH1::SetBit(TH1::kCanRebin), but with TH1::SetCanExtend(TH1::kAllAxes)

  ANA_CHECK(book(TH1D("cutflow", "cutflow", 1, 1, 2)));
  m_cutflowHist =hist("cutflow");
  m_cutflowHist->SetCanExtend(TH1::kAllAxes);

  ANA_CHECK(book(TH1D("cutflow_weighted", "cutflow_weighted", 1, 1, 2)));
  m_cutflowHistW =hist("cutflow_weighted");
  m_cutflowHistW->SetCanExtend(TH1::kAllAxes);

  // start labelling the bins for the event cutflow
  m_cutflow_all  = m_cutflowHist->GetXaxis()->FindBin("all");
  m_cutflowHistW->GetXaxis()->FindBin("all");

  m_cutflow_init  = m_cutflowHist->GetXaxis()->FindBin("init");
  m_cutflowHistW->GetXaxis()->FindBin("init");

  //
  // Tools

  if(m_applyGRLCut)
    { ANA_CHECK( m_grlTool.retrieve()); }

  if( m_trigConfTool.isSet() )
    ANA_CHECK( m_trigConfTool.retrieve(); );

  if( m_trigConfTool.isSet() )
    ANA_CHECK( m_trigDecTool.retrieve(); );
  
  return StatusCode::SUCCESS;
}

StatusCode BasicEventSelection::fileExecute()
{
  ANA_CHECK(AnaAlgorithm::fileExecute());

  //
  // Calculate meta data

  // get the MetaData tree once a new file is opened, with
  TTree* MetaData = dynamic_cast<TTree*>( wk()->inputFile()->Get("MetaData") );
  if ( !MetaData ) {
    ANA_MSG_ERROR( "MetaData tree not found! Exiting.");
    return StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);

  // Metadata for intial N (weighted) events are used to correctly normalise MC
  // if running on a MC DAOD which had some skimming applied at the derivation stage

  //check if file is from a DxAOD
  bool isDerivation = !MetaData->GetBranch("StreamAOD");

  if (  m_useMetaData )
    {
      // Now, let's find the actual information
      //
      const xAOD::CutBookkeeperContainer* completeCBC(nullptr);
      ANA_CHECK(inputMetaStore()->retrieve(completeCBC, "CutBookkeepers"));

      // Find the max cycle number
      int maxCycle(-1);
      for ( const xAOD::CutBookkeeper* cbk : *completeCBC )
	{
	  if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD")
	    maxCycle = cbk->cycle();
	}

      // Now, let's actually find the right one that contains all the needed info...
      const xAOD::CutBookkeeper* allEventsCBK(nullptr);
      const xAOD::CutBookkeeper* DxAODEventsCBK(nullptr);

      if ( isDerivation )
	{
	  if(m_derivationName != "")
	    {
	      ANA_MSG_INFO("Override auto config to look at DAOD made by Derivation Algorithm: " << m_derivationName);
	    }
	  else
	    {
	      ANA_MSG_INFO("Will autoconfig to look at DAOD made by Derivation Algorithm.");
	    }
	}

      for ( const xAOD::CutBookkeeper* cbk: *completeCBC )
	{
	  // Find initial book keeper
	  ANA_MSG_INFO("Complete cbk name: " << cbk->name() << " - stream: " << cbk->inputStream()  << " - cycle: " << cbk->cycle());
	  if( cbk->cycle() == maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" )
	    {
	      allEventsCBK = cbk;
	    }

	  // Find derivation book keeper
	  if ( isDerivation )
	    {
	      if(!m_derivationName.empty())
		{
		  if ( cbk->name() == m_derivationName )
		    DxAODEventsCBK = cbk;
		}
	      else if( cbk->name().find("Kernel") != std::string::npos )
		{
		  ANA_MSG_INFO("Auto config found DAOD made by Derivation Algorithm: " << cbk->name());
		  DxAODEventsCBK = cbk;
		}
	    } // is derivation

	  // Find and record AOD-level sumW information for all alternate weights
	  //  The nominal AllExecutedEvents will be filled later, due to posibility of multiple CBK entries
	  if(cbk->cycle() == maxCycle && cbk->name().substr(0,37) == "AllExecutedEvents_NonNominalMCWeight_" && cbk->name().length()!=17 && cbk->inputStream() == "StreamAOD")
	    {
	      // Extract weight index from name
	      int32_t idx=std::stoi(cbk->name().substr(37));
	      m_histSumW->Fill(idx, cbk->sumOfEventWeights());
	    }
	  else if(cbk->cycle() == maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD")
	    {
	      m_histSumW->Fill( 0., cbk->sumOfEventWeights());
	    }
	}

      uint64_t MD_initialNevents   = 0;
      double MD_initialSumW        = 0;
      double MD_initialSumWSquared = 0;      
      if(allEventsCBK == nullptr)
	{
	  ANA_MSG_WARNING("No allEventsCBK found (this is expected for DataScouting, otherwise not). Event numbers set to 0.");
	}
      else
	{
	  MD_initialNevents     = allEventsCBK->nAcceptedEvents();
	  MD_initialSumW        = allEventsCBK->sumOfEventWeights();
	  MD_initialSumWSquared = allEventsCBK->sumOfEventWeightsSquared();
	}

      if ( isDerivation && !DxAODEventsCBK )
	{
	  ANA_MSG_ERROR( "No CutBookkeeper corresponds to the selected Derivation Framework algorithm name. Check it with your DF experts! Aborting.");
	  return StatusCode::FAILURE;
      }

      uint64_t MD_finalNevents   = ( isDerivation ) ? DxAODEventsCBK->nAcceptedEvents()          : MD_initialNevents;
      double MD_finalSumW	 = ( isDerivation ) ? DxAODEventsCBK->sumOfEventWeights()        : MD_initialSumW;
      double MD_finalSumWSquared = ( isDerivation ) ? DxAODEventsCBK->sumOfEventWeightsSquared() : MD_initialSumWSquared;

      // Write metadata event bookkeepers to histogram
      //
      ANA_MSG_INFO( "Meta data from this file:");
      ANA_MSG_INFO( "Initial  events  = "                << static_cast<unsigned int>(MD_initialNevents) );
      ANA_MSG_INFO( "Selected events  = "                << static_cast<unsigned int>(MD_finalNevents) );
      ANA_MSG_INFO( "Initial  sum of weights = "         << MD_initialSumW);
      ANA_MSG_INFO( "Selected sum of weights = "         << MD_finalSumW);
      ANA_MSG_INFO( "Initial  sum of weights squared = " << MD_initialSumWSquared);
      ANA_MSG_INFO( "Selected sum of weights squared = " << MD_finalSumWSquared);

      m_cutflowHist ->Fill(m_cutflow_all, MD_initialNevents);
      m_cutflowHistW->Fill(m_cutflow_all, MD_initialSumW);

      m_histEventCount -> Fill(1, MD_initialNevents);
      m_histEventCount -> Fill(2, MD_finalNevents);
      m_histEventCount -> Fill(3, MD_initialSumW);
      m_histEventCount -> Fill(4, MD_finalSumW);
      m_histEventCount -> Fill(5, MD_initialSumWSquared);
      m_histEventCount -> Fill(6, MD_finalSumWSquared);
    }  

  return StatusCode::SUCCESS;
}

StatusCode BasicEventSelection::executeFirstEvent()
{
  ANA_CHECK(AnaAlgorithm::executeFirstEvent());

  //
  // Extra cuts
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK( evtStore()->retrieve(eventInfo, m_eventInfoContainerName) );

  ANA_MSG_INFO( "Setting up histograms based on isMC()");

  if ( !isMC() )
    {
      if ( m_applyGRLCut )
	{
	  m_cutflow_grl = m_cutflowHist->GetXaxis()->FindBin("GRL");
	  m_cutflowHistW->GetXaxis()->FindBin("GRL");
	}

      m_cutflow_lar  = m_cutflowHist->GetXaxis()->FindBin("LAr");
      m_cutflowHistW->GetXaxis()->FindBin("LAr");
      m_cutflow_tile = m_cutflowHist->GetXaxis()->FindBin("tile");
      m_cutflowHistW->GetXaxis()->FindBin("tile");
      m_cutflow_SCT = m_cutflowHist->GetXaxis()->FindBin("SCT");
      m_cutflowHistW->GetXaxis()->FindBin("SCT");
      m_cutflow_core = m_cutflowHist->GetXaxis()->FindBin("core");
      m_cutflowHistW->GetXaxis()->FindBin("core");
    }

  if ( m_applyJetCleaningEventFlag )
    {
      m_cutflow_jetcleaning = m_cutflowHist->GetXaxis()->FindBin("JetCleaning");
      m_cutflowHistW->GetXaxis()->FindBin("JetCleaning");
    }

  if ( !isMC() )
    {
      if ( m_applyIsBadBatmanFlag )
	{
	  m_cutflow_isbadbatman =  m_cutflowHist->GetXaxis()->FindBin("IsBadBatman");
	  m_cutflowHistW->GetXaxis()->FindBin("IsBadBatman");
	}
    }

  m_cutflow_npv  = m_cutflowHist->GetXaxis()->FindBin("NPV");
  m_cutflowHistW->GetXaxis()->FindBin("NPV");

  if ( !m_triggerSelection.empty() && m_applyTriggerCut )
    {
      m_cutflow_trigger = m_cutflowHist->GetXaxis()->FindBin("Trigger");
      m_cutflowHistW->GetXaxis()->FindBin("Trigger");
    }

  //
  // Create TTree for bookeeeping duplicated events
  if(m_checkDuplicates)
    {
      ANA_CHECK(book(TTree("duplicates","Info on duplicated events")));
      m_duplicatesTree=tree("duplicates");
      m_duplicatesTree->Branch("runNumber"  , &m_duplRunNumber  , "runNumber/I");
      m_duplicatesTree->Branch("eventNumber", &m_duplEventNumber, "eventNumber/L");
    }

  //
  // Print trigger information
  m_triggerSelectionList.clear();  
  m_extraTriggerSelectionList.clear();

  if ( !m_triggerSelection.empty() )
    {
      ANA_MSG_INFO( "*** Triggers used (in OR) are:\n");

      std::string token;
      std::istringstream ss(m_triggerSelection);
      while (std::getline(ss, token, ','))
	{
	  auto printingTriggerChainGroup = m_trigDecTool->getChainGroup(token);
	  std::vector<std::string> triggersUsed = printingTriggerChainGroup->getListOfTriggers();
	  for ( const std::string& triggerUsed : triggersUsed)
	    {
	      ANA_MSG_INFO("\t" << triggerUsed);
	      m_triggerSelectionList     .push_back(triggerUsed);
	      m_extraTriggerSelectionList.push_back(triggerUsed);
	    }
	}
    }

  if ( !m_extraTriggerSelection.empty() )
    {
      ANA_MSG_INFO( "*** Extra Trigger Info Saved are :\n");

      std::string token;
      std::istringstream ss(m_extraTriggerSelection);
      while (std::getline(ss, token, ','))
	{
	  auto printingTriggerChainGroup = m_trigDecTool->getChainGroup(token);
	  std::vector<std::string> triggersUsed = printingTriggerChainGroup->getListOfTriggers();
	  for ( const std::string& triggerUsed : triggersUsed)
	    {
	      ANA_MSG_INFO("\t" << triggerUsed);
	      m_extraTriggerSelectionList.push_back(triggerUsed);
	    }
	}
    }

  ANA_MSG_INFO( "Setting Up Tools");


  // 3.
  // initialize the CP::PileupReweightingTool
  //

  if ( m_doPRW )
    {
      m_prwTool.setProperty("DataScaleFactor", m_DataScaleFactor);
      if(m_autoconfigPRW)
	{ ANA_CHECK( autoconfigurePileupRWTool() ); }
      else
	{
	  ANA_CHECK( m_prwTool.setProperty("ConfigFiles"  , m_prwConfigFiles));
	  ANA_CHECK( m_prwTool.setProperty("LumiCalcFiles", m_lumiCalcFiles ));
	}

      ANA_CHECK( m_prwTool.retrieve() );
    }

  // As a check, let's see the number of events in our file (long long int)
  //
  ANA_MSG_INFO( "Number of events in file = " << evtStore()->event()->getEntries());

  // Initialize counter for number of entries
  m_eventCounter   = 0;

  ANA_MSG_INFO( "BasicEventSelection succesfully initialized!");

  return StatusCode::SUCCESS;
}



StatusCode BasicEventSelection :: execute ()
{
  ANA_CHECK(AnaAlgorithm::execute());
  
  ANA_MSG_DEBUG( "Basic Event Selection");

  // Print every 1000 entries, so we know where we are:
  //
  if ( (m_eventCounter % 1000) == 0 )
    {
      ANA_MSG_INFO( "Entry number = " << m_eventCounter);
    }
  m_eventCounter++;

  //------------------
  // Grab event
  //------------------
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK( evtStore()->retrieve(eventInfo, m_eventInfoContainerName) );

  //------------------------------------------------------------------------------------------
  // Declare an 'eventInfo' decorator with the MC event weight
  //------------------------------------------------------------------------------------------
  static SG::AuxElement::Decorator< float > mcEvtWeightDecor("mcEventWeight");
  static SG::AuxElement::Accessor< float >  mcEvtWeightAcc("mcEventWeight");

  float mcEvtWeight(1.0);

  // Check if need to create xAH event weight
  //
  if ( !mcEvtWeightDecor.isAvailable(*eventInfo) ) {
    if ( isMC() ) {
      const std::vector< float > weights = eventInfo->mcEventWeights(); // The weight (and systs) of all the MC events used in the simulation
      if ( weights.size() > 0 ) mcEvtWeight = weights[0];

    }
    // Decorate event with the *total* MC event weight
    //
    mcEvtWeightDecor(*eventInfo) = mcEvtWeight;
  } else {
    mcEvtWeight = mcEvtWeightAcc(*eventInfo);
  }

  //------------------------------------------------------------------------------------------
  // Fill initial bin of cutflow
  //------------------------------------------------------------------------------------------

  if( !m_useMetaData )
    {
      m_cutflowHist ->Fill( m_cutflow_all, 1 );
      m_cutflowHistW->Fill( m_cutflow_all, mcEvtWeight);

      m_histEventCount -> Fill(1, 1);
      m_histEventCount -> Fill(2, 1);
      m_histEventCount -> Fill(3, mcEvtWeight);
      m_histEventCount -> Fill(4, mcEvtWeight);
      m_histEventCount -> Fill(5, mcEvtWeight*mcEvtWeight);
      m_histEventCount -> Fill(6, mcEvtWeight*mcEvtWeight);
    }

  m_cutflowHist ->Fill( m_cutflow_init, 1 );
  m_cutflowHistW->Fill( m_cutflow_init, mcEvtWeight);

  //------------------------------------------------------------------------------------------
  // Update Pile-Up Reweighting
  //------------------------------------------------------------------------------------------
  if ( m_doPRW )
    {
      m_prwTool->applySystematicVariation(CP::SystematicSet()).ignore();
      ANA_CHECK(m_prwTool->apply( *eventInfo )); // NB: this call automatically decorates eventInfo with:
                                                 //  1.) the PU weight ("PileupWeight")
                                                 //  2.) the corrected mu ("corrected_averageInteractionsPerCrossing")
                                                 //  3.) the random run number ("RandomRunNumber")
                                                 //  4.) the random lumiblock number ("RandomLumiBlockNumber")
      // static SG::AuxElement::Decorator< float >  correctedAvgMu("corrected_averageInteractionsPerCrossing");
      static SG::AuxElement::Decorator< float > correctedAndScaledAvgMu("correctedScaled_averageInteractionsPerCrossing");
      static SG::AuxElement::Decorator< float > correctedMu("corrected_actualInteractionsPerCrossing");
      static SG::AuxElement::Decorator< float > correctedAndScaledMu("correctedScaled_actualInteractionsPerCrossing");

      correctedAndScaledAvgMu( *eventInfo ) = m_prwTool->getCorrectedAverageInteractionsPerCrossing( *eventInfo, true );
      correctedMu            ( *eventInfo ) = m_prwTool->getCorrectedActualInteractionsPerCrossing( *eventInfo );
      correctedAndScaledMu   ( *eventInfo ) = m_prwTool->getCorrectedActualInteractionsPerCrossing( *eventInfo, true );

      if ( isMC() && m_doPRWSys )
	{
	  CP::SystematicSet tmpSet;tmpSet.insert(CP::SystematicVariation("PRW_DATASF",1));
	  m_prwTool->applySystematicVariation( tmpSet ).ignore();
	  eventInfo->auxdecor< float >( "PileupWeight_UP" )= m_prwTool->getCombinedWeight( *eventInfo );
	  tmpSet.clear();tmpSet.insert(CP::SystematicVariation("PRW_DATASF",-1));
	  m_prwTool->applySystematicVariation( tmpSet ).ignore();
	  eventInfo->auxdecor< float >( "PileupWeight_DOWN")= m_prwTool->getCombinedWeight( *eventInfo );
	}
    }

  if ( m_actualMuMin > 0 )
    {
      // apply minimum pile-up cut
      if ( eventInfo->actualInteractionsPerCrossing() < m_actualMuMin )
	{
	  wk()->skipEvent();
	  return StatusCode::SUCCESS;
	}
    }

  if ( m_actualMuMax > 0 )
    {
      // apply maximum pile-up cut
      if ( eventInfo->actualInteractionsPerCrossing() > m_actualMuMax )
	{
          wk()->skipEvent();
          return StatusCode::SUCCESS;
	}
    }

  //------------------------------------------------------
  // If data, check if event passes GRL and event cleaning
  //------------------------------------------------------
  if ( !isMC() )
    {

      // Get the streams that the event was put in
      const std::vector<  xAOD::EventInfo::StreamTag > streams = eventInfo->streamTags();

      for ( const xAOD::EventInfo::StreamTag& stream : streams )
	{
	  ANA_MSG_DEBUG( "event has fired stream: " << stream.name() );
	}

      // GRL
      if ( m_applyGRLCut )
	{
	  if ( !m_grlTool->passRunLB( *eventInfo ) )
	    {
	      wk()->skipEvent();
	      return StatusCode::SUCCESS; // go to next event
	    }
	  m_cutflowHist ->Fill( m_cutflow_grl, 1 );
	  m_cutflowHistW->Fill( m_cutflow_grl, mcEvtWeight);
	}

    //------------------------------------------------------------
    // Apply event cleaning to remove events due to
    // problematic regions of the detector, and incomplete events.
    // Apply to data.
    //------------------------------------------------------------

    if ( m_applyEventCleaningCut && (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) ) {
      wk()->skipEvent();
      return StatusCode::SUCCESS;
    }
    m_cutflowHist ->Fill( m_cutflow_lar, 1 );
    m_cutflowHistW->Fill( m_cutflow_lar, mcEvtWeight);

    if ( m_applyEventCleaningCut && (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) ) {
      wk()->skipEvent();
      return StatusCode::SUCCESS;
    }
    m_cutflowHist ->Fill( m_cutflow_tile, 1 );
    m_cutflowHistW->Fill( m_cutflow_tile, mcEvtWeight);

    if ( m_applyEventCleaningCut && (eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error) ) {
      wk()->skipEvent();
      return StatusCode::SUCCESS;
    }
    m_cutflowHist ->Fill( m_cutflow_SCT, 1 );
    m_cutflowHistW->Fill( m_cutflow_SCT, mcEvtWeight);

    if ( m_applyCoreFlagsCut && (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) ) {
      wk()->skipEvent();
      return StatusCode::SUCCESS;
    }
    m_cutflowHist ->Fill( m_cutflow_core, 1 );
    m_cutflowHistW->Fill( m_cutflow_core, mcEvtWeight);

  }

  // more info: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HowToCleanJets2017
  if ( m_applyJetCleaningEventFlag && eventInfo->isAvailable<char>("DFCommonJets_eventClean_LooseBad") ) {
    if(eventInfo->auxdataConst<char>("DFCommonJets_eventClean_LooseBad")<1) {
	wk()->skipEvent();
	return StatusCode::SUCCESS;
      }
  }
  m_cutflowHist ->Fill( m_cutflow_jetcleaning, 1 );
  m_cutflowHistW->Fill( m_cutflow_jetcleaning, mcEvtWeight);

  // n.b. this cut should only be applied in 2015+16 data, and not to MC!
  // details here: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HowToCleanJets2017#IsBadBatMan_Event_Flag_and_EMEC
  if ( m_applyIsBadBatmanFlag && eventInfo->isAvailable<char>("DFCommonJets_isBadBatman") &&  !isMC() ) {
    if(eventInfo->auxdataConst<char>("DFCommonJets_isBadBatman")>0) {
      wk()->skipEvent();
      return StatusCode::SUCCESS;
    }
  }
  m_cutflowHist ->Fill( m_cutflow_isbadbatman, 1 );
  m_cutflowHistW->Fill( m_cutflow_isbadbatman, mcEvtWeight);

  //-----------------------------
  // Primary Vertex 'quality' cut
  //-----------------------------

  const xAOD::VertexContainer* vertices(nullptr);
  if ( !m_truthLevelOnly && m_applyPrimaryVertexCut )
    {
      ANA_CHECK( evtStore()->retrieve(vertices, m_vertexContainerName) );

      if ( !HelperFunctions::passPrimaryVertexSelection( vertices, m_PVNTrack ) )
	{
	  wk()->skipEvent();
	  return StatusCode::SUCCESS;
	}
    }
  m_cutflowHist ->Fill( m_cutflow_npv, 1 );
  m_cutflowHistW->Fill( m_cutflow_npv, mcEvtWeight);

  //---------------------
  // Trigger decision cut
  //---------------------

  if ( !m_triggerSelectionList.empty() && m_applyTriggerCut)
    {
      bool isPassed=false;
      for(const std::string& trigger : m_triggerSelectionList)
	isPassed|= m_trigDecTool->isPassed(trigger);

      if ( m_applyTriggerCut )
	{
	  if ( !isPassed )
	    {
	      wk()->skipEvent();
	      return StatusCode::SUCCESS;
	    }
	  m_cutflowHist ->Fill( m_cutflow_trigger, 1 );
	  m_cutflowHistW->Fill( m_cutflow_trigger, mcEvtWeight);
	}
    }

  // Store decision
  if ( !m_extraTriggerSelectionList.empty() && m_storeTrigDecisions  )
    {
      // Save info for the triggers used to skim events
      //
      for ( const std::string & trigger : m_extraTriggerSelectionList)
	{
	  SG::AuxElement::Decorator<char     > isPassed    (trigger                );
	  SG::AuxElement::Decorator<char     > disabled    (trigger+"_disabled"    );
	  SG::AuxElement::Decorator<float    > prescale    (trigger+"_prescale"    );
	  SG::AuxElement::Decorator<float    > prescaleLumi(trigger+"_prescaleLumi");
	  SG::AuxElement::Decorator<uint32_t > isPassedBits(trigger+"_isPassBits"  );

	  auto trigChain = m_trigDecTool->getChainGroup(trigger);

	  isPassed( *eventInfo ) = trigChain->isPassed();
	  disabled( *eventInfo ) = trigChain->getPrescale()<1;	  
	  prescale( *eventInfo ) = trigChain->getPrescale();

          bool doLumiPrescale = std::find(m_triggerUnprescaleList.begin(), m_triggerUnprescaleList.end(), trigger) != m_triggerUnprescaleList.end();
          if ( doLumiPrescale )
	    prescaleLumi( *eventInfo ) = m_prwTool->getDataWeight( *eventInfo, trigger, true );
	  else
	    prescaleLumi( *eventInfo) = -1;

	  isPassedBits( *eventInfo ) = m_trigDecTool->isPassedBits(trigger);
	}
    }

    if ( m_storePassL1 )
      {
	static SG::AuxElement::Decorator< char > passL1("passL1");
	passL1(*eventInfo)  = ( m_triggerSelection.find("L1_") != std::string::npos )  ? (int)m_trigDecTool->isPassed(m_triggerSelection.c_str()) : -1;
      }

    if ( m_storePassHLT )
      {
	static SG::AuxElement::Decorator< char > passHLT("passHLT");
	passHLT(*eventInfo) = ( m_triggerSelection.find("HLT_") != std::string::npos ) ? (int)m_trigDecTool->isPassed(m_triggerSelection.c_str()) : -1;
      }

    if ( m_storeTrigKeys )
      {
	static SG::AuxElement::Decorator< int > masterKey("masterKey");
	masterKey(*eventInfo) = m_trigConfTool->masterKey();
	static SG::AuxElement::Decorator< int > L1PSKey("L1PSKey");
	L1PSKey  (*eventInfo) = m_trigConfTool->lvl1PrescaleKey();
	static SG::AuxElement::Decorator< int > HLTPSKey("HLTPSKey");
	HLTPSKey (*eventInfo) = m_trigConfTool->hltPrescaleKey();
      }

    // Calculate distance to previous empty BCID and previous unpaired BCID, and save as decorations
    if( m_calcBCIDInfo && !isMC())
      {
	//Distance to previous empty BCID
	for (int i = eventInfo->bcid() - 1; i >= 0; i--)
	  {
	    //get the bunch group pattern for bunch crossing i
	    uint16_t bgPattern = m_trigConfTool->bunchGroupSet()->bgPattern()[i];
	    bool isEmpty = (bgPattern >> 3) & 0x1;
	    if (isEmpty)
	      {
		static SG::AuxElement::Decorator< int > DistEmptyBCID("DistEmptyBCID");
		DistEmptyBCID(*eventInfo) = eventInfo->bcid()-i;
		break;
	      }
	  }//for each bcid
	//Distance to previous unpaired crossing
	for (int i = eventInfo->bcid() - 1; i >= 0; i--)
	  {
	    //get the bunch group pattern for bunch crossing i
	    uint16_t bgPattern = m_trigConfTool->bunchGroupSet()->bgPattern()[i];
	    bool isUnpaired = !((bgPattern >> 1) & 0x1);
	    if (isUnpaired)
	      {
		static SG::AuxElement::Decorator< int > DistLastUnpairedBCID("DistLastUnpairedBCID");
		DistLastUnpairedBCID(*eventInfo) = eventInfo->bcid()-i;
		break;
	      }
	  }//for each bcid
	//Distance to next unpaired crossing
	for (int i = eventInfo->bcid() + 1; i <= 3654; i++)
	  {
	    //get the bunch group pattern for bunch crossing i
	    uint16_t bgPattern = m_trigConfTool->bunchGroupSet()->bgPattern()[i];
	    bool isUnpaired = !((bgPattern >> 1) & 0x1);
	    if (isUnpaired)
	      {
		static SG::AuxElement::Decorator< int > DistNextUnpairedBCID("DistNextUnpairedBCID");
		DistNextUnpairedBCID(*eventInfo) = i-eventInfo->bcid();
		break;
	      }
	  }//  for each bcid
      }//if data

    return StatusCode::SUCCESS;
}

// "Borrowed" from SUSYTools
// https://gitlab.cern.ch/atlas/athena/blob/3be30397de7c6cfdc15de38f532fdb4b9f338297/PhysicsAnalysis/SUSYPhys/SUSYTools/Root/SUSYObjDef_xAOD.cxx#L700
StatusCode BasicEventSelection::autoconfigurePileupRWTool()
{
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK( evtStore()->retrieve( eventInfo, m_eventInfoContainerName ) );

  // Determine simulation flavour
  std::string SimulationFlavour = isFastSim() ? "AFII" : "FS";

  // Extract campaign automatically from Run Number
  std::string mcCampaignMD = "";

  uint32_t runNum = eventInfo->runNumber();

  switch(runNum)
    {
    case 284500 :
      mcCampaignMD="mc16a";
      break;
    case 300000 :
      mcCampaignMD="mc16d";
      break;
    case 310000 :
      mcCampaignMD="mc16e";
      break;
    default :
      ANA_MSG_ERROR( "Could not determine mc campaign from run number! Impossible to autoconfigure PRW. Aborting." );
      return StatusCode::FAILURE;
      break;
    }
  ANA_MSG_INFO( "Determined MC campaign to be " << mcCampaignMD);

  // Sanity checks
  bool mc16X_GoodFromProperty = !m_mcCampaign.empty();
  bool mc16X_GoodFromMetadata = false;
  for(const std::string& mcCampaignP : m_mcCampaign) mc16X_GoodFromProperty &= ( mcCampaignP == "mc16a" || mcCampaignP == "mc16c" || mcCampaignP == "mc16d" || mcCampaignP == "mc16e" || mcCampaignP == "mc16f");
  if( mcCampaignMD == "mc16a" || mcCampaignMD == "mc16c" || mcCampaignMD == "mc16d" || mcCampaignMD == "mc16e" || mcCampaignMD == "mc16f") mc16X_GoodFromMetadata = true;

  if( !mc16X_GoodFromMetadata && !mc16X_GoodFromProperty )
    {
      // ::
      std::string MetadataAndPropertyBAD("");
      MetadataAndPropertyBAD += "autoconfigurePileupRWTool(): access to FileMetaData failed, but don't panic. You can try to manually set the 'mcCampaign' BasicEventSelection property to ";
      MetadataAndPropertyBAD += "'mc16a', 'mc16c', 'mc16d', 'mc16e', or 'mc16f' and restart your job. If you set it to any other string, you will still incur in this error.";
      ANA_MSG_ERROR( MetadataAndPropertyBAD );
      return StatusCode::FAILURE;
      // ::
    }

  if ( mc16X_GoodFromProperty && mc16X_GoodFromMetadata)
    {
      std::string mcCampaignStr;
      for(const std::string& mcCampaignP : m_mcCampaign)
	{
	  if(!mcCampaignStr.empty()) mcCampaignStr+=",";
	  mcCampaignStr+=mcCampaignP;
	}
      
      bool MDinP=false;
      for(const std::string& mcCampaignP : m_mcCampaign) MDinP |= (mcCampaignMD==mcCampaignP);
      if( !MDinP )
	{
	  // ::
	  std::string MetadataAndPropertyConflict("");
	  MetadataAndPropertyConflict += "autoconfigurePileupRWTool(): access to FileMetaData indicates a " + mcCampaignMD;
	  MetadataAndPropertyConflict += " sample, but the 'mcCampaign' property passed to BasicEventSelection is set to '" +mcCampaignStr;
	  MetadataAndPropertyConflict += "'. Prioritizing the value set by user: PLEASE DOUBLE-CHECK the value you set the 'mcCampaign' property to!";
	  ANA_MSG_WARNING( MetadataAndPropertyConflict );
	  // ::
	}
      else
	{
	  // ::
	  std::string NoMetadataButPropertyOK("");
	  NoMetadataButPropertyOK += "autoconfigurePileupRWTool(): access to FileMetaData succeeded, but the 'mcCampaign' property is passed to BasicEventSelection as '";
	  NoMetadataButPropertyOK += mcCampaignStr;
	  NoMetadataButPropertyOK += "'. Autoconfiguring PRW accordingly.";
	  ANA_MSG_WARNING( NoMetadataButPropertyOK );
	  // ::
	}
    }

  // ::
  // Retrieve the input file
  std::vector<std::string> mcCampaignList;
  if(!mc16X_GoodFromProperty)
    {
      mcCampaignList.push_back(mcCampaignMD);
    }
  else
    {
      mcCampaignList = m_mcCampaign;
    }

  ANA_MSG_INFO( "Setting MC campgains for CP::PileupReweightingTool:");
  for(const std::string& mcCampaign : mcCampaignList)
    ANA_MSG_INFO( "\t" << mcCampaign.c_str() );

  //
  float dsid = -999;
  dsid = eventInfo->mcChannelNumber();
  int DSID_INT = (int) dsid;

  std::vector<std::string> prwConfigFiles;
  for(const std::string& mcCampaign : mcCampaignList)
    {
      std::string prwConfigFile;
      for(const std::string& PRWSearchPath : m_prwSearchPaths)
	{
	  prwConfigFile = PathResolverFindCalibFile(PRWSearchPath + "/DSID" + std::to_string(DSID_INT/1000) +"xxx/pileup_" + mcCampaign + "_dsid" + std::to_string(DSID_INT) + "_" + SimulationFlavour + ".root");
	  if(!prwConfigFile.empty()) break;
	}

      TFile testF(prwConfigFile.data(),"read");
      if(testF.IsZombie())
	{
	  ANA_MSG_ERROR("autoconfigurePileupRWTool(): Missing PRW config file for DSID " << std::to_string(DSID_INT) << " in campaign " << mcCampaign);
	  return StatusCode::FAILURE;
	}
      else
	prwConfigFiles.push_back( prwConfigFile );
    }

  // Add actualMu config files
  for(const std::string& mcCampaign : mcCampaignList)
    {
      if( !m_prwActualMu2016File.empty() &&  mcCampaign == "mc16a" )
	prwConfigFiles.push_back(PathResolverFindCalibFile(m_prwActualMu2016File));
      if( !m_prwActualMu2017File.empty() && (mcCampaign == "mc16c" || mcCampaign=="mc16d") )
	prwConfigFiles.push_back(PathResolverFindCalibFile(m_prwActualMu2017File));
      if( !m_prwActualMu2018File.empty() && (mcCampaign == "mc16e" || mcCampaign=="mc16f") )
	prwConfigFiles.push_back(PathResolverFindCalibFile(m_prwActualMu2018File));
    }

  // also need to handle lumicalc files: only use 2015+2016 with mc16a
  // and only use 2017 with mc16c
  // according to instructions on https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ExtendedPileupReweighting#Tool_Properties

  std::vector<std::string> lumiCalcFiles;
  for(const std::string& mcCampaign : mcCampaignList)
    {
      for(const std::string& filename : m_lumiCalcFiles)
	{
	  // looking for things of format "stuff/data15_13TeV/stuff" etc
	  size_t pos = filename.find("data");
	  std::string year = filename.substr(pos+4, 2);

	  // Case mc16a: want 2015 and 2016
	  if (mcCampaign == "mc16a")
	    {
	      if (year == "15" || year == "16")
		lumiCalcFiles.push_back(filename);
	    }
	  else if (mcCampaign == "mc16c" || mcCampaign == "mc16d")
	    {
	      if (year == "17")
		lumiCalcFiles.push_back(filename);
	    }
	  else if (mcCampaign == "mc16e" || mcCampaign == "mc16f")
	    {
	      if (year == "18")
		lumiCalcFiles.push_back(filename);
	    }
	  else
	    {
	      ANA_MSG_ERROR( "No lumicalc file is suitable for your mc campaign!" );
	      return StatusCode::FAILURE;
	    }
	}
    }

  // Set everything and report on it.
  ANA_MSG_INFO( "Adding Pileup files for CP::PileupReweightingTool:");
  for(const std::string& prwConfigFile : prwConfigFiles)
    ANA_MSG_INFO("\t- " << prwConfigFile);
  ANA_CHECK( m_prwTool.setProperty("ConfigFiles", prwConfigFiles));

  ANA_MSG_INFO( "Adding LumiCalc files for CP::PileupReweightingTool:");
  for(const std::string& lumiCalcFile : lumiCalcFiles)
    ANA_MSG_INFO("\t- " << lumiCalcFile);
  ANA_CHECK( m_prwTool.setProperty("LumiCalcFiles", lumiCalcFiles));

  // Return gracefully
  return StatusCode::SUCCESS;
}

StatusCode BasicEventSelection :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  ANA_MSG_INFO( "Number of processed events \t= " << m_eventCounter);

  //after execution loop
  if(m_printBranchList)
    {
      xAOD::IOStats::instance().stats().printSmartSlimmingBranchList();
    }

  return StatusCode::SUCCESS;
}
