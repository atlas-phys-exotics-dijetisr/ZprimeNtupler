#include "ZprimeNtupler/TruthParticleNtupler.h"

#include "ZprimeNtupler/HelperFunctions.h"

#include <xAODTruth/TruthVertex.h>

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

TruthParticleNtupler::TruthParticleNtupler(const std::string& name, const std::string& detailStr, float units)
  : ParticleNtupler(name,detailStr,units,true)

{
  m_pdgId   =std::make_shared<std::vector<int>>();
  m_status  =std::make_shared<std::vector<int>>();
  m_barcode =std::make_shared<std::vector<int>>();

  if(m_infoSwitch.m_type)
    {
      m_is_higgs  =std::make_shared<std::vector<int>>();
      m_is_bhad   =std::make_shared<std::vector<int>>();
    }

  if(m_infoSwitch.m_bVtx)
    {
      m_Bdecay_x  =std::make_shared<std::vector<float>>();
      m_Bdecay_y  =std::make_shared<std::vector<float>>();
      m_Bdecay_z  =std::make_shared<std::vector<float>>();
    }

  if(m_infoSwitch.m_parents)
    {
      m_nParents        =std::make_shared<std::vector<int>>();
      m_parent_pdgId    =std::make_shared<std::vector< std::vector<int> >>();
      m_parent_barcode  =std::make_shared<std::vector< std::vector<int> >>();
      m_parent_status   =std::make_shared<std::vector< std::vector<int> >>();
    }

  if(m_infoSwitch.m_children)
    {
      m_nChildren      =std::make_shared<std::vector<int>>();
      m_child_pdgId    =std::make_shared<std::vector< std::vector<int> >>();
      m_child_barcode  =std::make_shared<std::vector< std::vector<int> >>();
      m_child_status   =std::make_shared<std::vector< std::vector<int> >>();
    }

  if(m_infoSwitch.m_dressed)
    {
      m_pt_dressed  =std::make_shared<std::vector<float>>();
      m_eta_dressed =std::make_shared<std::vector<float>>();
      m_phi_dressed =std::make_shared<std::vector<float>>();
      m_e_dressed   =std::make_shared<std::vector<float>>();
    }

  if(m_infoSwitch.m_origin)
    {
      m_origin =std::make_shared<std::vector<unsigned int>>();
    }
}

StatusCode TruthParticleNtupler::createBranches(TTree *tree)
{
  ANA_CHECK(ParticleNtupler::createBranches(tree));

  createBranch<int>(tree,"pdgId"  , m_pdgId  );
  createBranch<int>(tree,"status" , m_status );
  createBranch<int>(tree,"barcode", m_barcode);

  if(m_infoSwitch.m_type){
    createBranch<int>(tree,"is_higgs",                      m_is_higgs              );
    createBranch<int>(tree,"is_bhad",                       m_is_bhad              );
  }


  if(m_infoSwitch.m_bVtx){
    createBranch<float>(tree,"Bdecay_x",                      m_Bdecay_x              );
    createBranch<float>(tree,"Bdecay_y",                      m_Bdecay_y              );
    createBranch<float>(tree,"Bdecay_z",                      m_Bdecay_z              );
  }

  if(m_infoSwitch.m_parents){
    createBranch<int>         (tree,"nParents",                      m_nParents              );
    createBranch<std::vector<int> >(tree,"parent_pdgId",                  m_parent_pdgId          );
    createBranch<std::vector<int> >(tree,"parent_barcode",                m_parent_barcode        );
    createBranch<std::vector<int> >(tree,"parent_status",                 m_parent_status         );
  }

  if(m_infoSwitch.m_children){
    createBranch<int>         (tree,"nChildren",                    m_nChildren            );
    createBranch<std::vector<int> >(tree,"child_pdgId",                  m_child_pdgId          );
    createBranch<std::vector<int> >(tree,"child_barcode",                m_child_barcode        );
    createBranch<std::vector<int> >(tree,"child_status",                 m_child_status         );
  }

  if(m_infoSwitch.m_dressed){
    createBranch<float> (tree,"pt_dressed", m_pt_dressed );
    createBranch<float> (tree,"eta_dressed", m_eta_dressed );
    createBranch<float> (tree,"phi_dressed", m_phi_dressed );
    createBranch<float> (tree,"e_dressed", m_e_dressed );
  }

  if(m_infoSwitch.m_origin)
    {
      createBranch<unsigned int> (tree,"origin",m_origin);
    }

  return StatusCode::SUCCESS;
}


StatusCode TruthParticleNtupler::clear()
{
  ANA_CHECK(ParticleNtupler::clear());

  m_pdgId ->clear();
  m_status->clear();
  m_barcode->clear();

  if(m_infoSwitch.m_type){
    m_is_higgs  ->clear();
    m_is_bhad   ->clear();
  }

  if(m_infoSwitch.m_bVtx){
    m_Bdecay_x->clear();
    m_Bdecay_y->clear();
    m_Bdecay_z->clear();
  }

  if(m_infoSwitch.m_parents){
    m_nParents->clear();
    m_parent_pdgId->clear();
    m_parent_barcode->clear();
    m_parent_status->clear();
  }

  if(m_infoSwitch.m_children){
    m_nChildren->clear();
    m_child_pdgId->clear();
    m_child_barcode->clear();
    m_child_status->clear();
  }

  if(m_infoSwitch.m_dressed){
    m_pt_dressed->clear();
    m_eta_dressed->clear();
    m_phi_dressed->clear();
    m_e_dressed->clear();
  }

  if(m_infoSwitch.m_origin){
    m_origin->clear();
  }

  return StatusCode::SUCCESS;
}

StatusCode TruthParticleNtupler::FillTruthParticle( const xAOD::TruthParticle* truth )
{
  return FillTruthParticle(static_cast<const xAOD::IParticle*>(truth));
}

StatusCode TruthParticleNtupler::FillTruthParticle( const xAOD::IParticle* particle )
{
  ANA_CHECK(ParticleNtupler::FillParticle(particle));

  const xAOD::TruthParticle* truth=dynamic_cast<const xAOD::TruthParticle*>(particle);

  m_pdgId  ->push_back( truth->pdgId() );
  m_status ->push_back( truth->status() );
  m_barcode->push_back( truth->barcode() );

  if(m_infoSwitch.m_type)
    {
      m_is_higgs->push_back( (int)truth->isHiggs()        );
      m_is_bhad ->push_back( (int)truth->isBottomHadron() );
    }

  if(m_infoSwitch.m_bVtx)
    {
      if(truth->isBottomHadron() && truth->hasDecayVtx())
	{
	  const xAOD::TruthVertex* vtx = truth->decayVtx();
	  m_Bdecay_x->push_back(vtx->x());
	  m_Bdecay_y->push_back(vtx->y());
	  m_Bdecay_z->push_back(vtx->z());
	}
      else
	{
	  m_Bdecay_x->push_back(-999999.);
	  m_Bdecay_y->push_back(-999999.);
	  m_Bdecay_z->push_back(-999999.);
	}
    }

  if(m_infoSwitch.m_parents)
    {
      int nParents = truth->nParents();
      m_nParents->push_back(nParents);

      m_parent_pdgId  ->push_back(std::vector<int>());
      m_parent_barcode->push_back(std::vector<int>());
      m_parent_status ->push_back(std::vector<int>());
      for(int iparent = 0; iparent < nParents; ++iparent)
	{
	  const xAOD::TruthParticle* parent = truth->parent(iparent);
	  if(parent)
	    {
	      m_parent_pdgId  ->back().push_back(parent->pdgId());
	      m_parent_barcode->back().push_back(parent->barcode());
	      m_parent_status ->back().push_back(parent->status());
	    }
	  else
	    {
	      m_parent_pdgId  ->back().push_back(-99);
	      m_parent_barcode->back().push_back(-99);
	      m_parent_status ->back().push_back(-99);
	    }
	}
    }

  if(m_infoSwitch.m_children)
    {
      int nChildren = truth->nChildren();
      m_nChildren->push_back(nChildren);

      m_child_pdgId  ->push_back(std::vector<int>());
      m_child_barcode->push_back(std::vector<int>());
      m_child_status ->push_back(std::vector<int>());
      for(int ichild = 0; ichild < nChildren; ++ichild)
	{
	  const xAOD::TruthParticle* child = truth->child(ichild);
	  if(child)
	    {
	      m_child_pdgId  ->back().push_back(child->pdgId());
	      m_child_barcode->back().push_back(child->barcode());
	      m_child_status ->back().push_back(child->status());
	    }
	  else
	    {
	      m_child_pdgId  ->back().push_back(-99);
	      m_child_barcode->back().push_back(-99);
	      m_child_status ->back().push_back(-99);
	    }
	}
    }

  if(m_infoSwitch.m_dressed)
    {
      if( truth->isAvailable<float>("pt_dressed") )
	{
	  float pt_dressed = truth->auxdata<float>("pt_dressed");
	  m_pt_dressed->push_back(pt_dressed / m_units);
	}
      else
	{
	  m_pt_dressed->push_back(-999);
	}
      if( truth->isAvailable<float>("eta_dressed") )
	{
	  float eta_dressed = truth->auxdata<float>("eta_dressed");
	  m_eta_dressed->push_back(eta_dressed);
	}
      else
	{
	  m_eta_dressed->push_back(-999);
	}
      if( truth->isAvailable<float>("phi_dressed") )
	{
	  float phi_dressed = truth->auxdata<float>("phi_dressed");
	  m_phi_dressed->push_back(phi_dressed);
	}
      else
	{
	  m_phi_dressed->push_back(-999);
	}
    if( truth->isAvailable<float>("e_dressed") ){
      float e_dressed = truth->auxdata<float>("e_dressed");
      m_e_dressed->push_back(e_dressed / m_units);
    } else {
      m_e_dressed->push_back(-999);
    }
  }

  if(m_infoSwitch.m_origin){
    if( truth->isAvailable<unsigned int>("classifierParticleOrigin") ){
      unsigned int origin = truth->auxdata<unsigned int>("classifierParticleOrigin");
      m_origin->push_back(origin);
    } else {
      m_origin->push_back(0); // Non-defined
    }
  }

  return StatusCode::SUCCESS;
}

