#include <ZprimeNtupler/TruthPhotonJetOR.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <xAODJet/JetContainer.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODCore/AuxContainerBase.h>

#include <ZprimeNtupler/HelperFunctions.h>
#include <ZprimeNtupler/HelperClasses.h>

// this is needed to distribute the algorithm to the workers
ClassImp(TruthPhotonJetOR)

TruthPhotonJetOR :: TruthPhotonJetOR (const std::string& className)
: ZprimeAlgorithm(className)
{
  m_jetContainerName     = "";
  m_photonContainerName  = "";
  m_outJetContainerName  = "";

  m_minDR                = 0.2;
}

EL::StatusCode TruthPhotonJetOR :: setupJob (EL::Job& job)
{
  job.useXAOD();
  xAOD::Init("TruthPhotonJetOR").ignore();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthPhotonJetOR :: initialize ()
{
  ANA_CHECK(ZprimeAlgorithm::initialize());

  Info("initialize()", "TruthPhotonJetOR");

  // in case anything was missing or blank
  if(m_jetContainerName.empty())
    {
      Error("configure()", "Missing JetContainerName!");
      return EL::StatusCode::FAILURE;
    }
  if(m_photonContainerName.empty())
    {
      Error("configure()", "Missing PhotonContainerName!");
      return EL::StatusCode::FAILURE;
    }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TruthPhotonJetOR :: execute ()
{
  // this will be the collection processed - no matter what!!
  const xAOD::JetContainer* inJets = 0;
  ANA_CHECK(HelperFunctions::retrieve(inJets, m_jetContainerName, m_event, m_store));

  const DataVector<xAOD::TruthParticle>* inPhotons = 0;
  ANA_CHECK(HelperFunctions::retrieve(inPhotons, m_photonContainerName, m_event, m_store));

  //
  // Select the Zprime jets
  //
  const xAOD::TruthParticle *photon=(inPhotons->size()>0)?inPhotons->get(0):0;

  // Output container
  xAOD::JetContainer*     outJets    = new xAOD::JetContainer();
  xAOD::AuxContainerBase* outJetsAux = new xAOD::AuxContainerBase();
  outJets->setStore( outJetsAux );

  // Others are other jets
  for(auto jet : *inJets)
    {
      if(photon && photon->p4().DeltaR(jet->p4())<m_minDR) continue;

      xAOD::Jet *newJet = new xAOD::Jet();
      newJet->makePrivateStore(*jet);

      outJets->push_back(newJet);
    }


  // Store output
  ANA_CHECK(m_store->record(outJets    , m_outJetContainerName      ));
  ANA_CHECK(m_store->record(outJetsAux , m_outJetContainerName+"Aux"));

  return EL::StatusCode::SUCCESS;
}

