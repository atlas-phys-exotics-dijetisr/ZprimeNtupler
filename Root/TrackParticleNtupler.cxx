#include "ZprimeNtupler/TrackParticleNtupler.h"

#include "ZprimeNtupler/HelperFunctions.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

TrackParticleNtupler::TrackParticleNtupler(const std::string& name, const std::string& detailStr, float units)
  : ParticleNtupler(name, detailStr, units, true)

{
  if(m_infoSwitch.m_fitpars)
    {
      m_chiSquared                         = std::make_shared<std::vector<float>             >();
      m_d0                                 = std::make_shared<std::vector<float>             >();
      m_definingParametersCovMatrix        = std::make_shared<std::vector<std::vector<float>>>();
      m_expectInnermostPixelLayerHit       = std::make_shared<std::vector<unsigned char>     >();
      m_expectNextToInnermostPixelLayerHit = std::make_shared<std::vector<unsigned char>     >();
      m_numberDoF                          = std::make_shared<std::vector<float>             >();
    }

  if(m_infoSwitch.m_numbers)
    {
      m_numberOfInnermostPixelLayerHits       = std::make_shared<std::vector<unsigned char >>();
      m_numberOfNextToInnermostPixelLayerHits = std::make_shared<std::vector<unsigned char >>();
      m_numberOfPhiHoleLayers                 = std::make_shared<std::vector<unsigned char >>();
      m_numberOfPhiLayers                     = std::make_shared<std::vector<unsigned char >>();
      m_numberOfPixelDeadSensors              = std::make_shared<std::vector<unsigned char >>();
      m_numberOfPixelHits                     = std::make_shared<std::vector<unsigned char >>();
      m_numberOfPixelHoles                    = std::make_shared<std::vector<unsigned char >>();
      m_numberOfPixelSharedHits               = std::make_shared<std::vector<unsigned char >>();
      m_numberOfPrecisionHoleLayers           = std::make_shared<std::vector<unsigned char >>();
      m_numberOfPrecisionLayers               = std::make_shared<std::vector<unsigned char >>();
      m_numberOfSCTDeadSensors                = std::make_shared<std::vector<unsigned char >>();
      m_numberOfSCTHits                       = std::make_shared<std::vector<unsigned char >>();
      m_numberOfSCTHoles                      = std::make_shared<std::vector<unsigned char >>();
      m_numberOfSCTSharedHits                 = std::make_shared<std::vector<unsigned char >>();
      m_numberOfTRTHits                       = std::make_shared<std::vector<unsigned char >>();
      m_numberOfTRTOutliers                   = std::make_shared<std::vector<unsigned char >>();
    }

  m_phi    = std::make_shared<std::vector<float>>();
  m_qOverP = std::make_shared<std::vector<float>>();
  m_theta  = std::make_shared<std::vector<float>>();

  if(m_infoSwitch.m_vertex)
    {
      m_vz = std::make_shared<std::vector<float>>();
      m_z0 = std::make_shared<std::vector<float>>();
    }
}

StatusCode TrackParticleNtupler::createBranches(TTree *tree)
{
  ANA_CHECK(ParticleNtupler::createBranches(tree));

  if(m_infoSwitch.m_fitpars)
    {
      createBranch<float>(tree, "chiSquared", m_chiSquared);
      createBranch<float>(tree, "d0", m_d0);
      createBranch<std::vector<float>>(tree, "definingParametersCovMatrix", m_definingParametersCovMatrix);
      createBranch<unsigned char>(tree, "expectInnermostPixelLayerHit", m_expectInnermostPixelLayerHit);
      createBranch<unsigned char>(tree, "expectNextToInnermostPixelLayerHit", m_expectNextToInnermostPixelLayerHit);
      createBranch<float>(tree, "numberDoF", m_numberDoF);
    }

  if(m_infoSwitch.m_numbers)
    {
      createBranch<unsigned char>(tree, "numberOfInnermostPixelLayerHits", m_numberOfInnermostPixelLayerHits);
      createBranch<unsigned char>(tree, "numberOfNextToInnermostPixelLayerHits", m_numberOfNextToInnermostPixelLayerHits);
      createBranch<unsigned char>(tree, "numberOfPhiHoleLayers", m_numberOfPhiHoleLayers);
      createBranch<unsigned char>(tree, "numberOfPhiLayers", m_numberOfPhiLayers);
      createBranch<unsigned char>(tree, "numberOfPixelDeadSensors", m_numberOfPixelDeadSensors);
      createBranch<unsigned char>(tree, "numberOfPixelHits", m_numberOfPixelHits);
      createBranch<unsigned char>(tree, "numberOfPixelHoles", m_numberOfPixelHoles);
      createBranch<unsigned char>(tree, "numberOfPixelSharedHits", m_numberOfPixelSharedHits);
      createBranch<unsigned char>(tree, "numberOfPrecisionHoleLayers", m_numberOfPrecisionHoleLayers);
      createBranch<unsigned char>(tree, "numberOfPrecisionLayers", m_numberOfPrecisionLayers);
      createBranch<unsigned char>(tree, "numberOfSCTDeadSensors", m_numberOfSCTDeadSensors);
      createBranch<unsigned char>(tree, "numberOfSCTHits", m_numberOfSCTHits);
      createBranch<unsigned char>(tree, "numberOfSCTHoles", m_numberOfSCTHoles);
      createBranch<unsigned char>(tree, "numberOfSCTSharedHits", m_numberOfSCTSharedHits);
      createBranch<unsigned char>(tree, "numberOfTRTHits", m_numberOfTRTHits);
      createBranch<unsigned char>(tree, "numberOfTRTOutliers", m_numberOfTRTOutliers);
    }

  createBranch<float>(tree, "phi", m_phi);
  createBranch<float>(tree, "qOverP", m_qOverP);
  createBranch<float>(tree, "theta", m_theta);

  if(m_infoSwitch.m_vertex)
    {
      createBranch<float>(tree, "vz", m_vz);
      createBranch<float>(tree, "z0", m_z0);
    }

  return StatusCode::SUCCESS;
}

StatusCode TrackParticleNtupler::clear()
{
  ANA_CHECK(ParticleNtupler::clear());

  if(m_infoSwitch.m_fitpars)
    {
      m_chiSquared                        ->clear();
      m_d0                                ->clear();
      m_definingParametersCovMatrix       ->clear();
      m_expectInnermostPixelLayerHit      ->clear();
      m_expectNextToInnermostPixelLayerHit->clear();
      m_numberDoF                         ->clear();
    }

  if(m_infoSwitch.m_numbers)
    {
      m_numberOfInnermostPixelLayerHits      ->clear();
      m_numberOfNextToInnermostPixelLayerHits->clear();
      m_numberOfPhiHoleLayers                ->clear();
      m_numberOfPhiLayers                    ->clear();
      m_numberOfPixelDeadSensors             ->clear();
      m_numberOfPixelHits                    ->clear();
      m_numberOfPixelHoles                   ->clear();
      m_numberOfPixelSharedHits              ->clear();
      m_numberOfPrecisionHoleLayers          ->clear();
      m_numberOfPrecisionLayers              ->clear();
      m_numberOfSCTDeadSensors               ->clear();
      m_numberOfSCTHits                      ->clear();
      m_numberOfSCTHoles                     ->clear();
      m_numberOfSCTSharedHits                ->clear();
      m_numberOfTRTHits                      ->clear();
      m_numberOfTRTOutliers                  ->clear();
    }

  m_phi   ->clear();
  m_qOverP->clear();
  m_theta ->clear();

  if(m_infoSwitch.m_vertex)
    {
      m_vz->clear();
      m_z0->clear();
    }

  return StatusCode::SUCCESS;
}

StatusCode TrackParticleNtupler::FillTrackParticle( const xAOD::TrackParticle* track )
{
  return FillTrackParticle(static_cast<const xAOD::IParticle*>(track));
}

StatusCode TrackParticleNtupler::FillTrackParticle( const xAOD::IParticle* particle )
{
  ANA_CHECK(ParticleNtupler::FillParticle(particle));

  const xAOD::TrackParticle* track=dynamic_cast<const xAOD::TrackParticle*>(particle);

  if(m_infoSwitch.m_fitpars)
    {
      m_chiSquared->push_back( track->chiSquared() );
      m_d0        ->push_back( track->d0() );
      m_numberDoF ->push_back( track->numberDoF() );

      static SG::AuxElement::ConstAccessor<char> expectInnermostPixelLayerHit("expectInnermostPixelLayerHit");
      m_expectInnermostPixelLayerHit->push_back(expectInnermostPixelLayerHit(*track));

      static SG::AuxElement::ConstAccessor<char> expectNextToInnermostPixelLayerHit("expectNextToInnermostPixelLayerHit");
      m_expectNextToInnermostPixelLayerHit->push_back(expectNextToInnermostPixelLayerHit(*track));
    }

  if(m_infoSwitch.m_numbers)
    {
      static SG::AuxElement::ConstAccessor<unsigned char> numberOfInnermostPixelLayerHits("numberOfInnermostPixelLayerHits");
      m_numberOfInnermostPixelLayerHits->push_back(numberOfInnermostPixelLayerHits(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfNextToInnermostPixelLayerHits("numberOfNextToInnermostPixelLayerHits");
      m_numberOfNextToInnermostPixelLayerHits->push_back(numberOfNextToInnermostPixelLayerHits(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfPhiHoleLayers("numberOfPhiHoleLayers");
      m_numberOfPhiHoleLayers->push_back(numberOfPhiHoleLayers(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfPhiLayers("numberOfPhiLayers");
      m_numberOfPhiLayers->push_back(numberOfPhiLayers(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfPixelDeadSensors("numberOfPixelDeadSensors");
      m_numberOfPixelDeadSensors->push_back(numberOfPixelDeadSensors(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfPixelHits("numberOfPixelHits");
      m_numberOfPixelHits->push_back(numberOfPixelHits(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfPixelHoles("numberOfPixelHoles");
      m_numberOfPixelHoles->push_back(numberOfPixelHoles(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfPixelSharedHits("numberOfPixelSharedHits");
      m_numberOfPixelSharedHits->push_back(numberOfPixelSharedHits(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfPrecisionHoleLayers("numberOfPrecisionHoleLayers");
      m_numberOfPrecisionHoleLayers->push_back(numberOfPrecisionHoleLayers(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfPrecisionLayers("numberOfPrecisionLayers");
      m_numberOfPrecisionLayers->push_back(numberOfPrecisionLayers(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfSCTDeadSensors("numberOfSCTDeadSensors");
      m_numberOfSCTDeadSensors->push_back(numberOfSCTDeadSensors(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfSCTHits("numberOfSCTHits");
      m_numberOfSCTHits->push_back(numberOfSCTHits(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfSCTHoles("numberOfSCTHoles");
      m_numberOfSCTHoles->push_back(numberOfSCTHoles(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfSCTSharedHits("numberOfSCTSharedHits");
      m_numberOfSCTSharedHits->push_back(numberOfSCTSharedHits(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfTRTHits("numberOfTRTHits");
      m_numberOfTRTHits->push_back(numberOfTRTHits(*track) );

      static SG::AuxElement::ConstAccessor<unsigned char> numberOfTRTOutliers("numberOfTRTOutliers");
      m_numberOfTRTOutliers->push_back(numberOfTRTOutliers(*track) );
    }

  m_phi   ->push_back(track->phi   () );
  m_qOverP->push_back(track->qOverP() );
  m_theta ->push_back(track->theta () );

  if(m_infoSwitch.m_vertex)
    {
      m_vz->push_back(track->vz() );
      m_z0->push_back(track->z0() );
    }

  return StatusCode::SUCCESS;
}
