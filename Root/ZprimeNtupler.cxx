#include "ZprimeNtupler/ZprimeNtupler.h"
#include "ZprimeNtupler/HelperFunctions.h"

#include <regex>

#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <AthContainers/ConstDataVector.h>

#include <SampleHandler/MetaFields.h>

#include <xAODTracking/VertexContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODTrigger/JetRoIContainer.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODEventInfo/EventInfo.h>
#include <PathResolver/PathResolver.h>

#include <BoostedJetTaggers/JSSWTopTaggerANN.h>
#include <PMGTools/PMGCrossSectionTool.h>

#include "ZprimeNtupler/HelperFunctions.h"

using namespace Zprime;

Ntupler :: Ntupler (const std::string& name, ISvcLocator* pSvcLocator)
  : AnaAlgorithm(name, pSvcLocator)
{
  // Configurable properties
  declareProperty("InputAlgo"          , m_inputAlgo         );

  declareProperty("TruthLevelOnly"     , m_truthLevelOnly    );
  
  declareProperty("EventDetailStr"     , m_eventDetailStr    );
  declareProperty("TrigDetailStr"      , m_trigDetailStr     );

  declareProperty("Containers"         , m_containersStr     );

  declareProperty("SaveZprimeDecay"    , m_saveZprimeDecay   );
  declareProperty("CalcExtraVariables" , m_calcExtraVariables);
  declareProperty("DijetFatjetSlicing" , m_dijetFatjetSlicing);

  //
  // Tools
  declareProperty ("PMGCrossSectionTool", m_PMGCrossSectionTool, "Cross-section tool");
  declareProperty ("JetTagger"          , m_JetTagger          , "AAN Jet Tagger tool");
}

StatusCode Ntupler::initialize ()
{
  ANA_CHECK(AnaAlgorithm::initialize());

  //
  // Parse containers
  static const std::regex re_container("([a-zA-Z0-9]+)\\(([a-zA-Z0-9_]+)->([a-zA-Z]+)\\);([0-9a-zA-Z :,\\._]*);([0-9a-zA-Z :,\\._]*)");

  for(const std::string& containerStr : m_containersStr)
    {
      std::smatch matches;
      if(!std::regex_match(containerStr.begin(), containerStr.end(), matches, re_container))
	{
	  ANA_MSG_ERROR("Unable to parse container string: " << containerStr );
	  return StatusCode::FAILURE;
	}

      if(matches.size()!=6)
	{
	  ANA_MSG_ERROR("Wrong number of fields in container string: " << matches.size());
	  return StatusCode::FAILURE;
	}

      std::string type            = matches[1];
      std::string inContainerName = matches[2];
      std::string branchName      = matches[3];
      std::string detailStr       = matches[4];
      std::string detailStrSyst   = matches[5];

      if(type=="Jet")
	{
	  m_containers.push_back(NtuplerContainerDef(ObjectType::Jet, inContainerName, branchName, detailStr, detailStrSyst));
	}
      else if(type=="FatJet")
	{
	  m_containers.push_back(NtuplerContainerDef(ObjectType::FatJet, inContainerName, branchName, detailStr, detailStrSyst));
	}
      else if(type=="Photon")
	{
	  m_containers.push_back(NtuplerContainerDef(ObjectType::Photon, inContainerName, branchName, detailStr, detailStrSyst));
	}
      else if(type=="Muon")
	{
	  m_containers.push_back(NtuplerContainerDef(ObjectType::Muon, inContainerName, branchName, detailStr, detailStrSyst));
	}
      else if(type=="Electron")
	{
	  m_containers.push_back(NtuplerContainerDef(ObjectType::Electron, inContainerName, branchName, detailStr, detailStrSyst));
	}
      else if(type=="TruthParticle")
	{
	  m_containers.push_back(NtuplerContainerDef(ObjectType::TruthParticle, inContainerName, branchName, detailStr, detailStrSyst));
	}
      else
	{
	  ANA_MSG_ERROR("Unknown container type: " << type);
	  return StatusCode::FAILURE;
	}
  
    }
  return StatusCode::SUCCESS;
}

StatusCode Ntupler::executeFirstEvent()
{
  ANA_CHECK(AnaAlgorithm::executeFirstEvent());

  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK(evtStore()->retrieve(eventInfo, m_eventInfoContainerName));

  //
  // x-sec tool
  m_weight_xs=1.;
  if(isMC())
    {
      ANA_CHECK( m_PMGCrossSectionTool.retrieve() );
      m_PMGCrossSectionTool->readInfosFromDir(PathResolverFindCalibDirectory("dev/PMGTools"));
      std::vector<int> loadedDSIDs=m_PMGCrossSectionTool->getLoadedDSIDs();

      double xs;
      double eff;
      double kfactor;
      uint32_t dsid=eventInfo->mcChannelNumber();
      if( std::find(loadedDSIDs.begin(),loadedDSIDs.end(),dsid)!=loadedDSIDs.end() )
	{ // Have data, use it
	  xs     =m_PMGCrossSectionTool->getAMIXsection(dsid);
	  eff    =m_PMGCrossSectionTool->getFilterEff  (dsid);
	  kfactor=m_PMGCrossSectionTool->getKfactor    (dsid);
	}
      else
	{ // Try sample information
	  xs     =wk()->metaData()->castDouble(SH::MetaFields::crossSection    ,1);
	  eff    =wk()->metaData()->castDouble(SH::MetaFields::filterEfficiency,1);
	  kfactor=wk()->metaData()->castDouble(SH::MetaFields::kfactor         ,1);
	}
      m_weight_xs = xs * eff * kfactor;
    }

  //
  //Set up boosted jet taggers
  if(m_calcExtraVariables)
    {
      //m_JetTagger.setProperty( "ConfigFile", "JSSANNTagger_AntiKt10LCTopoTrimmed_20190827.dat");
      //m_JetTagger.setProperty( "CalibArea", "JSSWTopTaggerANN/Rel21");
      //m_JetTagger.setProperty( "IsMC", isMC());
      ANA_CHECK( m_JetTagger.retrieve() );
  }  

  //
  // Pythia dijet JZ*W samples
  if(m_isMC && !m_truthLevelOnly && ((361020<=eventInfo->mcChannelNumber() && eventInfo->mcChannelNumber()<=361032 ) ||
				     (364700<=eventInfo->mcChannelNumber() && eventInfo->mcChannelNumber()<=364712 )))
    m_cf_cleanPileup=init_cutflow("CleanPileup");

  //
  // Pythia dijet JZ*W samples with largeR jets
  if(m_isMC && m_dijetFatjetSlicing &&
     (eventInfo->mcChannelNumber() == 364702 ||
      eventInfo->mcChannelNumber() == 364703))
    m_cf_dijetFatJetSlicing=init_cutflow("DijetFatJetSlicing");

  return StatusCode::SUCCESS;
}

StatusCode Ntupler::execute()
{
  ANA_CHECK(AnaAlgorithm::execute());

  //
  // Useful containers
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, m_eventInfoContainerName));  

  const xAOD::VertexContainer* vertices = nullptr;
  if(!m_truthLevelOnly)
    ANA_CHECK(evtStore()->retrieve(vertices, m_vertexContainerName));  

  //
  // Pythia dijet sample cleaning
  //
  if(m_cf_cleanPileup>=0)
    {
      const xAOD::JetContainer* recoJets=0;
      ANA_CHECK(evtStore()->retrieve(recoJets,  "AntiKt4EMPFlowJets"));

      const xAOD::JetContainer* truthJets=0;
      ANA_CHECK(evtStore()->retrieve(truthJets, "AntiKt4TruthJets"));

      if(recoJets->size() > 1) {
	float pTAvg = ( recoJets->at(0)->pt() + recoJets->at(1)->pt() ) /2.0;
	if( truthJets->size() == 0 || (pTAvg / truthJets->at(0)->pt() > 1.4) ) {
	  wk()->skipEvent();
	  return StatusCode::SUCCESS;
	}
      }

      cutflow(m_cf_cleanPileup);
    }

  //
  // Pythia dijet sample splicing with the dedicated fat jet sample
  //
  if(m_cf_dijetFatJetSlicing>=0)
    {
      const xAOD::JetContainer* truthFatJets=0;
      ANA_CHECK(evtStore()->retrieve(truthFatJets,  "AntiKt10TruthJets"));

      switch(eventInfo->mcChannelNumber())
	{
	case 364702:
	  if(truthFatJets->size()>0 && truthFatJets->at(0)->pt()>160e3)
	    {
	      wk()->skipEvent();
	      return StatusCode::SUCCESS;
	    }
	  break;
	case 364703:
	  if(truthFatJets->size()>0 && truthFatJets->at(0)->pt()>400e3)
	    {
	      wk()->skipEvent();
	      return StatusCode::SUCCESS;
	    }
	  break;
	default:
	  ANA_MSG_ERROR("Running dijet fat jet slicing on unknown sample: " << eventInfo->mcChannelNumber());
	  return StatusCode::FAILURE;
	  break;
	}

      cutflow(m_cf_dijetFatJetSlicing);
    }

  //
  // Event weights
  //
  double mcEventWeight = 1;
  if(isMC())
    mcEventWeight = eventInfo->mcEventWeight();

  eventInfo->auxdecor< float >("weight_xs") = m_weight_xs;
  eventInfo->auxdecor< float >("weight")    = mcEventWeight * m_weight_xs;

  //
  // Truth info
  //
  if(isMC())
    {
      float Zprime_pt  =0;
      float Zprime_eta =0;
      float Zprime_phi =0;
      float Zprime_m   =0;
      int   Zprime_pdg =0;

      float reso0_pt   =0;
      float reso0_eta  =0;
      float reso0_phi  =0;
      float reso0_E    =0;

      float reso1_pt   =0;
      float reso1_eta  =0;
      float reso1_phi  =0;
      float reso1_E    =0;

      float ISR_pt     =0;
      float ISR_eta    =0;
      float ISR_phi    =0;
      float ISR_E      =0;
      float ISR_pdgId  =0;

      float Wboson_pt  =0;
      float Wboson_eta =0;
      float Wboson_phi =0;
      float Wboson_m   =0;

      float Zboson_pt  =0;
      float Zboson_eta =0;
      float Zboson_phi =0;
      float Zboson_m   =0;

      float Higgs_pt   =0;
      float Higgs_eta  =0;
      float Higgs_phi  =0;
      float Higgs_m    =0;
      int   Higgs_decay=0;

      const xAOD::TruthParticleContainer* truths=nullptr;

      if(evtStore()->contains<xAOD::TruthParticleContainer>("TruthParticles"               ))
	ANA_CHECK(evtStore()->retrieve(truths, "TruthParticles"               ));
      if(evtStore()->contains<xAOD::TruthParticleContainer>("TruthBoson"                   ))
	ANA_CHECK(evtStore()->retrieve(truths, "TruthBoson"                   ));
      if(evtStore()->contains<xAOD::TruthParticleContainer>("TruthBosonsWithDecayParticles"))
	ANA_CHECK(evtStore()->retrieve(truths, "TruthBosonsWithDecayParticles"));
      if(truths!=nullptr)
	{
	  for(const xAOD::TruthParticle* truth : *truths)
	    {
	      // Status code of important particle depends on generator
	      switch(generator())
		{
		case Generator::Sherpa:
		  if(truth->status()!=3) continue;
		  break;
		case Generator::PowhegPythia:
		  if(truth->status()!=62) continue;
		  break;
		default:
		  // Use whatever..
		  break;
		}

	      switch(truth->pdgId())
		{
		  // Higgs
		case 25:
		  Higgs_pt  =truth->pt();
		  if(Higgs_pt>0)
		    {
		      Higgs_eta =truth->eta();
		      Higgs_phi =truth->phi();
		    }
		  Higgs_m   =truth->m();

		  // Determine decay type
		  truth=HelperFunctions::findPreDecayTruth(truth);
		  if(truth==nullptr || truth->nChildren()==0)
		    Higgs_decay=0;
		  else
		    Higgs_decay=abs(truth->child(0)->pdgId());
		  break;

		  // Z
		case 23:
		  Zboson_pt =truth->pt();
		  if(Zboson_pt>0)
		    {
		      Zboson_eta=truth->eta();
		      Zboson_phi=truth->phi();
		    }
		  Zboson_m  =truth->m();
		  break;

		  // W
		case 24:
		case -24:
		  Wboson_pt =truth->pt();
		  if(Wboson_pt>0)
		    {
		      Wboson_eta=truth->eta();
		      Wboson_phi=truth->phi();
		    }
		  Wboson_m  =truth->m();
		  break;
		  
		  // Zprime
		case 101:
		case 5000001:
		  Zprime_pt =truth->pt();
		  if(Zprime_pt>0)
		    {
		      Zprime_eta=truth->eta();
		      Zprime_phi=truth->phi();
		    }
		  Zprime_m  =truth->m();
		  Zprime_pdg=truth->pdgId();
		  break;
		default:
		  break;
		}

              if(m_saveZprimeDecay && truth->pdgId()==101)
		{
		  // Find the ISR particle
		  if(truth->barcode() == 5)
		    {
		      const xAOD::TruthVertex *Zprime_vertex = truth->prodVtx();

		      for(const xAOD::TruthParticle *truth2 : *truths)
			{
			  const xAOD::TruthVertex *truth_vertex = truth2->prodVtx();
			  if(truth2->pdgId()!=101 && truth_vertex == Zprime_vertex)
			    {
			      ISR_pt    = truth2->pt();
			      ISR_eta   = truth2->eta();
			      ISR_phi   = truth2->phi();
			      ISR_E     = truth2->e();
			      ISR_pdgId = truth2->pdgId();
			      break;
			    }
			}
		    }
		  if(truth->nChildren() == 2)
		    {
		      reso0_pt  = truth->child(0)->pt();
		      reso0_eta = truth->child(0)->eta();
		      reso0_phi = truth->child(0)->phi();
		      reso0_E   = truth->child(0)->e();
		  
		      reso1_pt  = truth->child(1)->pt();
		      reso1_eta = truth->child(1)->eta();
		      reso1_phi = truth->child(1)->phi();
		      reso1_E   = truth->child(1)->e();
		    }
		}
	    }
	}

      eventInfo->auxdecor< float >("Higgs_pt"   ) = Higgs_pt /1000;
      eventInfo->auxdecor< float >("Higgs_eta"  ) = Higgs_eta ;
      eventInfo->auxdecor< float >("Higgs_phi"  ) = Higgs_phi ;
      eventInfo->auxdecor< float >("Higgs_m"    ) = Higgs_m  /1000;
      eventInfo->auxdecor< int   >("Higgs_decay") = Higgs_decay;

      eventInfo->auxdecor< float >("Zboson_pt"  ) = Zboson_pt/1000;
      eventInfo->auxdecor< float >("Zboson_eta" ) = Zboson_eta;
      eventInfo->auxdecor< float >("Zboson_phi" ) = Zboson_phi;
      eventInfo->auxdecor< float >("Zboson_m"   ) = Zboson_m /1000;

      eventInfo->auxdecor< float >("Wboson_pt"  ) = Wboson_pt/1000;
      eventInfo->auxdecor< float >("Wboson_eta" ) = Wboson_eta;
      eventInfo->auxdecor< float >("Wboson_phi" ) = Wboson_phi;
      eventInfo->auxdecor< float >("Wboson_m"   ) = Wboson_m /1000;
      
      eventInfo->auxdecor< float >("Zprime_pt"  ) = Zprime_pt/1000;
      eventInfo->auxdecor< float >("Zprime_eta" ) = Zprime_eta;
      eventInfo->auxdecor< float >("Zprime_phi" ) = Zprime_phi;
      eventInfo->auxdecor< float >("Zprime_m"   ) = Zprime_m /1000;
      eventInfo->auxdecor< int   >("Zprime_pdg" ) = Zprime_pdg;

      eventInfo->auxdecor< float >("reso0_pt"   ) = reso0_pt/1000;
      eventInfo->auxdecor< float >("reso0_eta"  ) = reso0_eta;
      eventInfo->auxdecor< float >("reso0_phi"  ) = reso0_phi;
      eventInfo->auxdecor< float >("reso0_E"    ) = reso0_E/1000;

      eventInfo->auxdecor< float >("reso1_pt"   ) = reso1_pt/1000;
      eventInfo->auxdecor< float >("reso1_eta"  ) = reso1_eta;
      eventInfo->auxdecor< float >("reso1_phi"  ) = reso1_phi;
      eventInfo->auxdecor< float >("reso1_E"    ) = reso1_E/1000;

      eventInfo->auxdecor< float >("ISR_pt"   ) = ISR_pt/1000;
      eventInfo->auxdecor< float >("ISR_eta"  ) = ISR_eta;
      eventInfo->auxdecor< float >("ISR_phi"  ) = ISR_phi;
      eventInfo->auxdecor< float >("ISR_E"    ) = ISR_E/1000;
      eventInfo->auxdecor< int   >("ISR_pdgId") = ISR_pdgId;
    }

  //
  // Loop over Systematics
  //

  // get vector of strings giving the names
  std::vector<std::string>* systNames = nullptr;
  if(!m_inputAlgo.empty())
    ANA_CHECK(evtStore()->retrieve(systNames, m_inputAlgo));

  // Fill the tree
  if(m_tree==nullptr)
    {
      if(systNames!=nullptr)
	{
	  ANA_CHECK(createTree(*systNames));
	}
      else
	{
	  ANA_CHECK(createTree());
	}
    }

  if(systNames!=nullptr)
    {
      ANA_CHECK(fillTree( eventInfo, vertices, *systNames ));
    }
  else
    {
      ANA_CHECK(fillTree( eventInfo, vertices ));
    }

  return StatusCode::SUCCESS;
}


StatusCode Ntupler::createTree(const std::vector<std::string>& systNames)
{
  // Create and register the tree
  ANA_CHECK(book(TTree("outTree", "Calibrated Ntuples")));

  TTree * outTree = tree("outTree");

  //
  // Create the output tree
  //
  m_tree = new ZprimeMiniTree(outTree, m_isMC, m_isMC, m_saveZprimeDecay, m_calcExtraVariables);
  if( m_truthLevelOnly )
    {
      m_tree->AddEvent  ( m_eventDetailStr );
    }
  else
    {
      m_tree->AddEvent  ( m_eventDetailStr );
      m_tree->AddTrigger( m_trigDetailStr );
    }

  for(const NtuplerContainerDef& container : m_containers)
    {
      for(const std::string& systName : systNames)
	{
	  std::string containerName=container.m_containerName+systName;
	  std::string branchName   =container.m_branchName+systName;
	  std::string detailStr    =(systName.empty())?container.m_detailStr:container.m_detailStrSyst;

	  switch(container.m_type)
	    {
	    case ObjectType::Jet:
	      ANA_MSG_DEBUG("Create ObjectType::Jet");
	      if(evtStore()->contains<xAOD::JetContainer>(containerName))
		m_tree->AddJets          (detailStr, branchName);
	      break;
	    case ObjectType::FatJet:
	      ANA_MSG_DEBUG("Create ObjectType::FatJet");
	      if(evtStore()->contains<xAOD::JetContainer>(containerName))
		m_tree->AddFatJets       (detailStr, branchName);
	      break;	      
	    case ObjectType::Photon:
	      ANA_MSG_DEBUG("Create ObjectType::Photon");
	      if(evtStore()->contains<xAOD::PhotonContainer>(containerName))
		m_tree->AddPhotons       (detailStr, branchName);
	      break;
	    case ObjectType::Muon:
	      ANA_MSG_DEBUG("Create ObjectType::Muon");
	      if(evtStore()->contains<xAOD::MuonContainer>(containerName))
		m_tree->AddMuons         (detailStr, branchName);
	      break;
	    case ObjectType::Electron:
	      ANA_MSG_DEBUG("Create ObjectType::Electron");
	      if(evtStore()->contains<xAOD::ElectronContainer>(containerName))
		m_tree->AddElectrons     (detailStr, branchName);
	      break;
	    case ObjectType::TruthParticle:
	      ANA_MSG_DEBUG("Create ObjectType::TruthParticle");
	      if(evtStore()->contains<xAOD::TruthParticleContainer>(containerName))
		m_tree->AddTruthParticles(detailStr, branchName);
	      break;
	    }
	}
    }

  return StatusCode::SUCCESS;
}

StatusCode Ntupler :: fillTree ( const xAOD::EventInfo* eventInfo,
			   const xAOD::VertexContainer* vertices,
			   const std::vector<std::string>& systNames)
{
  ANA_MSG_DEBUG("Ntupler::executeTree()");

  int pv = HelperFunctions::getPrimaryVertexLocation( vertices );
  const xAOD::Vertex *primaryVertex = (pv>=0)?vertices->at(pv):nullptr;

  //
  //  fill the tree
  //
  ANA_MSG_DEBUG(" Filling Event");
  if(eventInfo) m_tree->FillEvent( eventInfo, nullptr );

  if(!m_truthLevelOnly)
    {
      ANA_MSG_DEBUG(" Filling Trigger");
      if(eventInfo) m_tree->FillTrigger(eventInfo);
    }

  ANA_MSG_DEBUG(" Filling Particles");

  for(const NtuplerContainerDef& containerInfo : m_containers)
    {
      ANA_MSG_DEBUG("  Filling " << containerInfo.m_containerName);
      for(const std::string& systName : systNames)
	{
	  std::string containerName=containerInfo.m_containerName+systName;
	  std::string branchName   =containerInfo.m_branchName+systName;
	  switch(containerInfo.m_type)
	    {
	    case ObjectType::Jet:
	      {
		const xAOD::JetContainer* jets = nullptr;
		if(evtStore()->contains<xAOD::JetContainer>(containerName))
		  {
		    ANA_CHECK(evtStore()->retrieve(jets,    containerName));
		    m_tree->FillJets( jets, primaryVertex, pv, branchName );
		  }
		break;
	      }

	    case ObjectType::Photon:
	      {
		const xAOD::PhotonContainer* photons = nullptr;
		if(evtStore()->contains<xAOD::PhotonContainer>(containerName))
		  {
		    ANA_CHECK(evtStore()->retrieve(photons, containerName));
		    m_tree->FillPhotons( photons, branchName );
		  }
		break;
	      }

	    case ObjectType::Muon:
	      {
		const xAOD::MuonContainer* muons = nullptr;
		if(evtStore()->contains<xAOD::MuonContainer>(containerName))
		  {
		    ANA_CHECK(evtStore()->retrieve(muons, containerName));
		    m_tree->FillMuons( muons, 0, branchName );
		  }
		break;
	      }

	    case ObjectType::Electron:
	      {
		const xAOD::ElectronContainer* electrons = nullptr;
		if(evtStore()->contains<xAOD::ElectronContainer>(containerName))
		  {
		    ANA_CHECK(evtStore()->retrieve(electrons, containerName));
		    m_tree->FillElectrons( electrons, 0, branchName );
		  }
		break;
	      }

	    case ObjectType::TruthParticle:
	      {
		const xAOD::TruthParticleContainer* truths = nullptr;
		if(evtStore()->contains<xAOD::TruthParticleContainer>(containerName))
		  {
		    ANA_CHECK(evtStore()->retrieve(truths, containerName));
		    m_tree->FillTruthParticles( truths, branchName );
		  }
		break;
	      }

	    case ObjectType::FatJet:
	      {
		const xAOD::JetContainer* fatjets = nullptr;
		if(evtStore()->contains<xAOD::JetContainer>(containerName))
		  {
		    ANA_CHECK(evtStore()->retrieve(fatjets, containerName));
		    m_tree->FillFatJets( fatjets, HelperFunctions::getPrimaryVertexLocation( vertices ), branchName );
		  }
		break;
	      }

	    default:
	      {
		ANA_MSG_ERROR("Unknown container type for " << containerName);
		return false;
	      }

	    }
	}
    }

  m_tree->Fill();

  return StatusCode::SUCCESS;
}

StatusCode Ntupler :: finalize ()
{
  ANA_CHECK(AnaAlgorithm::finalize());

  //write_cutflow(m_treeStream, "Test");

  // Write out the Metadata
  TFile *treeFile = wk()->getOutputFile("ANALYSIS");

  TH1* MetaData_EventCount = static_cast<TH1*>(hist("MetaData_EventCount")->Clone());
  MetaData_EventCount->SetDirectory( treeFile );

  TH1* MetaData_SumW = static_cast<TH1*>(hist("MetaData_SumW")->Clone());
  MetaData_SumW->SetDirectory( treeFile );
  
  return StatusCode::SUCCESS;
}

void Ntupler::calculateExtraVariables(const xAOD::JetContainer* fatjets)
{
  static SG::AuxElement::ConstAccessor<float> Tau1_wta ("Tau1_wta");
  static SG::AuxElement::ConstAccessor<float> Tau2_wta ("Tau2_wta");

  for(const xAOD::Jet *fatjet_itr : *fatjets)
    {
      if(Tau1_wta.isAvailable(*fatjet_itr) && Tau2_wta.isAvailable(*fatjet_itr))
	{
	  float tau21wta = Tau2_wta(*fatjet_itr)/Tau1_wta(*fatjet_itr);
	}

      m_JetTagger->tag(*fatjet_itr);
    }
}
