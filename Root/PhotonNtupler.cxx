#include "ZprimeNtupler/PhotonNtupler.h"

#include <AsgTools/MessageCheck.h>
using namespace asg::msgUserCode;

using namespace Zprime;

PhotonNtupler::PhotonNtupler(const std::string& name, const std::string& detailStr, float units, bool mc)
  : ParticleNtupler(name, detailStr, units, mc, true)
{

  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "20") != m_infoSwitch.m_isoCones.end()){
    m_ptcone20                    =std::make_shared<std::vector<float> >();
    m_ptvarcone20                 =std::make_shared<std::vector<float> >();
    m_topoetcone20                =std::make_shared<std::vector<float> >();
    m_isIsolated_Cone20           =std::make_shared<std::vector<int>   >();
  }
  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "30") != m_infoSwitch.m_isoCones.end()){
    m_ptcone30                    =std::make_shared<std::vector<float> >();
    m_ptvarcone30                 =std::make_shared<std::vector<float> >();
    m_topoetcone30                =std::make_shared<std::vector<float> >();
  }
  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "40") != m_infoSwitch.m_isoCones.end()){
    m_ptcone40                    =std::make_shared<std::vector<float> >();
    m_ptvarcone40                 =std::make_shared<std::vector<float> >();
    m_topoetcone40                =std::make_shared<std::vector<float> >();
    m_isIsolated_Cone40CaloOnly   =std::make_shared<std::vector<int>   >();
    m_isIsolated_Cone40           =std::make_shared<std::vector<int>   >();
  }

      // PID
  if(m_infoSwitch.m_PID){
    m_n_IsLoose  = 0;
    m_n_IsMedium = 0;
    m_n_IsTight  = 0;

    m_IsLoose    =std::make_shared<std::vector<int>   >();
    m_IsMedium   =std::make_shared<std::vector<int>   >();
    m_IsTight    =std::make_shared<std::vector<int>   >();
  }

  if(m_infoSwitch.m_purity)
    {
      m_radhad1    =std::make_shared<std::vector<float> >();
      m_radhad     =std::make_shared<std::vector<float> >();
      m_e277	   =std::make_shared<std::vector<float> >();
      m_reta	   =std::make_shared<std::vector<float> >();
      m_rphi	   =std::make_shared<std::vector<float> >();
      m_weta2      =std::make_shared<std::vector<float> >();
      m_f1	   =std::make_shared<std::vector<float> >();
      m_wtot	   =std::make_shared<std::vector<float> >();
      m_deltae     =std::make_shared<std::vector<float> >();
      m_eratio     =std::make_shared<std::vector<float> >();
    }

  if(m_infoSwitch.m_effSF && m_mc)
    {
      m_LooseEffSF =std::make_shared<std::vector<float>>();
      m_MediumEffSF=std::make_shared<std::vector<float>>();
      m_TightEffSF =std::make_shared<std::vector<float>>();

      m_LooseEffSF_Error =std::make_shared<std::vector<float>>();
      m_MediumEffSF_Error=std::make_shared<std::vector<float>>();
      m_TightEffSF_Error =std::make_shared<std::vector<float>>();
    }

  if(m_infoSwitch.m_trigger)
    {
      m_trigMatched=std::make_shared<std::vector<std::vector<std::string> >>();
    }
}

StatusCode PhotonNtupler::createBranches(TTree *tree)
{
  ANA_CHECK(ParticleNtupler::createBranches(tree));

  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "20") != m_infoSwitch.m_isoCones.end())
    {
    createBranch<float>(tree, "ptcone20",                  m_ptcone20                  );
    createBranch<float>(tree, "ptvarcone20",               m_ptvarcone20               );
    createBranch<float>(tree, "topoetcone20",              m_topoetcone20              );
    createBranch<int>  (tree, "isIsolated_Cone20",         m_isIsolated_Cone20         );
  }
  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "30") != m_infoSwitch.m_isoCones.end()){
    createBranch<float>(tree, "ptcone30",                  m_ptcone30                  );
    createBranch<float>(tree, "ptvarcone30",               m_ptvarcone30               );
    createBranch<float>(tree, "topoetcone30",              m_topoetcone30              );
  }
  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "40") != m_infoSwitch.m_isoCones.end()){
    createBranch<float>(tree, "ptcone40",                  m_ptcone40                  );
    createBranch<float>(tree, "ptvarcone40",               m_ptvarcone40               );
    createBranch<float>(tree, "topoetcone40",              m_topoetcone40              );
    createBranch<int>  (tree, "isIsolated_Cone40CaloOnly", m_isIsolated_Cone40CaloOnly );
    createBranch<int>  (tree, "isIsolated_Cone40",         m_isIsolated_Cone40         );
  }

  // PID
  if(m_infoSwitch.m_PID){
    tree->Branch(("n"+m_name+"_IsLoose").c_str(),      &m_n_IsLoose);
    createBranch<int>(tree,  "IsLoose"  , m_IsLoose );

    tree->Branch(("n"+m_name+"_IsMedium").c_str(),      &m_n_IsMedium);
    createBranch<int>(tree,  "IsMedium" , m_IsMedium);

    tree->Branch(("n"+m_name+"_IsTight").c_str(),      &m_n_IsTight);
    createBranch<int>(tree,  "IsTight"  , m_IsTight );
  }

  // purity
  if(m_infoSwitch.m_purity){
    createBranch<float>(tree,"radhad1", m_radhad1);
    createBranch<float>(tree,"radhad" , m_radhad );
    createBranch<float>(tree,"e277"   , m_e277   );
    createBranch<float>(tree,"reta"   , m_reta   );
    createBranch<float>(tree,"rphi"   , m_rphi   );
    createBranch<float>(tree,"weta2"  , m_weta2  );
    createBranch<float>(tree,"f1"     , m_f1     );
    createBranch<float>(tree,"wtot"   , m_wtot   );
    createBranch<float>(tree,"deltae" , m_deltae );
    createBranch<float>(tree,"eratio" , m_eratio );
  }

  // effSF
  if(m_infoSwitch.m_effSF && m_mc){
    createBranch<float>(tree, "LooseEffSF" , m_LooseEffSF);
    createBranch<float>(tree, "MediumEffSF", m_MediumEffSF);
    createBranch<float>(tree, "TightEffSF" , m_TightEffSF);

    createBranch<float>(tree, "LooseEffSF_Error" , m_LooseEffSF_Error);
    createBranch<float>(tree, "MediumEffSF_Error", m_MediumEffSF_Error);
    createBranch<float>(tree, "TightEffSF_Error" , m_TightEffSF_Error);
  }

  // trigger
  if(m_infoSwitch.m_trigger){
    createBranch<std::vector<std::string> >(tree, "trigMatched", m_trigMatched);
  }

  return StatusCode::SUCCESS;
}

StatusCode PhotonNtupler::clear()
{

  ParticleNtupler::clear();

  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "20") != m_infoSwitch.m_isoCones.end()){
    m_ptcone20		  -> clear() ;
    m_ptvarcone20	  -> clear() ;
    m_topoetcone20	  -> clear() ;
    m_isIsolated_Cone20	  -> clear() ;
  }
  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "30") != m_infoSwitch.m_isoCones.end()){
    m_ptcone30		  -> clear() ;
    m_ptvarcone30	  -> clear() ;
    m_topoetcone30	  -> clear() ;
  }
  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "40") != m_infoSwitch.m_isoCones.end()){
    m_ptcone40		  -> clear() ;
    m_ptvarcone40	  -> clear() ;
    m_topoetcone40        -> clear();
    m_isIsolated_Cone40CaloOnly-> clear();
    m_isIsolated_Cone40	  -> clear() ;
  }

  // PID
  if(m_infoSwitch.m_PID){
    m_n_IsLoose = 0;
    m_IsLoose -> clear();

    m_n_IsMedium = 0;
    m_IsMedium-> clear();

    m_n_IsTight = 0;
    m_IsTight -> clear();
  }

  // purity
  if(m_infoSwitch.m_purity){
    m_radhad1-> clear();
    m_radhad -> clear();
    m_e277   -> clear()	;
    m_reta   -> clear()	;
    m_rphi   -> clear()	;
    m_weta2  -> clear()	;
    m_f1     -> clear()	;
    m_wtot   -> clear()	;
    m_deltae -> clear();
    m_eratio -> clear();
    //std::vector<float> m_w1
  }

  // effSF
  if(m_infoSwitch.m_effSF && m_mc){
    m_LooseEffSF ->clear();
    m_MediumEffSF->clear();
    m_TightEffSF ->clear();

    m_LooseEffSF_Error ->clear();
    m_MediumEffSF_Error->clear();
    m_TightEffSF_Error ->clear();
  }

  // trigger
  if(m_infoSwitch.m_trigger){
    m_trigMatched->clear();
  }

  return StatusCode::SUCCESS;
}


StatusCode PhotonNtupler::FillPhoton( const xAOD::Photon* photon )
{
  return FillPhoton(static_cast<const xAOD::IParticle*>(photon));
}

StatusCode PhotonNtupler::FillPhoton( const xAOD::IParticle* particle )
{
  ANA_CHECK(ParticleNtupler::FillParticle(particle));

  const xAOD::Photon* photon=dynamic_cast<const xAOD::Photon*>(particle);


  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "20") != m_infoSwitch.m_isoCones.end()){
    m_ptcone20     -> push_back( photon->isolation( xAOD::Iso::ptcone20    ) / m_units  );
    m_ptvarcone20  -> push_back( photon->isolation( xAOD::Iso::ptvarcone20 ) / m_units  );
    m_topoetcone20 -> push_back( photon->isolation( xAOD::Iso::topoetcone20) / m_units  );
    static SG::AuxElement::Accessor<char> isIsoCone20Acc            ("isIsolated_FixedCutLoose");
    safeFill<char, int, xAOD::Photon>(photon, isIsoCone20Acc, m_isIsolated_Cone20, -1);
  }
  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "30") != m_infoSwitch.m_isoCones.end()){
    m_ptcone30     -> push_back( photon->isolation( xAOD::Iso::ptcone30    ) / m_units  );
    m_ptvarcone30  -> push_back( photon->isolation( xAOD::Iso::ptvarcone30 ) / m_units  );
    m_topoetcone30 -> push_back( photon->isolation( xAOD::Iso::topoetcone30) / m_units  );
  }
  if(m_infoSwitch.m_isolation || std::find(m_infoSwitch.m_isoCones.begin(), m_infoSwitch.m_isoCones.end(), "40") != m_infoSwitch.m_isoCones.end()){
    m_ptcone40     -> push_back( photon->isolation( xAOD::Iso::ptcone40    ) / m_units  );
    m_ptvarcone40  -> push_back( photon->isolation( xAOD::Iso::ptvarcone40 ) / m_units  );
    m_topoetcone40 -> push_back( photon->isolation( xAOD::Iso::topoetcone40) / m_units  );

    static SG::AuxElement::Accessor<char> isIsoCone40CaloOnlyAcc    ("isIsolated_FixedCutTightCaloOnly");
    safeFill<char, int, xAOD::Photon>(photon, isIsoCone40CaloOnlyAcc, m_isIsolated_Cone40CaloOnly, -1);

    static SG::AuxElement::Accessor<char> isIsoCone40Acc            ("isIsolated_FixedCutTight");
    safeFill<char, int, xAOD::Photon>(photon, isIsoCone40Acc, m_isIsolated_Cone40, -1);
  }

  if ( m_infoSwitch.m_PID ) {

    static SG::AuxElement::Accessor<bool> phLooseAcc  ("PhotonID_Loose");
    safeFill<bool, int, xAOD::Photon>(photon, phLooseAcc, m_IsLoose, -1);

    static SG::AuxElement::Accessor<bool> phMediumAcc ("PhotonID_Medium");
    safeFill<bool, int, xAOD::Photon>(photon, phMediumAcc, m_IsMedium, -1);

    static SG::AuxElement::Accessor<bool> phTightAcc  ("PhotonID_Tight");
    safeFill<bool, int, xAOD::Photon>(photon, phTightAcc, m_IsTight, -1);

  }

  if (m_infoSwitch.m_purity) {
    static SG::AuxElement::Accessor<float> radhad1  ("Rhad1"  );
    static SG::AuxElement::Accessor<float> radhad   ("Rhad"   );
    static SG::AuxElement::Accessor<float> e277     ("e277"   );
    static SG::AuxElement::Accessor<float> reta     ("Reta"   );
    static SG::AuxElement::Accessor<float> rphi     ("Rphi"   );
    static SG::AuxElement::Accessor<float> weta2    ("weta2"  );
    static SG::AuxElement::Accessor<float> f1       ("f1"     );
    static SG::AuxElement::Accessor<float> wtot     ("wtots1" );
    //static SG::AuxElement::Accessor<float> w1       ("w1"     );
    static SG::AuxElement::Accessor<float> deltae   ("DeltaE" );
    static SG::AuxElement::Accessor<float> eratio   ("Eratio" );

    m_radhad1  -> push_back( radhad1(*photon) );
    m_radhad   -> push_back( radhad (*photon) );
    m_e277     -> push_back( e277   (*photon) );
    m_reta     -> push_back( reta   (*photon) );
    m_rphi     -> push_back( rphi   (*photon) );
    m_weta2    -> push_back( weta2  (*photon) );
    m_f1       -> push_back( f1     (*photon) );
    m_wtot     -> push_back( wtot   (*photon) );
    m_deltae   -> push_back( deltae (*photon) );
    m_eratio   -> push_back( eratio (*photon) );
  }

  if (m_infoSwitch.m_effSF && m_mc)
    {
      static SG::AuxElement::ConstAccessor<float> PhotonID_Tight_EffSF  ("PhotonID_Tight_EffSF"  );
      static SG::AuxElement::ConstAccessor<float> PhotonID_Medium_EffSF ("PhotonID_Medium_EffSF" );
      static SG::AuxElement::ConstAccessor<float> PhotonID_Loose_EffSF  ("PhotonID_Loose_EffSF"  );

      static SG::AuxElement::ConstAccessor<float> PhotonID_Tight_EffSF_Error  ("PhotonID_Tight_EffSF_Error" );
      static SG::AuxElement::ConstAccessor<float> PhotonID_Medium_EffSF_Error ("PhotonID_Medium_EffSF_Error" );
      static SG::AuxElement::ConstAccessor<float> PhotonID_Loose_EffSF_Error  ("PhotonID_Loose_EffSF_Error" );

      m_TightEffSF  ->push_back( PhotonID_Tight_EffSF (*photon) );
      m_MediumEffSF ->push_back( PhotonID_Medium_EffSF(*photon) );
      m_LooseEffSF  ->push_back( PhotonID_Loose_EffSF (*photon) );

      m_TightEffSF_Error  ->push_back( PhotonID_Tight_EffSF_Error (*photon) );
      m_MediumEffSF_Error ->push_back( PhotonID_Medium_EffSF_Error(*photon) );
      m_LooseEffSF_Error  ->push_back( PhotonID_Loose_EffSF_Error (*photon) );
    }

  if (m_infoSwitch.m_trigger)
    {
      static SG::AuxElement::ConstAccessor< std::vector< std::string> > trigMatched("trigMatched");

      m_trigMatched ->push_back( trigMatched(*photon) );
    }

  return StatusCode::SUCCESS;
}
