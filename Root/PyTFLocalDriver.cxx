/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


//
// includes
//

#include <ZprimeNtupler/PyTFLocalDriver.h>

#include <fstream>
#include <sstream>
#include <TSystem.h>
#include <AsgTools/StatusCode.h>
#include <EventLoop/Job.h>
#include <EventLoop/ManagerData.h>
#include <EventLoop/MessageCheck.h>
#include <RootCoreUtils/Assert.h>
#include <RootCoreUtils/ShellExec.h>
#include <RootCoreUtils/ThrowMsg.h>

//
// method implementations
//

ClassImp(Zprime::PyTFLocalDriver)

namespace Zprime
{
  void PyTFLocalDriver ::
  testInvariant () const
  {
    RCU_INVARIANT (this != 0);
  }



  PyTFLocalDriver ::
  PyTFLocalDriver ()
  {
    RCU_NEW_INVARIANT (this);
  }



  ::StatusCode PyTFLocalDriver ::
  doManagerStep (EL::Detail::ManagerData& data) const
  {
    RCU_READ_INVARIANT (this);
    using namespace EL::msgEventLoop;
    ANA_CHECK (Zprime::PyTFDriver::doManagerStep (data));
    switch (data.step)
    {
    case EL::Detail::ManagerStep::batchScriptVar:
      {
        data.batchSkipReleaseSetup = true;
      }
      break;

    case EL::Detail::ManagerStep::submitJob:
    case EL::Detail::ManagerStep::doResubmit:
      {
        // safely ignoring: resubmit

        const std::string dockerImage {
          data.options.castString(EL::Job::optDockerImage)};
        const std::string dockerOptions {
          data.options.castString(EL::Job::optDockerOptions)};

        std::ostringstream tmpdirName;
        tmpdirName << data.submitDir << "/tmp";
        if (!data.resubmit)
        {
          if (gSystem->MakeDirectory (tmpdirName.str().c_str()) != 0)
            RCU_THROW_MSG ("failed to create directory " + tmpdirName.str());
        }

	std::ostringstream cmd;
	cmd << "cd " << tmpdirName.str() << " && ";
	if (!dockerImage.empty())
	  cmd << "docker run --rm -v " << RCU::Shell::quote (data.submitDir) << ":" << RCU::Shell::quote (data.submitDir) << " " << dockerOptions << " " << dockerImage << " ";
	cmd << RCU::Shell::quote (data.submitDir) << "/submit/run 0";
	RCU::Shell::exec (cmd.str());

	data.submitted = true;
      }
      break;

    default:
      break;
    }
    return ::StatusCode::SUCCESS;
  }
}
