/* Algorithm Wrapper */
#include <ZprimeNtupler/Algorithm.h>
#include <ZprimeNtupler/ZprimeAlgorithm.h>

/* Event and Jet Selectors */
#include <ZprimeNtupler/ElectronSelector.h>
#include <ZprimeNtupler/PhotonSelector.h>
#include <ZprimeNtupler/TauSelector.h>
#include <ZprimeNtupler/JetSelector.h>
#include <ZprimeNtupler/DebugTool.h>
#include <ZprimeNtupler/TruthSelector.h>
#include <ZprimeNtupler/TrackSelector.h>
#include <ZprimeNtupler/MuonSelector.h>

/* Calibrations */
#include <ZprimeNtupler/ElectronCalibrator.h>
#include <ZprimeNtupler/PhotonCalibrator.h>
#include <ZprimeNtupler/JetCalibrator.h>
#include <ZprimeNtupler/TauCalibrator.h>
#include <ZprimeNtupler/HLTJetRoIBuilder.h>
#include <ZprimeNtupler/HLTJetGetter.h>

/* Missing Energy Reconstruction */
#include <ZprimeNtupler/METConstructor.h>

/* Scale Factors */
#include <ZprimeNtupler/ElectronEfficiencyCorrector.h>
#include <ZprimeNtupler/MuonEfficiencyCorrector.h>
#include <ZprimeNtupler/TauEfficiencyCorrector.h>
#include <ZprimeNtupler/BJetEfficiencyCorrector.h>
#include <ZprimeNtupler/HbbTagAlgorithm.h>

/* Other */
#include <ZprimeNtupler/HelperFunctions.h>
#include <ZprimeNtupler/OverlapRemover.h>
#include <ZprimeNtupler/TrigMatcher.h>
#include <ZprimeNtupler/TauJetMatching.h>
#include <ZprimeNtupler/Writer.h>
#include <ZprimeNtupler/MessagePrinterAlgo.h>
#include <ZprimeNtupler/MuonInFatJetCorrector.h>
#include <ZprimeNtupler/TruthPhotonJetOR.h>
#include <ZprimeNtupler/PyTFDriver.h>
#include <ZprimeNtupler/PyTFLocalDriver.h>
#include <ZprimeNtupler/PyTFSlurmDriver.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ namespace xAH;
#pragma link C++ function xAH::addRucio;

#pragma link C++ class xAH::Algorithm+;
#pragma link C++ class ZprimeAlgorithm+;

#pragma link C++ class ElectronSelector+;
#pragma link C++ class PhotonSelector+;
#pragma link C++ class TauSelector+;
#pragma link C++ class JetSelector+;
#pragma link C++ class DebugTool+;
#pragma link C++ class TruthSelector+;
#pragma link C++ class TrackSelector+;
#pragma link C++ class MuonSelector+;

#pragma link C++ class ElectronCalibrator+;
#pragma link C++ class PhotonCalibrator+;
#pragma link C++ class JetCalibrator+;
#pragma link C++ class HLTJetRoIBuilder+;
#pragma link C++ class TauCalibrator+;
#pragma link C++ class HLTJetGetter+;

#pragma link C++ class METConstructor+;

#pragma link C++ class ElectronEfficiencyCorrector+;
#pragma link C++ class MuonEfficiencyCorrector+;
#pragma link C++ class TauEfficiencyCorrector+;
#pragma link C++ class BJetEfficiencyCorrector+;
#pragma link C++ class HbbTagAlgorithm+;

#pragma link C++ class OverlapRemover+;
#pragma link C++ class TrigMatcher+;
#pragma link C++ class TauJetMatching+;
#pragma link C++ class Writer+;
#pragma link C++ class MessagePrinterAlgo+;
#pragma link C++ class MuonInFatJetCorrector+;
#pragma link C++ class TruthPhotonJetOR+;
#pragma link C++ class Zprime::PyTFDriver+;
#pragma link C++ class Zprime::PyTFLocalDriver+;
#pragma link C++ class Zprime::PyTFSlurmDriver+;

#endif
